# 彰師大地理-LTSER React.js
---
## 複製專案repo
1. [Bitbucket](https://bitbucket.org/)開啟專案repo
2. 切換分支 `main` -> `dev`
3. 點擊右上角 **Clone**
4. 複製專案repo

-  使用sourcetree
	1. 選擇 **Clone in Sourcetree**
	2. 修改專案資料夾目的地路徑及書籤名稱
	3. 點擊 **Clone**

- 或是使用terminal
	1. terminal cd至專案資料夾目的地
	2. 複製 `git clone https://yourusername@bitbucket.org/thousandaitech/ltser-ncue-web-frontend.git`
	3. 於terminal貼上複製的內容
---
## 啟動專案
1. 創建一個檔名為`.env`的檔案
2. 複製 **Slack #檔案** 裡的內容，貼進 `.env`
3. terminal cd至專案資料夾
4. 安裝專案所需套件 `yarn install`
5. 啟動專案 `yarn start`
6. 至瀏覽器開啟 [http://127.0.0.1:7070](http://127.0.0.1:7070)