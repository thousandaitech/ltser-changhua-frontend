export const researchList = [
  {
    id: 1,
    title: '蚵車安全性之實車測試與電腦模擬分析',
    research: {
      author: '謝宏仁',
      year: 2012,
      reference: '校系名稱：大葉大學工學院碩士在職專班',
    },
    link: 'https://www.airitilibrary.com/Publication/alDetailedMesh?DocID=05781434-201803-201807240007-201807240007-1-18&PublishTypeID=P001',
    tags: [1],
    views: 0,
  },
];
