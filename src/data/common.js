import Swal from 'sweetalert2';

import sponsorImg1 from 'img/footer/nstc.png';
import sponsorImg2 from 'img/footer/ltser-taiwan.png';

import ecoEnvironmentalSurveyImg1 from 'img/icon/macrobenthos.svg';
import ecoEnvironmentalSurveyImg2 from 'img/icon/water-quality.svg';
import ecoEnvironmentalSurveyImg3 from 'img/icon/air-quality.svg';
import ecoEnvironmentalSurveyImg4 from 'img/icon/weather.svg';

import socialSurveyImg1 from 'img/icon/social-economic-data.svg';
import socialSurveyImg1_1 from 'img/icon/changhua.svg';
import socialSurveyImg1_2 from 'img/icon/fangyuan.svg';
import socialSurveyImg2 from 'img/icon/interviews.svg';
import socialSurveyImg3 from 'img/icon/local-literature-and-news.svg';
import socialSurveyImg3_1 from 'img/icon/news.svg';
import socialSurveyImg3_2 from 'img/icon/local-literature.svg';
import socialSurveyImg4 from 'img/icon/questionnaire-survey.svg';
import researchesImg1 from 'img/icon/researches.svg';
import researchesImg2 from 'img/icon/issue.svg';
import researchesImg3 from 'img/icon/related-researches.svg';

import aboutImg1 from 'img/icon/logo.png';
import aboutImg2 from 'img/icon/background-and-purposes.svg';
import aboutImg3 from 'img/icon/researcher.svg';

import { SERVICE_EMAIL } from 'utils/config';

export const GRAPH_HEIGHT = {
  lg: 800,
  md: 600,
  default: 400,
};

export const GRAPH_SPEC = {
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
};

export const handleResponsive = (width, spec) => {
  switch (spec) {
    case 'xsm':
      return width < GRAPH_SPEC.sm;
    case 'sm':
      return width < GRAPH_SPEC.md && width >= GRAPH_SPEC.sm;
    case 'md':
      return width < GRAPH_SPEC.lg && width >= GRAPH_SPEC.md;
    case 'lg':
      return width < GRAPH_SPEC.xl && width >= GRAPH_SPEC.lg;
    case 'mobile':
      return width < GRAPH_SPEC.md;
    case 'tablet':
      return width >= GRAPH_SPEC.md;
    default:
      return width >= GRAPH_SPEC.xl;
  }
};

const OVERVIEW_MAP_SPEC = {
  sm: {
    width: 3 * 25,
    height: 4 * 25,
  },
  md: {
    width: 3 * 50,
    height: 4 * 50,
  },
  town: {
    sm: {
      width: 3 * 25,
      height: 4 * 25,
    },
    md: {
      width: 3 * 50,
      height: 4 * 50,
    },
  },
  village: {
    sm: {
      width: 4 * 25,
      height: 3 * 25,
    },
    md: {
      width: 4 * 50,
      height: 3 * 50,
    },
  },
};

export const handleOverviewMapSpec = (width, scale) => {
  if (scale) {
    return {
      width: handleResponsive(width, 'mobile')
        ? OVERVIEW_MAP_SPEC[scale].sm.width
        : OVERVIEW_MAP_SPEC[scale].md.width,
      height: handleResponsive(width, 'mobile')
        ? OVERVIEW_MAP_SPEC[scale].sm.height
        : OVERVIEW_MAP_SPEC[scale].md.height,
    };
  } else {
    return {
      width: handleResponsive(width, 'mobile')
        ? OVERVIEW_MAP_SPEC.sm.width
        : OVERVIEW_MAP_SPEC.md.width,
      height: handleResponsive(width, 'mobile')
        ? OVERVIEW_MAP_SPEC.sm.height
        : OVERVIEW_MAP_SPEC.md.height,
    };
  }
};

export const GRAPH_COLOR = {
  tooltip: {
    background: {
      dark: 'rgba(1, 116, 187, 0.8)',
      light: 'rgba(234, 241, 248, 0.9)',
    },
    text: {
      dark: 'rgba(255, 255, 255, 1)',
      light: 'rgba(51, 51, 51, 1)',
    },
  },
  legend: {
    double: ['rgba(1, 116, 187, 1)', 'rgba(177, 75, 75, 1)'],
    multiple: [
      'rgba(84, 112, 198 ,1)',
      'rgba(145, 204, 117 ,1)',
      'rgba(250, 200, 88 ,1)',
      'rgba(238, 102, 102 ,1)',
      'rgba(115, 192, 222 ,1)',
      'rgba(59, 162, 114 ,1)',
      'rgba(252, 132, 82 ,1)',
      'rgba(154, 96, 180 ,1)',
      'rgba(234, 124, 204 ,1)',
    ],
    plain: ['rgba(1, 116, 187, 0.4)', 'rgba(177, 75, 75, 0.4)'],
    border: 'rgba(51, 51, 51, 1)',
    style: {
      basic: {
        primary: 'rgba(68, 160, 201, 1)',
        secondary: 'rgba(1, 116, 187, 1)',
        light: 'rgba(255, 255, 255, 1)',
        accent: 'rgba(231, 162, 145, 1)',
        error: 'rgba(207, 104, 78, 1)',
        gray: {
          4: 'rgba(189, 189, 189, 1)',
          5: 'rgba(224, 224, 224, 1)',
        },
      },
      opacity: {
        primary: 'rgba(68, 160, 201, 0.1)',
        secondary: 'rgba(68, 160, 201, 0.6)',
        hover: 'rgba(55, 145, 53, 0.2)',
        dark: 'rgba(0, 0, 0, 0.5)',
        light: 'rgba(255, 255, 255, 0.6)',
        error: 'rgba(207, 104, 78, 0.2)',
      },
    },
  },
};

export const SWAL_TOAST = Swal.mixin({
  toast: true,
  position: 'top-start',
  showConfirmButton: false,
  timer: 5000,
  timerProgressBar: false,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer);
    toast.addEventListener('mouseleave', Swal.resumeTimer);
  },
  customClass: {
    container: 'c-alert__container',
    popup: 'c-alert__toast',
    title: 'c-alert__subtitle',
  },
});

export const SCROLL_OFFSET = '-96px';

export const RECAPTCHA_HINT =
  '請先完成reCAPTCHA驗證。如未出現，請重新整理頁面。';

export const routeList = [
  {
    id: '0',
    title: '首頁',
    link: '',
    isHeaderShow: false,
  },
  {
    id: '1',
    title: '關於LTSER_彰化',
    link: 'about',
    icon: aboutImg1,
    list: [
      {
        id: '1-1',
        title: '成立背景與目的',
        link: 'background-and-purposes',
        icon: aboutImg2,
      },
      {
        id: '1-2',
        title: '人員職務',
        link: 'researcher',
        icon: aboutImg3,
      },
      {
        id: '1-3',
        title: '聯絡我們',
        website: `mailto:${SERVICE_EMAIL}`,
      },
    ],
    isHeaderShow: true,
  },
  {
    id: '2',
    title: '最新消息',
    link: 'activities',
    isHeaderShow: true,
  },
  {
    id: '3',
    title: '環境生態調查',
    link: 'eco-environmental-survey',
    hash: 'ecoEnvironmentalSurvey',
    list: [
      {
        id: '3-1',
        title: '底棲生物',
        link: 'macrobenthos',
        icon: ecoEnvironmentalSurveyImg1,
      },
      {
        id: '3-2',
        title: '水質觀測',
        link: 'water-quality',
        icon: ecoEnvironmentalSurveyImg2,
      },
      {
        id: '3-3',
        title: '空氣品質與氣象',
        link: 'air-quality',
        icon: ecoEnvironmentalSurveyImg3,
      },
    ],
    isHeaderShow: true,
  },
  {
    id: '4',
    title: '社會調查',
    link: 'social-survey',
    hash: 'socialSurvey',
    list: [
      {
        id: '4-1',
        title: '社會經濟資料',
        link: 'social-economic-data',
        icon: socialSurveyImg1,
        list: [
          {
            id: '4-1-1',
            title: '彰化縣',
            link: 'changhua-county',
            icon: socialSurveyImg1_1,
          },
          {
            id: '4-1-2',
            title: '芳苑鄉',
            link: 'fangyuan-township',
            icon: socialSurveyImg1_2,
          },
        ],
      },
      {
        id: '4-2',
        title: '文史資料',
        link: 'local-literature-and-news',
        icon: socialSurveyImg3,
        list: [
          {
            id: '4-3-1',
            title: '新聞報導整理',
            link: 'news',
            icon: socialSurveyImg3_1,
          },
          {
            id: '4-3-2',
            title: '地方文獻 (雜誌、書籍等)',
            link: 'local-literature',
            icon: socialSurveyImg3_2,
          },
        ],
      },
      {
        id: '4-3',
        title: '訪談資料',
        link: 'interviews',
        icon: socialSurveyImg2,
      },
      {
        id: '4-4',
        title: '問卷調查',
        link: 'questionnaire-survey',
        icon: socialSurveyImg4,
      },
    ],
    isHeaderShow: true,
  },
  { id: '5', title: '地景資料', link: 'landscapes', isHeaderShow: true },
  {
    id: '6',
    title: '主題研究',
    link: 'researches',
    hash: 'researches',
    icon: researchesImg1,
    list: [
      {
        id: '6-1',
        title: '相關研究',
        link: 'related-researches',
        icon: researchesImg3,
      },
      {
        id: '6-2',
        title: '聚焦主題',
        link: 'issue',
        icon: researchesImg2,
      },
    ],
    isHeaderShow: true,
  },
  {
    id: '7',
    title: 'English',
    link: 'english',
    list: [
      {
        id: '7-1',
        title: 'About Us',
        link: 'about-us',
      },
      {
        id: '7-2',
        title: 'LTSER',
        link: 'about-us',
        paragraph: 'LTSER',
      },
      {
        id: '7-3',
        title: 'LTSER Changhua',
        link: 'about-us',
        paragraph: 'LTSERChanghua',
      },
      {
        id: '7-4',
        title: 'Mission & Values',
        link: 'about-us',
        paragraph: 'missionAndValues',
      },
      {
        id: '7-5',
        title: 'Contact Us',
        website: `mailto:${SERVICE_EMAIL}`,
      },
    ],
    isHeaderShow: true,
  },
  {
    id: '8',
    title: '會員登入',
    link: 'login',
    isAuthOnly: true,
    isHeaderShow: true,
  },
  {
    id: '9',
    title: '會員登入',
    link: 'auth',
    isAuthOnly: true,
    list: [
      {
        id: '8-1',
        title: '會員資料',
        link: 'member',
        icon: researchesImg1,
      },
      {
        id: '8-2',
        title: '下載紀錄',
        link: 'record',
        icon: researchesImg2,
      },
      {
        id: '8-3',
        title: '進階資料使用辦法',
        link: 'application',
        icon: researchesImg2,
      },
      {
        id: '8-4',
        title: '登出',
        btn: true,
        icon: researchesImg2,
      },
    ],
    isHeaderShow: true,
  },
  {
    id: '10',
    title: '註冊',
    link: 'signup',
    isHeaderShow: false,
  },
  {
    id: '11',
    title: '註冊',
    link: 'mail-verification',
    isHeaderShow: false,
  },
  {
    id: '12',
    title: '會員登入',
    link: 'login',
    isHeaderShow: false,
    list: [
      {
        id: '12-1',
        title: '忘記密碼',
        link: 'forgot-password',
      },
      {
        id: '12-2',
        title: '重設密碼',
        link: 'reset-password',
      },
    ],
  },
  {
    id: '13',
    title: '隱私權政策聲明',
    link: 'privacy-policy',
    isHeaderShow: false,
  },
  {
    id: '14',
    title: '會員約定條款',
    link: 'member-terms',
    isHeaderShow: false,
  },
];

export const sponsorList = [
  {
    id: 1,
    title: '國家科學及技術委員會',
    subtitle: '計畫補助單位',
    img: sponsorImg1,
    link: 'https://www.nstc.gov.tw',
  },
  {
    id: 2,
    title: 'LTSER Taiwan',
    subtitle: '',
    img: sponsorImg2,
    link: 'https://sites.google.com/view/ltser-taiwan',
  },
];

export const footerList = [
  { id: 1, title: '隱私權政策聲明', link: '/privacy-policy' },
  { id: 2, title: '會員約定條款', link: '/member-terms' },
];

export const sectionList = [
  {
    id: 'eco-environmental-survey',
    title: '環境生態調查',
    subtitle:
      '依現有議題對芳苑鄉進行環境生態調查，目前已有之調查項目包含底棲生物(蟹類)、水質觀測，介接環境部環境資料開放平臺的空氣品質資料及交通部中央氣象署氣象觀測資料，並陸續增加調查項目',
    customStyle: 'light',
    hash: 'ecoEnvironmentalSurvey',
  },
  {
    id: 'social-survey',
    title: '社會調查',
    subtitle:
      '整理芳苑的社會經濟基礎統計資料並以圖表方式呈現，蒐集當地相關文史資料。團隊也在芳苑當地進行田野調查及拜訪權益關係人，建置訪談資料庫及進行問卷調查',
    customStyle: 'gray',
    hash: 'socialSurvey',
  },
];

export const taiwanCityCountyList = [
  '基隆市',
  '臺北市',
  '新北市',
  '桃園市',
  '新竹縣',
  '新竹市',
  '苗栗縣',
  '臺中市',
  '彰化縣',
  '南投縣',
  '雲林縣',
  '嘉義縣',
  '嘉義市',
  '臺南市',
  '高雄市',
  '屏東縣',
  '宜蘭縣',
  '花蓮縣',
  '臺東縣',
  '澎湖縣',
  '金門縣',
  '連江縣',
  '其他',
];

export const baseMapList = [
  {
    id: 'emap98',
    title: '電子地圖',
    url: 'https://wmts.nlsc.gov.tw/wmts/EMAP98/default/GoogleMapsCompatible/{z}/{y}/{x}',
    attribution:
      '&copy; <a href="http://maps.nlsc.gov.tw" target="blank">國土測繪圖資網路地圖服務系統</a>',
  },
  {
    id: 'spot',
    title: 'SPOT影像',
    url: 'https://data.csrsr.ncu.edu.tw/SP/SP2022NC_3857/{z}/{x}/{y}.png',
    attribution:
      '&copy; <a href="https://data.csrsr.ncu.edu.tw/index_WMTS.php" target="blank">SPOT衛星影像介接服務平台</a>',
  },
  {
    id: 'photo2020',
    title: '2020年正射影像圖',
    url: 'https://wmts.nlsc.gov.tw/wmts/PHOTO2020/default/GoogleMapsCompatible/{z}/{y}/{x}',
    attribution:
      '&copy; <a href="http://maps.nlsc.gov.tw" target="blank">國土測繪圖資網路地圖服務系統</a>',
  },
  {
    id: 'en',
    title: '英文版',
    url: 'https://wmts.nlsc.gov.tw/wmts/EMAP97/default/GoogleMapsCompatible/{z}/{y}/{x}',
    attribution:
      '&copy; <a href="http://maps.nlsc.gov.tw" target="blank">國土測繪圖資網路地圖服務系統</a>',
  },
];
