export const siteList = [
  {
    id: '1',
    date: '2023.10.19',
    title: '路上代天府',
    category: ['3'],
    address: '彰化縣芳苑鄉福榮村廟後巷12之1號',
    location: [23.891805822311984, 120.33289400099767],
    intro: `建於1936年，主神為五府千歲。`,
    history: {
      story: [
        {
          id: 1,
          time: '1936年',
          title: `祖厝建立將五府千歲迎奉於廟內`,
        },
        {
          id: 2,
          time: '1991年',
          title: `現今廟址動工`,
        },
        {
          id: 3,
          time: '1997年',
          title: `現今廟址完工，入火安座`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201043_120523a_13~0044_hr4',
          time: '2012年',
          title: `西南方興建一棟建築物與金爐`,
        },
        {
          id: 2,
          image: '',
          time: '2013年',
          title: `門口牌坊改建，停車場新增籃球場`,
        },
      ],
    },
    stakeholder: `路上代天府管理委員會、信徒、當地民眾`,
    reference: {
      網站: [
        {
          id: 1,
          title: `文化資源地理資訊系統－路上代天府`,
          link: 'http://crgis.rchss.sinica.edu.tw/temples/ChanghuaCounty/fangyuan/0723012-LSDTF',
        },
      ],
    },
    pics: ['1-1'],
  },
  {
    id: '2',
    date: '2023.10.19',
    title: '好厝邊風電廣場',
    category: ['1', '2'],
    address: '彰化縣芳苑鄉博愛村',
    location: [23.96661602784725, 120.33951781837256],
    intro: `位於王功漁港南側，前身是擁有當地重要人文地景的竹管屋廣場，多年來空間閒置荒蕪、雜草叢生，近兩年，在彰化外海執行「彰芳暨西島離岸風場」的「哥本哈根基礎建設基金CIP 」，為善盡企業責任，在風場正對的芳苑鄉轄區，由公所提供土地，主動砸下600萬元，把荒廢的竹管屋廣場，打造成融合離岸風電與在地元素的共融公園，名為「好厝邊風電廣場」，2021年底落成啟用。`,
    history: {
      story: [
        {
          id: 1,
          time: '1996年',
          title: `「王功甦醒」活動中，社區發動的義工，依照先民居住的樣式，遵循古老方式興建，重現王功地區的竹管屋，並將其保留於王功漁港南側的廣場`,
        },
        {
          id: 2,
          time: '2019年',
          title: `開始規劃、設計、施工，將竹管屋廣場改造為共融公園`,
        },
        {
          id: 3,
          time: '2021年',
          title: `好厝邊風電廣場落成啟用`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201014_65009_019',
          time: '1976年',
          title: `永興海埔地尚未出現`,
        },
        {
          id: 2,
          image: '94201014_070622a_14~0011_rgb',
          time: '2007年',
          title: `永興海埔地已開發，竹管屋及廣場已出現`,
        },
        {
          id: 3,
          image: '94201014_171007_0154_mtbn',
          time: '2017年',
          title: `台61西濱快速道路王功路段完工，經過廣場西側`,
        },
        {
          id: 4,
          image: '94201014_190925z_14~3274_hr4',
          time: '2019年',
          title: `從圖中看起來，竹管屋廣場開始進行整地`,
        },
        {
          id: 5,
          image: '94201014_211031a_14~0305_hr4',
          time: '2021年',
          title: `從圖中可見，風電廣場已接近完工狀態`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、丹麥哥本哈根基礎建設基金CIP 、博愛社區發展協會、彰化區漁會、當地居民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `彰化縣政府全球資訊網-芳苑鄉公所`,
          link: 'https://town.chcg.gov.tw/fangyuan/06travel/travel01_con.aspx?new_id=818',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `彰化荒廢廣場蛻變共融公園 燈塔溜滑梯、海豚椅吸睛`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/3760899',
        },
      ],
    },
    pics: ['2-1'],
  },
  {
    id: '3',
    date: '2023.10.19',
    title: '後港溪出海口',
    category: ['6', '1'],
    address: '彰化縣芳苑鄉和平村',
    location: [23.966774, 120.325212],
    intro: `後港溪即為舊趙甲排水，位於出海口王功漁港附近的河段紅樹林生長面積不斷擴散，導致行水不易，並產生淤積、淹水，相關單位正研擬改善方法，欲適當移除紅樹林。`,
    history: {
      story: [
        {
          id: 1,
          time: '1986年',
          title: `林務局自彰化沿海溼地種植紅樹林`,
        },
        {
          id: 2,
          time: '2006年',
          title: `海茄苳數量不斷增加，造成泥沙嚴重淤積，破壞王功漁港灘的生態，時任芳苑鄉長林清彬及彰化縣議員洪和爐要求彰化縣政府能夠及早辦理後港溪河道的疏濬、彰化海岸第一次舉辦拔紅樹林幼苗活動`,
        },
        {
          id: 3,
          time: '2008年',
          title: `後港溪由於紅樹林快速蔓延造成嚴重淤積`,
        },
        {
          id: 4,
          time: '2009年',
          title: `持續舉辦拔除紅樹林幼苗行動`,
        },
        {
          id: 5,
          time: '2022年',
          title: `河川局在王功漁港海洋食研基地舉辦紅樹林生態環境工作坊`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201013_141013a_13~0014_hr4 ',
          time: '2014年',
          title: `河道看起來淤積嚴重`,
        },
        {
          id: 2,
          image: '',
          time: '2015年',
          title: `河川經過清淤，流域面積擴大`,
        },
        {
          id: 3,
          image: '',
          time: '2017年',
          title: `河岸植被有增加趨勢`,
        },
        {
          id: 4,
          image: '94201013_181104z_13~0418_hr4',
          time: '2018年',
          title: `同時有植被大面積覆蓋及泥沙淤積問題`,
        },
      ],
    },
    stakeholder: `河川局、彰化縣政府環保局、彰化海岸保育行動聯盟、台灣生態學會、行政院農業委員會林務局南投林區管理處、芳苑鄉公所、財團法人荒野基金會、彰化縣野鳥學會、彰化縣環保聯盟`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `長錯地方？王功漁港紅樹林部分移除 海空步道保留`,
        },
        {
          id: 2,
          title: `後港溪河道遭海岸漂沙嚴重淤積`,
        },
      ],
    },
    pics: ['3-1'],
  },
  {
    id: '4',
    date: '2023.10.19',
    title: '台灣基督長老教會王功教會',
    category: ['3'],
    address: '彰化縣芳苑鄉和平村芳漢路王功段229號',
    location: [23.95895791315573, 120.33620576697652],
    intro: `隸屬台灣基督長老教會，源於十九世紀馬偕及馬雅各在台灣的宣教工作。王功教會最初是由吳金古長老來王功廟口佈道獲信後而漸漸擴展，接著發展為王功支會，再與母會芳苑教會合併，並於現址興建教堂，現以成為王功地區的信仰中心。此外，本教會歷史悠久，也見證了王功的興衰，從歷來的教友中，也能夠了解當地的產業發展狀況。`,
    history: {
      story: [
        {
          id: 1,
          time: '1947年',
          title: `芳苑堂會分設王功支會`,
        },
        {
          id: 2,
          time: '1949年',
          title: `與母會芳苑教會合併，由謝慶裕牧師牧養信徒，其也成為王功教會首任牧師，同年開始於現址興建禮拜堂`,
        },
        {
          id: 3,
          time: '1951年',
          title: `禮拜堂落成啟用`,
        },
        {
          id: 4,
          time: '1957年',
          title: `為教會發展需要，增購鄰近土地`,
        },
        {
          id: 5,
          time: '1965年',
          title: `興建牧師館`,
        },
        {
          id: 6,
          time: '1981年',
          title: `籌建新的禮拜堂`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201014_65009_19',
          time: '1976年',
          title: `王功教會已存在`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、台灣基督長老教會、當地牧師及信徒`,
    reference: {
      網站: [
        {
          id: 1,
          title: `台灣基督長老教會`,
          link: 'http://www.pct.org.tw/ChurchHistory.aspx?strOrgNo=C06035',
        },
      ],
      研究: [
        {
          id: 1,
          title: `社會網絡與王功基督長老教會早期發展之研究`,
          link: 'https://hdl.handle.net/11296/6vvq38',
        },
      ],
    },
    pics: ['4-1'],
  },
  {
    id: '5',
    date: '2023.11.20',
    title: '蚵田/棚',
    category: ['5'],
    address: '彰化縣芳苑鄉新寶村',
    location: [23.98944177018285, 120.32914919225209],
    intro: `芳苑鄉位於彰化縣西南角，是為風頭水尾地區，受地形與氣候的限制，早期不利於耕作，因而發展了養蚵文化，又以1960年代，為養蚵最盛行的時期。`,
    history: {
      story: [
        {
          id: 1,
          time: '1960S',
          title: `為養蚵最盛行的時期。`,
        },
        {
          id: 2,
          time: '2010S',
          title: `養蚵漸以王功村為中心。`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201003_65002_360',
          time: '1976年',
          title: `該區域還未看見有蚵田`,
        },
        {
          id: 2,
          image: '94201003_070622a_13~0356_rgb',
          time: '2007年',
          title: `該年分有拍攝到退潮的潮間帶，此時北側已經有部分蚵田`,
        },
        {
          id: 3,
          image: '94201003_100921a_13~0456_hr4',
          time: '2015年',
          title: `此區域有明顯可見的蚵田`,
        },
        {
          id: 4,
          image: '',
          time: '',
          title: `註：該地景因位潮間帶，受漲退潮影響可見度，僅展示部分年份，無法只用此做為蚵田變遷依據。`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、王功觀光發展協會、王功相關觀光產業、王功蚵藝館、王功漁業養殖協會、海牛文化、海牛學校、風電業者、當地居民`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `風電場傷蚵？彰縣下周邀廠商開會`,
          link: 'https://udn.com/news/story/7325/6368361',
        },
        {
          id: 2,
          title: `彰化沿海珍珠蚵大量死亡 蚵農陳情怪離岸風場害的`,
          link: 'https://udn.com/news/story/7325/6367213',
        },
        {
          id: 3,
          title: `進口蚵大增，本土蚵受創！四縣市蚵農串連陳情 官方：自由貿易難設限，僅能加強產地標示`,
          link: 'https://www.newsmarket.com.tw/blog/158712/',
        },
        {
          id: 4,
          title: `彰化珍珠蚵產地遭棄300噸垃圾逾1年 養殖戶、環團憂陷食安污染`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/3402043',
        },
        {
          id: 5,
          title: `蚵畫人生 王功漁港摸黑出海的養蚵人家`,
          link: 'https://www.ntdtv.com.tw/b5/20210917/video/304385.html?%E8%9A%B5%E7%95%AB%E4%BA%BA%E7%94%9F%20%E7%8E%8B%E5%8A%9F%E6%BC%81%E6%B8%AF%E6%91%B8%E9%BB%91%E5%87%BA%E6%B5%B7%E7%9A%84%E9%A4%8A%E8%9A%B5%E4%BA%BA%E5%AE%B6',
        },
        {
          id: 6,
          title: `天氣異常因素衝擊　王功「珍珠蚵」普遍生長緩慢`,
          link: 'https://www.taiwanhot.net/news/359411/%E5%A4%A9%E6%B0%A3%E7%95%B0%E5%B8%B8%E5%9B%A0%E7%B4%A0%E8%A1%9D%E6%93%8A+%E7%8E%8B%E5%8A%9F%E3%80%8C%E7%8F%8D%E7%8F%A0%E8%9A%B5%E3%80%8D%E6%99%AE%E9%81%8D%E7%94%9F%E9%95%B7%E7%B7%A9%E6%85%A2',
        },
        {
          id: 7,
          title: `開發離岸風場改變潮間帶生態 漁民說：離岸風機對沿海養殖影響鉅大`,
          link: 'https://www.watchmedia01.com/enews-20220617030501.html',
        },
        {
          id: 8,
          title: `芳苑海空步道爆紅 採蚵車載客合法性引關注`,
          link: 'https://www.chinatimes.com/realtimenews/20211011002879-260405?chdtv',
        },
        {
          id: 9,
          title: `蚵仔也怕地震？ 蚵農搶收蚵仔 大小只剩一半`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/1607675',
        },
        {
          id: 10,
          title: `彰化芳苑珍珠蚵成空包彈 農民疑外海風場「天天震」惹禍`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/3951401',
        },
      ],
      網站: [
        {
          id: 1,
          title: `蚵農採收 濁水溪流域人文資料庫`,
          link: 'https://crc.culture.tw/ChoshuiRiver/en-us/map/262137',
        },
      ],
    },
    pics: ['5-1'],
  },
  {
    id: '6',
    date: '2023.10.19',
    title: '彰化王功生態景觀橋',
    category: ['2', '1'],
    address: '彰化縣芳苑鄉王功村',
    location: [23.9674396905369, 120.32997335979464],
    intro: `王功生態景觀橋位於通往王功漁港南邊的後港溪上，距離漁港入口前約400公尺，沿堤頂階梯爬上堤頂即可看到這座充滿藝術及設計感的橋梁。王功生態景觀橋長度97.5公尺，寬度約2.5至3.5公尺。橋樑型式本身利用拱與折板結合成一俐落的橋型。橋樑動態流線的設計猶如摺紙，將鋼鐵生硬的感覺轉為輕盈。`,
    history: {
      story: [
        {
          id: 1,
          time: '2004年',
          title: `竣工`,
        },
        {
          id: 2,
          time: '2004年',
          title: `榮獲日本SD REVIEW建築雜誌年度獎`,
        },
        {
          id: 3,
          time: '2005年',
          title: `榮獲台灣建築首獎殊榮`,
        },
        {
          id: 4,
          time: '2018年',
          title: `王功生態景觀橋有負面新聞`,
        },
        {
          id: 5,
          time: '2019年',
          title: `進行補強及除鏽`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201013_65002_366',
          time: '1976年',
          title: `此處尚未建設橋梁`,
        },
        {
          id: 2,
          image: '94201013_070622a_13~0352_rgb',
          time: '2007年',
          title: `可從此處看見橋梁已建設完成`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、王功觀光業者、立聯合建築師事務所`,
    reference: {
      網站: [
        {
          id: 1,
          title: `王功生態景觀橋`,
          link: 'https://town.chcg.gov.tw/fangyuan/06travel/travel01_con.aspx?new_id=813',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `王功景觀橋又裂又臭 最美建築失風華`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/2469096',
        },
        {
          id: 2,
          title: `彰化王功景觀橋 鋼骨鏽蝕超落漆`,
          link: 'https://news.cts.com.tw/cts/general/201806/201806261929451.html',
        },
        {
          id: 3,
          title: `王功跨港兩橋 除鏽或補強施工`,
          link: 'https://news.ltn.com.tw/news/life/paper/1321872',
        },
        {
          id: 4,
          title: `廖偉立建築師作品「王功生態景觀橋」`,
          link: 'https://www.xinmedia.com/article/176848',
        },
      ],
      遊記: [
        {
          id: 1,
          title: `王功景觀生態橋~橋身造型前衛，婚紗拍攝取景點`,
          link: 'https://travel.yam.com/article/103828',
        },
        {
          id: 2,
          title: `彰化芳苑│王功生態景觀橋-王功漁港三大地標之一 - 藍色起士的美食主義`,
          link: 'https://www.walkerland.com.tw/article/view/196598',
        },
        {
          id: 3,
          title: `彰化芳苑｜王功漁港不只燈塔，六處推薦拍攝點`,
          link: 'https://travel.yam.com/article/111363',
        },
      ],
    },
    pics: ['6-1'],
  },
  {
    id: '7',
    date: '2023.10.19',
    title: '王功小型風力發電廠',
    category: ['1', '2'],
    address: '彰化縣芳苑鄉新寶村',
    location: [23.984836211140706, 120.33816135495618],
    intro: `座落於魚塭之間的王功風力發電機因與藍天白雲相襯，故有著外星人的秘密基地與外星人打蛋器之稱。這裡的發電機主要是蓄電用的風車，形狀為三葉式造型，隸屬於台灣電力公司的風力發電站，整片約有400多座，是新高能源科技公司研發生產的小風力發電機，更是目前全世界唯一獲得認證的垂直軸小風機，但因執照與各種因素，目前尚未開始運轉。`,
    history: {
      story: [
        {
          id: 1,
          time: '2017年5月',
          title: `業者違法於王功養殖區動工興建十公頃風電廠。`,
        },
        {
          id: 2,
          time: '2017年8月',
          title: `彰化縣養殖漁業發展協會發文檢舉業者違規開發。`,
        },
        {
          id: 3,
          time: '2017年10月',
          title: `彰化縣府依《區域計畫法》罰款業者，但未勒令停工，也未要求恢復原狀。
          `,
        },
        {
          id: 4,
          time: '2017年12月',
          title: `能源局向業者核發核准備案函，業者與台電簽訂《購售電力契約》
          `,
        },
        {
          id: 5,
          time: '2018年1月',
          title: `業者持能源局備案函，第一次向縣府提出開發申請，縣府要求「容許使用」改為「整區變更」。`,
        },
        {
          id: 6,
          time: '2018年5月',
          title: `風機電廠全數完工，縣府再度依《區域計畫法》罰款，仍未要求恢復原狀。`,
        },
        {
          id: 7,
          time: '2020年4月',
          title: `廠商拆卸更新風機設備，再度引發媒體關注。`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201004_65P057_010',
          time: '1976年',
          title: `此處為魚塭，且土地的劃分也不同於近代`,
        },
        {
          id: 2,
          image: '94201004_171007_0154_mtbn',
          time: '2017年',
          title: `小風機建設的基座出現`,
        },
        {
          id: 3,
          image: '94201004_181104z_14~0521_hr4',
          time: '2018年',
          title: `建設完成`,
        },
      ],
    },
    stakeholder: `彰化縣政府、能源局、芳苑鄉公所、彰化縣養殖漁業發展協會、風電業者、當地養殖漁業者、彰化縣環境保護聯盟`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `彰化爆紅「外星人基地」拆除了！王功風力發電區執照卡關`,
          link: 'https://playing.ltn.com.tw/article/19391/1',
        },
        {
          id: 2,
          title: `只是維修王功「外星人秘密基地」沒拆`,
          link: 'https://news.ltn.com.tw/news/life/paper/1369128',
        },
        {
          id: 3,
          title: `美到像在外星球！王功風力發電區未發電先吸睛`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/2454974',
        },
        {
          id: 4,
          title: `異遊彰化奇幻世界 不只蚵豢更科幻`,
          link: 'https://news.ltn.com.tw/news/lifeweekly/paper/1250999',
        },
        {
          id: 5,
          title: `違法風機詐騙案01》「外星人基地」入侵全台最大養殖區，吸金坑殺投資，彰化縣府竟輔導`,
          link: 'https://www.newsmarket.com.tw/blog/132537/',
        },
        {
          id: 6,
          title: `違法風機詐騙案02》不肖業者崩壞綠能，能源局坐視違法，營建署：應勒令`,
          link: 'https://www.newsmarket.com.tw/blog/132539/',
        },
        {
          id: 7,
          title: `陸域小風機躉購費率大砍22% 公民電廠與社區電網堪憂`,
          link: 'https://e-info.org.tw/node/215493',
        },
      ],
    },
    pics: ['7-1'],
  },
  {
    id: '8',
    date: '2023.10.19',
    title: '漢寶濕地',
    category: ['6', '2'],
    address: '彰化縣芳苑鄉漢寶村',
    location: [24.009244141556668, 120.35298262787536],
    intro: `漢寶濕地腹地位於彰化福興鄉和芳苑鄉的交界處，介於舊濁水溪出海口與萬興排水溝之海岸地區，全長約9公里。廣義的漢寶濕地分為海域和陸域兩部分，海域為一大片的潮間灘地，陸域部分多為養殖魚塭，另外還包括沼澤、田地、草澤等，整體棲地環境十分多樣，孕育豐富生態資源。`,
    history: {
      story: [
        {
          id: 1,
          time: '1983年',
          title: `水利局為了護堤，漢寶濕地種植海茄苳。`,
        },
        {
          id: 2,
          time: '2017年',
          title: `分年分段辦理漢寶海堤海岸環境改善工程。`,
        },
        {
          id: 3,
          time: '2020年',
          title: `完工，汛期發揮防潮、禦浪防災功能。`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94212094_65P057_016',
          time: '1976年',
          title: `當時代的漢寶濕地`,
        },
        {
          id: 2,
          image: '94212094_070622a_14~0003_rgb',
          time: '2007年',
          title: `此影像可以看出漢寶濕地的溝槽走向，且岸邊出現魚塭養殖池`,
        },
        {
          id: 3,
          image: '94212094_150518a_14~0247_hr4',
          time: '2015年',
          title: `此時漢寶濕地北側出現紅樹林`,
        },
        {
          id: 4,
          image: '94212094_181104z_14~0526_hr4',
          time: '2018年',
          title: `此時可看出漢寶濕地沿岸提岸地景有所變更`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、當地環保團體、當地養殖漁業者`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `目擊候鳥遭風機擊落 鳥會：在錯的位置留空間不能稱為鳥類廊道`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/3403297',
        },
        {
          id: 2,
          title: `保護國寶濕地！彰化海岸濕地具國際級潛力 環團呼籲儘速公告劃設`,
          link: 'https://www.newsmarket.com.tw/blog/105474/',
        },
        {
          id: 3,
          title: `彰化海岸施作陸域風機 選在瀕危鳥類棲地 鳥會要求立即停工`,
          link: 'https://e-info.org.tw/node/223228',
        },
        {
          id: 4,
          title: `當海岸棲地插滿風機，蝙蝠、鳥兒、白海豚何去何從？`,
          link: 'https://www.newsmarket.com.tw/blog/155672/',
        },
        {
          id: 5,
          title: `永續國寶濕地之美 海龍守護芳苑之心`,
          link: 'https://www.epochtimes.com/b5/21/9/12/n13227489.htm',
        },
        {
          id: 6,
          title: `漢寶溼地生態佳 白蛤量多遊客撿拾趣`,
          link: 'https://www.ntdtv.com.tw/b5/20160530/video/172650.html?%E6%BC%A2%E5%AF%B6%E6%BA%BC%E5%9C%B0%E7%94%9F%E6%85%8B%E4%BD%B3%20%E7%99%BD%E8%9B%A4%E9%87%8F%E5%A4%9A%E9%81%8A%E5%AE%A2%E6%92%BF%E6%8B%BE%E8%B6%A3',
        },
        {
          id: 7,
          title: `彰化漢寶濕地 白蛤大爆發`,
          link: 'https://www.chinatimes.com/realtimenews/20160524004907-260405?chdtv',
        },
        {
          id: 8,
          title: `漁網捕到成排死雞 遊客漢寶溼地受驚嚇`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/2188740',
        },
        {
          id: 9,
          title: `彰縣漢寶溼地屢遭破壞 將裝監視器`,
          link: 'https://news.ltn.com.tw/news/life/paper/1365249',
        },
        {
          id: 10,
          title: `遊客越界採文蛤 地主亮刀嚇「全部倒掉」`,
          link: 'https://news.ltn.com.tw/news/society/breakingnews/2216886',
        },
        {
          id: 11,
          title: `埃及聖䴉無天敵 彰化漢寶溼地成繁殖樂園`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/1674980',
        },
        {
          id: 12,
          title: `2年暴增近3倍！漢寶濕地驚見800巢穴　埃及聖䴉恐排擠本土鷺類生存`,
          link: 'https://pets.ettoday.net/news/1133208',
        },
        {
          id: 13,
          title: `埃及聖䴉大爆發 漢寶濕地驚見800巢`,
          link: 'https://news.ltn.com.tw/news/local/paper/1184724',
        },
        {
          id: 14,
          title: `駭人！嘉義數千隻埃及聖䴉竟是來自.....`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/2716222',
        },
        {
          id: 15,
          title: `埃及聖䴉氾濫 網路發起BB槍撲殺`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/1686989',
        },
        {
          id: 16,
          title: `彰化縣海岸最後的淨土　漢寶濕地發現帶鰕虎罕見魚種`,
          link: 'https://news.housefun.com.tw/news/article/110831220852.html',
        },
        {
          id: 17,
          title: `漢寶濕地淨灘 中華電信員工響應無塑日運動`,
          link: 'https://udn.com/news/story/7266/5921293',
        },
        {
          id: 18,
          title: `外來植物侵彰化海岸濕地 灘地快成樹林`,
          link: 'https://news.pts.org.tw/article/559490',
        },
        {
          id: 19,
          title: `破壞王出沒！堵馬桶、3秒膠黏門…漢寶溼地廁所頻遭惡搞`,
          link: 'https://news.ltn.com.tw/news/Changhua/breakingnews/3130783',
        },
        {
          id: 20,
          title: `首屆彰化漢寶溼地馬拉松開跑推廣沿海生態之美`,
          link: 'https://tw.news.yahoo.com/%E9%A6%96%E5%B1%86%E5%BD%B0%E5%8C%96%E6%BC%A2%E5%AF%B6%E6%BA%BC%E5%9C%B0%E9%A6%AC%E6%8B%89%E6%9D%BE%E9%96%8B%E8%B7%91%E6%8E%A8%E5%BB%A3%E6%B2%BF%E6%B5%B7%E7%94%9F%E6%85%8B%E4%B9%8B%E7%BE%8E-035648014.html',
        },
        {
          id: 21,
          title: `讓漢寶溼地美麗　彰化千人淨灘清出3噸多垃圾`,
          link: 'https://www.ettoday.net/news/20190311/1396026.htm',
        },
        {
          id: 22,
          title: `弘光科大百名師生關心彰化漢寶生態環境 組隊服務居民`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/2462645',
        },
        {
          id: 23,
          title: `弘光學生設感測器監控水中溫度 助文蛤養殖業減少損失`,
          link: 'https://tw.news.yahoo.com/%E5%BC%98%E5%85%89%E5%AD%B8%E7%94%9F%E8%A8%AD%E6%84%9F%E6%B8%AC%E5%99%A8%E7%9B%A3%E6%8E%A7%E6%B0%B4%E4%B8%AD%E6%BA%AB%E5%BA%A6-%E5%8A%A9%E6%96%87%E8%9B%A4%E9%A4%8A%E6%AE%96%E6%A5%AD%E6%B8%9B%E5%B0%91%E6%90%8D%E5%A4%B1-081350018.html',
        },
        {
          id: 24,
          title: `國小漢寶濕地戶外教學 蚵車翻覆12傷`,
          link: 'https://news.cts.com.tw/cts/society/201806/201806081927673.html',
        },
      ],
      遊記: [
        {
          id: 1,
          title: `彰化芳苑｜挖蛤、玩沙、賞鳥、看螃蟹，漢寶溼地好好玩！`,
          link: 'https://travel.yam.com/article/119086',
        },
        {
          id: 2,
          title: `彰化挖蛤蜊|免費挖蛤蜊景點《漢寶濕地》，鏡面沙灘、親子挖蛤蜊、生態觀察`,
          link: 'https://tiyama.tw/changhua-clam/',
        },
        {
          id: 3,
          title: `【免費挖蛤蜊景點】漢寶濕地-玩沙、戲水、抓螃蟹、挖蛤蜊，行前準備與小提醒`,
          link: 'https://jackla39.pixnet.net/blog/post/565684622',
        },
        {
          id: 4,
          title: `漢寶濕地-鮮甜蛤蜊自己挖樂趣多，淡季出遊不擁擠`,
          link: 'https://a99a99a99a.pixnet.net/blog/post/113338619',
        },
        {
          id: 5,
          title: `彰化~漢寶溼地....超多蛤蜊-摸蛤蜊兼洗褲`,
          link: 'https://duck303088.pixnet.net/blog/post/260328581',
        },
      ],
      研究: [
        {
          id: 1,
          title: `彰化縣芳苑鄉漢寶養蚵濕地之生態與環境研究`,
          link: 'https://hdl.handle.net/11296/3ybe8h',
        },
        {
          id: 2,
          title: `臺灣漢寶濕地之紊流風場統計特性與模型`,
          link: 'https://hdl.handle.net/11296/8xhj2a',
        },
      ],
    },
    pics: ['8-1'],
  },
  {
    id: '9',
    date: '2023.10.19',
    title: '王功漁港',
    category: ['2', '1'],
    address: '彰化縣芳苑鄉王功村漁港路900號',
    location: [23.969026846522773, 120.3245436672074],
    intro: `王功漁港是臺灣西部極具特色的漁港，自從「王功甦醒」全國文藝季舉辦後，近年更全力朝休閒漁村觀光產業發展，該區域內的人文、漁業養殖及濕地生態，都相當豐富。其「王功漁火」更是昔日彰化縣八景之一。`,
    history: {
      story: [
        {
          id: 1,
          time: '1968年',
          title: `政府利用該地的海埔新生地進行現代化港區工程。`,
        },
        {
          id: 2,
          time: '1969年',
          title: `完工`,
        },
        {
          id: 3,
          time: '1996年',
          title: `舉辦「全國藝文季 －王功甦醒活動」`,
        },
        {
          id: 4,
          time: '1997年',
          title: `舉辦「王功嘉年華」`,
        },
        {
          id: 5,
          time: '2005年',
          title: `開始辦理「王功漁火節」`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201013_65002_366',
          time: '1976年',
          title: `當時王功漁港的情形，還未王者之弓橋的出現`,
        },
        {
          id: 2,
          image: '94201013_070622a_13~0352_rgb',
          time: '2007年',
          title: `已出現王者之弓橋，漁港內的設施及陸地的建設都有所不同`,
        },
        {
          id: 3,
          image: '94201013_100921a_13~0452_hr4',
          time: '2010年',
          title: `王功漁港因退潮可見港內淤積情形`,
        },
        {
          id: 4,
          image: '',
          time: '2020年',
          title: `陸地出現新建設規劃`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、王功觀光發展協會、相關觀光產業、王功蚵藝館`,
    reference: {
      網站: [
        {
          id: 1,
          title: `王功漁港（王功漁火碼頭）`,
          link: 'https://www.taiwan.net.tw/m1.aspx?sNo=0001113&id=9489',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `走訪王功漁港 師生探索潮間帶體驗小小蚵農`,
          link: 'https://tw.news.yahoo.com/%E8%B5%B0%E8%A8%AA%E7%8E%8B%E5%8A%9F%E6%BC%81%E6%B8%AF-%E5%B8%AB%E7%94%9F%E6%8E%A2%E7%B4%A2%E6%BD%AE%E9%96%93%E5%B8%B6%E9%AB%94%E9%A9%97%E5%B0%8F%E5%B0%8F%E8%9A%B5%E8%BE%B2-023333936.html',
        },
        {
          id: 2,
          title: `長錯地方？王功漁港紅樹林部分移除 海空步道保留`,
          link: 'https://udn.com/news/story/7325/6233650',
        },
        {
          id: 3,
          title: `數萬盞燈點亮王功漁港！港口變身璀璨光展區`,
          link: 'https://newtalk.tw/news/view/2020-08-15/451303',
        },
        {
          id: 4,
          title: `CP值高　王功潮間帶環教小旅行正夯`,
          link: 'https://www.watchmedia01.com/anews-20220405025900.html',
        },
        {
          id: 5,
          title: `王功光影燈季登場 帶動王功漁港夜間觀光`,
          link: 'https://www.peopo.org/news/476667',
        },
        {
          id: 6,
          title: `夜太美 「王功漁火節」打造LED燈港吸客`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/2142935',
        },
        {
          id: 7,
          title: `防疫優先 2021王功漁火節宣告取消`,
          link: 'https://www.chinatimes.com/realtimenews/20210615003101-260405?chdtv',
        },
        {
          id: 8,
          title: `熱力十足嗨王功！大啖鮮蚵賞煙火滿足五感饗宴`,
          link: 'https://newtalk.tw/news/view/2019-07-12/271402',
        },
        {
          id: 9,
          title: `王功漁港絕美夕陽！全台最特別燈塔　美到讓人屏息`,
          link: 'https://travel.ettoday.net/article/1272542.htm',
        },
        {
          id: 10,
          title: `王功漁港打造生態廊道`,
          link: 'https://tw.news.yahoo.com/%E7%8E%8B%E5%8A%9F%E6%BC%81%E6%B8%AF%E6%89%93%E9%80%A0%E7%94%9F%E6%85%8B%E5%BB%8A%E9%81%93-142949428.html',
        },
        {
          id: 11,
          title: `王功漁港上萬尾魚暴斃 腐爛多日臭味嚇跑遊客`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/2521743',
        },
        {
          id: 12,
          title: `被下毒？王功漁港「泛池」 冒出萬尾死魚`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/2354673',
        },
        {
          id: 13,
          title: `彰化王功漁港逢大潮　無風無雨竟然會淹大水`,
          link: 'https://www.ettoday.net/news/20201019/1835117.htm',
        },
        {
          id: 14,
          title: `「漲九降」王功版 晴天淹水海水包圍安檢所`,
          link: 'https://tw.news.yahoo.com/%E6%BC%B2%E4%B9%9D%E9%99%8D-%E7%8E%8B%E5%8A%9F%E7%89%88-%E6%99%B4%E5%A4%A9%E6%B7%B9%E6%B0%B4%E6%B5%B7%E6%B0%B4%E5%8C%85%E5%9C%8D%E5%AE%89%E6%AA%A2%E6%89%80-054528508.html',
        },
        {
          id: 15,
          title: `王功出現這些「龍」 居民搖頭嘆惡「夢」`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/1826520',
        },
      ],
      遊記: [
        {
          id: 1,
          title: `彰化│芳苑王功漁港，吹海風嘗美食，搭鐵牛車採蚵摸蜆仔，芳苑燈塔打卡去`,
          link: 'https://travel.yam.com/article/121024',
        },
      ],
      研究: [
        {
          id: 1,
          title: `王功地區觀光資源與遊客行為之研究`,
          link: 'http://ntcuir.ntcu.edu.tw/bitstream/987654321/17051/2/BSO106110.pdf',
        },
        {
          id: 2,
          title: `王功漁港地區遊客重遊意願及再發展之研究`,
          link: 'https://www.airitilibrary.com/Publication/alDetailedMesh?docid=19925530-201109-201111040015-201111040015-15-36',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `王功美食街周邊停車場及綠美化工程`,
          link: 'chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/https://www.chcg.gov.tw/files/38_980615_%E7%AF%84%E6%9C%AC-%E8%A6%8F%E5%8A%83%E8%A8%AD%E8%A8%88-%E7%8E%8B%E5%8A%9F%E5%9C%B0%E5%8D%80%E5%86%8D%E7%99%BC%E5%B1%95%E8%A8%88%E7%95%AB%E6%A1%88.pdf',
        },
        {
          id: 2,
          title: `「全國水環境改善計畫」-106 年度彰化縣政府【王功水岸環境營造計畫】`,
          link: 'chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/https://water.chcg.gov.tw/files/1.106%e5%b9%b4%e5%ba%a6%e5%bd%b0%e5%8c%96%e7%b8%a3%e5%85%a8%e5%9c%8b%e6%b0%b4%e7%92%b0%e5%a2%83%e6%94%b9%e5%96%84%e8%a8%88%e7%95%ab-%e3%80%90%e7%8e%8b%e5%8a%9f%e6%b0%b4%e5%b2%b8%e7%92%b0%e5%a2%83%e7%87%9f%e9%80%a0%e8%a8%88%e7%95%ab%e3%80%91-%e6%96%bd%e5%b7%a5%e5%89%8d%e7%94%9f%e6%85%8b%e6%aa%a2%e6%a0%b8_43_1090909.pdf',
        },
        {
          id: 3,
          title: `彰化縣芳苑鄉王功漁港區漁火碼頭景觀改造設計及工程委託設計監造案`,
          link: 'http://localdoc.ncl.edu.tw/tmld/detail1.jsp?xmlid=0000726266&displayMode=detail&title=%E5%BD%B0%E5%8C%96%E7%B8%A3%E8%8A%B3%E8%8B%91%E9%84%89%E7%8E%8B%E5%8A%9F%E6%BC%81%E6%B8%AF%E5%8D%80%E6%BC%81%E7%81%AB%E7%A2%BC%E9%A0%AD%E6%99%AF%E8%A7%80%E6%94%B9%E9%80%A0%E8%A8%AD%E8%A8%88%E5%8F%8A%E5%B7%A5%E7%A8%8B%E5%A7%94%E8%A8%97%E8%A8%AD%E8%A8%88%E7%9B%A3%E9%80%A0%E6%A1%88%E6%9C%8D%E5%8B%99%E5%BB%BA%E8%AD%B0%E6%9B%B8&isBrowsing=true',
        },
      ],
    },
    pics: ['9-1', '9-2'],
  },
  {
    id: '10',
    date: '2023.10.19',
    title: '彰化縣民權華德福實驗國民中小學',
    category: ['4'],
    address: '彰化縣芳苑鄉五俊村新生巷5-1號',
    location: [23.933313, 120.347168],
    intro: `民權國小創立於57年，在105年審議通過試辦國小華德福實驗教育，並於108學年度正式實施與增設國中部。課程強調陶冶學生身、心、靈，而非以往的純知識及技能培育。針對不同時期學生需求，給予適性發展，並關心學生學習節奏、強調親近自然，和看重藝術教育。啟發孩子的創造力，以想像力為本，跨足多樣態學習。`,
    history: {
      story: [
        {
          id: 1,
          time: '1963年',
          title: `後寮分校設立五俊分班五班`,
        },
        {
          id: 2,
          time: '1964年',
          title: `五俊分班遷移新校地`,
        },
        {
          id: 3,
          time: '1968年',
          title: `五俊分班獨立改為「民權國校」`,
        },
        {
          id: 4,
          time: '1968年',
          title: `成立民權國小，何東亞先生代理校長`,
        },
        {
          id: 5,
          time: '1969年',
          title: `許委琳先生接任第一任校長`,
        },
        {
          id: 6,
          time: '2015年',
          title: `轉型試辦華德福實驗教育計劃`,
        },
        {
          id: 7,
          time: '2016年',
          title: `正式實施華德福實驗教育，校名:彰化縣芳苑鄉民權國民小學`,
        },
        {
          id: 8,
          time: '2019年',
          title: `改制為華德福實驗國民中小學，校名:彰化縣民權華德福實驗國民中小學`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201024_65009_026',
          time: '1976年',
          title: `民權僅開發左半部主大樓，右側並沒有過多開發`,
        },
        {
          id: 2,
          image: '94201024_070622a_14~0015_rgb',
          time: '2007年',
          title: `校園內許多新建物建成、重新裝潢，操場也有所整修`,
        },
        {
          id: 3,
          image: '94201024_090505a_14~0209_rgb',
          time: '2009年',
          title: `操場外側有了環形跑道，左側建築重新裝上白色鐵皮並和北側屋頂銜接`,
        },
      ],
    },
    stakeholder: `彰化縣政府、民權師生與職員、地方仕紳、教育民團(ex:洪掛基金會)`,
    reference: {
      網站: [
        {
          id: 1,
          title: `民權故事 全民權國民小學沿革、紀事`,
          link: 'https://www.mcws.chc.edu.tw/departments/2/show',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `轉型的陣痛，公辦公營實驗學校的教師觀點`,
          link: 'https://npost.tw/archives/64380',
        },
        {
          id: 2,
          title: `彰化民權華德福實驗中小學 國中部第一屆招收5人`,
          link: 'https://www.chinatimes.com/realtimenews/20190910003392-260405?chdtv',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `改制校名 彰化縣民權華德福實驗國民中小學揭牌 結合秋季路跑啟動實驗教育新紀元`,
          link: 'https://education.chcg.gov.tw/03bulletin/bulletin02_con.asp?bull_id=302143',
        },
        {
          id: 2,
          title: `施政成果`,
          link: 'https://www2.chcg.gov.tw/main/main_act/main_print2.asp?main_id=28818&act_id=408',
        },
        {
          id: 3,
          title: `國民中小學校鄉鎮市區別統計 (104學年度)`,
          link: 'https://stats.moe.gov.tw/files/ebook/basic_city/104basic_city.pdf',
        },
        {
          id: 4,
          title: `國民中小學校鄉鎮市區別統計 (105學年度)`,
          link: 'https://stats.moe.gov.tw/files/ebook/basic_city/105basic_city.pdf',
        },
        {
          id: 5,
          title: `國民中小學校鄉鎮市區別統計 (107學年度)`,
          link: 'https://stats.moe.gov.tw/files/ebook/basic_city/107basic_city.pdf',
        },
        {
          id: 6,
          title: `國民中小學校鄉鎮市區別統計 (108學年度)`,
          link: 'https://stats.moe.gov.tw/files/ebook/basic_city/108basic_city.pdf',
        },
        {
          id: 7,
          title: `109年度偏鄉地區學校及非山區市學校(非原住民學校)設施設備計畫-西棟教室修繕`,
          link: 'https://www.taiwanbuying.com.tw/ShowDetailUCS.ASP?RecNo=4038496#_=_',
        },
        {
          id: 8,
          title: `109年度生活科技教室基本設備採購案`,
          link: 'https://www.taiwanbuying.com.tw/ShowDetailUCS.ASP?RecNo=4038497#_=_',
        },
        {
          id: 9,
          title: `108年度改善及充實華德福實驗教育教學環境設備-教室整建工程`,
          link: 'https://www.taiwanbuying.com.tw/ShowDetailUCS.ASP?RecNo=4038498#_=_',
        },
      ],
    },
    pics: ['10-1'],
  },
  {
    id: '11',
    date: '2023.10.19',
    title: '芳苑海牛學校',
    category: ['2', '5'],
    address: '彰化縣芳苑鄉芳苑村普天街24號',
    location: [23.92898147292833, 120.31929665642079],
    intro: `芳苑海牛學校由校長魏清水申請並經彰化縣文化局核可的無形文化資產商家。以觀光的形式讓遊客認識潮間生態與養蚵、海牛文化，並同時強調文化延續、青年返鄉發展養蚵業和贍養、善待牛隻。`,
    history: {
      story: [
        {
          id: 1,
          time: '日治時期',
          title: `可見芳苑潮間帶牛車運蚵的影像紀錄`,
        },
        {
          id: 2,
          time: '1950~1960S',
          title: `發展和盛行出訓練黃牛拉車採蚵的漁法(海牛)`,
        },
        {
          id: 3,
          time: '1980S',
          title: `鐵牛出現，開始與海牛競爭`,
        },
        {
          id: 4,
          time: '1990S',
          title: `鐵牛大量取代海牛，海牛漸稀`,
        },
        {
          id: 5,
          time: '2012年',
          title: `海牛文化受到英國廣播公司BBC報導，並以此打開知名度`,
        },
        {
          id: 6,
          time: '2016年',
          title: `「芳苑潮間帶牛車採蚵文化」登入我國無形文化資產`,
        },
        {
          id: 7,
          time: '2020年5月',
          title: `成立海牛學校，培育新牛`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_65002_372',
          time: '1976年',
          title: `此處尚未被建設`,
        },
        {
          id: 2,
          image: '94201023_070622a_13~0348_rgb',
          time: '2007年',
          title: `此處已有建物出現`,
        },
        {
          id: 3,
          image: '94201023_111022a_13~0665_hr4',
          time: '2013年',
          title: `出現一處新建的白色小房屋`,
        },
        {
          id: 4,
          image: '',
          time: '2017年',
          title: `出現的新小白房屋出現擴建`,
        },
        {
          id: 5,
          image: '',
          time: '2020年',
          title: `小白屋又再次擴建`,
        },
      ],
    },
    stakeholder: `農委會、彰化縣二林斗芳苑休閒農業區發展協會、彰化縣政府、芳苑養蚵業者`,
    reference: {
      網站: [
        {
          id: 1,
          title: `芳苑海牛學校`,
          link: 'https://school-10830.business.site/',
        },
        {
          id: 2,
          title: `芳苑海牛學校Facebook`,
          link: 'https://www.facebook.com/%E8%8A%B3%E8%8B%91%E6%B5%B7%E7%89%9B%E5%AD%B8%E6%A0%A1-113916470152363/',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `我家住海邊：尋找台灣即將消失的漁法`,
        },
        {
          id: 2,
          title: `旅讀No116｜尋味亞洲米其林`,
        },
        {
          id: 3,
          title: `芳苑海牛。彰化縣政府`,
        },
      ],
      新聞: [
        {
          id: 1,
          title: `彰化唯一「海牛文化」體驗！坐牛車採蚵　最新純白海空步道看夕陽`,
          link: 'https://travel.ettoday.net/article/2265965.htm',
        },
        {
          id: 2,
          title: `芳苑特有海牛文化 遊客坐牛車下海體驗`,
          link: 'https://www.ntdtv.com.tw/b5/20220622/video/332590.html?%E8%8A%B3%E8%8B%91%E7%89%B9%E6%9C%89%E6%B5%B7%E7%89%9B%E6%96%87%E5%8C%96%20%E9%81%8A%E5%AE%A2%E5%9D%90%E7%89%9B%E8%BB%8A%E4%B8%8B%E6%B5%B7%E9%AB%94%E9%A9%97',
        },
        {
          id: 3,
          title: `即將失傳的台灣漁法──彰化芳苑海牛採蚵`,
          link: 'https://udn.com/news/story/12674/6085699',
        },
        {
          id: 4,
          title: `芳苑海牛剩8頭 彰化培訓新血接班`,
          link: 'https://tw.news.yahoo.com/%E8%8A%B3%E8%8B%91%E6%B5%B7%E7%89%9B%E5%89%A98%E9%A0%AD-%E5%BD%B0%E5%8C%96%E5%9F%B9%E8%A8%93%E6%96%B0%E8%A1%80%E6%8E%A5%E7%8F%AD-201000740.html?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8&guce_referrer_sig=AQAAAMkkETooanpUfi-4CNC8YGj7Jk6JmjpiKjNlVzST6D6bir9dBbAjitQKjxu347Y4FC97RT-US5qboU1ia7bCMdpbksnQR5uypE7xN6VyQS8ijCI2cunTeKqkC63QP73EB1vfWI74Ak619w9vHwuWU0lB9wpLBmGe0Atl-yPGhZc_',
        },
        {
          id: 5,
          title: `芳苑海牛學校正式開學 農委會許可立案`,
          link: 'https://tw.news.yahoo.com/%E8%8A%B3%E8%8B%91%E6%B5%B7%E7%89%9B%E5%AD%B8%E6%A0%A1%E6%AD%A3%E5%BC%8F%E9%96%8B%E5%AD%B8-%E8%BE%B2%E5%A7%94%E6%9C%83%E8%A8%B1%E5%8F%AF%E7%AB%8B%E6%A1%88-113701997.html',
        },
        {
          id: 6,
          title: `「海牛犂蚵田」獨步全球　紅到BBC還創學校`,
          link: 'https://www.appledaily.com.tw/micromovie/20200501/OTWJXWCCMLAW2WUGFTQ2K6IZHM',
        },
        {
          id: 7,
          title: `只剩8頭！ 彰化舉行海牛文化節保護無形文化`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/3345900',
        },
        {
          id: 8,
          title: `海牛數量劇降 芳苑無形文化資產待援`,
          link: 'https://www.ntdtv.com.tw/b5/20191014/video/255858.html?%E6%B5%B7%E7%89%9B%E6%95%B8%E9%87%8F%E5%8A%87%E9%99%8D%20%E8%8A%B3%E8%8B%91%E7%84%A1%E5%BD%A2%E6%96%87%E5%8C%96%E8%B3%87%E7%94%A2%E5%BE%85%E6%8F%B4',
        },
        {
          id: 9,
          title: `微解封9人座小巴只能坐3人? 旅遊限制多業者興趣缺缺`,
          link: 'https://udn.com/news/story/122173/5595608',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `府授文演字第1050441848A號`,
          link: 'https://data.boch.gov.tw/upload/documents/2021-12-16/af4cf29f-8b3d-49d9-9b88-1b87f786df7a/%E5%BA%9C%E6%8E%88%E6%96%87%E6%BC%94%E5%AD%97%E7%AC%AC1050441848A%E8%99%9F%2001.pdf',
        },
        {
          id: 2,
          title: `府授文戲字第1090156807A號`,
          link: 'https://data.boch.gov.tw/upload/documents/2021-12-16/beb66a8a-0622-446b-be1a-b1e14ce5527d/%E5%BA%9C%E6%8E%88%E6%96%87%E6%88%B2%E5%AD%97%E7%AC%AC1090156807A%E8%99%9F01.pdf',
        },
        {
          id: 3,
          title: `芳苑潮間帶牛車採蚵`,
          link: 'https://nchdb.boch.gov.tw/embed/assets/overview/tkp/20200723000001',
        },
      ],
    },
    pics: ['11-1', '11-2'],
  },
  {
    id: '12',
    date: '2023.10.19',
    title: '番挖水耕農場',
    category: ['5', '2'],
    address: '彰化縣芳苑鄉仁愛村斗苑路185號',
    location: [23.921326901633996, 120.3209264160877],
    intro: `番挖水耕農場主要利用簡易溫室和水耕系統，種植水耕蔬菜、香料食物及可食用的花卉，運用自動化栽培作物，提供原料給知名餐館和果菜市場。該農場除了實現在地農業精神，也供應當地就業機會和推廣食農與校園的農園教育，以升級農業生活品質。`,
    history: {
      story: [
        {
          id: 1,
          time: '2003年',
          title: `創建番挖水耕農場`,
        },
        {
          id: 2,
          time: '2020年',
          title: `和在地蚵農、漁民、生態廚師等與南投水保局合作推廣芳苑永續觀光`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201033_070622a_13~0344_rgb',
          time: '2007年',
          title: `尚未開發至今設座標的位置，已經可見橘瓦房舍的地基`,
        },
        {
          id: 2,
          image: '94201033_090501a_13~0181_rgb',
          time: '2008年',
          title: `橘瓦房舍建成`,
        },
        {
          id: 3,
          image: '94201033_110509_0318_mtbn',
          time: '2011年',
          title: `農場土地擴增，新增簡易溫室8座`,
        },
        {
          id: 4,
          image: '94201033_161103a_13~0209_hr4',
          time: '2016年',
          title: `部分溫室破損，推測是颱風侵襲導致，橘瓦房設擴建`,
        },
        {
          id: 5,
          image: '94201033_181104z_13~0428_hr4',
          time: '2018年',
          title: `藍色條狀物拆除`,
        },
      ],
    },
    stakeholder: `遊客、附近居民、彰化縣芳苑鄉第27班蔬菜產銷班、果菜市場、農委會、水保局南投分局`,
    reference: {
      官網: [
        {
          id: 1,
          title: `番挖水耕農場Facebook`,
          link: 'https://www.facebook.com/fanwa61/',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `種苗臉譜 致力推廣安全食材~番挖水耕農場洪新有先生`,
          link: 'file:///C:/Users/hanfu/Downloads/%E3%80%8A%E7%A8%AE%E8%8B%97%E8%87%89%E8%AD%9C%E3%80%8B%E8%87%B4%E5%8A%9B%E6%8E%A8%E5%BB%A3%E5%AE%89%E5%85%A8%E9%A3%9F%E6%9D%90%EF%BD%9E%E7%95%AA%E6%8C%96%E6%B0%B4%E8%80%95%E8%BE%B2%E5%A0%B4%E6%B4%AA%E6%96%B0%E6%9C%89%E5%85%88%E7%94%9F(%E6%B4%AA%E7%91%9B%E7%A9%97).pdf',
        },
        {
          id: 2,
          title: `《鄉間小路》2022年2月號 植迷花事`,
          link: 'https://www.agriharvest.tw/archives/74902',
        },
        {
          id: 3,
          title: `彰化西海岸主題盛宴 生態廚師聯合發表`,
          link: 'https://tw.news.yahoo.com/%E5%BD%B0%E5%8C%96%E8%A5%BF%E6%B5%B7%E5%B2%B8%E4%B8%BB%E9%A1%8C%E7%9B%9B%E5%AE%B4-%E7%94%9F%E6%85%8B%E5%BB%9A%E5%B8%AB%E8%81%AF%E5%90%88%E7%99%BC%E8%A1%A8-044257916.html',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `旅讀No116｜尋味亞洲米其林`,
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `行銷彰化漁村最美風景:蚵田、海牛、濕地 用吃體驗西海岸漁村`,
          link: 'https://nantou.swcb.gov.tw/News/achievements_more?id=57EBCCF95FCC491BB975BBBF174F6C4C',
        },
        {
          id: 2,
          title: `TO”GO旅遊雜誌初體驗～彰化芳苑海牛遊程拍攝實況`,
          link: 'https://nantou.swcb.gov.tw/News/achievements_more?id=8085E82D21594498A2953EDCE1DBF9FC',
        },
      ],
    },
    pics: ['12-1'],
  },
  {
    id: '13',
    date: '2023.11.20',
    title: '油籽學堂',
    category: ['5', '2'],
    address: '彰化縣芳苑鄉仁愛村斗苑路芳苑段',
    location: [23.922500499716897, 120.32170306847847],
    intro: `油籽學堂是由新建美公司旗下的體驗館，負責向旅客介紹芳苑傳統農作與生態環境及產業相關知識。樓內除了設有製油時光廊道，配合解說使大眾了解其歷史，更有開設DIY手作教學空間，能實際體驗製油過程。`,
    history: {
      story: [
        {
          id: 1,
          time: '1933年',
          title: `陳籤開立臺灣早期的油車行「新建發製油」。`,
        },
        {
          id: 2,
          time: '1949年',
          title: `擴大業務範圍，申請機房電話`,
        },
        {
          id: 3,
          time: '1950S',
          title: `美援提供穩定原料，創造輝煌製油時光`,
        },
        {
          id: 4,
          time: '1984年10月',
          title: `陳松治將油廠設立為「新建美製油廠有限公司」，並創立油品品牌「主婦」`,
        },
        {
          id: 5,
          time: '2004年',
          title: `正式更名「新建美興業股份有限公司」`,
        },
        {
          id: 6,
          time: '2006年',
          title: `通過食品衛生安全HACCP認證`,
        },
        {
          id: 7,
          time: '2013年',
          title: `榮獲彰化縣十大伴手禮`,
        },
        {
          id: 8,
          time: '2014年',
          title: `規劃芳苑本地油品品牌「油籽學堂」`,
        },
        {
          id: 9,
          time: '2015年',
          title: `奪得中華民國全國商業總會「優良老店」殊榮`,
        },
        {
          id: 10,
          time: '2016年',
          title: `獲得彰化縣政府金好獎`,
        },
        {
          id: 11,
          time: '2020年',
          title: `成立油籽學堂體驗館，升級為觀光、教育場所`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_070622a_13~0348_rgb',
          time: '2007年',
          title: `現址仍為空地，作停車場用途`,
        },
        {
          id: 2,
          image: '94201023_190925z_13~3160_hr4',
          time: '2019年',
          title: `得見本館建成`,
        },
      ],
    },
    stakeholder: `合作的各級學校師生、周遭居民、經濟部中小企業處、蔗青文化工作室、觀光旅客、契作農民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `油籽學堂`,
          link: 'http://goodoil.com.tw/article_d.php?lang=tw&tb=1&id=37',
        },
        {
          id: 2,
          title: `油籽學堂Facebook`,
          link: 'https://www.facebook.com/oilschool/',
        },
        {
          id: 3,
          title: `公司登記查詢中心`,
          link: 'https://www.findcompany.com.tw/%E6%96%B0%E5%BB%BA%E7%BE%8E%E8%88%88%E6%A5%AD%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8',
        },
        {
          id: 4,
          title: `彰化旅遊資訊網`,
          link: 'https://tourism.chcg.gov.tw/AttractionsContent.aspx?id=441&chk=68655f98-1b2a-4ee2-af70-8360efd1692f',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `超級寒流不怕　企業為中低收入獨居長輩送暖`,
          link: 'https://www.nownews.com/news/5152384?fbclid=IwAR1d1DKJb13JEfXAZ-WFnq8s4qRW5G9kYc6tkvQcJciqvQs1XNoSmG7a0os',
        },
        {
          id: 2,
          title: `彰化新建美油籽學堂 食農觀光開創新商機`,
          link: 'https://www.peopo.org/news/465522',
        },
        {
          id: 3,
          title: `優良老店－新建美 傳承台灣好油`,
          link: 'https://www.chinatimes.com/newspapers/20151130000151-260210?chdtv',
        },
        {
          id: 4,
          title: `走進油車間：育德國小與新建美油籽學堂`,
          link: 'https://www.foodiedu.org/news/922',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `城鄉創生 魅力臺灣`,
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `風暴過後 從產品溯源振興食油產業`,
          link: 'https://share.sbtr.org.tw/blogView?GUID=21',
        },
      ],
    },
    pics: ['13-1'],
  },
  {
    id: '14',
    date: '2023.10.19',
    title: '海空步道與紅樹林',
    category: ['2', '6', '1'],
    address: '彰化縣芳苑鄉芳中村',
    location: [23.93131758122763, 120.31497999119044],
    intro: `海空步道全長約1公里，其周邊設有觀夕平台、賞鳥亭及底棲環境觀察平台。為提供紅樹林的環境教育場域，步道側設置低矮護欄，讓民眾能近距離觀察紅樹林豐富的生態。`,
    history: {
      story: [
        {
          id: 1,
          time: '1983年',
          title: `第四河川局開始於二林溪出海口種植水筆仔、海茄冬`,
        },
        {
          id: 2,
          time: '2013年',
          title: `進行「彰化海岸永續整體規劃-紅樹林生態資源調查暨分析委託研究計畫」，對紅樹林進行周遭調查、評估，發現陸域化問題`,
        },
        {
          id: 3,
          time: '2014年',
          title: `進行芳苑濕地紅樹林暨其週邊整體環境規劃設計`,
        },
        {
          id: 4,
          time: '2017年',
          title: `經濟部水利署水環境前瞻計畫全國水環境改善計畫核定`,
        },
        {
          id: 5,
          time: '2018年',
          title: `開始施工，施工階段將評估選擇對潮間帶擾動最小之最適工法，並妥予篩選對海岸環境較無影響之鋪設材料，以減少泥質灘地之損失面積，及避免改變其地形地貌。`,
        },
        {
          id: 6,
          time: '2021年',
          title: `正式啟用，開放遊客進入`,
        },
        {
          id: 7,
          time: '2022年',
          title: `進行芳苑濕地紅樹林暨其週邊整體環境(第二期)規劃設計`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_65002_373',
          time: '1976年',
          title: `現址的紅樹林尚未種植`,
        },
        {
          id: 2,
          image: '',
          time: '2019年',
          title: `可見紅樹林生長茂密`,
        },
        {
          id: 3,
          image: '',
          time: '2020年',
          title: `得見步道整建大體完成`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、彰化縣野鳥學會、彰化縣環境保護聯盟、水利署第四河川局`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `芳苑紅樹林海空步道挨批破壞環境 縣府提二期計畫圍堵`,
          link: 'https://udn.com/news/story/7325/6215930',
        },
        {
          id: 2,
          title: `爆紅的滋味 海空步道與芳苑濕地`,
          link: 'https://udn.com/news/story/7325/6233650',
        },
        {
          id: 3,
          title: `長錯地方？王功漁港紅樹林部分移除 海空步道保留`,
          link: 'https://e-info.org.tw/node/232467',
        },
        {
          id: 4,
          title: `芳苑鄉紅樹林海空步道今啟用 海天一色引人入勝`,
          link: 'https://udn.com/news/story/7325/5728191',
        },
        {
          id: 5,
          title: `芳苑紅樹林海空步道有罩門 昨天人潮暴量今銳減`,
          link: 'https://udn.com/news/story/7325/5808638',
        },
        {
          id: 6,
          title: `彰化芳苑紅樹林海空步道遊客暴量 地方憂喜參半`,
          link: 'https://udn.com/news/story/7325/5761257',
        },
        {
          id: 7,
          title: `芳苑濕地紅樹林海空步道　彰化人文生態打卡新景點`,
          link: 'https://www.businesstoday.com.tw/article/category/183031/post/202109140031/',
        },
        {
          id: 8,
          title: `熱潮退?! 芳苑鄉海空步道人潮砍半`,
          link: 'https://news.cts.com.tw/cts/life/202202/202202042070824.html',
        },
        {
          id: 9,
          title: `盼重現台灣「五番挖」榮景 海空步道納入水環境計畫`,
          link: 'https://udn.com/news/story/7325/5888377',
        },
        {
          id: 10,
          title: `海空步道湧入人潮太震撼 彰化國際海牛文化節喊停`,
          link: 'https://udn.com/news/story/7325/5765900',
        },
        {
          id: 11,
          title: `芳苑濕地海空步道暴紅 帶動觀光熱`,
          link: 'https://udn.com/news/story/7325/5820363',
        },
        {
          id: 12,
          title: `「海空步道」加持！蚵車全變載客遊覽車 蚵農跑去賺外快`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/3699051',
        },
        {
          id: 13,
          title: `人多問題多！海空步道暴紅 附近宮廟廁所爆滿`,
          link: 'https://tw.news.yahoo.com/%E4%BA%BA%E5%A4%9A%E5%95%8F%E9%A1%8C%E5%A4%9A-%E6%B5%B7%E7%A9%BA%E6%AD%A5%E9%81%93%E6%9A%B4%E7%B4%85-%E9%99%84%E8%BF%91%E5%AE%AE%E5%BB%9F%E5%BB%81%E6%89%80%E7%88%86%E6%BB%BF-063501728.html',
        },
        {
          id: 14,
          title: `芳苑紅樹林海空步道湧數千遊客 擅抓招潮蟹恐釀生態衝擊`,
          link: 'https://news.pts.org.tw/article/548774',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `建構永續社區的職能-以彰化縣海岸永續整體規劃案為例`,
          link: 'https://ttqs.wda.gov.tw/Column_Files/97e6bfdd48c2438598567cf27a8afbc7/%E5%BB%BA%E6%A7%8B%E6%B0%B8%E7%BA%8C%E7%A4%BE%E5%8D%80%E7%9A%84%E8%81%B7%E8%83%BD-%E4%BB%A5%E5%BD%B0%E5%8C%96%E7%B8%A3%E6%B5%B7%E5%B2%B8%E6%B0%B8%E7%BA%8C%E6%95%B4%E9%AB%94%E8%A6%8F%E5%8A%83%E6%A1%88%E7%82%BA%E4%BE%8B-%E8%A8%B1%E5%B3%AF%E9%8A%98.pdf',
        },
        {
          id: 2,
          title: `2022年第353期《高鐵新鮮感　KOL帶路暢玩》: 行遍天下4月號`,
        },
      ],
      網站: [
        {
          id: 1,
          title: `芳苑濕地紅樹林海空步道`,
          link: 'https://tourism.chcg.gov.tw/AttractionsContent.aspx?id=450&chk=c23fe166-038d-4c0b-9c13-e44354a69e3a',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `經濟部「全國水環境改善計畫」芳苑濕地紅樹林暨其週邊整體環境改善-工程工作計畫書`,
          link: 'https://flwe.wra.gov.tw/cl.aspx?n=16990',
        },
        {
          id: 2,
          title: `經濟部「全國水環境改善計畫」芳苑濕地紅樹林暨其週邊整體環境改善(第二期)-工作計畫書`,
          link: 'https://flwe.wra.gov.tw/cl.aspx?n=29164',
        },
        {
          id: 3,
          title: `102年彰化海岸永續整體規劃-芳苑濕地紅樹林生態營造計畫`,
          link: 'https://wetland-tw.tcd.gov.tw/upload/file/20190603145923102.pdf',
        },
        {
          id: 4,
          title: `彰化海岸永續整體規劃-紅樹林生態資源調查暨分析委託研究計畫`,
          link: 'https://wetland-tw.tcd.gov.tw/upload/file/20190531102039429.pdf',
        },
        {
          id: 5,
          title: `彰化海岸永續整體規劃-紅樹林生態資源調查暨分析委託研究計畫`,
          link: 'https://www.taiwanbuying.com.tw/ShowCCDetailOri.ASP?RecNo=2513682',
        },
      ],
    },
    pics: ['14-1'],
  },
  {
    id: '15',
    date: '2023.10.19',
    title: '普天宮',
    category: ['3', '1', '2'],
    address: '彰化縣芳苑鄉芳苑村芳樂街12號',
    location: [23.929195480743957, 120.31668084294775],
    intro: `普天宮位於舊台十七線旁，相傳福建水汛台灣協防右哨千總陳成功因渡海受恩所設建的宮廟。主祀神明為湄洲天上聖母，並有各路神衹作陪祀，是彰化縣沿海地區的信仰中心之一。因故不斷更址，於1990年代才搬遷至今日位置，信仰至今已有三百多年的歷史。`,
    history: {
      story: [
        {
          id: 1,
          time: '1697年(康熙36年) ',
          title: `千總陳成功感念聖母恩澤而建廟`,
        },
        {
          id: 2,
          time: '1859年(咸豐9年) ',
          title: `舊廟被潮水沖坍，改遷至芳苑街上`,
        },
        {
          id: 3,
          time: '1990S',
          title: `為了擴建，遷移至現址`,
        },
        {
          id: 4,
          time: '2020年',
          title: `信徒打造凌霄寶殿建成，並打造玉皇大帝金尊`,
        },
        {
          id: 5,
          time: '2021年',
          title: `海空步道開放，使旅客湧入普天宮停車場`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_65002_374',
          time: '1976年',
          title: `普天宮現址仍是田地`,
        },
        {
          id: 2,
          image: '94201023_070622a_13~0348_rgb',
          time: '2007年',
          title: `得見宮廟與停車場建成`,
        },
        {
          id: 3,
          image: '94201023_111022a_13~0665_hr4',
          time: '2008年',
          title: `宮廟北方綠地大多被去除，多呈裸露地`,
        },
        {
          id: 4,
          image: '94201023_211031a_13~0346_hr4',
          time: '2021年',
          title: `得見廟宇西南面，香爐已落成，廟宇西邊台61線也已運作，周遭因應人潮劃設更多停車位，海空、海牛熱潮令車輛充斥在其停車場`,
        },
      ],
    },
    stakeholder: `普天宮管理委員會、海空步道與海牛遊客、周遭宮廟、芳苑居民、彰化縣政府`,
    reference: {
      網站: [
        {
          id: 1,
          title: `芳苑普天宮Facebook`,
          link: 'https://www.facebook.com/%E8%8A%B3%E8%8B%91%E6%99%AE%E5%A4%A9%E5%AE%AE-1639941082981789/',
        },
        {
          id: 2,
          title: `彰化縣芳苑鄉普天宮`,
          link: 'http://crgis.rchss.sinica.edu.tw/temples/ChanghuaCounty/fangyuan/0723003-PTG',
        },
        {
          id: 3,
          title: `白馬峯普天宮`,
          link: 'https://tourism.chcg.gov.tw/AttractionsContent.aspx?id=141&chk=0c702e4a-729b-4899-a468-6843125cd176',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `彰化芳苑紅樹林海空步道遊客暴量 地方憂喜參半`,
          link: 'https://udn.com/news/story/7325/5761257',
        },
        {
          id: 2,
          title: `人多問題多！海空步道暴紅 附近宮廟廁所爆滿`,
          link: 'https://tw.news.yahoo.com/%E4%BA%BA%E5%A4%9A%E5%95%8F%E9%A1%8C%E5%A4%9A-%E6%B5%B7%E7%A9%BA%E6%AD%A5%E9%81%93%E6%9A%B4%E7%B4%85-%E9%99%84%E8%BF%91%E5%AE%AE%E5%BB%9F%E5%BB%81%E6%89%80%E7%88%86%E6%BB%BF-063501728.html',
        },
        {
          id: 3,
          title: `普天宮媽祖爽快賜4聖筊　2022國際海牛文化節5月7日登場`,
          link: 'https://www.watchmedia01.com/anews-20220324041016.html',
        },
        {
          id: 4,
          title: `全台最大媽祖廟芳苑普天宮 凌宵寶殿慶成玉皇大帝入火安座`,
          link: 'https://www.chinatimes.com/realtimenews/20201219003223-264216?chdtv',
        },
        {
          id: 5,
          title: `彰化縣府小紅包曝光 13家宮廟年節期間發送`,
          link: 'https://udn.com/news/story/7325/6067355',
        },
        {
          id: 6,
          title: `2018彰化縣媽祖聯合遶境祈福在芳苑普天宮起駕`,
          link: 'https://tw.news.yahoo.com/%E5%BD%B1-2018%E5%BD%B0%E5%8C%96%E7%B8%A3%E5%AA%BD%E7%A5%96%E8%81%AF%E5%90%88%E9%81%B6%E5%A2%83%E7%A5%88%E7%A6%8F%E5%9C%A8%E8%8A%B3%E8%8B%91%E6%99%AE%E5%A4%A9%E5%AE%AE%E8%B5%B7%E9%A7%95-132850619.html',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `旅讀No116｜尋味亞洲米其林`,
        },
        {
          id: 2,
          title: `圖解節慶觀光與民俗: SOP標準流程與案例分析`,
        },
        {
          id: 3,
          title: `我家住海邊：尋找台灣即將消失的漁法`,
        },
        {
          id: 4,
          title: `圖解台灣迎媽祖`,
        },
        {
          id: 5,
          title: `公民不冷血：新世紀台灣公民行動事件簿`,
        },
        {
          id: 6,
          title: `台中地區休閒農業旅遊導覽手冊`,
        },
        {
          id: 7,
          title: `臺灣風物，第27卷`,
        },
        {
          id: 8,
          title: `臺灣寺廟概覽`,
        },
        {
          id: 9,
          title: `媽祖傳說`,
        },
        {
          id: 10,
          title: `全國寺廟名冊`,
        },
        {
          id: 11,
          title: `國史館現藏民國人物傳記史料彙編，第30卷`,
        },
        {
          id: 12,
          title: `臺灣舊地名之沿革，第2卷，第2篇`,
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `彰化海岸永續整體規劃-紅樹林生態資源調查暨分析委託研究計畫`,
          link: 'https://wetland-tw.tcd.gov.tw/upload/file/20190531102039429.pdf',
        },
        {
          id: 2,
          title: `地方宗教慶典行政成果`,
          link: 'https://religion.moi.gov.tw/LocalCelebration/Content?ci=96&cid=32',
        },
      ],
    },
    pics: ['15-1'],
  },
  {
    id: '16',
    date: '2023.10.19',
    title: '晨陽學園',
    category: ['4'],
    address: '彰化縣芳苑鄉新街村新上路1號',
    location: [23.90231399162027, 120.31160776752678],
    intro: `將廢棄的新街國小重新整理，成立晨陽學園。成立目的為建立一個可讓中輟生快樂學習、安心居住的家。透過補救教學、職能培育和環島等活動，幫助這群10至15歲的學生找到人生的目標，發揮自己的專長。`,
    history: {
      story: [
        {
          id: 1,
          time: '1975年',
          title: `新街國小獨立`,
        },
        {
          id: 2,
          time: '2003年',
          title: `新街國小廢校`,
        },
        {
          id: 3,
          time: '2008年',
          title: `晨陽學園成立`,
        },
      ],
      data: [
        {
          id: 1,
          image: '',
          time: '2008年',
          title: `晨陽學園創立，可見校舍更新`,
        },
        {
          id: 2,
          image: '',
          time: '2019年',
          title: `校舍屋頂新增太陽能光電板`,
        },
      ],
    },
    stakeholder: `教育部青年發展署、彰化縣政府、彰化縣社團法人愛鄰社會福利協會、學園內部社工與學生`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `廢棄新街國小 規劃少年學園`,
          link: 'http://neoedu.blogspot.com/2007/10/blog-post_02.html',
        },
        {
          id: 2,
          title: `一年廢一校？彰化縣教師工會籲縣府重視偏鄉師生權益`,
          link: 'https://udn.com/news/story/6885/5149831',
        },
      ],
    },
    pics: ['16-1'],
  },
  {
    id: '17',
    date: '2023.10.19',
    title: '芳苑慈恩日間照顧中心',
    category: ['4'],
    address: '彰化縣芳苑鄉頂廍村頂廍段上林路403號',
    location: [23.911678431946022, 120.32911659279468],
    intro: `彰化縣政府委託慈恩社福慈善基金會，利用廢棄的路上國小頂廍分校閒置空間，設置芳苑第一家日照中心。希望可以讓長者在熟悉的社區內接受家庭式的照顧，減輕家屬對年長者的照顧負擔。`,
    history: {
      story: [
        {
          id: 1,
          time: '1956年',
          title: `路上國小頂廍分校成立`,
        },
        {
          id: 2,
          time: '1998年',
          title: `興建綜合球場`,
        },
        {
          id: 3,
          time: '2003年',
          title: `路上國小頂廍分校廢校`,
        },
        {
          id: 4,
          time: '2020年',
          title: `芳苑慈恩日間照顧中心開幕`,
        },
      ],
      data: [
        {
          id: 1,
          image: '',
          time: '2015年',
          title: `日照中心尚未成立，此處仍為廢棄校舍，樹木生長茂密，屋頂未整修`,
        },
        {
          id: 2,
          image: '94201033_211031a_13~0350_hr4',
          time: '2021年',
          title: `日照中心已成立，當年配合COVID-19疫情，可見廣場上有紅色帳篷`,
        },
      ],
    },
    stakeholder: `彰化縣政府、慈恩社福慈善基金會、當地居民`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `彰化縣第一家！路上頂廍分校廢棄校舍 改建日照中心開幕`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/3262248',
        },
        {
          id: 2,
          title: `國小校舍重生 芳苑第1家日照服務開跑了`,
          link: 'https://www.chcg.gov.tw/ch/newsdetail.asp?bull_id=320838',
        },
      ],
      網站: [
        {
          id: 1,
          title: `彰化縣芳苑鄉路上國小；學校歷史沿革大事記`,
          link: 'https://www.lses.chc.edu.tw/departments/2/show',
        },
      ],
    },
    pics: ['17-1'],
  },
  {
    id: '18',
    date: '2023.11.20',
    title: '過湖農莊',
    category: ['5', '2'],
    address: '彰化縣芳苑鄉新街村過湖路36號',
    location: [23.89189503772095, 120.30855204730553],
    intro: `為了克服芳苑鄉土地貧瘠的問題，農莊主人在2007年引進日本水耕技術，期望在芳苑鄉也能種植出許多經濟作物。除了種植蔬菜，農莊主人也設計許多體驗活動讓民眾可以透過實作更加了解水耕技術。還會不定期舉辦農業與環境相關議題講座，讓當地社區民眾更加了解農業與環境的關聯性，期望對當地能帶來更好的發展。`,
    history: {
      story: [
        {
          id: 1,
          time: '2007年',
          title: `農莊主人還未引進水耕技術，此處為一片綠地`,
        },
        {
          id: 2,
          time: '2009年',
          title: `已引進水耕技術，可見出現水耕植物種植區`,
        },
        {
          id: 3,
          time: '2017年',
          title: `水耕蔬菜體驗營`,
        },
        {
          id: 4,
          time: '2017年',
          title: `講座海報`,
        },
        {
          id: 5,
          time: '2021年',
          title: `過湖農莊體驗活動行程表`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201043_070622a_13~0340_rgb',
          time: '2007年',
          title: `主建物旁為一片荒地`,
        },
        {
          id: 2,
          image: '94201043_091009a_13~0259_rgb',
          time: '2009年',
          title: `主建物旁水耕溫室完成興建`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉農會、過湖農莊主人`,
    reference: {
      網站: [
        {
          id: 1,
          title: `過湖農莊Facebook粉絲團`,
          link: 'https://www.chcg.gov.tw/ch/newsdetail.asp?bull_id=251173',
        },
        {
          id: 2,
          title: `彰化縣政府青年夢想方舟－認識水耕蔬菜與體驗DIY`,
          link: 'https://visit.chc.edu.tw/search/144/show/vendor',
        },
      ],
      遊記: [
        {
          id: 1,
          title: `彰化縣農業體驗-芳苑過湖農莊（水耕蔬菜體驗活動）`,
          link: 'https://www.i-play.tw/?p=9654',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `「2017文化起藝逍遙遊－健康快樂FUN暑假」 開始報名囉！歡迎踴躍報名參加`,
          link: 'https://www.chcg.gov.tw/ch/newsdetail.asp?bull_id=251173',
        },
      ],
    },
    pics: ['18-1'],
  },
  {
    id: '19',
    date: '2023.10.19',
    title: '太陽能板環保發電區',
    category: ['5', '1'],
    address: '彰化縣芳苑鄉新街村',
    location: [23.902627573894804, 120.30202603555522],
    intro: `位於新街海堤與西濱快速公路之間的一處太陽能板區。`,
    history: {
      story: [
        {
          id: 1,
          time: '2017年',
          title: `開始架設太陽能板`,
        },
        {
          id: 2,
          time: '2018年',
          title: `太陽能板架設完畢`,
        },
      ],
      data: [
        {
          id: 1,
          image: '',
          time: '2017年',
          title: `可見此處已在放樣與架設太陽能板架`,
        },
        {
          id: 2,
          image: '',
          time: '2018年',
          title: `此處太陽能板已架設完畢`,
        },
      ],
    },
    stakeholder: `台灣電力公司、經濟部水利署、當地居民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `全國土地使用分區資料查詢系統`,
          link: 'https://nsp.tcd.gov.tw/tcdgm/Default.aspx',
        },
        {
          id: 2,
          title: `國土測繪圖資服務雲`,
          link: 'https://maps.nlsc.gov.tw/',
        },
      ],
    },
    pics: ['19-1'],
  },
  {
    id: '20',
    date: '2023.10.19',
    title: '芳成段572地號空品淨化區',
    category: ['1'],
    address: '彰化縣芳苑鄉三成村',
    location: [23.88769311093816, 120.33263957155395],
    intro: `透過種植植物來改善環境、空氣品質，且希望透過此做法達到環境教育的目的。目前此區域種植阿勃勒、象牙木和桑樹等樹木。且由力鵬企業股份有限公司進行認養及經常性維護。`,
    history: {
      story: [
        {
          id: 1,
          time: '2005年3月',
          title: `芳苑鄉向中央提出此處要申請成為空品淨化區`,
        },
        {
          id: 2,
          time: '2005年8月',
          title: `環境保護署到此處探勘`,
        },
        {
          id: 3,
          time: '2005年12月',
          title: `空品淨化區開工`,
        },
        {
          id: 4,
          time: '2006年3月',
          title: `空品淨化區完工`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201043_070622a_13~0340_rgb',
          time: '2007年',
          title: `空品淨化區剛建立`,
        },
        {
          id: 2,
          image: '94201043_211031a_13~0354_hr4',
          time: '2021年',
          title: `空品淨化區近年地貌`,
        },
      ],
    },
    stakeholder: `行政院環境保護署、彰化縣環境保護局、芳苑鄉公所、力鵬企業股份有限公司`,
    reference: {
      網站: [
        {
          id: 1,
          title: `空氣品質淨化區及環境綠化育苗計畫申報及查詢系統`,
          link: 'https://freshair.epa.gov.tw/airweb/backoffice/s07_2.aspx?serino=8438',
        },
        {
          id: 2,
          title: `行政院環境保護署－空氣品質改善維護資訊網`,
          link: 'https://air.epa.gov.tw/EnvTopics/AirQuality_11.aspx',
        },
      ],
    },
    pics: ['20-1'],
  },
  {
    id: '21',
    date: '2023.10.19',
    title: '新街排水',
    category: ['1'],
    address: '彰化縣芳苑鄉新街村',
    location: [23.906263129733613, 120.30513273831053],
    intro: `建於2006-2007年間。位於彰化縣芳苑鄉的排水系統，地勢低窪，每逢下豪大雨，即導致此處淹水嚴重。且一但遇到漲潮，淹水就更不易排出。彰化縣政府為了解決以上問題將設置「新街抽水站」。`,
    history: {
      story: [
        {
          id: 1,
          time: '2005年',
          title: `彰化縣政府預計新增新街排水幹線`,
        },
        {
          id: 2,
          time: '2021年',
          title: `受盧碧颱風影響，此處淹水嚴重`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201032_071002a_12_0467_rgb',
          time: '2007年',
          title: `可見新街排水系統已建完畢`,
        },
        {
          id: 2,
          image: '',
          time: '2017年',
          title: `排水系統出口新增一台橘色抽水機，且靠海一側的漁塭呈現特殊色`,
        },
        {
          id: 3,
          image: '94201032_181108z_12~1543_hr4',
          time: '2018年',
          title: `抽水機依舊存在。靠海側漁塭面積縮減，可見西濱快速道路工程興建中`,
        },
        {
          id: 4,
          image: '94201032_211031a_12~0144_hr4',
          time: '2021年',
          title: `排水系統出口新增一台抽水機，共兩台`,
        },
      ],
    },
    stakeholder: `芳苑鄉公所、彰化縣政府`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `彰化新街新設抽水站 居民盼啟用後別再淹水到床邊`,
          link: 'https://udn.com/news/story/7325/6351834',
        },
        {
          id: 2,
          title: `改善芳苑交通及淹水 新肚橋改建、增設新街排水抽水站`,
          link: 'https://tw.news.yahoo.com/news/%E6%94%B9%E5%96%84%E8%8A%B3%E8%8B%91%E4%BA%A4%E9%80%9A%E5%8F%8A%E6%B7%B9%E6%B0%B4-%E6%96%B0%E8%82%9A%E6%A9%8B%E6%94%B9%E5%BB%BA-%E5%A2%9E%E8%A8%AD%E6%96%B0%E8%A1%97%E6%8E%92%E6%B0%B4%E6%8A%BD%E6%B0%B4%E7%AB%99-045803348.html',
        },
        {
          id: 3,
          title: `王惠美縣長爭取治水計畫1.1億提前執行解決芳苑大城淹水問題 蘇揆：不分黨派 趕快進行`,
          link: 'https://www.chcg.gov.tw/ch/newsdetail.asp?bull_id=339350',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `彰化縣縣管區域排水一覽表`,
          link: 'https://www.chcg.gov.tw/files/57_1050728_376475300A0000000_0250232A00_ATTCH9.pdf',
        },
      ],
    },
    pics: ['21-1'],
  },
  {
    id: '22',
    date: '2023.10.19',
    title: '王功福海宮',
    category: ['3', '2'],
    address: '彰化縣芳苑鄉民生村芳漢路王功段2號 ',
    location: [23.952929492784463, 120.33453164481429],
    intro: `福海宮創建於清嘉慶十年(西元1812年)，主祀媽祖，同時恭奉註生娘娘、玉皇大帝、觀音菩薩、福德正神、三教教主、五教教主以及十八羅漢。而廟前廣場佔地遼闊，並擁有大片的停車場，方便信徒到此參拜。福海宮坐東朝西，是依據原址退後三步興建，廣場豎立著兩座旗杆，代表福海宮屬於一間「官廟」，在台灣非常少見。右後方有一座「福海公園」，右前方則有一口「龍泉井」，兩者與廟前的旗杆具有陰陽均衡之意。 `,
    history: {
      story: [
        {
          id: 1,
          time: '1958年',
          title: `成立福海宮重建委員會`,
        },
        {
          id: 2,
          time: '1959年',
          title: `成立第一屆管理委員會`,
        },
        {
          id: 3,
          time: '1963年',
          title: `改建南側襌房，同年建福海公園及噴水池`,
        },
        {
          id: 4,
          time: '1974年',
          title: `天井通風樓及前後殿屋頂重修`,
        },
        {
          id: 5,
          time: '1975年',
          title: `建鐘鼓樓、大拜亭、深井`,
        },
        {
          id: 6,
          time: '1978年',
          title: `兩廂襌房竣工`,
        },
        {
          id: 7,
          time: '1982~1985年',
          title: `後殿竣工`,
        },
        {
          id: 8,
          time: '1991~1994年',
          title: `牌樓、圍牆竣工，廟前廣場舖設混泥土`,
        },
        {
          id: 9,
          time: '1994~1997年',
          title: `龍泉井竣工、重設廣場旗杆`,
        },
        {
          id: 10,
          time: '1997~2000年',
          title: `廟前廣場舖泉州石`,
        },
        {
          id: 11,
          time: '2003~2006年',
          title: `圓通寶殿及凌霄寶殿屋頂翻新及剪粘、新建高壓磚大停車場、新建景觀公園`,
        },
        {
          id: 12,
          time: '2006~2009年',
          title: `二樓圓通寶殿左右翼增建財神殿及文昌殿、宮前牌樓整修、增建第三停車場`,
        },
        {
          id: 13,
          time: '2022年',
          title: `舉辦彰化縣媽祖文化節`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201014_65009_019 ',
          time: '1976年',
          title: `福海宮已座落於現址`,
        },
        {
          id: 2,
          image: '94201014_070622a_14~0011_rgb',
          time: '2007年',
          title: `殿堂、停車場、福海公園皆已增建，省道17號已開通`,
        },
        {
          id: 3,
          image: '94201014_121001a_14~0832_hr4 ',
          time: '2012年',
          title: `下方空地尚未增建停車場`,
        },
        {
          id: 4,
          image: '',
          time: '2013年',
          title: `下方空地改為停車格`,
        },
      ],
    },
    stakeholder: `彰化縣政府文化局、芳苑鄉公所、王功福海宮管理委員會、民生與王功社區發展協會、福海宮信眾、當地居民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `王功福海宮管理委員會`,
          link: 'http://www.fuhaigong.org.tw/01.php',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `福海宮宮誌`,
          link: 'https://www.th.gov.tw/epaper/site/page/122/1741',
        },
      ],
    },
    pics: ['22-1'],
  },
  {
    id: '23',
    date: '2023.10.19',
    title: '王功飛行場',
    category: ['1', '2'],
    address: '彰化縣芳苑鄉博愛村 ',
    location: [23.963942694638988, 120.33453073906689],
    intro: `位於永興海埔地接近王功漁港的地區，原本雜草叢生，但由於被遙控飛機玩家視為熱門飛行地點，進而玩家們出錢整地，開闢了這座王功飛行場。`,
    history: {
      story: [
        {
          id: 1,
          time: '1983~1985年',
          title: `永興海埔地開發`,
        },
        {
          id: 2,
          time: '2009年',
          title: `遙控飛機玩家經地主同意，出錢進行整地，開闢出飛機跑道`,
        },
        {
          id: 3,
          time: '2022年',
          title: `跑道感覺已荒廢，周圍雜草叢生、垃圾滿地`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201014_65009_19',
          time: '1976年',
          title: `永興海埔地尚未出現`,
        },
        {
          id: 2,
          image: ' 94201014_091009a_14~0011_rgb ',
          time: '2009年',
          title: `開闢出飛機跑道`,
        },
        {
          id: 3,
          image: '',
          time: '2015年',
          title: `跑道看起來略微荒廢，長滿雜草`,
        },
        {
          id: 4,
          image: '94201014_161103a_14~0060_hr4',
          time: '2016年',
          title: `從圖中看起來，跑道有經過重新劃設整治`,
        },
      ],
    },
    stakeholder: `博愛社區發展協會、當地地主、遙控飛機玩家、當地居民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `蚵畫人生`,
          link: 'http://www.ork.org.tw/',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `王功遙控模型飛機場 飛行新樂園`,
          link: 'https://news.ltn.com.tw/news/local/paper/314232',
        },
      ],
    },
    pics: ['23-1'],
  },
  {
    id: '24',
    date: '2023.10.19',
    title: '王功漁業回收廢棄物暫置區',
    category: ['5', '1'],
    address: '彰化縣芳苑鄉博愛村',
    location: [23.96603775987721, 120.33697784272248],
    intro: `2019年「向海致敬」計畫正式啟動，彰化縣政府將縣內部分海灘，開放單位進行認養維護，並進行淨灘活動，再配合環保署的「海岸淨灘認養系統」，讓有意願為環境盡一份力的民眾申請，加入淨灘的行列。而為了減少彰化海域汙染，彰化縣環保局與農業處合作，在漁港建置蚵殼堆置場及漁網回收設施，讓漁廢可以進行分選，減少進入海洋的可能性。其中，王功漁業回收廢棄物暫置區位於永興海埔地，它的出現，即是「向海致敬」計畫中對彰化縣漁民、漁會回收漁業廢棄物的獎勵專案，只要將漁廢清運至王功的暫置區，就可以獲得獎勵金。`,
    history: {
      story: [
        {
          id: 1,
          time: '2019年',
          title: `「向海致敬」計畫開始實施`,
        },
        {
          id: 2,
          time: '2021年',
          title: `實施將漁廢清運至王功暫置區的獎勵專案`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201014_65009_19',
          time: '1976年',
          title: `永興海埔地尚未開發`,
        },
        {
          id: 2,
          image: '',
          time: '2017年',
          title: `永興海埔地和台61線皆已出現`,
        },
        {
          id: 3,
          image: '',
          time: '2020年',
          title: `較完整的開發地出現`,
        },
        {
          id: 4,
          image: '94201014_211031a_14~0305_hr4',
          time: '2021年',
          title: `成為一片看起來整理過的漁廢暫置區`,
        },
      ],
    },
    stakeholder: `漁業署、彰化縣政府環保局、彰化縣政府、芳苑鄉公所、彰化縣漁民、當地居民`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `永續海洋環境　彰化縣政府攜手地方守護海洋資源`,
          link: 'https://www.businesstoday.com.tw/article/category/183027/post/202105260037/',
        },
      ],
      檔案: [
        {
          id: 1,
          title: `110 年度彰化縣養殖漁業廢棄物回收計畫`,
          link: 'https://town.chcg.gov.tw/files/110%20%E5%B9%B4%E5%BA%A6%20%E5%BD%B0%E5%8C%96%E7%B8%A3%E9%A4%8A%E6%AE%96%E6%BC%81%E6%A5%AD%E5%BB%A2%E6%A3%84%E7%89%A9%E5%9B%9E%E6%94%B6%20%E8%A8%88%E7%95%AB_51_1100907.pdf',
        },
      ],
    },
    pics: ['24-1'],
  },
  {
    id: '25',
    date: '2023.10.19',
    title: '舊趙甲排水興仁段',
    category: ['1'],
    address: '彰化縣芳苑鄉興仁村  ',
    location: [23.957427284397838, 120.34798101320902],
    intro: `舊趙甲排水為流經芳苑鄉的排水系統，其中位於興仁村的河段近年曾進行清淤及生態維護工程。2018年工程規畫階段，彰化縣政府委託景丰科技股份有限公司進行當地生態評估，並有許多民間團體及代表聽取其說明及意見回饋，是一套頗完整的治理工程。工程內容主要涵蓋清除不利排水及雜亂的植被、河道清淤，以及保育生態的相關設施興建。`,
    history: {
      story: [
        {
          id: 1,
          time: '2018年',
          title: `進行舊趙甲沿岸生態檢核計畫`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201014_181104z_14~0516_hr4 ',
          time: '2018年',
          title: `舊趙甲排水沿岸仍能觀察到密集植被分布`,
        },
        {
          id: 2,
          image: '',
          time: '2019年',
          title: `舊趙甲排水沿岸已不見植被，堤岸道路正在整治中`,
        },
        {
          id: 3,
          image: '',
          time: '2020年',
          title: `工程看起來已趨近完成階段，植被已移除，排水河道變寬，周圍道路也重新鋪設完成`,
        },
      ],
    },
    stakeholder: `彰化縣政府環保局、芳苑鄉公所、興仁社區發展協會、當地居民`,
    reference: {
      檔案: [
        {
          id: 1,
          title: `107年度彰化縣生態檢核工作計畫_舊趙甲排水幹線(第一期) 治理工程`,
          link: 'https://data.depositar.io/dataset/18f6a/resource/4809fbdc-8d1d-444d-b39a-f11701cfbd6b',
        },
      ],
    },
    pics: ['25-1'],
  },
  {
    id: '26',
    date: '2023.10.19',
    title: '台灣漢寶園',
    category: ['2', '5'],
    address: '彰化縣芳苑鄉漢寶村成功路486號',
    location: [24.007525547750735, 120.35952478383867],
    intro: `台灣漢寶園致力於環境教育，其園內的眺望台還可以遠眺海邊的潮汐變化及王功夕照，且到這裡還可以體驗「摸蛤兼洗褲」的親子體驗活動，並於漢寶園內多元風格的藝術造景及裝置藝術一同拍照留念。`,
    history: {
      story: [
        {
          id: 1,
          time: '',
          title: `漢寶園之前是營利的休閒農場因經營不善而結束營業，法拍之後由台明將企業得標，改變成一座生態園區。`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94212095_65P057_038',
          time: '1976年',
          title: `此處尚未建設(為條狀的農田)`,
        },
        {
          id: 2,
          image: '94212095_070622a_15~0365_rgb',
          time: '2007年',
          title: `發現此處有建物出現`,
        },
        {
          id: 3,
          image: '94212095_111022a_15~0473_hr4',
          time: '2011年',
          title: `小魚池被填補，大魚池也有整頓，小正方形屋頂翻修成綠色、右邊屋頂翻修成白色`,
        },
        {
          id: 4,
          image: '94212095_121001a_15~0795_hr4',
          time: '2012年',
          title: `小魚池變成「魚」的造型`,
        },
      ],
    },
    stakeholder: `彰化縣政府、彰化縣養殖協會、台明將股份有限公司`,
    reference: {
      網站: [
        {
          id: 1,
          title: `漢寶園休閒農場`,
          link: 'https://www.chcla.com.tw/page/product/show.aspx?num=77',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `漢寶園復育溼地 種出大片苦檻藍`,
          link: 'https://news.ltn.com.tw/news/local/paper/928297',
        },
      ],
    },
    pics: ['26-1'],
  },
  {
    id: '27',
    date: '2023.10.19',
    title: '王者之弓橋',
    category: ['1'],
    address: '彰化縣芳苑鄉王功村漁港路',
    location: [23.968182753424394, 120.32423074051113],
    intro: `「王者之弓橋」為王功漁港內的一座跨港景觀橋，整體橋長八十二公尺，寬四點五公尺；拱頂高二十公尺，橋版面最高處為八公尺。橋樑材質採用最容易塑形之鋼料鑄成，並以在地地名「王功」引申「王者之弓」構思，為之命名。`,
    history: {
      story: [
        {
          id: 1,
          time: '2019年',
          title: `進行補強及除鏽`,
        },
      ],
      data: [
        {
          id: 1,
          image: '4201013_65002_366',
          time: '1976年',
          title: `王者之弓橋尚未出現`,
        },
        {
          id: 2,
          image: '94201013_070622a_13~0352_rgb',
          time: '2007年',
          title: `已出現王者之弓橋`,
        },
        {
          id: 3,
          image: '',
          time: '2013年',
          title: `王者之弓橋左側出現了一塊新上色(與橋同色)的區域/裝飾地景`,
        },
        {
          id: 4,
          image: '',
          time: '2020年',
          title: `左側的地景又出現變化`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `王功跨港兩橋 除鏽或補強施工`,
          link: 'https://news.ltn.com.tw/news/life/paper/1321872',
        },
      ],
      遊記: [
        {
          id: 1,
          title: `彰化芳苑│王者之弓橋-藍色跨港景觀橋.可欣賞漁港風光.王功燈塔和風力發電風車`,
          link: 'https://blue74.net/14887/fangyuan-bridge/',
        },
        {
          id: 2,
          title: `王者之弓橋~藍色橋身的拱橋，能觀賞膠筏、漁船等漁港風光，夕照拍攝的熱門景點`,
          link: 'https://ya7.tw/the-bow-of-the-king/',
        },
      ],
    },
    pics: ['27-1'],
  },
  {
    id: '28',
    date: '2023.10.19',
    title: '台61線',
    category: ['1'],
    address: '彰化縣芳苑鄉',
    location: [23.96997342936247, 120.34283861374333],
    intro: `台61線，又稱西部濱海快速公路，通常被簡稱為西濱快速公路或西濱快，是縱貫臺灣西部沿海地區的省道快速公路，也是全台最長的快速公路，計畫長度共301.834公里。`,
    history: {
      story: [
        {
          id: 1,
          time: '1981年',
          title: `行政院核定`,
        },
        {
          id: 2,
          time: '1982年',
          title: `動工興建`,
        },
        {
          id: 3,
          time: '1986年',
          title: `原計劃預定完工通車，但因為西濱快速道路原先採封閉式道路設計（即全線高架立體化），而引起沿線居民強烈抗爭。`,
        },
        {
          id: 4,
          time: '1991年',
          title: `西濱大橋通車，為台61線最初通車的路段。`,
        },
        {
          id: 5,
          time: '1992年',
          title: `為紓解中山高速公路的交通壅塞，將西部濱海公路提高標準為快速公路，郝柏村內閣納入「六年國建」計畫之一。`,
        },
        {
          id: 6,
          time: '2013年',
          title: `福興交流道－漢寶交流道通車。`,
        },
        {
          id: 7,
          time: '2014年',
          title: `漢寶交流道－王功交流道通車。`,
        },
        {
          id: 8,
          time: '2017年12月',
          title: `王功交流道-芳苑交流道通車`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201014_100921a_14~0942_hr4',
          time: '2010年',
          title: `已出現部分台61已規劃的土地`,
        },
        {
          id: 2,
          image: '94201014_111022a_14~0624_hr4',
          time: '2011年',
          title: `台61之路線已出現部分基礎建設`,
        },
        {
          id: 3,
          image: '',
          time: '2013年',
          title: `已架設好基礎路面`,
        },
        {
          id: 4,
          image: '94201014_141012a_14~0473_hr4',
          time: '2014年',
          title: `漢寶交流道－王功交流道通車`,
        },
        {
          id: 5,
          image: '',
          time: '2020年',
          title: `王功-芳苑交流道已有車輛行駛`,
        },
      ],
    },
    stakeholder: `行政院、彰化縣政府`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `貨櫃車自撞路燈翻覆 台61線王功北上芳苑段車流回堵全線封閉`,
          link: 'https://udn.com/news/story/7320/5765062',
        },
        {
          id: 2,
          title: `台61線漢寶至王功路段訂於102年12月29日15時開放通車`,
          link: 'https://168.motc.gov.tw/theme/news/post/1906121101409',
        },
        {
          id: 3,
          title: `西濱漢寶至王功路段通車　辦路跑與祈福`,
          link: 'https://news.housefun.com.tw/news/article/10773352234.html',
        },
        {
          id: 4,
          title: `西濱漢寶王功段通車 千人頂風路跑`,
          link: 'https://www.chinatimes.com/newspapers/20131230000500-260107?chdtv',
        },
        {
          id: 5,
          title: `台61線芳苑交流道通車 脊背橋設計成亮點`,
          link: 'https://www.ftvnews.com.tw/news/detail/2017C19C12M1',
        },
      ],
      遊記: [
        {
          id: 1,
          title: `台61線 西濱快速公路，蒐集八里到台南的西部獨有風景`,
          link: 'https://smiletaiwan.cw.com.tw/article/2088',
        },
      ],
    },
    pics: ['28-1'],
  },
  {
    id: '29',
    date: '2023.10.19',
    title: '彰化王功風力發電站',
    category: ['1'],
    address: '彰化縣芳苑鄉新寶村',
    location: [23.98951027660185, 120.33927362211625],
    intro: `王功風力發電站位於臺灣彰化縣芳苑鄉王功漁港北側防風林，為隸屬於台灣電力公司的風力發電站。該發電站為台灣電力公司所推動之「風力發電十年發展計畫」中的第三期風力開發工程`,
    history: {
      story: [
        {
          id: 1,
          time: '2008年',
          title: `通過行政院環境保護署的環境影響評估審查。`,
        },
        {
          id: 2,
          time: '2008年12月',
          title: `由中興電工集團得標後開始動工興建`,
        },
        {
          id: 3,
          time: '2011年',
          title: `商轉年分`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201004_070622a_14~0007_rgb',
          time: '2007年',
          title: `還未有風機`,
        },
        {
          id: 2,
          image: '94201004_100921a_14~0946_hr4',
          time: '2010年',
          title: `風機已插設完成`,
        },
        {
          id: 3,
          image: '94201004_111022a_14~0628_hr4',
          time: '2011年',
          title: `風機開始商轉`,
        },
      ],
    },
    stakeholder: `行政院、中興電工集團、台灣電力公司、當地居民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `風力資訊整合平台`,
          link: 'https://pro.twtpo.org.tw/GIS/',
        },
      ],
    },
    pics: ['29-1'],
  },
  {
    id: '30',
    date: '2023.10.19',
    title: '王功新生地',
    category: ['1'],
    address: '彰化縣芳苑鄉新寶村、王功村',
    location: [23.98001800540953, 120.33851860173135],
    intro: `1969年彰化王功海埔新生地已開發完成。王功海埔新生地是為配合王功漁港的修建，由省土地資源開發委員會開發完成的彰化王功海埔新生地，可作雙期水田之用，甚具經濟價值。`,
    history: {
      story: [
        {
          id: 1,
          time: '1969年',
          title: `王功新生地開發完成`,
        },
        {
          id: 2,
          time: '2021年',
          title: `王功新生地海堤整體環境改善`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201004_65P057_010',
          time: '1976年',
          title: `大部分面積都以農田與魚塭為主`,
        },
        {
          id: 2,
          image: '94201004_070622a_14~0007_rgb',
          time: '2007年',
          title: `出現新寶國小及一區的建築物(工廠、長老教會)`,
        },
        {
          id: 3,
          image: '94201004_121001a_14~0828_hr4',
          time: '2012年',
          title: `出現一個很大面積的裸露地`,
        },
        {
          id: 4,
          image: '94201004_181104z_14~0521_hr4',
          time: '2018年',
          title: `新寶國小北方太陽能板架設完成`,
        },
        {
          id: 5,
          image: '',
          time: '',
          title: `註：此地景非單一點位而是指王功新生地範圍區域`,
        },
      ],
    },
    stakeholder: `臺灣省政府、學校、工廠、當地居民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `彰化縣王功海埔新生地開發完成`,
          link: 'https://catalog.digitalarchives.tw/item/00/31/a2/49.html',
        },
      ],
      研究: [
        {
          id: 1,
          title: `臺灣西部海岸線的演變及海埔地的開發`,
          link: 'chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/https://www.geo.ntnu.edu.tw/wp-content/uploads/2022/01/61.pdf',
        },
      ],
    },
    pics: ['30-1'],
  },
  {
    id: '31',
    date: '2023.10.19',
    title: '漁民廣場',
    category: ['2', '1'],
    address: '彰化縣芳苑鄉信義村',
    location: [23.92270311702863, 120.31160341929007],
    intro: `觀海平台巡站為芳苑海巡一站，因傍臨海岸，且有停車位提供，故亦成為一個可見海天一色的觀光景點。此外，從漁民廣場上可眺望蚵田，並在特定時節能見著鐵牛搬運的特殊人文地景。`,
    history: {
      story: [
        {
          id: 1,
          time: '2010年',
          title: `生態民團在漁民廣場抗議國光石化，並倡議保護白海豚`,
        },
        {
          id: 2,
          time: '2020年',
          title: `進行「109年彰化縣芳苑鄉台61線芳苑沿線漁民廣場整修」`,
        },
        {
          id: 3,
          time: '2021年',
          title: `進行「110年彰化縣芳苑鄉漁民廣場空間改善計畫」`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_65002_372',
          time: '1976年',
          title: `尚未沉積於今平台所在地，故未開發`,
        },
        {
          id: 2,
          image: '94201023_091009a_13~0267_rgb',
          time: '2008年',
          title: `可見平台已畫停車格`,
        },
        {
          id: 3,
          image: '94201023_100921a_13~0448_hr4',
          time: '2010年',
          title: `可見平台後向北側的紅石磚行道鋪成`,
        },
        {
          id: 4,
          image: '94201023_100921a_13~0448_hr4',
          time: '2018年',
          title: `平台陸域向海擴張，海堤後方道路劃設與西濱動工`,
        },
      ],
    },
    stakeholder: `遊客、附近居民、海巡署、芳苑鄉公所、採蚵業者、生態保育民團、國發會`,
    reference: {
      網站: [
        {
          id: 1,
          title: `古城新貌~彰化縣城鄉風貌成果宣導網。`,
          link: 'https://landscape.chcg.gov.tw/townresult_description.php?PNo=15',
        },
        {
          id: 2,
          title: `風頭水尾的「濕」落之地｜彰化海岸濕地面臨哪些威脅？`,
          link: 'https://ourisland.pts.org.tw/content/8699',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `搶救白海豚 股東訪芳苑棲地`,
          link: 'https://news.ltn.com.tw/news/local/paper/441623',
        },
        {
          id: 2,
          title: `彰化向中央爭取經費做事 3地方創生案通過`,
          link: 'https://www.peopo.org/news/474852',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `110年彰化縣芳苑鄉漁民廣場空間改善計畫–開放標案`,
          link: 'https://pcc.mlwmlw.org/tender/%E5%BD%B0%E5%8C%96%E7%B8%A3%E8%8A%B3%E8%8B%91%E9%84%89%E5%85%AC%E6%89%80/1100017366',
        },
        {
          id: 2,
          title: `109年彰化縣芳苑鄉台61線芳苑沿線漁民廣場整修–開放標案`,
          link: 'https://pcc.mlwmlw.org/tender/%E5%BD%B0%E5%8C%96%E7%B8%A3%E8%8A%B3%E8%8B%91%E9%84%89%E5%85%AC%E6%89%80/1090015160',
        },
        {
          id: 3,
          title: `芳苑鄉公所`,
          link: 'https://town.chcg.gov.tw/fangyuan/04bulletin/bulletin02_con.aspx?bull_id=163379',
        },
        {
          id: 4,
          title: `109年彰化縣芳苑鄉漁民廣場空間改善計畫`,
          link: 'https://landscape.chcg.gov.tw/upload/other_photo/photo_794_165839525692.pdf',
        },
        {
          id: 5,
          title: `109年彰化縣芳苑鄉台61線芳苑沿線漁民廣場整修`,
          link: 'https://landscape.chcg.gov.tw/upload/other_photo/photo_789_165839431756.pdf',
        },
      ],
    },
    pics: ['31-1'],
  },
  {
    id: '32',
    date: '2023.11.20',
    title: '芳苑鄉立圖書館',
    category: ['4', '1'],
    address: '彰化縣芳苑鄉芳苑村斗苑路芳苑段206號',
    location: [23.924438652917512, 120.31992364372569],
    intro: `鄉立圖書館成立於民國79年，為提供芳苑居民休閒、借閱資料的用途。原借用於文化活動中心地下室，但隨著地域狹濕、讀者數和增加的問題，故移居於現址。在民國101年進行館舍環境改善，以提供民眾更舒適的閱讀環境。`,
    history: {
      story: [
        {
          id: 1,
          time: '1990年',
          title: `圖書館成立，暫存在文化活動中心地下室`,
        },
        {
          id: 2,
          time: '2002年',
          title: `搬遷於現址`,
        },
        {
          id: 3,
          time: '2012年',
          title: `得文化局補助，進行館舍改善計畫`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_070622a_13~0348_rgb',
          time: '2007年',
          title: `建物樓頂為粉色漆`,
        },
        {
          id: 2,
          image: '94201023_211031a_13~0346_hr4',
          time: '2021年',
          title: `樓頂從粉色漆變成白色漆，周遭走道樹葉有定期修剪`,
        },
      ],
    },
    stakeholder: `芳苑居民、彰化縣文化局、讀者、教育部。`,
    reference: {
      網站: [
        {
          id: 1,
          title: `芳苑鄉立圖書館`,
          link: 'https://lib.bocach.gov.tw/cp.aspx?n=1213',
        },
        {
          id: 2,
          title: `彰化縣芳苑鄉立圖書館管理規則`,
          link: 'https://town.chcg.gov.tw/files/54_1031201_%E5%BD%B0%E5%8C%96%E7%B8%A3%E8%8A%B3%E8%8B%91%E9%84%89%E7%AB%8B%E5%9C%96%E6%9B%B8%E9%A4%A8%E7%AE%A1%E7%90%86%E8%A6%8F%E5%89%87.pdf',
        },
        {
          id: 3,
          title: `芳苑鄉立圖書館Facebook`,
          link: 'https://www.facebook.com/%E8%8A%B3%E8%8B%91%E9%84%89%E7%AB%8B%E5%9C%96%E6%9B%B8%E9%A4%A8-2310829435839787/',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `彰化縣推動公共圖書館總館—分館體系計畫 修正計畫書`,
        },
        {
          id: 2,
          title: `芳苑鄉立圖書館106年多元閱讀實施計畫活動簡章`,
          link: 'https://itaiwan.moe.gov.tw/docnews_info.php?id=2346',
        },
      ],
    },
    pics: ['32-1'],
  },
  {
    id: '33',
    date: '2023.10.19',
    title: '永興海埔地',
    category: ['6', '5', '1'],
    address: '彰化縣芳苑鄉永興村',
    location: [23.95812324746187, 120.31917845998944],
    intro: `永興海埔地為二林溪出海口周遭的地景，由人文與自然共織而成。雖然是重要的養殖魚場與海堤位置，然而因水資源配套不足，效益並不如預期。`,
    history: {
      story: [
        {
          id: 1,
          time: '1980年',
          title: `政府挖除舊海堤填造永興海埔地`,
        },
        {
          id: 2,
          time: '1986',
          title: `政府對永興海埔地進行開發`,
        },
        {
          id: 3,
          time: '1990年',
          title: `永興海埔地的開發商祥成公司與彰化縣府發生弊案`,
        },
        {
          id: 4,
          time: '1996年',
          title: `賀伯颱風引發暴潮現象，造成災損`,
        },
        {
          id: 5,
          time: '2020年',
          title: `配合海岸管理法，水利署核定「彰化縣一級海岸防護計畫」`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201013_65002_366',
          time: '1976年',
          title: `永興海埔地尚未沉積至今日位置`,
        },
        {
          id: 2,
          image: ' 94201013_070622a_13~0352_rgb',
          time: '2007年',
          title: `得見永興海埔地發育至今日之規模`,
        },
        {
          id: 3,
          image: '94201013_141013a_13~0014_hr4',
          time: '2014年',
          title: `零星魚塭出現不同的顏色`,
        },
        {
          id: 4,
          image: '',
          time: '2015年',
          title: `部分魚塭出現紅色池水，各家魚塭範疇略有變化`,
        },
      ],
    },
    stakeholder: `養殖業者、社團法人中華民國野鳥學會、彰化縣政府、彰化野鳥學會、台灣護聖宮教育基金會、彰化縣環境保護聯盟`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `永興海埔新生地 排水溝破裂`,
          link: 'https://www.merit-times.com/NewsPage.aspx?unid=565534',
        },
        {
          id: 2,
          title: `芳苑永興海埔地 大杓鷸來過冬`,
          link: 'https://news.ltn.com.tw/news/local/paper/820014',
        },
        {
          id: 3,
          title: `還給海洋一個呼吸 彰化海岸垃圾問題提解方`,
          link: 'https://tw.news.yahoo.com/%E9%82%84%E7%B5%A6%E6%B5%B7%E6%B4%8B-%E5%80%8B%E5%91%BC%E5%90%B8-%E5%BD%B0%E5%8C%96%E6%B5%B7%E5%B2%B8%E5%9E%83%E5%9C%BE%E5%95%8F%E9%A1%8C%E6%8F%90%E8%A7%A3%E6%96%B9-120806185.html',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `彰化永興區海埔地鄰近地區土地利用之經濟分析`,
        },
        {
          id: 2,
          title: `我們的島: 臺灣三十年環境變遷全紀錄`,
        },
        {
          id: 3,
          title: `國立臺灣大學理學院地理學系地理學報第 14-18 期`,
        },
      ],
      網站: [
        {
          id: 1,
          title: `彰化--王功永興海埔地(Changhua--Wanggong Yongxing Tidal Land)`,
          link: 'https://ebird.org/hotspot/L2753464',
        },
        {
          id: 2,
          title: `第六回：來彰化（一）`,
          link: 'https://jcshieh.tw/?p=1006',
        },
        {
          id: 3,
          title: `堤防邊的舊水門`,
          link: 'https://crc.culture.tw/ChoshuiRiver/zh-tw/%E5%9C%B0%E5%9C%96/261383',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `彰化永興海埔新生地質詢稿`,
          link: 'https://www.ly.gov.tw/Pages/Detail.aspx?nodeid=5701&pid=50986',
        },
        {
          id: 2,
          title: `彰化縣地政看板`,
          link: 'https://land.chcg.gov.tw/07other/main.asp?main_id=15286',
        },
        {
          id: 3,
          title: `彰化縣一級海岸防護計畫(核定本)`,
          link: 'https://www-ws.wra.gov.tw/Download.ashx?u=LzAwMS9VcGxvYWQvNDAxL3JlbGZpbGUvMC82OTQ0LzRhOWY1YjZjLWRhOTQtNGU3YS04YTUzLTMzZmI5NmY3YmNkYy5wZGY%3D&n=5b2w5YyW57ij5LiA57Sa5rW35bK46Ziy6K236KiI55WrLnBkZg%3D%3D',
        },
        {
          id: 4,
          title: `臺灣海岸詳介`,
          link: 'https://www.wra.gov.tw/News_Content.aspx?n=3253&s=19356',
        },
      ],
    },
    pics: ['33-1'],
  },
  {
    id: '34',
    date: '2023.10.19',
    title: '吾龍宮',
    category: ['3'],
    address: '彰化縣芳苑鄉頂廍村',
    location: [23.90686966881172, 120.32574806151375],
    intro: `一處位於芳苑鄉頂廍村的寺廟，主神為瑤池金母。`,
    history: {
      story: [],
      data: [
        {
          id: 1,
          image: '94201033_120523a_13~0048_hr4',
          time: '2012年',
          title: `原宮廟的樣貌，西側水池還在`,
        },
        {
          id: 2,
          image: '',
          time: '2013年',
          title: `宮廟西側屋頂有縮減，西側水池還在`,
        },
        {
          id: 3,
          image: '94201033_141013a_13~0022_hr4',
          time: '2014年',
          title: `宮廟西側的屋頂縮減部分變多，宮廟西南方的水池乾凅`,
        },
        {
          id: 4,
          image: '',
          time: '2015年',
          title: `宮廟西側的屋頂縮減部分相較前幾年變得更多，宮廟西側兩個水池皆乾凅`,
        },
        {
          id: 5,
          image: '',
          time: '2017年',
          title: `宮廟西側鐵皮只剩西北方一小隅，原西側水池變成農田`,
        },
      ],
    },
    stakeholder: `吾龍宮宮主、當地民眾、信眾`,
    reference: {
      網站: [
        {
          id: 1,
          title: `芳苑吾龍宮Facebook粉絲團`,
          link: 'https://www.facebook.com/FY.wulong/',
        },
      ],
    },
    pics: ['34-1'],
  },
  {
    id: '35',
    date: '2023.10.19',
    title: '寶樹堂',
    category: ['3'],
    address: '彰化縣芳苑鄉福榮村',
    location: [23.88930866787804, 120.33161203736138],
    intro: `寶樹堂為謝姓的宗廟，而「謝」在芳苑鄉東南邊的幾個小村落三成、福榮、路上和路平四個村莊為大姓。`,
    history: {
      story: [],
      data: [
        {
          id: 1,
          image: '94201043_091009a_13~0259_rgb',
          time: '2009年',
          title: `祠堂附近植被植物較稀少`,
        },
        {
          id: 2,
          image: '',
          time: '2019年',
          title: `祠堂屋頂漆為綠色，庭院內植被植物較09年茂盛`,
        },
      ],
    },
    stakeholder: `台灣謝氏宗親總會、謝氏寶樹家族`,
    reference: {
      網站: [
        {
          id: 1,
          title: `謝氏寶樹家族`,
          link: 'http://www.xie-b-c.com/about2.asp?sn=154',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `國立中央圖書館台灣分館館刊－歌謠中的村莊歷史`,
          link: 'https://www.ntl.edu.tw/public/Attachment/992511111794.pdf',
        },
      ],
    },
    pics: ['35-1'],
  },
  {
    id: '36',
    date: '2023.10.19',
    title: '草湖國小',
    category: ['4'],
    address: '彰化縣芳苑鄉文津村芳草路66號',
    location: [23.956106877835936, 120.367444690519],
    intro: `現址前身為台中州北斗郡秋津尋常高等小學校，為日治時期自1898年起，以中央或地方經費所開設的兒童義務教育學校之一。而草湖國小之始為草湖地方人士於今草湖國中現址，創立草湖私古軒，教授漢文及日文。民國35年，接收原本日本人的秋津小學校，並遷校於現址。民國39年，改制為彰化縣芳苑鄉草湖國民學校。民國57年再改為彰化縣芳苑鄉草湖國民小學。`,
    history: {
      story: [
        {
          id: 1,
          time: '1979年',
          title: `興建水泥綜合球場1座 `,
        },
        {
          id: 2,
          time: '1988年',
          title: `改建司令台`,
        },
        {
          id: 3,
          time: '1995年',
          title: `整建中庭、興建北面側門、改建校門及東側圍牆`,
        },
        {
          id: 4,
          time: '1997年',
          title: `新建單身教師宿舍`,
        },
        {
          id: 5,
          time: '1998年',
          title: `改建運動場、新建教師車棚`,
        },
        {
          id: 6,
          time: '1999年',
          title: `新建家長接送區及綜合球場`,
        },
        {
          id: 7,
          time: '2003年',
          title: `老舊校舍防水防熱加蓋鋼構屋頂、老舊校舍油漆粉刷工程2011年 圍牆修繕工程`,
        },
        {
          id: 8,
          time: '2013年',
          title: `禮堂整修及屋頂防水防漏工程`,
        },
        {
          id: 9,
          time: '2014年',
          title: `斜屋頂破損修繕暨校舍建物補強工程`,
        },
        {
          id: 10,
          time: '2015年',
          title: `蘇迪勒風災校園修復工程`,
        },
        {
          id: 11,
          time: '2018年',
          title: `校園樹木修剪計畫`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201015_65P36a_23 ',
          time: '1976年',
          title: `草湖國小已在現址`,
        },
        {
          id: 2,
          image: '94201015_070622a_15~0373_rgb ',
          time: '2007年',
          title: `主要建築及操場已翻新、校園左上方已新增建物、球場已建成`,
        },
        {
          id: 3,
          image: '',
          time: '2013年',
          title: `校園左下方建物屋頂尚未翻新`,
        },
        {
          id: 4,
          image: ' 94201015_141012a_15~0432_hr4 ',
          time: '2014年',
          title: `校園左下方建物屋頂翻新完畢`,
        },
        {
          id: 5,
          image: ' 94201015_161102a_15~0652_hr4',
          time: '2016年',
          title: `校園建築物已裝設太陽能光電板`,
        },
      ],
    },
    stakeholder: `彰化縣政府教育處、芳苑鄉公所 、草湖國小教職員生`,
    reference: {
      網站: [
        {
          id: 1,
          title: `彰化縣芳苑鄉草湖國民小學`,
          link: 'https://www.thes.chc.edu.tw/',
        },
        {
          id: 2,
          title: `彰化縣芳苑鄉草湖國民小學校友聯誼會`,
          link: 'https://www.facebook.com/groups/122700567807852/',
        },
      ],
    },
    pics: ['36-1'],
  },
  {
    id: '37',
    date: '2023.10.19',
    title: '彰化縣警察局芳苑分局',
    category: ['4'],
    address: '彰化縣芳苑鄉三合村斗苑路三合段522號',
    location: [23.90679421532215, 120.3477203178226],
    intro: `隸屬彰化縣警察局，前身為二林分局，後以芳苑分局之名，在現址興建辦公廳舍，民國95年完工啟用，下轄芳苑、二林、大城、竹塘四個鄉鎮。`,
    history: {
      story: [
        {
          id: 1,
          time: '1952年',
          title: `二林分局成立`,
        },
        {
          id: 2,
          time: '1973年',
          title: `二林分局改建`,
        },
        {
          id: 3,
          time: '2005年',
          title: `芳苑分局於現址動土興建`,
        },
        {
          id: 4,
          time: '2006年',
          title: `芳苑分局完工啟用`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201034_65004_77 ',
          time: '1976年',
          title: `芳苑分局尚未建造`,
        },
        {
          id: 2,
          image: '94201034_070622a_14~0019_rgb ',
          time: '2007年',
          title: `芳苑分局已存在`,
        },
        {
          id: 3,
          image: '',
          time: '2017年',
          title: `周圍綠地大幅減少，警局前廣場停車格重新劃設`,
        },
        {
          id: 4,
          image: '',
          time: '2019年',
          title: `屋頂已裝設太陽能光電板`,
        },
      ],
    },
    stakeholder: `彰化縣政府、彰化縣警察局、芳苑鄉公所、芳苑分局員警、芳苑分局下轄派出所、當地居民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `彰化縣警察局全球資訊網`,
          link: 'https://www.chpb.gov.tw/fangyuan',
        },
      ],
    },
    pics: ['37-1'],
  },
  {
    id: '38',
    date: '2023.11.20',
    title: '草湖國中',
    category: ['4'],
    address: '彰化縣芳苑鄉草湖村功湖路草湖段140號',
    location: [23.95876858186888, 120.3781877654965],
    intro: `前身為二林初中草湖分部，曾經借用草湖國小教室上課，1964年正式獨立為草湖初級中學，1968年，改制為彰化縣立草湖國民中學。近年則推動地方海洋教育課程有成。`,
    history: {
      story: [
        {
          id: 1,
          time: '1964年',
          title: `獨立為草湖初級中學`,
        },
        {
          id: 2,
          time: '1965年',
          title: `完成辦公大樓`,
        },
        {
          id: 3,
          time: '1968年',
          title: `改制為彰化縣立草湖國民中學`,
        },
        {
          id: 4,
          time: '1969~1971年間',
          title: `新建教職員宿舍及擴充辦公室`,
        },
        {
          id: 5,
          time: '1971~1978年間',
          title: `增建圖書館`,
        },
        {
          id: 6,
          time: '1978年',
          title: `增購校地`,
        },
        {
          id: 7,
          time: '1983~1987年間',
          title: `增建司令臺及芳苑亭乙座，完成忠勇、孝親精神堡壘`,
        },
        {
          id: 8,
          time: '1987~1991年間',
          title: `完成青雲臺`,
        },
        {
          id: 9,
          time: '1995~1996年間',
          title: `增建教職員工宿舍大樓`,
        },
        {
          id: 10,
          time: '1996~1999年間',
          title: `興建學生活動中心聚賢館`,
        },
        {
          id: 11,
          time: '1999~2003年間',
          title: `興建校園風雨走廊、美化綠化校園`,
        },
        {
          id: 12,
          time: '2007~2014年間',
          title: `修繕PU球場`,
        },
        {
          id: 13,
          time: '2021年',
          title: `於全國海洋教案設計獲得特優`,
        },
        {
          id: 14,
          time: '2022年',
          title: `重新鋪設的蔚藍操場及籃球場啟用`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201015_65P36a_23 ',
          time: '1976年',
          title: `草湖國中已存在`,
        },
        {
          id: 2,
          image: ' 94201015_070622a_15~0373_rgb ',
          time: '2007年',
          title: `校園設施漸趨完善，建物增加`,
        },
        {
          id: 3,
          image: '94201015_121001a_15~0787_hr4',
          time: '2012年',
          title: `操場上方之建築看起來正在進行拆除整修`,
        },
        {
          id: 5,
          image: '4201015_141012a_15~0432_hr4',
          time: '2014年',
          title: `操場下方的建築更新`,
        },
        {
          id: 6,
          image: '',
          time: '2017年',
          title: `全校建築物屋頂已新增太陽能光電板`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、草湖國中教職員生`,
    reference: {
      網站: [
        {
          id: 1,
          title: `彰化縣草湖國中全球資訊網`,
          link: 'https://sites.google.com/a/thjh.chc.edu.tw/2016home/school/school-3',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `蔚藍運動跑道-彰化草湖國中60周年校慶大禮`,
          link: 'https://watchmedia01.com/anews-20220109034819.html',
        },
      ],
    },
    pics: ['38-1'],
  },
  {
    id: '39',
    date: '2023.10.19',
    title: '下庄龍天宮',
    category: ['3'],
    address: '彰化縣芳苑鄉興仁村建成路580巷6號',
    location: [23.952185458073473, 120.34048171778227],
    intro: `芳苑鄉興仁村的民間廟宇，主祀池府王爺。建築外觀為單進單出式，正殿有彩繪壁畫，廟右側有一土地公廟。`,
    history: {
      story: [
        {
          id: 1,
          time: '1978年',
          title: `建廟落成`,
        },
        {
          id: 2,
          time: '1987年',
          title: `整修`,
        },
        {
          id: 3,
          time: '2007年',
          title: `再度修繕`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201014_65009_019 ',
          time: '1976年',
          title: `龍天宮尚未建造`,
        },
        {
          id: 2,
          image: '94201014_070622a_14~0011_rgb',
          time: '2007年',
          title: `龍天宮已落成`,
        },
        {
          id: 3,
          image: '',
          time: '2008年',
          title: `圖上的寺廟狀況看起來與前一年有落差，可能如歷史資料所述正進行修繕`,
        },
        {
          id: 4,
          image: '',
          time: '2019年',
          title: `寺廟整體及外觀看起來已進行翻新`,
        },
        {
          id: 5,
          image: '',
          time: '2020年',
          title: `寺廟建物看起來有增加`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、龍天宮管理委員會、興仁社區發展協會、當地居民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `文化資源地理資訊系統`,
          link: 'http://crgis.rchss.sinica.edu.tw/temples/ChanghuaCounty/fangyuan/0723018-LTG',
        },
      ],
    },
    pics: ['39-1'],
  },
  {
    id: '40',
    date: '2023.11.20',
    title: '二林溪三合段',
    category: ['6'],
    address: '彰化縣芳苑鄉三合村',
    location: [23.921786, 120.356162],
    intro: `為二林溪位於芳苑三合村的河段，該處流經芳苑工業區東側。`,
    history: {
      story: [],
      data: [
        {
          id: 1,
          image: '94201034_65004_77 ',
          time: '1976年',
          title: `芳苑工業區尚未規劃`,
        },
        {
          id: 2,
          image: ' 94201034_070622a_14~0019_rgb ',
          time: '2007年',
          title: `河道左側的芳苑工業區已建成`,
        },
        {
          id: 3,
          image: '',
          time: '2015年',
          title: `從圖中可見上方河道兩側植被生長茂盛，壓縮河道`,
        },
      ],
    },
    stakeholder: `經濟部水利署河川局、彰化縣政府環保局、彰化縣政府農業處、芳苑鄉公所、芳苑工業區廠商、當地居民`,
    reference: {
      新聞: [
        {
          id: 1,
          title: `驚！二林溪堤岸道路縮水大半 竟因被這佔領…`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/2422913',
        },
        {
          id: 2,
          title: `綠鬣蜥入侵 彰化縣二林溪恐有潰堤危機`,
          link: 'https://www.chinatimes.com/realtimenews/20170905003888-260405',
        },
      ],
      檔案: [
        {
          id: 1,
          title: `108年度彰化縣政府-二林溪水域景觀環境營造計畫整體計畫書`,
          link: 'https://water.chcg.gov.tw/files/%E4%BA%8C%E6%9E%97%E6%BA%AA%E6%B0%B4%E5%9F%9F%E6%99%AF%E8%A7%80%E7%92%B0%E5%A2%83%E7%87%9F%E9%80%A0%E8%A8%88%E7%95%AB%E6%95%B4%E9%AB%94%E8%A8%88%E7%95%AB%E6%9B%B8_43_1080424.pdf',
        },
      ],
    },
    pics: ['40-1'],
  },
  {
    id: '41',
    date: '2023.10.19',
    title: '文津村302地號之受汙染農地',
    category: ['5'],
    address: '彰化縣芳苑鄉文津村',
    location: [23.943142, 120.370616],
    intro: `為芳苑文津村一處受鋅汙染的農地，由環保署公告之。`,
    history: {
      story: [],
      data: [
        {
          id: 1,
          image: '94201025_65P36a_028 ',
          time: '1976年',
          title: `此地段皆是一般農地 `,
        },
        {
          id: 2,
          image: '',
          time: '2019年',
          title: `此農地周圍已有排水溝渠，以及疑似工廠的建成`,
        },
        {
          id: 3,
          image: '',
          time: '2020年',
          title: `此農地由一般狀態突然變成荒蕪的樣貌`,
        },
      ],
    },
    stakeholder: `環保署、彰化縣政府、芳苑鄉公所、文津社區發展協會、周遭工廠、農地地主`,
    reference: {
      網站: [
        {
          id: 1,
          title: `農業及農地資源盤查結果查詢圖台`,
          link: 'https://map.coa.gov.tw/farmland/survey.html',
        },
      ],
      檔案: [
        {
          id: 1,
          title: `環保署-農地污染場址清冊`,
          link: 'https://sgw.epa.gov.tw/Upload/CMS/Misc/manual/%E5%A4%AA%E9%99%BD%E5%85%89%E9%9B%BB/File/%E9%99%84%E4%BB%B61_%E8%BE%B2%E5%9C%B0%E6%B1%A1%E6%9F%93%E5%A0%B4%E5%9D%80%E6%B8%85%E5%86%8A_1100813.pdf',
        },
      ],
    },
    pics: ['41-1'],
  },
  {
    id: '42',
    date: '2023.10.19',
    title: '愛園民宿',
    category: ['5', '2'],
    address: '彰化縣芳苑鄉草湖村二溪路草一段7-1號',
    location: [23.92555073667684, 120.37824737444747],
    intro: `由主人親自建造，離道路有一段距離，是獨立且寧靜的鄉村民宿，占地二千六百坪，整間民宿所使用的電，全部來自太陽能發電的環保綠能，綠蔭叢中可見融合日式與西式的建築，視野遼闊，在高處可觀賞四周美麗的鄉村美景。`,
    history: {
      story: [
        {
          id: 1,
          time: '2022年',
          title: `民宿已進行拆除(現地調查之狀況)`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201025_070622a_15~0377_rgb ',
          time: '2007年',
          title: `愛園民宿已存在`,
        },
        {
          id: 2,
          image: '94201025_091008a_15~1197_rgb',
          time: '2009年',
          title: `民宿外開闢連接主要道路的小徑`,
        },
        {
          id: 3,
          image: '94201025_111022a_15~0485_hr4',
          time: '2011年',
          title: `民宿加裝太陽能板`,
        },
      ],
    },
    stakeholder: `草湖社區發展協會、民宿主人、住宿客人`,
    reference: {
      網站: [
        {
          id: 1,
          title: `彰化縣政府全球資訊網-芳苑鄉公所`,
          link: 'https://town.chcg.gov.tw/fangyuan/06travel/travel01_con.aspx?new_id=839',
        },
      ],
    },
    pics: ['42-1'],
  },
  {
    id: '43',
    date: '2023.10.19',
    title: '漢寶G場',
    category: ['2', '5'],
    address: '彰化縣芳苑鄉漢寶村芳漢路二段129號',
    location: [24.011226287408856, 120.38083252332433],
    intro: `漢寶G場佇立在一片綠油油的稻田中，此餐廳前身為養雞場，因應雞價下跌，老闆娘林足蜜轉型拆掉部分養雞場改為餐廳，取諧音「G場」，從桶仔雞開始賣起，結合雞場自產的雞和蛋，料理出讓人百吃不厭的親切家常菜，並推廣著三低 ( 低油、低脂、低鹽 ) 一高 ( 高纖維 )的理念，以芳苑最優的在地新鮮食材，創造出餐廳 3G 的口碑。`,
    history: {
      story: [
        {
          id: 1,
          time: '2001年',
          title: `行政院農業委員會提出「輔導農家婦女開創副業計畫」。`,
        },
        {
          id: 2,
          time: '2022年',
          title: `漢寶G場非營業中`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94212095_65P057_038',
          time: '1976年',
          title: `此處尚未建設`,
        },
        {
          id: 2,
          image: '94212095_070622a_15~0365_rgb',
          time: '2007年',
          title: `發現此處有建物出現`,
        },
        {
          id: 3,
          image: '',
          time: '2013年',
          title: `前端新出現一白色屋頂的建物`,
        },
        {
          id: 4,
          image: '94212095_161102a_15~0644_hr4',
          time: '2016年',
          title: `下方又有一建物屋頂修建(變白色)`,
        },
        {
          id: 5,
          image: '94212095_181105z_15~0875_hr4',
          time: '2018年',
          title: `出現一個藍色的地物(有移動、變大)`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉畜牧業者、芳苑鄉雞蛋產銷班`,
    reference: {
      網站: [
        {
          id: 1,
          title: `芳苑鄉公所`,
          link: 'https://town.chcg.gov.tw/fangyuan/06travel/travel01_con.aspx?new_id=841',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `漁村奇雞 海味輕食 田媽媽－漢寶G場鄉村餐廳`,
          link: 'https://www.coa.gov.tw/ws.php?id=18975&print=Y',
        },
      ],
      遊記: [
        {
          id: 1,
          title: `漢寶G場 94i旅遊`,
          link: 'https://www.94i.club/post/%E6%BC%A2%E5%AF%B6G%E5%A0%B4',
        },
      ],
    },
    pics: ['43-1'],
  },
  {
    id: '44',
    date: '2023.10.19',
    title: '北天宮',
    category: ['3'],
    address: '彰化縣芳苑鄉建平村草漢路草二段228巷5號',
    location: [23.984792795083116, 120.38988006680104],
    intro: `北天宮係芳苑建平村「北山宮」之分靈，供奉萬府千歲，及各神佛神威廣被建平、新生二村於民國40餘年，於庄內十字路口建立北天宮。`,
    history: {
      story: [
        {
          id: 1,
          time: '1951年',
          title: `創立`,
        },
        {
          id: 2,
          time: '1994年',
          title: `遷建於現址`,
        },
        {
          id: 3,
          time: '1999年',
          title: `入火安座`,
        },
        {
          id: 4,
          time: '',
          title: `註：現今北天宮廟旁之建築空間已為該社區之關懷據點。`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201006_65P36a_173',
          time: '1976年',
          title: `建物尚未出現`,
        },
        {
          id: 2,
          image: '94201006_071024a_16_0630_rgb',
          time: '2007年',
          title: `建物出現`,
        },
        {
          id: 3,
          image: '94201006_161102a_16~0515_hr4',
          time: '2016年',
          title: `建物旁邊出現擴建`,
        },
      ],
    },
    stakeholder: `彰化縣政府、建平村「北天宮」、廟宇管理委員會`,
    reference: {
      網站: [
        {
          id: 1,
          title: `文化資源地理資訊系統`,
          link: 'http://crgis.rchss.sinica.edu.tw/temples/ChanghuaCounty/fangyuan/0723025-BTG',
        },
        {
          id: 2,
          title: `彰化芳苑北天宮`,
          link: 'https://blog.xuite.net/cc567051/twblog/143624451',
        },
      ],
    },
    pics: ['44-1'],
  },
  {
    id: '45',
    date: '2023.10.19',
    title: '芳苑教會',
    category: ['3'],
    address: '彰化縣芳苑鄉仁愛村斗苑路芳苑段245號',
    location: [23.924775697880172, 120.31858373981875],
    intro: `芳苑教會，是台灣基督長老教會，於1940年升格為堂會。除了宣揚教義外，也致力於對弱勢照顧，設立相關關懷據點，國家議題、主權和時事亦皆有涉略。`,
    history: {
      story: [
        {
          id: 1,
          time: '1895年',
          title: `在原斗設立丈八斗教會(二林原斗教會)`,
        },
        {
          id: 2,
          time: '1899年',
          title: `因信徒增加，建竹造番仔挖基督講義所`,
        },
        {
          id: 3,
          time: '1940年',
          title: `升格成堂會`,
        },
        {
          id: 4,
          time: '1941年',
          title: `許乃萱牧師擔任芳苑堂會第一任牧師`,
        },
        {
          id: 5,
          time: '1942年',
          title: `購地於現址，並建木造殿堂`,
        },
        {
          id: 6,
          time: '1946年',
          title: `因颱風侵襲而倒塌`,
        },
        {
          id: 7,
          time: '1947年',
          title: `改磚造建堂，更名為「芳苑教會」`,
        },
        {
          id: 8,
          time: '1962年',
          title: `改建為今日禮拜堂`,
        },
        {
          id: 9,
          time: '1984年',
          title: `慶祝設教85週年，重新裝潢教堂內部`,
        },
        {
          id: 10,
          time: '1999年',
          title: `慶祝設教百年，整頓教會廚房、廁所、車庫及植樹`,
        },
        {
          id: 11,
          time: '2009年',
          title: `教會再次進行外觀整修`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_65002_372',
          time: '1976年',
          title: `西側以草木為主，並未完全整治`,
        },
        {
          id: 2,
          image: '94201023_070622a_13~0348_rgb',
          time: '2007年',
          title: `中庭種植樹木，西側改建為屋舍`,
        },
        {
          id: 3,
          image: '94201023_091009a_13~0267_rgb',
          time: '2009年',
          title: `屋頂重漆為紅色`,
        },
        {
          id: 4,
          image: '',
          time: '2019年',
          title: `西南方建物被白色鐵皮覆蓋`,
        },
      ],
    },
    stakeholder: `信眾、台灣基督教長老教會、芳苑社區民眾、芳苑鄉公所、彰化縣長`,
    reference: {
      網站: [
        {
          id: 1,
          title: `台灣基督長老教會`,
          link: 'http://www.pct.org.tw/churchdata.aspx?strOrgNo=C06037',
        },
        {
          id: 2,
          title: `芥助網`,
          link: 'https://ms-community.azurewebsites.net/',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `臺灣省通志．卷二：人民志宗教篇（3）`,
        },
        {
          id: 2,
          title: `彰化縣志稿: 臺灣省-第2篇`,
        },
        {
          id: 3,
          title: `台灣古早教會巡禮`,
        },
      ],
      新聞: [
        {
          id: 1,
          title: `「盧佳惠牧師談芳苑教會的神蹟」之訪談錄`,
          link: 'https://memory.culture.tw/Home/Detail?Id=599528&IndexCode=Culture_Media&Keyword=%E8%8A%B3%E8%8B%91%E9%84%89&SearchMode=Precise&LanguageMode=Transfer&p=5',
        },
        {
          id: 2,
          title: `臺灣基督教長老教會芳苑教會`,
          link: 'https://memory.culture.tw/Home/Detail?Id=599345&IndexCode=Culture_Place&Keyword=%E8%8A%B3%E8%8B%91%E6%95%99%E6%9C%83&SearchMode=Precise&LanguageMode=Transfer',
        },
        {
          id: 3,
          title: `芳苑長老教會藝雕屏風`,
          link: 'https://memory.culture.tw/Home/Detail?Id=599301&IndexCode=Culture_Object&Keyword=%E8%8A%B3%E8%8B%91%E6%95%99%E6%9C%83&SearchMode=Precise&LanguageMode=Transfer',
        },
        {
          id: 4,
          title: `臺灣基督長老教會芳苑教會盧佳惠牧師`,
          link: 'https://memory.culture.tw/Home/Detail?Id=599389&IndexCode=Culture_People&Keyword=%E8%8A%B3%E8%8B%91%E6%95%99%E6%9C%83&SearchMode=Precise&LanguageMode=Transfer',
        },
        {
          id: 5,
          title: `「芳苑洪再添記憶中的聖詩與父親」之訪談錄`,
          link: 'https://memory.culture.tw/Home/Detail?Id=599527&IndexCode=Culture_Media&Keyword=%E8%8A%B3%E8%8B%91%E6%95%99%E6%9C%83&SearchMode=Precise&LanguageMode=Transfer',
        },
        {
          id: 6,
          title: `彰化芳苑長老教會關懷陪伴隱身漁村中的新住民家庭`,
          link: 'https://ct.org.tw/html/news/3-3.php?cat=10&article=1349023',
        },
        {
          id: 7,
          title: `疫情衝擊公益捐 富味鄉舉辦寒冬送暖前進彰化芳苑鄉`,
          link: 'https://money.udn.com/money/story/10871/6056227?from=edn_related_storybottom',
        },
        {
          id: 8,
          title: `芳苑教會社區照顧關懷據點成立C級巷弄長照站`,
          link: 'https://www.peopo.org/news/419789',
        },
        {
          id: 9,
          title: `落花生的力量 彰化芳苑路上長老教會 陪伴偏鄉孩子成長`,
          link: 'https://www.newsmarket.com.tw/blog/74869/',
        },
      ],
    },
    pics: ['45-1'],
  },
  {
    id: '46',
    date: '2023.10.19',
    title: '永興社區發展協會與番穟復育',
    category: ['4', '5'],
    address: '彰化縣芳苑鄉永興村芳漢路永興段230號',
    location: [23.939574560530126, 120.32655499536907],
    intro: `永興社區發展協會成立於民國99年10月10日，致力於促進居民和諧與激發愛鄉意識，並有個發表意見及對故鄉有所營造的空間。由於居民的付出與理事長的投入，分別於民國100年、101年和102年榮獲彰化縣「模範社區」、「最佳潛力社區」與「最佳創意社區」，並在民國102年成功復育三林穟等事蹟。`,
    history: {
      story: [
        {
          id: 1,
          time: '2010年',
          title: `成立社區發展協會`,
        },
        {
          id: 2,
          time: '2011年',
          title: `與大葉大學合作參與101年社區營造點徵選暨輔導計畫，編寫永興文化手冊`,
        },
        {
          id: 3,
          time: '2011年',
          title: `參與林務局社區植樹綠美化計畫，並獲得「模範社區」的殊榮`,
        },
        {
          id: 4,
          time: '2012年',
          title: `參加101年度台灣燈會(元宵節)花燈製作展覽，榮獲「佳作」`,
        },
        {
          id: 5,
          time: '2012年',
          title: `在彰化縣社區規畫師駐地輔導計畫中，入選「最佳潛力社區」`,
        },
        {
          id: 6,
          time: '2012年',
          title: `配合南投林區管理處參加「101年社區配合植樹美化活動」`,
        },
        {
          id: 7,
          time: '2013年',
          title: `於彰化縣社區規畫師駐地輔導計畫榮獲「最佳創意社區」`,
        },
        {
          id: 8,
          time: '2013年',
          title: `與水保局南投分局合作介紹、推銷在地農產`,
        },
        {
          id: 9,
          time: '2013年',
          title: `三林穟在永興社區復育`,
        },
        {
          id: 10,
          time: '2013年',
          title: `和大葉大學師生合作參辦文化局營造輔導計畫，製作三林港相關繪本`,
        },
        {
          id: 11,
          time: '2014年',
          title: `與村凡培根團隊陳鯤生老師參與水保局南投分局主持的「103年度農村在生陪更計畫 實作課程」，建立魚菜共生示範區`,
        },
        {
          id: 12,
          time: '2014年',
          title: `與朝陽科技大學合作，參與103年度社區規劃註地輔導計畫，修整、綠化周遭舊房、環境`,
        },
        {
          id: 13,
          time: '2014年',
          title: `完成文化部暨國立彰化生活美學館-永興社區的書目《三林港的百年風華》`,
        },
        {
          id: 14,
          time: '2014年',
          title: `協會建立照顧關懷據點`,
        },
        {
          id: 15,
          time: '2015年',
          title: `通過農村再生計畫核定，和參與104年度社區營造點徵選及輔導計畫(社區圓夢)，獲得15萬補助`,
        },
        {
          id: 16,
          time: '2016年',
          title: `參加水保局南投分局相關等執行計畫，並始播三林穟種子，種植面積達4分半地，及推廣相關料理`,
        },
        {
          id: 17,
          time: '2017年',
          title: `彰化縣政府推動社區營造三期與村落文化發展計畫、營造點徵選及輔導計畫`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_65002_372',
          time: '1976年',
          title: `現址仍是荒煙蔓草，沒有開發`,
        },
        {
          id: 2,
          image: '94201023_070622a_13~0348_rgb',
          time: '2007年',
          title: `可見現址的建物已建成`,
        },
        {
          id: 3,
          image: '',
          time: '2019年',
          title: `主建物右側草堆叢生，但有人為修整之貌`,
        },
        {
          id: 4,
          image: '',
          time: '2020年',
          title: `主建物前緣多了個鐵皮屋簷，右側草堆被去除`,
        },
      ],
    },
    stakeholder: `彰化縣政府、內政部營建署、林務局、水保局南投分局、林務局南投林區管理處、彰化縣芳苑鄉三林港社區合作社`,
    reference: {
      網站: [
        {
          id: 1,
          title: `永興社區發展協會網站`,
          link: 'http://sixstar.moc.gov.tw/blog/528003/myBlogArticleAction.do?method=doListArticleByPk&articleId=49184',
        },
        {
          id: 2,
          title: `芳苑鄉永興社區發展協會Facebook`,
          link: 'https://www.facebook.com/%E8%8A%B3%E8%8B%91%E9%84%89%E6%B0%B8%E8%88%88%E7%A4%BE%E5%8D%80%E7%99%BC%E5%B1%95%E5%8D%94%E6%9C%83-1426560240995753/',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `臺灣志略`,
        },
        {
          id: 2,
          title: `臺灣府志`,
        },
        {
          id: 3,
          title: `番仔秫`,
          link: 'http://140.112.114.11/lc/item.php?item=%E7%95%AA%E4%BB%94%E7%A7%AB&L2=%E7%A9%80%E4%B9%8B%E5%B1%AC',
        },
        {
          id: 4,
          title: `三林港的百年風華`,
        },
      ],
      新聞: [
        {
          id: 1,
          title: `穟消失半世紀 芳苑復育成功`,
          link: 'https://news.ltn.com.tw/news/local/paper/1076425',
        },
        {
          id: 2,
          title: `101年彰化縣社區規劃師駐地輔導計畫成果發表會`,
          link: 'http://changhua.tranews.com/Show/Style2/News/c1_News.asp?SItemId=0271030&ProgramNo=A000001000002&SubjectNo=3276124',
        },
      ],
      影音: [
        {
          id: 1,
          title: `復育萬穟 風華再現 永興社區三林穟復育紀實`,
          link: 'https://www.youtube.com/watch?v=RVQJagXDMh0',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `101年度「臺灣城鄉風貌整體規劃示範計畫」政策引導型申請補助作業須知`,
          link: 'https://www.cpami.gov.tw/filesys/file/chinese/dept/update/up10102.pdf',
        },
        {
          id: 2,
          title: `彰化縣芳苑鄉永興社區發展協會`,
          link: 'https://cloud.culture.tw/frontsite/inquiry/emapInquiryAction.do?method=showEmapDetail&indexId=34133',
        },
      ],
    },
    pics: ['46-1'],
  },
  {
    id: '47',
    date: '2023.10.19',
    title: '芳苑國小',
    category: ['4'],
    address: '彰化縣芳苑鄉芳苑村斗苑路芳苑段230號',
    location: [23.924914818183446, 120.31918978463777],
    intro: `芳苑國小創建於大正年間，初為二林公學校分校，於1919年獨立成校。光復後，正式更名為芳苑國民小學，並沿用至今。芳苑國小曾是芳苑地區大校，但隨著芳苑衰退，就學數僅剩下近百人。近年推動海牛文化復興，其中的活動與教案於2021年獲得教育卓越金質獎的肯定。`,
    history: {
      story: [
        {
          id: 1,
          time: '1914~1915年(大正4年)',
          title: `成立二林公學校分校，暫租民房和普天宮為校舍使用`,
        },
        {
          id: 2,
          time: '1919年',
          title: `建造木造教室，獨立成為「番挖公學校」`,
        },
        {
          id: 3,
          time: '1945年',
          title: `更名「芳苑國民小學」`,
        },
        {
          id: 4,
          time: '1950S',
          title: `芳苑國小就學數達到1600人`,
        },
        {
          id: 5,
          time: '1971年',
          title: `校友洪掛捐建圖書館`,
        },
        {
          id: 6,
          time: '2018年',
          title: `校內五、六年級生製作海牛繪本，推廣地方特色`,
        },
        {
          id: 7,
          time: '2019年',
          title: `舉辦105年校慶，邀請地方海牛業者辦理「芳苑海牛文化節」`,
        },
        {
          id: 8,
          time: '2021年',
          title: `以「芳蚵永續海牛展藝~最閃亮的珍珠」獲得教育部教育卓越獎金質殊榮`,
        },
        {
          id: 9,
          time: '2022年',
          title: `洪掛基金會與校友捐助資金，重整校內「洪掛圖書館」`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_65002_372',
          time: '1976年',
          title: `校舍簡約，仍可見較大範圍植樹`,
        },
        {
          id: 2,
          image: '94201023_091009a_13~0267_rgb',
          time: '2009年',
          title: `校園內許多新建物建成，特別是靠北邊一側`,
        },
        {
          id: 3,
          image: '94201023_141013a_13~0018_hr4',
          time: '2014年',
          title: `得見操場外圈磚紅色跑道整修完成`,
        },
        {
          id: 4,
          image: ' 94201023_161103a_13~0213_hr4',
          time: '2016年',
          title: `校舍頂多設有光電板，特別是近東北側建築`,
        },
        {
          id: 5,
          image: '94201023_211031a_13~0346_hr4',
          time: '2021年',
          title: `部分北側校舍頂樓重新上漆，籃球場也有所整修`,
        },
      ],
    },
    stakeholder: `國小師生、彰化縣政府、基金會與地方人士、海牛業者`,
    reference: {
      網站: [
        {
          id: 1,
          title: `彰化縣芳苑國民小學校網。`,
          link: 'https://www.fyes.chc.edu.tw/index.php#tag_news_aQmvh55',
        },
        {
          id: 2,
          title: `芳苑國小Facebook。`,
          link: 'https://www.facebook.com/fyes.official/',
        },
        {
          id: 3,
          title: `「芳苑國小吳偉誠校長說校史」之訪談錄`,
          link: 'https://crc.culture.tw/ChoshuiRiver/en-us/test7/599526',
        },
        {
          id: 4,
          title: `芳苑國小芳苑漁歌裝飾`,
          link: 'https://crc.culture.tw/ChoshuiRiver/en-us/test9/599424',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `彰化味 NO96夏季刊`,
        },
      ],
      新聞: [
        {
          id: 1,
          title: `阿公捐的圖書館要拆了 後代捐款整建「全鄉最美」`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/3906726',
        },
        {
          id: 2,
          title: `「海牛文化」打頭陣 偏鄉小校勇奪全國教學卓越金質獎`,
          link: 'https://udn.com/news/story/6898/5909224',
        },
        {
          id: 3,
          title: `文化部長真的回信 芳苑國小學童「有點嚇到」但受鼓勵`,
          link: 'https://udn.com/news/story/6898/5636432',
        },
        {
          id: 4,
          title: `芳苑國小學童種稻收成 請出1963年骨董風穀機`,
          link: 'https://www.chinatimes.com/realtimenews/20200714003730-260421?chdtv',
        },
        {
          id: 5,
          title: `國內首見「海牛」遊行 獻給芳苑國小105年校慶`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/2744214',
        },
        {
          id: 6,
          title: `彰南農村再生－文化扎根 芳苑國小「家鄉文化彩繪風箏DIY」活動`,
          link: 'https://tw.news.yahoo.com/%E5%BD%B0%E5%8D%97%E8%BE%B2%E6%9D%91%E5%86%8D%E7%94%9F-%E6%96%87%E5%8C%96%E6%89%8E%E6%A0%B9-%E8%8A%B3%E8%8B%91%E5%9C%8B%E5%B0%8F-%E5%AE%B6%E9%84%89%E6%96%87%E5%8C%96%E5%BD%A9%E7%B9%AA%E9%A2%A8%E7%AE%8Fdiy-%E6%B4%BB%E5%8B%95-083724174.html',
        },
        {
          id: 7,
          title: `認識家鄉特色文化 彰化芳苑國小畫海牛繪本`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/2392628',
        },
      ],
      影音: [
        {
          id: 1,
          title: `彰視新聞 芳苑海牛文化節`,
          link: 'https://www.youtube.com/watch?v=JOqMvbmlzM4&t=114s',
        },
        {
          id: 2,
          title: `芳苑國小海牛文化節 見證世界級文化`,
          link: 'https://www.youtube.com/watch?v=q1QDoChw5Rk&t=259s',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `芳苑國小95週年校慶暨新校舍落成剪綵典禮`,
          link: 'https://www.chcg.gov.tw/ch/newsdetail.asp?bull_id=51765',
        },
        {
          id: 2,
          title: `110年教學卓越獎-彰化縣芳苑鄉芳苑國民小學 方案全文`,
          link: 'https://cirn.moe.edu.tw/Upload/file/38483/105236.pdf',
        },
        {
          id: 3,
          title: `CINR 標竿典範`,
          link: 'https://cirn.moe.edu.tw/Benchmark/Results/detail.aspx?sid=23&mid=1263&tid=5150',
        },
        {
          id: 4,
          title: `彰化縣芳苑鄉「縣立芳苑國小」109學年學校概況`,
          link: 'https://stats.moe.gov.tw/edugis/SchoolInfoCJ/074758/C/%E5%BD%B0%E5%8C%96%E7%B8%A3/%E8%8A%B3%E8%8B%91%E9%84%89/%E7%B8%A3%E5%B8%82%E7%AB%8B/%E5%81%8F%E9%81%A0/%E7%84%A1',
        },
      ],
    },
    pics: ['47-1'],
  },
  {
    id: '48',
    date: '2023.10.19',
    title: '潮安宮',
    category: ['3'],
    address: '彰化縣芳苑鄉芳中村',
    location: [23.927783016931635, 120.31576624109469],
    intro: `潮安宮祭祀主神為五年千歲，是王爺類信仰。相傳是在地漁民於同治年間出海捕魚所遇的神祇。神尊因為靈驗且有求必應，故鄉里信眾漸多，並在同治八年建廟，成為當時地區重要信仰。`,
    history: {
      story: [
        {
          id: 1,
          time: '1868年',
          title: `漁民陳海却於海上迎五年千歲祭拜`,
        },
        {
          id: 2,
          time: '1869年(同治8年)',
          title: `廟宇竣工，香火鼎盛`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_070622a_13~0348_rgb',
          time: '2007年',
          title: `得見主建物頂呈黃漆`,
        },
        {
          id: 2,
          image: '94201023_121001a_13~0676_hr4',
          time: '2012年',
          title: `主建物南方附上紅色鐵皮，整體樓瓦有翻修的跡象`,
        },
      ],
    },
    stakeholder: `潮安宮管理委員會、周遭居民、周遭宮廟(ex:芳安宮、普天宮) `,
    reference: {
      書籍: [
        {
          id: 1,
          title: `臺灣舊地名之沿革 - 第2卷`,
        },
        {
          id: 2,
          title: `臺灣民間信仰論集`,
        },
      ],
      網站: [
        {
          id: 1,
          title: `潮安宮`,
          link: 'http://crgis.rchss.sinica.edu.tw/temples/ChanghuaCounty/fangyuan/0723001-CAG',
        },
      ],
    },
    pics: ['48-1'],
  },
  {
    id: '49',
    date: '2023.10.19',
    title: '二林溪之永興段',
    category: ['6', '1'],
    address: '彰化縣芳苑鄉永興村',
    location: [23.935900803274013, 120.31976013722516],
    intro: `二林溪永興段為出海口且近永興海埔地。由於周遭土地長期人為利用，因此除了農漁工業排水問題外，更有民用廢水汙染問題，，在2012年時曾出現魚群暴斃一事。`,
    history: {
      story: [
        {
          id: 1,
          time: '1983年',
          title: `第四河川局開始於二林溪出海口種植水筆仔、海茄冬`,
        },
        {
          id: 2,
          time: '2012年',
          title: `二林溪出海口曾有魚群暴斃，水產試驗所介入調查`,
        },
        {
          id: 3,
          time: '2013年',
          title: `進行「彰化海岸永續整體規劃-紅樹林生態資源調查暨分析委託研究計畫」`,
        },
        {
          id: 4,
          time: '2019年',
          title: `地方政府提出「全國水環境改善計畫」【二林溪水域環境營造計畫】整體計畫工作計畫書`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_65002_372',
          time: '1976年',
          title: `標地僅有些微泥沙沉積，整體河道蜿蜒`,
        },
        {
          id: 2,
          image: '94201023_070622a_13~0348_rgb',
          time: '2007年',
          title: `得見海埔新生地發育，舊西濱道路拓寬，於永興和芳苑村間的河段被截彎取直`,
        },
        {
          id: 3,
          image: '94201023_100921a_13~0448_hr4',
          time: '2010年',
          title: `得見二林溪出海口有大量紅樹林生長`,
        },
        {
          id: 4,
          image: '',
          time: '2017年',
          title: `得見二林溪出海口紅樹林更發茂密，近海西濱快速道路動工，河道上設立地基`,
        },
        {
          id: 5,
          image: '',
          time: '2020年',
          title: `得見西濱快速道路已通車，芳苑村海空步道建成`,
        },
      ],
    },
    stakeholder: `沿岸居民、水利署、彰化縣政府、芳苑鄉公所`,
    reference: {
      網站: [
        {
          id: 1,
          title: `二林溪`,
          link: 'https://crc.culture.tw/ChoshuiRiver/en-us/map/599370',
        },
        {
          id: 2,
          title: `過二林溪有感`,
          link: 'https://ipoem.nmtl.gov.tw/nmtlpoem?uid=12&pid=1137',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `「全國水環境改善計畫」【二林溪水域環境營造計畫】整體計畫工作計畫書`,
          link: 'https://water.chcg.gov.tw/files/1081113(%E5%88%9D%E7%A8%BF)%E4%BA%8C%E6%9E%97%E6%BA%AA%E6%B0%B4%E5%9F%9F%E7%92%B0%E5%A2%83%E7%87%9F%E9%80%A0%E8%A8%88%E7%95%AB%E5%B7%A5%E4%BD%9C%E8%A8%88%E7%95%AB%E6%9B%B8-1(%E5%85%A7%E6%96%87)_43_1081113.pdf',
        },
        {
          id: 2,
          title: `溶氧量不足？遭倒廢水？二林溪出海口，魚群暴斃`,
          link: 'https://www.tfrin.gov.tw/News_Content.aspx?n=4022&sms=9162&s=206427',
        },
        {
          id: 3,
          title: `102年彰化海岸永續整體規劃-芳苑濕地紅樹林生態營造計畫`,
          link: 'https://wetland-tw.tcd.gov.tw/upload/file/20190603145923102.pdf',
        },
        {
          id: 4,
          title: `彰化海岸永續整體規劃-紅樹林生態資源調查暨分析委託研究計畫`,
          link: 'https://wetland-tw.tcd.gov.tw/upload/file/20190531102039429.pdf',
        },
        {
          id: 5,
          title: `彰化海岸永續整體規劃-紅樹林生態資源調查暨分析委託研究計畫`,
          link: 'https://www.taiwanbuying.com.tw/ShowCCDetailOri.ASP?RecNo=2513682',
        },
      ],
    },
    pics: ['49-1'],
  },
  {
    id: '50',
    date: '2023.10.19',
    title: '佑隆畜牧場',
    category: ['5'],
    address: '彰化縣芳苑鄉路上村',
    location: [23.898605360933423, 120.33080271080294],
    intro: `在蛋雞養殖場還未建立時是農田，而根據國土測繪圖資服務雲此處在2016-2018年被劃設為旱作田。現為一處蛋雞養殖場。`,
    history: {
      story: [],
      data: [
        {
          id: 1,
          image: '',
          time: '2017年',
          title: `農田`,
        },
        {
          id: 2,
          image: '94201033_181104z_13~0428_hr4',
          time: '2018年',
          title: `廠房出現`,
        },
        {
          id: 3,
          image: '',
          time: '2019年',
          title: `廠房屋頂新增太陽能光電板`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉農會、蛋雞產銷班`,
    reference: {
      網站: [
        {
          id: 1,
          title: `行政院農委會畜牧場登記管理系統`,
          link: 'https://aris.coa.gov.tw/lFOnlineSearchRs-ranch',
        },
      ],
    },
    pics: ['50-1'],
  },
  {
    id: '51',
    date: '2023.10.19',
    title: '大炳畜牧場',
    category: ['5'],
    address: '彰化縣芳苑鄉新街村',
    location: [23.9007897443716, 120.31122202857347],
    intro: `一處蛋鴨養殖場。`,
    history: {
      story: [],
      data: [
        {
          id: 1,
          image: '',
          time: '2015年',
          title: `還未蓋鐵皮，且有水池`,
        },
        {
          id: 2,
          image: '94201033_161103a_13~0209_hr4',
          time: '2016年',
          title: `搭建鐵皮`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉農會、產銷班`,
    reference: {
      網站: [
        {
          id: 1,
          title: `行政院農委會畜牧場登記管理系統`,
          link: 'https://aris.coa.gov.tw/lFOnlineSearchRs-ranch',
        },
      ],
    },
    pics: ['51-1'],
  },
  {
    id: '52',
    date: '2023.10.19',
    title: '秉豐養豬場',
    category: ['5'],
    address: '芳苑鄉頂廍村斗苑路頂後巷505巷169號',
    location: [23.9058151689499, 120.33308958196812],
    intro: `一處位於芳苑鄉頂廍村的豬隻養殖場，也負責回收彰化地區的廚餘。`,
    history: {
      story: [],
      data: [
        {
          id: 1,
          image: '94201034_111022a_14~0616_hr4',
          time: '2011年',
          title: `廠區搭建更多鐵皮`,
        },
        {
          id: 2,
          image: '',
          time: '2013年',
          title: `廠區鐵皮縮減，搭建新設施`,
        },
      ],
    },
    stakeholder: `彰化縣農會、產銷班、彰化縣環保局、彰化縣養豬協會`,
    reference: {
      網站: [
        {
          id: 1,
          title: `行政院農委會畜牧場登記管理系統`,
          link: 'https://aris.coa.gov.tw/lFOnlineSearchRs-ranch',
        },
      ],
    },
    pics: ['52-1'],
  },
  {
    id: '53',
    date: '2023.10.19',
    title: '路上國小',
    category: ['4'],
    address: '彰化縣芳苑鄉路上村新上路12號',
    location: [23.89582262918825, 120.32820737860568],
    intro: `1922年成立，原有新街、頂廍兩座分校。兩分校皆在2003年廢校。現今的路上國小積極推動AI數位教育與雙語教育，期待能與國際接軌。`,
    history: {
      story: [
        {
          id: 1,
          time: '1922年',
          title: `成立`,
        },
        {
          id: 2,
          time: '1932年',
          title: `擴建校地`,
        },
        {
          id: 3,
          time: '1956年',
          title: `成立兩座分校`,
        },
        {
          id: 4,
          time: '2003年',
          title: `兩座分校廢校`,
        },
        {
          id: 5,
          time: '2007年',
          title: `斜屋頂工程完工`,
        },
        {
          id: 6,
          time: '2016年',
          title: `太陽光電發電設備設置計畫`,
        },
        {
          id: 7,
          time: '2018年',
          title: `設置太陽光電發電系統`,
        },
        {
          id: 8,
          time: '2022年',
          title: `學校積極推動雙語教育，在暑假也開設英文教學輔導課程`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201043_070622a_13~0340_rgb',
          time: '2007年',
          title: `斜屋頂工程尚未完工`,
        },
        {
          id: 2,
          image: '',
          time: '2008年',
          title: `可見斜屋頂工程已完工`,
        },
        {
          id: 3,
          image: '94201043_161103a_13~0205_hr4',
          time: '2016年',
          title: `屋頂上新增太陽能光電板`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、當地居民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `彰化縣芳苑鄉路上國小－學校歷史沿革大事記`,
          link: 'https://www.lses.chc.edu.tw/departments/2/show',
        },
        {
          id: 2,
          title: `專業、卓越、幸福路上－路上國小Facebook社團`,
          link: 'https://www.facebook.com/groups/321231088359328/',
        },
      ],
      新聞: [
        {
          id: 1,
          title: `迷你小學雙語教育讓人刮目相看 路上國小百年校慶端特色`,
          link: 'https://www.chinatimes.com/realtimenews/20220403002741-260405?fbclid=IwAR32viusY1if_qUoEXI8-xR-nlvXTqJAwRlMpbwwMDgQk-W6mg19ylu43BA&chdtv',
        },
      ],
    },
    pics: ['53-1'],
  },
  {
    id: '54',
    date: '2023.10.19',
    title: '天賜養雞場',
    category: ['5'],
    address: '彰化縣芳苑鄉三成村',
    location: [23.890023884042378, 120.3431186636387],
    intro: `一處蛋雞養殖場。`,
    history: {
      story: [],
      data: [
        {
          id: 1,
          image: '94201044_100921a_14~0930_hr4',
          time: '2010年',
          title: `廠區西邊的溫室塑膠屋頂有些許剝落`,
        },
        {
          id: 2,
          image: '94201044_121001a_14~0844_hr4',
          time: '2012年',
          title: `廠區西邊的溫室塑膠屋頂大面積毀損`,
        },
        {
          id: 3,
          image: '',
          time: '2013年',
          title: `廠區西邊原溫室變成農田`,
        },
        {
          id: 4,
          image: '94201044_181104z_14~0501_hr4',
          time: '2018年',
          title: `場區西側土地整地中`,
        },
        {
          id: 5,
          image: '',
          time: '2019年',
          title: `西側廠區建設完畢`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉農會、產銷班`,
    reference: {
      網站: [
        {
          id: 1,
          title: `行政院農業委員會－國產雞蛋溯源平台`,
          link: 'https://www.tafte-poultry.org.tw/TraceNo/5140068015',
        },
      ],
    },
    pics: ['54-1'],
  },
  {
    id: '55',
    date: '2023.10.19',
    title: '劉根畜牧場',
    category: ['5'],
    address: '彰化縣芳苑鄉三成村',
    location: [23.8814144116319, 120.33717399948],
    intro: `一處養豬場，有養殖種公母豬和肉豬。在2018年11月至2019年9月間在畜牧場區屋頂加蓋太陽能板。`,
    history: {
      story: [],
      data: [
        {
          id: 1,
          image: '94201044_181104z_14~0501_hr4',
          time: '2018年',
          title: `屋頂還未新增太陽能光電板`,
        },
        {
          id: 2,
          image: '',
          time: '2019年',
          title: `屋頂新增太陽能光電板`,
        },
      ],
    },
    stakeholder: `產銷班、彰化縣政府、芳苑鄉農會、彰化縣養豬協會、畜牧場負責人`,
    reference: {
      網站: [
        {
          id: 1,
          title: `行政院農委會畜牧場登記管理系統`,
          link: 'https://aris.coa.gov.tw/lFOnlineSearchRs-ranch',
        },
      ],
    },
    pics: ['55-1'],
  },
  {
    id: '56',
    date: '2023.10.19',
    title: '路上基督長老教會',
    category: ['3'],
    address: '彰化縣芳苑鄉福榮村上林路469號',
    location: [23.892127536063423, 120.33367339843973],
    intro: `建於1952年的教會，積極的與社區互動，設有弱勢家庭子女課後陪讀班和社區關懷據點。`,
    history: {
      story: [
        {
          id: 1,
          time: '1952年',
          title: `設立`,
        },
        {
          id: 2,
          time: '1967年',
          title: `重建`,
        },
        {
          id: 3,
          time: '2016年',
          title: `東側房屋興建完畢`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201044_091009a_14~0023_rgb',
          time: '2009年',
          title: `東側房屋為平房`,
        },
        {
          id: 2,
          image: '94201044_121001a_14~0844_hr4',
          time: '2012年',
          title: `東側被夷為平地`,
        },
        {
          id: 3,
          image: '',
          time: '2015年',
          title: `東側工程動工中`,
        },
        {
          id: 4,
          image: '94201044_161103a_14~0048_hr4',
          time: '2016年',
          title: `東側房屋興建完畢`,
        },
      ],
    },
    stakeholder: `芳苑鄉公所、台灣基督長老教會、信徒及牧師`,
    reference: {
      網站: [
        {
          id: 1,
          title: `台灣基督長老教會－路上教會`,
          link: 'http://www.pct.org.tw/churchdata.aspx?strOrgNo=C06038',
        },
      ],
    },
    pics: ['56-1'],
  },
  {
    id: '57',
    date: '2023.10.19',
    title: '陽光水棧',
    category: ['2', '5'],
    address: '彰化縣芳苑鄉王功村漁港六路38號',
    location: [23.969067590466576, 120.3280161623386],
    intro: `「陽光水棧」位於王功漁港旁，是一棟醒目的地中海式風格的藍白色建築，前身曾是養殖池，洪金釵(田媽媽)與先生一磚一瓦 打造出這間夢幻小屋，成為當地的知名餐廳。`,
    history: {
      story: [
        {
          id: 1,
          time: '2001年',
          title: `行政院農業委員會提出「輔導農家婦女開創副業計畫」。`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201013_65002_366 ',
          time: '1976年',
          title: `該地為養殖池`,
        },
        {
          id: 2,
          image: ' 94201013_211031a_13~0342_hr4',
          time: '2021年',
          title: `陽光水棧已成為知名特色餐廳地點`,
        },
      ],
    },
    stakeholder: `當地居民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `芳苑鄉公所`,
          link: 'https://town.chcg.gov.tw/fangyuan/06travel/travel01_con.aspx?new_id=838',
        },
        {
          id: 2,
          title: `農業易遊網`,
          link: 'https://ezgo.coa.gov.tw/zh-TW/Front/Tianma/Detail/929',
        },
      ],
      遊記: [
        {
          id: 1,
          title: `吃吃喝喝彰化王功陽光水棧+鹿港包子饅頭`,
          link: 'https://girlee.pixnet.net/blog/post/37602503',
        },
        {
          id: 2,
          title: `『彰化芳苑』閃爍西南角 土地生活節-地中海餐廳的美味海鮮大餐料理（田媽媽陽光水棧餐廳）`,
          link: 'https://www.walkerland.com.tw/article/view/53317',
        },
        {
          id: 3,
          title: `彰化芳苑美食｜陽光水棧海鮮餐廳，農委會輔導的田媽媽也有海鮮大餐唷！`,
          link: 'https://ha-blog.tw/23-1121-20211126/',
        },
      ],
    },
    pics: ['57-1'],
  },
  {
    id: '58',
    date: '2023.10.19',
    title: '牧羊女親子主題園區',
    category: ['2', '5'],
    address: '彰化縣芳苑鄉王功村漁港路780號',
    location: [23.96822684249027, 120.32870228684811],
    intro: `牧羊女主題園區的理念是將原有羊的養殖讓遊客可以產生互動並且學習到生態養殖的觀念，園區有努比亞、吐根寶、撒能、阿爾拜因等四種山羊生態，園內有生態導覽解說，還可以自己體驗餵羊、擠羊奶，並可嚐到新鮮的羊奶、羊奶冰棒及冰淇淋等羊乳製品。牧羊女主題園區將原本小小的養殖空間，化身成為了一個教育園地，讓大家可以更理解山羊養殖過程以及對生命的尊重。`,
    history: {
      story: [],
      data: [
        {
          id: 1,
          image: '94201013_65002_366',
          time: '1976年',
          title: `尚未有建物`,
        },
        {
          id: 2,
          image: '',
          time: '2013年',
          title: `開始建設`,
        },
        {
          id: 3,
          image: '',
          time: '2017年',
          title: `屋頂新增太陽能光電板`,
        },
      ],
    },
    stakeholder: `王功觀光產業發展協會、芳苑鄉相關觀光業者`,
    reference: {
      網站: [
        {
          id: 1,
          title: `彰化旅遊網資訊網`,
          link: 'https://tourism.chcg.gov.tw/AttractionsContent.aspx?id=62&chk=c885f95e-674c-4b9c-b22b-e8514e662684',
        },
      ],
      遊記: [
        {
          id: 1,
          title: `彰化芳苑 牧羊女親子主題園區`,
          link: 'https://s2678882.pixnet.net/blog/post/99347624-%E5%BD%B0%E5%8C%96%E8%8A%B3%E8%8B%91--%E7%89%A7%E7%BE%8A%E5%A5%B3%E8%A6%AA%E5%AD%90%E4%B8%BB%E9%A1%8C%E5%9C%92%E5%8D%80',
        },
        {
          id: 2,
          title: `【彰化親子景點】王功牧羊女～搭採蚵車，摸蛤、品蚵、餵小羊、啖羊奶冰超值體驗`,
          link: 'https://yoyoman822.pixnet.net/blog/post/61360159',
        },
      ],
    },
    pics: ['58-1'],
  },
  {
    id: '59',
    date: '2023.10.19',
    title: '台灣基督教恩惠福音會新寶教會',
    category: ['3'],
    address: '彰化縣芳苑鄉新寶村永豐巷13-3號',
    location: [23.976547187193887, 120.35053465171272],
    intro: `1970 年由蔡長橋牧師夫婦本著『神愛世人』的使命，在彰化縣芳苑鄉新寶村成立「台灣基督教恩惠福音會新寶教會」。1975 年新寶教會開始服務照顧失依、失孤、行為偏差、隔代教養等弱勢單親家庭的兒童及青少年。`,
    history: {
      story: [
        {
          id: 1,
          time: '1970年',
          title: `由蔡長橋牧師夫婦本著『神愛世人』的使命，在彰化縣芳苑鄉新寶村成立「台灣基督教恩惠福音會新寶教會」。`,
        },
        {
          id: 2,
          time: '1975年',
          title: `差派黃春益牧師夫婦回新寶教會服事，完成主耶穌基督傳福音拯救靈魂的使命，並在教會弟兄姐妹的支持中開始服務照顧失依、失孤、行為偏差、隔代教養等弱勢單親家庭的兒童及青少年。`,
        },
        {
          id: 3,
          time: '2004年',
          title: `成立「社團法人彰化縣基督教新寶社會關懷協會」，幫助青少年、兒童、婦幼、弱勢族群、老人及其他需要幫助的人，找到生命的目標，生活的幫助，和生存的能力為宗旨。`,
        },
        {
          id: 4,
          time: '2008年',
          title: `在各界善心人士的協助下成立『財團法人彰化縣私立基督教新寶教會社會福利慈善事業基金會』，並積極籌畫興建院舍事宜。`,
        },
        {
          id: 5,
          time: '2012年',
          title: `在各界愛心人士協助下完成附設「彰化縣私立恩惠兒童及少年之家」新建院舍及立案。`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201004_65P057_010',
          time: '1976年',
          title: `建物已出現`,
        },
        {
          id: 2,
          image: '94201004_070622a_14~0007_rgb',
          time: '2007年',
          title: `建物未擴建`,
        },
        {
          id: 3,
          image: '94201004_091009a_14~0007_rgb',
          time: '2009年',
          title: `教會主體向後擴建`,
        },
      ],
    },
    stakeholder: `財團法人彰化縣私立基督教新寶教會社會福利慈善事業基金會、社團法人彰化縣基督教新寶社會關懷協會、相關基督宗教組織、信仰民眾`,
    reference: {
      網站: [
        {
          id: 1,
          title: `芳苑鄉公所`,
          link: 'https://town.chcg.gov.tw/fangyuan/06travel/travel01_con.aspx?new_id=810',
        },
        {
          id: 2,
          title: `新寶恩惠中心`,
          link: 'https://www.facebook.com/enhueihome/',
        },
        {
          id: 3,
          title: `財團法人彰化縣私立基督教新寶教會社會福利慈善事業基金會`,
          link: 'http://enhuei.org.tw/about.php',
        },
      ],
    },
    pics: ['59-1'],
  },
  {
    id: '60',
    date: '2023.10.19',
    title: '台灣基督長老教會草湖教會',
    category: ['3'],
    address: '彰化縣芳苑鄉草湖村草漢路草一段182號  ',
    location: [23.961294, 120.382395],
    intro: `隸屬台灣基督長老教會，源於十九世紀馬偕及馬雅各在台灣的宣教工作。草湖教會則是王功教會的分設教會，為草湖村居民的信仰中心。`,
    history: {
      story: [
        {
          id: 1,
          time: '1957年',
          title: `分設草湖教會，陳東桂兄弟捐地，讓當地基督教信徒擁有聚會地點`,
        },
        {
          id: 2,
          time: '1973年',
          title: `發起建堂運動，同年教堂正式完工`,
        },
        {
          id: 3,
          time: '2008年',
          title: `與彰化基督教青年會一同辦理弱勢家庭兒童課輔班，協助社區內有困難的家庭，提供一個讓孩子下課後完成功課且安全的場所`,
        },
        {
          id: 4,
          time: '2022年',
          title: `草湖教會看起來有持續和中華民國快樂學習協會合作幫助孩童`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201015_65P36a_023',
          time: '1976年',
          title: `草湖教會已存在`,
        },
        {
          id: 2,
          image: '94201015_070622a_15~0373_rgb',
          time: '2007年',
          title: `教會旁已新增了兩幢小建築物`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、台灣基督長老教會、當地牧師及信徒`,
    reference: {
      網站: [
        {
          id: 1,
          title: `台灣基督長老教會`,
          link: 'http://www.pct.org.tw/ChurchHistory.aspx?strOrgNo=C06029',
        },
        {
          id: 2,
          title: `中華民國快樂學習協會`,
          link: 'https://afterschool368.org/secretbc.php?m=4&s=67',
        },
      ],
    },
    pics: ['60-1'],
  },
  {
    id: '61',
    date: '2022.07.10',
    title: '理想大苑 ',
    category: ['5', '2'],
    address: '彰化縣芳苑鄉三合村林功路201號  ',
    location: [23.914034914470577, 120.3610399208401],
    intro: `是芳苑一棟新穎的包棟民宿，原為民宿老闆的老家，後來其退休返鄉，整理老家成為民宿。 `,
    history: {
      story: [],
      data: [
        {
          id: 1,
          image: '94201035_65P36a_034  ',
          time: '1976年',
          title: `民宅尚未建成`,
        },
        {
          id: 2,
          image: '94201035_070622a_15~0381_rgb ',
          time: '2007年',
          title: `民宅已建成，為民宿主人原本的老家`,
        },
        {
          id: 3,
          image: '',
          time: '2019年',
          title: `民宅尚未翻新`,
        },
        {
          id: 4,
          image: '',
          time: '2020年',
          title: `原本的民宅經過翻新，成為民宿`,
        },
      ],
    },
    stakeholder: `三合社區發展協會、理想大苑民宿主人`,
    reference: {
      網站: [
        {
          id: 1,
          title: `理想大苑-中部彰化包棟民宿`,
          link: 'https://m.facebook.com/pages/category/Sports---recreation/%E7%90%86%E6%83%B3%E5%A4%A7%E8%8B%91-%E4%B8%AD%E9%83%A8%E5%BD%B0%E5%8C%96%E5%8C%85%E6%A3%9F%E6%B0%91%E5%AE%BF-102616038254896/',
        },
      ],
    },
    pics: ['61-1'],
  },
  {
    id: '62',
    date: '2023.10.19',
    title: '芳苑鄉第一公墓及納骨堂',
    category: ['1'],
    address: '彰化縣芳苑鄉草湖村二溪路草一段369號  ',
    location: [23.93575409688202, 120.3819154739902],
    intro: `位於草湖村與二林鎮交界之處，從早年即是當地的公墓區域，直到1998年公告禁葬，後變成雜草叢生的區域。近年為解決鄉內塔位不足的情況，規劃在原本第一公墓的區域新建納骨堂，並實施遷葬。`,
    history: {
      story: [
        {
          id: 1,
          time: '1998年',
          title: `芳苑第一公墓宣布禁葬`,
        },
        {
          id: 2,
          time: '2016~2017年',
          title: `第一公墓實施遷葬工程`,
        },
        {
          id: 3,
          time: '2020年',
          title: `第一公墓納骨堂寶慧堂完工啟用`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201025_65P36a_028',
          time: '1976年',
          title: `第一公墓區已存在`,
        },
        {
          id: 2,
          image: ' 94201025_100922a_15~0031_hr4 ',
          time: '2010年',
          title: `公墓上方水池尚未被填平`,
        },
        {
          id: 3,
          image: '94201025_111022a_15~0485_hr4',
          time: '2011年',
          title: `公墓上方水池已被填平`,
        },
        {
          id: 4,
          image: '',
          time: '2017年',
          title: `納骨堂基地產生，原本公墓區逐漸遷移`,
        },
        {
          id: 5,
          image: '94201025_181105z_15~0860_hr4',
          time: '2018年',
          title: `納骨堂進入施工狀態，原本公墓區完全消失`,
        },
        {
          id: 6,
          image: '',
          time: '2020年',
          title: `納骨堂建築明顯為完工狀態`,
        },
      ],
    },
    stakeholder: `彰化縣政府、芳苑鄉公所、草湖社區發展協會、殯葬業者、原公墓往生者之親人、當地居民`,
    reference: {
      檔案: [
        {
          id: 1,
          title: `芳苑鄉第一公墓納骨塔收費減免標準`,
          link: 'https://town.chcg.gov.tw/files/%E9%99%84%E4%BB%B6%EF%BC%92%EF%BC%9A%E8%8A%B3%E8%8B%91%E9%84%89%E7%AC%AC%E4%B8%80%E5%85%AC%E5%A2%93%E7%B4%8D%E9%AA%A8%E5%A1%94%E6%94%B6%E8%B2%BB%E6%B8%9B%E5%85%8D%E6%A8%99%E6%BA%96%E5%8F%8A%E8%BE%A6%E7%90%86%E5%A1%94%E4%BD%8D%E7%99%BB%E8%A8%98%E6%87%89%E5%82%99%E6%96%87%E4%BB%B6_54_1090528.pdf',
        },
        {
          id: 2,
          title: `彰化縣芳苑鄉第一公墓遷葬優惠辦法`,
          link: 'https://town.chcg.gov.tw/files/54_1050104_104.12.31%E9%81%B7%E8%91%AC%E5%84%AA%E6%83%A0%E8%BE%A6%E6%B3%95_54_1060728.odt',
        },
        {
          id: 3,
          title: `彰化縣芳苑鄉殯葬設施管理自治條例部分條文及第 十三條附表修正草案總說明`,
          link: 'https://tbstmp.cyhg.gov.tw/02/P02S02-do.aspx?id=378&code=3',
        },
        {
          id: 4,
          title: `芳苑鄉第一公墓納骨堂啟用公告`,
          link: 'https://w3fs.tainan.gov.tw/Download.ashx?u=LzAwMS9VcGxvYWQvNDMvcmVsZmlsZS8xMDcxNi83Njc3MTQzLzRhNDg1NjllLTcwNmMtNGJlOS1iYjcwLTAxMzdjMzE5NWUwYS5wZGY%3d&n=5b2w5YyW57ij6Iqz6IuR6YSJ44CM56ys5LiA5YWs5aKT57SN6aqo5aCCKOWvtuaFp%2bWgginjgI3llZ%2fnlKjlhazlkYoucGRm',
        },
      ],
    },
    pics: ['62-1'],
  },
  {
    id: '63',
    date: '2023.11.20',
    title: '大將爺廟',
    category: ['3'],
    address: '彰化縣芳苑鄉信義村',
    location: [23.921799277103197, 120.31457201103971],
    intro: `大將爺廟主祀為明末名將劉綎，由於位於公墓附近，故被居民視為鎮壓、管轄芳苑鬼魂的主神。其廟前有香爐一座，廟門如墓門，呼應著大將爺是管理鬼的神祇。廟柱兩側有放置桌椅，可供居民休閒、祭祀，屬於地方性質廟宇。`,
    history: {
      story: [
        {
          id: 1,
          time: '1783年(乾隆8年)',
          title: `得匾額「萬古英靈」`,
        },
        {
          id: 2,
          time: '1925年',
          title: `日治重建`,
        },
        {
          id: 3,
          time: '1980年',
          title: `重建並刻著「英靈宮」，定為早期宮名`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201033_65002_378',
          time: '1976年',
          title: `僅可見部分開發痕跡，廟宇規模不大`,
        },
        {
          id: 2,
          image: '94201033_070622a_13~0344_rgb',
          time: '2007年',
          title: `得見廟宇建設面積拓增與周遭開發`,
        },
        {
          id: 3,
          image: '',
          time: '2015年',
          title: `得見入口門面修整，西北方新設金爐，東方屋頂鐵皮重新鋪設`,
        },
      ],
    },
    stakeholder: `信眾、附近居民`,
    reference: {
      書籍: [
        {
          id: 1,
          title: `台灣風物-卷39`,
        },
        {
          id: 2,
          title: `臺灣省通志．卷二(人民志宗教篇)`,
        },
      ],
      網站: [
        {
          id: 1,
          title: `大將爺廟`,
          link: 'https://crc.culture.tw/ChoshuiRiver/en-us/map/599338',
        },
      ],
    },
    pics: ['63-1'],
  },
  {
    id: '64',
    date: '2023.10.19',
    title: '海牛驛站',
    category: ['2', '5'],
    address: '彰化縣芳苑鄉芳中村芳漢路芳二段161巷100號',
    location: [23.928540505793183, 120.31678925105251],
    intro: `海牛驛站，是芳苑當地蚵農因應產業轉型，以提供導覽潮間帶生態、教育民眾海洋知識與介紹海牛文化，所衍生出的觀光產業商家。`,
    history: {
      story: [
        {
          id: 1,
          time: '日治時期',
          title: `可見芳苑潮間帶牛車運蚵的影像紀錄`,
        },
        {
          id: 2,
          time: '1965~1960S',
          title: `發展和盛行出訓練黃牛拉車採蚵的漁法(海牛)`,
        },
        {
          id: 3,
          time: '1980S',
          title: `鐵牛出現，開始與海牛競爭`,
        },
        {
          id: 4,
          time: '1990S',
          title: `鐵牛大量取代海牛，海牛漸稀`,
        },
        {
          id: 5,
          time: '2016年',
          title: `「芳苑潮間帶牛車採蚵文化」登入我國無形文化資產`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201023_111022a_13~0665_hr4',
          time: '2011年',
          title: `主樓頂改造，樓旁(今海牛驛站茶亭位置)已經建設`,
        },
        {
          id: 2,
          image: '',
          time: '2013年',
          title: `主樓頂改漆綠色`,
        },
        {
          id: 3,
          image: '94201023_141013a_13~0018_hr4',
          time: '2014年',
          title: `海牛驛站白色屋簷設置完成`,
        },
        {
          id: 4,
          image: '94201023_211031a_13~0346_hr4',
          time: '2021年',
          title: `主樓頂改呈深色底，白色塊體消失，屋簷也呈深色狀`,
        },
      ],
    },
    stakeholder: `芳苑周遭商家、彰化縣政府`,
    reference: {
      網站: [
        {
          id: 1,
          title: `海牛驛站 Manateelnn Facebook`,
          link: 'https://www.facebook.com/ManateeInn/',
        },
      ],
      書籍: [
        {
          id: 1,
          title: `我家住海邊：尋找台灣即將消失的漁法`,
        },
        {
          id: 2,
          title: `芳苑海牛`,
        },
        {
          id: 3,
          title: `旅讀No116｜尋味亞洲米其林`,
        },
      ],
      新聞: [
        {
          id: 1,
          title: `海牛採蚵生態旅遊`,
          link: 'https://tourism.chcg.gov.tw/AttractionsContent.aspx?id=438&chk=328889f5-db47-495d-b93f-74dfb263a6c3',
        },
        {
          id: 2,
          title: `乘海牛採蚵趣 外國人也愛玩`,
          link: 'https://news.ltn.com.tw/news/local/paper/897101',
        },
        {
          id: 3,
          title: `芳苑蚵農轉型成功為海牛驛站`,
          link: 'https://www.peopo.org/news/305121',
        },
        {
          id: 4,
          title: `《我家住海邊》即將失傳的台灣漁法──彰化芳苑海牛採蚵`,
          link: 'https://udn.com/news/story/12674/6085699',
        },
        {
          id: 5,
          title: `【蘋果人物】「海牛犂蚵田」獨步全球　紅到BBC還創學校`,
          link: 'https://www.appledaily.com.tw/micromovie/20200501/OTWJXWCCMLAW2WUGFTQ2K6IZHM',
        },
        {
          id: 6,
          title: `只剩8頭！ 彰化舉行海牛文化節保護無形文化`,
          link: 'https://news.ltn.com.tw/news/life/breakingnews/3345900',
        },
        {
          id: 7,
          title: `海牛數量劇降 芳苑無形文化資產待援`,
          link: 'https://www.ntdtv.com.tw/b5/20191014/video/255858.html?%E6%B5%B7%E7%89%9B%E6%95%B8%E9%87%8F%E5%8A%87%E9%99%8D%20%E8%8A%B3%E8%8B%91%E7%84%A1%E5%BD%A2%E6%96%87%E5%8C%96%E8%B3%87%E7%94%A2%E5%BE%85%E6%8F%B4',
        },
      ],
      政府資訊: [
        {
          id: 1,
          title: `府授文演字第1050441848A號`,
          link: 'https://data.boch.gov.tw/upload/documents/2021-12-16/af4cf29f-8b3d-49d9-9b88-1b87f786df7a/%E5%BA%9C%E6%8E%88%E6%96%87%E6%BC%94%E5%AD%97%E7%AC%AC1050441848A%E8%99%9F%2001.pdf',
        },
        {
          id: 2,
          title: `府授文戲字第1090156807A號`,
          link: 'https://data.boch.gov.tw/upload/documents/2021-12-16/beb66a8a-0622-446b-be1a-b1e14ce5527d/%E5%BA%9C%E6%8E%88%E6%96%87%E6%88%B2%E5%AD%97%E7%AC%AC1090156807A%E8%99%9F01.pdf',
        },
        {
          id: 3,
          title: `芳苑潮間帶牛車採蚵`,
          link: 'https://nchdb.boch.gov.tw/embed/assets/overview/tkp/20200723000001',
        },
        {
          id: 4,
          title: `TO”GO旅遊雜誌初體驗～彰化芳苑海牛遊程拍攝實況`,
          link: 'https://nantou.swcb.gov.tw/News/achievements_more?id=8085E82D21594498A2953EDCE1DBF9FC',
        },
      ],
    },
    pics: ['64-1'],
  },
  {
    id: '65',
    date: '2023.10.19',
    title: '龍鳳宮',
    category: ['3'],
    address: '彰化縣芳苑鄉後寮村寮北巷15-1號',
    location: [23.92236657843694, 120.34395783186315],
    intro: `龍鳳宮為原先福壽宮改址易名而來，是道教與諸民間信仰所構成的寺廟。當中主神為廣澤尊王、玄天上帝和北極之尊等，並有諸神的祭祀、酬神的典禮。`,
    history: {
      story: [
        {
          id: 1,
          time: '1952年',
          title: `神壇設立`,
        },
        {
          id: 2,
          time: '1953年',
          title: `取乩救世`,
        },
        {
          id: 3,
          time: '1960年',
          title: `舊宮(比較龍鳳宮)落成，定名為「福壽宮」`,
        },
        {
          id: 4,
          time: '1971年',
          title: `福壽宮請旨年限已過，加上後寮人丁漸旺，配合工業區發展，易址另名為「龍鳳宮」`,
        },
        {
          id: 5,
          time: '1989年',
          title: `龍鳳宮建成，進行入宮大典`,
        },
        {
          id: 6,
          time: '2017年',
          title: `新建的牌樓、廁所建成並進行開光儀式`,
        },
      ],
      data: [
        {
          id: 1,
          image: '94201034_65004_077',
          time: '1976年',
          title: `現址仍為田地`,
        },
        {
          id: 2,
          image: '94201034_070622a_14~0019_rgb',
          time: '2007年',
          title: `得見今之宮廟建成`,
        },
        {
          id: 3,
          image: '',
          time: '2015年',
          title: `得見龍鳳宮牌樓動工`,
        },
        {
          id: 4,
          image: '',
          time: '2017年',
          title: `得見牌樓建成，西南角灰黑色塊體拆除，改為植樹`,
        },
      ],
    },
    stakeholder: `龍鳳宮管理委員會、在地居民`,
    reference: {
      網站: [
        {
          id: 1,
          title: `龍鳳宮Facebook`,
          link: 'https://www.facebook.com/%E8%8A%B3%E8%8B%91%E9%84%89-%E5%BE%8C%E5%AF%AE%E6%9D%91-%E9%BE%8D%E9%B3%B3%E5%AE%AE-201299787375342/',
        },
        {
          id: 2,
          title: `龍鳳宮`,
          link: 'http://crgis.rchss.sinica.edu.tw/temples/ChanghuaCounty/fangyuan/0723005-LFG',
        },
      ],
    },
    pics: ['65-1'],
  },
];
