export const newsList = [
  {
    id: 1,
    title: '爆紅的滋味，海空步道與芳苑濕地',
    news: {
      reference: '我們的島',
      date: '2021-10-11',
      reporter: '陳佳利',
      photographer: '陳添寶',
    },
    link: 'https://ourisland.pts.org.tw/content/8361',
    // tags: [1, 2, 3],
    views: 0,
  },
];

export const literatureList = [
  {
    id: 1,
    title: '浮水蓮花：王功人文產業變遷紀實',
    literature: {
      subtitle: '',
      author: '李修瑋',
      publisher: '彰化縣立文化中心',
      place: '',
      date: '1996-06',
      refId: '100788386',
    },
    link: 'https://tm.ncl.edu.tw/article?u=022_002_00001644&lang=chn',
    tags: [],
    views: 0,
  },
  {
    id: 2,
    title: '芳苑鄉志政事篇',
    literature: {
      subtitle: '館藏光復後台灣地區官修地方志',
      author: '',
      publisher: '芳苑鄉公所',
      place: '彰化縣芳苑鄉',
      date: '1997',
      refId: '',
    },
    link: 'https://tm.ncl.edu.tw/article?u=006_001_0000304501&lang=chn',
    tags: [],
    views: 0,
  },
  {
    id: 3,
    title: '芳苑鄉志地理篇',
    literature: {
      subtitle: '館藏光復後台灣地區官修地方志',
      author: '',
      publisher: '芳苑鄉公所',
      place: '彰化縣芳苑鄉',
      date: '1997',
      refId: '',
    },
    link: 'https://tm.ncl.edu.tw/article?u=006_001_0000304518&lang=chn',
    tags: [],
    views: 0,
  },
  {
    id: 4,
    title: '甦醒中的王功：大家來寫村史',
    literature: {
      subtitle: '館藏光復後台灣地區官修地方志',
      author: '魏金絨',
      publisher: '彰化縣文化局',
      place: '',
      date: '200508',
      refId: '101697740',
    },
    link: 'https://tm.ncl.edu.tw/article?u=022_004_00003551&lang=chn',
    tags: [],
    views: 0,
  },
];

export const stakeholderList = [
  {
    stakeholder: 1,
    categoryId: '0',
    groupId: null,
    optionId: 'G',
    title: '治理機關',
  },
  {
    stakeholder: 2,
    categoryId: '0',
    groupId: null,
    optionId: 'C',
    title: '社會團體',
  },
  {
    stakeholder: 3,
    categoryId: '0',
    groupId: null,
    optionId: 'I',
    title: '職業團體',
  },
  {
    stakeholder: 4,
    categoryId: '0',
    groupId: null,
    optionId: 'P',
    title: '生產者',
  },
  {
    stakeholder: 5,
    categoryId: '0',
    groupId: null,
    optionId: 'D',
    title: '開發商',
  },
];

export const questionerList = [
  {
    id: 1,
    title: '治理機關',
  },
  {
    id: 2,
    title: '研究團隊',
  },
  {
    id: 3,
    title: '生產者',
  },
];

export const issueList = [
  {
    id: 1,
    title: '近年來蚵農陳訴蚵減產，這個產業問題與環境變遷之間的關係是什麼？',
    questionerId: 2,
    type: 'single',
    tags: [
      {
        id: 1,
        title: '其他關鍵字',
        list: ['夕陽'],
      },
    ],
    ref: {
      title: '空氣品質與氣象',
      link: '/eco-environmental-survey/air-quality',
    },
    href: 'https://storymaps.arcgis.com/stories/6dcbe955ed08419b9ee39fac57592605',
  },
  {
    id: 2,
    title: `近年家發展推動「綠電」設施，對於芳苑沿海及外海的農漁業發展是否有所影響？`,
    questionerId: 2,
    type: 'multiple',
    tags: [
      {
        id: 1,
        title: '產業',
        list: ['1.1農業', '1.2漁業/養殖業', '1.4綠能'],
      },
      {
        id: 2,
        title: '景觀生態',
        list: ['4.2.3養殖地景變遷'],
      },
      {
        id: 3,
        title: '治理與建設',
        list: ['5.1.1光電', '5.1.2風電'],
      },
    ],
    ref: {
      title: '水質觀測',
      link: '/eco-environmental-survey/water-quality',
    },
  },
  {
    id: 3,
    title: `芳苑紅樹林的擴張與移除是否會影響泥灘地螃蟹的種類組成與數量孌化？`,
    questionerId: 2,
    type: 'multiple',
    tags: [
      {
        id: 1,
        title: '景觀生態',
        list: ['4.1.1底棲生物', '4.1.2紅樹林'],
      },
    ],
    ref: {
      title: '底棲生物',
      link: '/eco-environmental-survey/macrobenthos',
    },
  },
];

export const categoryList = [
  {
    id: '1',
    title: '產業',
  },
  {
    id: '2',
    title: '人口',
  },
  {
    id: '3',
    title: '地方',
  },
  {
    id: '4',
    title: '景觀生態',
  },
  {
    id: '5',
    title: '治理與建設',
  },
  {
    id: '6',
    title: '土地利用',
  },
  {
    id: '7',
    title: '永續實踐',
  },
  // {
  //   id: '8',
  //   title: '重要關鍵詞',
  // },
];

export const tag2List = {
  records: [
    {
      tag2: 1,
      categoryId: '1',
      optionId: '1',
      title: '農業',
    },
    {
      tag2: 2,
      categoryId: '1',
      optionId: '2',
      title: '漁業/養殖業',
    },
    {
      tag2: 4,
      categoryId: '1',
      optionId: '4',
      title: '綠能',
    },
    {
      tag2: 11,
      categoryId: '4',
      groupId: '1',
      title: '潮間帶',
    },
    {
      tag2: 12,
      categoryId: '4',
      groupId: '2',
      title: '產業地景',
    },
    {
      tag2: 14,
      categoryId: '5',
      groupId: '1',
      title: '國家級開發案',
    },
  ],
};

export const tag3List = {
  records: [
    {
      tag3: 25,
      categoryId: '8',
      groupId: null,
      optionId: '3',
      title: '夕陽',
    },
    {
      tag3: 13,
      categoryId: '4',
      groupId: null,
      optionId: '1',
      title: '養殖地景變遷',
    },
    {
      tag3: 20,
      categoryId: '5',
      groupId: '1',
      optionId: '1',
      title: '光電',
    },
    {
      tag3: 21,
      categoryId: '5',
      groupId: '1',
      optionId: '2',
      title: '風電',
    },
    {
      tag3: 11,
      categoryId: '4',
      groupId: '1',
      optionId: '1',
      title: '底棲生物',
    },
    {
      tag3: 12,
      categoryId: '4',
      groupId: '1',
      optionId: '2',
      title: '紅樹林',
    },
  ],
};

export const optionList = [
  {
    categoryId: '1',
    title: '產業',
    list: [
      {
        tag2: 1,
        categoryId: '1',
        optionId: '1',
        title: '農業',
      },
      {
        tag2: 2,
        categoryId: '1',
        optionId: '2',
        title: '漁業/養殖業',
      },
      {
        tag2: 3,
        categoryId: '1',
        optionId: '3',
        title: '畜牧業',
      },
      {
        tag2: 4,
        categoryId: '1',
        optionId: '4',
        title: '綠能',
      },
      {
        tag2: 5,
        categoryId: '1',
        groupId: '5',
        title: '觀光',
        list: [
          {
            tag3: 1,
            categoryId: '1',
            groupId: '5',
            optionId: '1',
            title: '地方節慶',
          },
          {
            tag3: 2,
            categoryId: '1',
            groupId: '5',
            optionId: '2',
            title: '地方體驗',
          },
        ],
      },
    ],
  },
  {
    categoryId: '2',
    title: '人口',
    list: [
      {
        tag2: 6,
        groupId: '1',
        title: '人口與族群',
        list: [
          {
            tag3: 3,
            optionId: '1',
            title: '返鄉人口',
          },
          {
            tag3: 4,
            optionId: '2',
            title: '新住民',
          },
          {
            tag3: 5,
            optionId: '3',
            title: '移工',
          },
          {
            tag3: 6,
            optionId: '4',
            title: '高齡人口',
          },
          {
            tag3: 7,
            optionId: '5',
            title: '移入人口(關係人口)',
          },
        ],
      },
      {
        tag2: 7,
        groupId: '2',
        title: '人口過程',
        list: [
          {
            tag3: 8,
            optionId: '1',
            title: '人口老化',
          },
          {
            tag3: 9,
            optionId: '2',
            title: '人口外流',
          },
          {
            tag3: 10,
            optionId: '3',
            title: '人口回流',
          },
        ],
      },
    ],
  },
  {
    categoryId: '3',
    title: '地方',
    list: [
      {
        tag2: 8,
        optionId: '1',
        title: '地方特性',
      },
      {
        tag2: 9,
        optionId: '2',
        title: '宗教事業',
      },
      {
        tag2: 10,
        optionId: '3',
        title: '文化/古蹟',
      },
    ],
  },
  {
    categoryId: '4',
    title: '景觀生態',
    list: [
      {
        tag2: 11,
        groupId: '1',
        title: '潮間帶',
        list: [
          {
            tag3: 11,
            optionId: '1',
            title: '底棲生物',
          },
          {
            tag3: 12,
            optionId: '2',
            title: '紅樹林',
          },
        ],
      },
      {
        tag2: 12,
        groupId: '2',
        title: '產業地景',
        list: [
          {
            tag3: 13,
            optionId: '1',
            title: '養殖地景變遷',
          },
          {
            tag3: 14,
            optionId: '2',
            title: '農業地景變遷',
          },
          {
            tag3: 15,
            optionId: '3',
            title: '畜牧業地景變遷',
          },
        ],
      },
      {
        tag2: 13,
        groupId: '3',
        title: '環境威脅',
        list: [
          {
            tag3: 16,
            optionId: '1',
            title: '環境汙染',
          },
          {
            tag3: 17,
            optionId: '2',
            title: '噪音',
          },
          {
            tag3: 18,
            optionId: '3',
            title: '氣候變遷',
          },
          {
            tag3: 19,
            optionId: '4',
            title: '環境劣化',
          },
        ],
      },
    ],
  },
  {
    categoryId: '5',
    title: '治理與建設',
    list: [
      {
        tag2: 14,
        groupId: '1',
        title: '國家級開發案',
        list: [
          {
            tag3: 20,
            optionId: '1',
            title: '光電',
          },
          {
            tag3: 21,
            optionId: '2',
            title: '風電',
          },
          {
            tag3: 22,
            optionId: '3',
            title: '海空步道',
          },
        ],
      },
      {
        tag2: 15,
        optionId: '2',
        title: '鄉鎮級基礎建設',
      },
      {
        tag2: 16,
        optionId: '3',
        title: '政策規劃',
      },
    ],
  },
  {
    categoryId: '6',
    title: '土地利用',
    list: [
      {
        tag2: 17,
        optionId: '1',
        title: '土地利用(陸域)',
      },
      {
        tag2: 18,
        optionId: '2',
        title: '土地利用(潮間帶)',
      },
    ],
  },
  {
    categoryId: '7',
    title: '永續實踐',
    list: [
      {
        tag2: 19,
        optionId: '1',
        title: '生態旅遊',
      },
      {
        tag2: 20,
        optionId: '2',
        title: '環境教育',
      },
      {
        tag2: 21,
        optionId: '3',
        title: '循環經濟',
      },
      {
        tag2: 22,
        optionId: '4',
        title: '其他',
      },
    ],
  },
  {
    categoryId: '8',
    title: '重要關鍵詞',
    list: [
      {
        tag2: 24,
        groupId: '1',
        title: '其他',
        list: [
          {
            tag3: 23,
            optionId: '1',
            title: '蚵藝',
          },
          {
            tag3: 24,
            optionId: '2',
            title: '海牛文化',
          },
          {
            tag3: 25,
            optionId: '3',
            title: '夕陽',
          },
          {
            tag3: 26,
            optionId: '4',
            title: '地方特產/品牌',
          },
          {
            tag3: 27,
            optionId: '5',
            title: '政府補助政策',
          },
          {
            tag3: 28,
            optionId: '6',
            title: '土地產權複雜',
          },
          {
            tag3: 29,
            optionId: '7',
            title: '土壤鹽化',
          },
          {
            tag3: 30,
            optionId: '8',
            title: '優養化',
          },
          {
            tag3: 31,
            optionId: '9',
            title: '漁獲減少',
          },
          {
            tag3: 32,
            optionId: '10',
            title: '生物疾病',
          },
          {
            tag3: 33,
            optionId: '11',
            title: '養殖技術',
          },
          {
            tag3: 34,
            optionId: '12',
            title: '排水循環',
          },
          {
            tag3: 35,
            optionId: '13',
            title: '漁電共生',
          },
          {
            tag3: 36,
            optionId: '14',
            title: '不利耕種區(光電先行區)',
          },
          {
            tag3: 37,
            optionId: '15',
            title: '風電/光電選址(爭議)',
          },
          {
            tag3: 38,
            optionId: '16',
            title: '開發商補償金',
          },
          {
            tag3: 39,
            optionId: '17',
            title: '開發商社會責任',
          },
        ],
      },
    ],
  },
];

export const setList = [
  { id: 'intersection', title: '交集' },
  {
    id: 'union',
    title: '聯集',
  },
];
