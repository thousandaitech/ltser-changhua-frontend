export const activityList = [
  {
    id: 1,
    title: '2022王功漁火節擇定國曆8/27-28登場',
    activities: {
      reference: '彰化區漁會',
      time: '2022/04/13 22:00',
    },
    link: 'https://www.cfa101.org.tw/Comm_NewsPage?sid=AACFC2E56FBBD30CCC1B8089B9298F02&enid=32085F977C307ADE1B75DAFFF9730F04',
    tags: ['1'],
    views: 0,
  },
  {
    id: 2,
    title:
      '「2022彰化縣媽祖祈福文化節」擇定國曆9月23、24、25日(農曆8月28、29、30日)辦理',
    activities: {
      reference: '文化局',
      time: '2022/07/04 22:00',
    },
    link: 'https://www.chcg.gov.tw/ch/newsdetail.asp?bull_id=356907',
    tags: ['2'],
    views: 0,
  },
];
