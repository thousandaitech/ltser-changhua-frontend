import researcherImg1 from 'img/about/researcher/wsf.jpg';
import researcherImg2 from 'img/about/researcher/lhz.jpg';
import researcherImg3 from 'img/about/researcher/syl.jpg';
import researcherImg4 from 'img/about/researcher/tjy.jpg';
import researcherImg5 from 'img/about/researcher/cyq.jpg';
import researcherImg6 from 'img/about/researcher/chc.jpg';
import researcherImg7 from 'img/about/researcher/lyz.jpg';
import researcherImg8 from 'img/about/researcher/ypz.jpg';
import researcherImg9 from 'img/about/researcher/lwq.jpg';
import researcherImg10 from 'img/about/researcher/lkl.jpg';

export const researcherList = [
  {
    id: 1,
    name: '王素芬',
    title: '國立彰化師範大學地理學系 教授',
    duty: '計畫主持人',
    contact: 'sfwang@cc.ncue.edu.tw',
    img: researcherImg1,
  },
  {
    id: 2,
    name: '林惠真',
    title: '東海大學生命科學系 教授',
    duty: '共同主持人',
    contact: 'hclin@thu.edu.tw',
    img: researcherImg2,
  },
  {
    id: 3,
    name: '宋郁玲',
    title: '國立彰化師範大學地理學系 教授',
    duty: '共同主持人',
    contact: 'yuling@cc.ncue.edu.tw',
    img: researcherImg3,
  },
  {
    id: 4,
    name: '陳毅青',
    title: '國立彰化師範大學地理學系 副教授',
    duty: '共同主持人',
    contact: 'yichinchen@cc.ncue.edu.tw',
    img: researcherImg5,
  },
  {
    id: 5,
    name: '涂建翊',
    title: '國立彰化師範大學地理學系 教授',
    duty: '協同主持人',
    contact: 'jienyi@cc.ncue.edu.tw',
    img: researcherImg4,
  },
  {
    id: 6,
    name: '遲恒昌',
    title: '國立彰化師範大學地理學系 副教授',
    duty: '協同主持人',
    contact: 'hcchi@cc.ncue.edu.tw',
    img: researcherImg6,
  },
  {
    id: 7,
    name: '楊珮甄',
    title: '團隊研究人員',
    duty: '研究助理',
    img: researcherImg8,
  },
  {
    id: 8,
    name: '林韋齊',
    title: '團隊研究人員',
    duty: '研究助理',
    img: researcherImg9,
  },
  {
    id: 9,
    name: '李昆霖',
    title: '團隊研究人員',
    duty: '研究助理',
    img: researcherImg10,
  },
];
