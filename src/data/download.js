export const downloadList = [
  {
    id: '1',
    title: '核心觀測站水質調查',
    download: {
      content: '核心觀測站於彰化縣芳苑鄉沿海進行水質調查所獲取之資料',
      col: '',
      reference: 'LTSER_Changhua',
      time: '2022-03-02',
      counts: 4,
      file: ['PDF', 'ZIP'],
    },
    link: 'https://data.depositar.io/dataset/fy_wq',
    views: 127,
  },
  {
    id: '2',
    title: '核心觀測站泥灘地調查',
    download: {
      content:
        '核心觀測站於彰化縣芳苑鄉沿海潮間帶泥灘地進行底棲生物觀測及底質採樣調查資料',
      col: '',
      reference: 'LTSER_Changhua',
      time: '2021-02-07',
      counts: 2,
      file: ['ZIP'],
    },
    link: 'https://data.depositar.io/dataset/fy_crab',
    views: 64,
  },
  {
    id: '3',
    title: '芳苑測站氣象資料',
    download: {
      content: '交通部中央氣象局芳苑測站歷年氣象資料',
      col: '',
      reference: '中央氣象局大氣資料庫',
      time: '2021-08-31',
      counts: 1,
      file: ['ZIP'],
    },
    link: 'https://data.depositar.io/dataset/19b3d',
    views: 144,
  },
];
