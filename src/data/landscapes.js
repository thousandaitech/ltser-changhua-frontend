import { LANDSCAPE_TILE_SUFFIX, LANDSCAPE_TILE_URL } from 'utils/config';

export const navList = [
  {
    id: '',
    param: '',
    title: '全部',
  },
  {
    id: '1',
    param: 'major-construction',
    title: '重大建設',
  },
  {
    id: '2',
    param: 'tourist-attraction',
    title: '旅遊景點',
  },
  {
    id: '3',
    param: 'religion',
    title: '宗教信仰',
  },
  {
    id: '4',
    param: 'agencies',
    title: '機關設施',
  },
  {
    id: '5',
    param: 'industry',
    title: '產業經濟',
  },
  {
    id: '6',
    param: 'natural-landscape',
    title: '自然地景',
  },
];

export const villageList = [
  '漢寶村',
  '新寶村',
  '王功村',
  '崙腳村',
  '新生村',
  '路平村',
  '博愛村',
  '和平村',
  '民生村',
  '興仁村',
  '文津村',
  '草湖村',
  '三合村',
  '芳苑村',
  '芳中村',
  '信義村',
  '永興村',
  '五俊村',
  '仁愛村',
  '後寮村',
  '新街村',
  '頂廍村',
  '路上村',
  '路平村',
  '福榮村',
  '三成村',
];

export const overlayList = [
  {
    id: '1976-1',
    image: '94201003_65002_360',
    title: '新寶新生地',
    coordinate: [
      [23.971812, 120.306484],
      [23.999811, 120.334728],
    ],
  },
  {
    id: '1976-2',
    image: '94201004_65P057_010',
    title: '新寶',
    coordinate: [
      [23.971842, 120.331423],
      [23.999836, 120.359672],
    ],
  },
  {
    id: '1976-3',
    image: '94201005_65P057_045',
    title: '十戶',
    coordinate: [
      [23.971868, 120.35645],
      [23.999857, 120.384705],
    ],
  },
  {
    id: '1976-4',
    image: '94201013_65002_366',
    title: '王功漁港',
    coordinate: [
      [23.946865, 120.30644],
      [23.974863, 120.334679],
    ],
  },
  {
    id: '1976-5',
    image: '94201014_65009_019',
    title: '王功',
    coordinate: [
      [23.946813, 120.331463],
      [23.974807, 120.359708],
    ],
  },
  {
    id: '1976-6',
    image: '94201023_65002_372',
    title: '芳苑',
    coordinate: [
      [23.921836, 120.306486],
      [23.949834, 120.334719],
    ],
  },
  {
    id: '1976-7',
    image: '94201024_65009_026',
    title: '五圳',
    coordinate: [
      [23.921865, 120.331415],
      [23.949859, 120.359654],
    ],
  },
  {
    id: '1976-8',
    image: '94201032_65002_350',
    title: '新街',
    coordinate: [
      [23.896854, 120.281429],
      [23.924857, 120.309652],
    ],
  },
  {
    id: '1976-9',
    image: '94201033_65002_378',
    title: '頂廓',
    coordinate: [
      [23.896807, 120.306443],
      [23.924805, 120.334671],
    ],
  },
  {
    id: '1976-10',
    image: '94201042_65002_343',
    title: '三豐',
    coordinate: [
      [23.871826, 120.281479],
      [23.899828, 120.309697],
    ],
  },
  {
    id: '1976-11',
    image: '94201043_65002_384',
    title: '新厝',
    coordinate: [
      [23.871859, 120.306487],
      [23.899857, 120.33471],
    ],
  },
  {
    id: '1976-12',
    image: '94212094_65P057_016',
    title: '漢寶海堤',
    coordinate: [
      [23.996871, 120.33147],
      [24.024865, 120.359725],
    ],
  },
  {
    id: '1976-13',
    image: '94212095_65P057_038',
    title: '海尾',
    coordinate: [
      [23.996815, 120.356414],
      [24.024804, 120.384675],
    ],
  },

  {
    id: '2007-1',
    image: '94201003_070622a_13~0356_rgb',
    title: '新寶新生地',
    coordinate: [
      [23.971812, 120.306484],
      [23.999811, 120.334728],
    ],
  },
  {
    id: '2007-2',
    image: '94201004_070622a_14~0007_rgb',
    title: '新寶',
    coordinate: [
      [23.971842, 120.331423],
      [23.999836, 120.359672],
    ],
  },
  {
    id: '2007-3',
    image: '94201005_070622a_15~0369_rgb',
    title: '十戶',
    coordinate: [
      [23.971868, 120.35645],
      [23.999857, 120.384705],
    ],
  },
  {
    id: '2007-4',
    image: '94201013_070622a_13~0352_rgb',
    title: '王功漁港',
    coordinate: [
      [23.946865, 120.30644],
      [23.974863, 120.334679],
    ],
  },
  {
    id: '2007-5',
    image: '94201014_070622a_14~0011_rgb',
    title: '王功',
    coordinate: [
      [23.946813, 120.331463],
      [23.974807, 120.359708],
    ],
  },
  {
    id: '2007-6',
    image: '94201023_070622a_13~0348_rgb',
    title: '芳苑',
    coordinate: [
      [23.921836, 120.306486],
      [23.949834, 120.334719],
    ],
  },
  {
    id: '2007-7',
    image: '94201024_070622a_14~0015_rgb',
    title: '五圳',
    coordinate: [
      [23.921865, 120.331415],
      [23.949859, 120.359654],
    ],
  },
  {
    id: '2007-8',
    image: '94201032_071002a_12_0467_rgb',
    title: '新街',
    coordinate: [
      [23.896854, 120.281429],
      [23.924857, 120.309652],
    ],
  },
  {
    id: '2007-9',
    image: '94201033_070622a_13~0344_rgb',
    title: '頂廓',
    coordinate: [
      [23.896807, 120.306443],
      [23.924805, 120.334671],
    ],
  },
  {
    id: '2007-10',
    image: '94201042_071002a_12_0463_rgb',
    title: '三豐',
    coordinate: [
      [23.871826, 120.281479],
      [23.899828, 120.309697],
    ],
  },
  {
    id: '2007-11',
    image: '94201043_070622a_13~0340_rgb',
    title: '新厝',
    coordinate: [
      [23.871859, 120.306487],
      [23.899857, 120.33471],
    ],
  },
  {
    id: '2007-12',
    image: '94212094_070622a_14~0003_rgb',
    title: '漢寶海堤',
    coordinate: [
      [23.996871, 120.33147],
      [24.024865, 120.359725],
    ],
  },
  {
    id: '2007-13',
    image: '94212095_070622a_15~0365_rgb',
    title: '海尾',
    coordinate: [
      [23.996815, 120.356414],
      [24.024804, 120.384675],
    ],
  },

  {
    id: '2015-1',
    image: '94201003_150518a_13~0162_hr4',
    title: '新寶新生地',
    coordinate: [
      [23.971948, 120.306542],
      [23.999747, 120.334551],
    ],
  },
  {
    id: '2015-2',
    image: '94201004_150518a_14~0243_hr4',
    title: '新寶',
    coordinate: [
      [23.971924, 120.33155],
      [23.999718, 120.359565],
    ],
  },
  {
    id: '2015-3',
    image: '94201005_150518a_15~0125_hr4',
    title: '十戶',
    coordinate: [
      [23.97194, 120.356558],
      [23.99973, 120.384578],
    ],
  },
  {
    id: '2015-4',
    image: '94201013_150518a_13~0166_hr4',
    title: '王功漁港',
    coordinate: [
      [23.946937, 120.306578],
      [23.974736, 120.334582],
    ],
  },
  {
    id: '2015-5',
    image: '94201014_150518a_14~0239_hr4',
    title: '王功',
    coordinate: [
      [23.946958, 120.33158],
      [23.974752, 120.35959],
    ],
  },
  {
    id: '2015-6',
    image: '94201023_150518a_13~0170_hr4',
    title: '芳苑',
    coordinate: [
      [23.921926, 120.306564],
      [23.949725, 120.334563],
    ],
  },
  {
    id: '2015-7',
    image: '94201024_150518a_14~0235_hr4',
    title: '五圳',
    coordinate: [
      [23.921947, 120.331562],
      [23.949741, 120.359566],
    ],
  },
  {
    id: '2015-8',
    image: '94201032_150518a_12~0333_hr4',
    title: '新街',
    coordinate: [
      [23.896936, 120.281557],
      [23.924739, 120.309545],
    ],
  },
  {
    id: '2015-9',
    image: '94201033_150518a_13~0174_hr4',
    title: '頂廓',
    coordinate: [
      [23.896961, 120.30655],
      [23.924759, 120.334543],
    ],
  },
  {
    id: '2015-10',
    image: '94201042_150518a_12~0329_hr4',
    title: '三豐',
    coordinate: [
      [23.871925, 120.281597],
      [23.899728, 120.309579],
    ],
  },
  {
    id: '2015-11',
    image: '94201043_150518a_13~0178_hr4',
    title: '新厝',
    coordinate: [
      [23.87195, 120.306585],
      [23.899748, 120.334573],
    ],
  },
  {
    id: '2015-12',
    image: '94212094_150518a_14~0247_hr4',
    title: '漢寶海堤',
    coordinate: [
      [23.996935, 120.331568],
      [24.024729, 120.359588],
    ],
  },
  {
    id: '2015-13',
    image: '94212095_150518a_15~0129_hr4',
    title: '海尾',
    coordinate: [
      [23.996951, 120.356532],
      [24.024741, 120.384557],
    ],
  },

  {
    id: '2021-1',
    image: '94201003_211031a_13~0338_hr4',
    title: '新寶新生地',
    coordinate: [
      [23.973219, 120.308009],
      [23.998476, 120.333084],
    ],
  },
  {
    id: '2021-2',
    image: '94201004_211031a_14~0309_hr4',
    title: '新寶',
    coordinate: [
      [23.973194, 120.333017],
      [23.998447, 120.358097],
    ],
  },
  {
    id: '2021-3',
    image: '94201013_211031a_13~0342_hr4',
    title: '王功漁港',
    coordinate: [
      [23.948208, 120.308044],
      [23.973465, 120.333065],
    ],
  },
  {
    id: '2021-4',
    image: '94201014_211031a_14~0305_hr4',
    title: '王功',
    coordinate: [
      [23.948228, 120.333048],
      [23.973482, 120.358073],
    ],
  },
  {
    id: '2021-5',
    image: '94201023_211031a_13~0346_hr4',
    title: '芳苑',
    coordinate: [
      [23.923197, 120.30803],
      [23.948454, 120.333095],
    ],
  },
  {
    id: '2021-6',
    image: '94201024_211031a_14~0301_hr4',
    title: '五圳',
    coordinate: [
      [23.923217, 120.333029],
      [23.948471, 120.358099],
    ],
  },
  {
    id: '2021-7',
    image: '94201032_211031a_12~0144_hr4',
    title: '新街',
    coordinate: [
      [23.898207, 120.283023],
      [23.923468, 120.308078],
    ],
  },
  {
    id: '2021-8',
    image: '94201033_211031a_13~0350_hr4',
    title: '頂廓',
    coordinate: [
      [23.898231, 120.308016],
      [23.923489, 120.333076],
    ],
  },
  {
    id: '2021-9',
    image: '94201042_211031a_12~0140_hr4',
    title: '三豐',
    coordinate: [
      [23.873196, 120.283013],
      [23.898457, 120.308064],
    ],
  },
  {
    id: '2021-10',
    image: '94201043_211031a_13~0354_hr4',
    title: '新厝',
    coordinate: [
      [23.87322, 120.308051],
      [23.898477, 120.333057],
    ],
  },
  {
    id: '2021-11',
    image: '94212094_211031a_14~0313_hr4',
    title: '漢寶海堤',
    coordinate: [
      [23.998205, 120.333036],
      [24.023458, 120.358071],
    ],
  },
];

export const yearMapList = [
  {
    year: '1976年',
    id: 'landscape65',
    url: `${LANDSCAPE_TILE_URL}/1976${LANDSCAPE_TILE_SUFFIX}`,
  },
  {
    year: '2007年',
    id: 'landscape2007',
    url: `${LANDSCAPE_TILE_URL}/2007${LANDSCAPE_TILE_SUFFIX}`,
  },
  {
    year: '2008年',
    id: 'PHOTO2014',
    url: 'https://wmts.nlsc.gov.tw/wmts/PHOTO2014/default/GoogleMapsCompatible/{z}/{y}/{x}',
  },
  {
    year: '2009年',
    id: 'landscape2009',
    url: `${LANDSCAPE_TILE_URL}/2009${LANDSCAPE_TILE_SUFFIX}`,
  },
  {
    year: '2010年',
    id: 'landscape2010',
    url: `${LANDSCAPE_TILE_URL}/2010${LANDSCAPE_TILE_SUFFIX}`,
  },
  {
    year: '2011年',
    id: 'landscape2011',
    url: `${LANDSCAPE_TILE_URL}/2011${LANDSCAPE_TILE_SUFFIX}`,
  },
  {
    year: '2012年',
    id: 'landscape2012',
    url: `${LANDSCAPE_TILE_URL}/2012${LANDSCAPE_TILE_SUFFIX}`,
  },
  {
    year: '2013年',
    id: 'PHOTO2015',
    url: 'https://wmts.nlsc.gov.tw/wmts/PHOTO2015/default/GoogleMapsCompatible/{z}/{y}/{x}',
  },
  {
    year: '2014年',
    id: 'landscape2014',
    url: `${LANDSCAPE_TILE_URL}/2014${LANDSCAPE_TILE_SUFFIX}`,
  },
  {
    year: '2015年',
    id: 'PHOTO2016',
    url: 'https://wmts.nlsc.gov.tw/wmts/PHOTO2016/default/GoogleMapsCompatible/{z}/{y}/{x}',
  },
  {
    year: '2016年',
    id: 'landscape2016',
    url: `${LANDSCAPE_TILE_URL}/2016${LANDSCAPE_TILE_SUFFIX}`,
  },
  {
    year: '2017年',
    id: 'PHOTO2019',
    url: 'https://wmts.nlsc.gov.tw/wmts/PHOTO2019/default/GoogleMapsCompatible/{z}/{y}/{x}',
  },
  {
    year: '2018年',
    id: 'landscape2018',
    url: `${LANDSCAPE_TILE_URL}/2018${LANDSCAPE_TILE_SUFFIX}`,
  },
  {
    year: '2019年',
    id: 'PHOTO2020',
    url: 'https://wmts.nlsc.gov.tw/wmts/PHOTO2020/default/GoogleMapsCompatible/{z}/{y}/{x}',
  },
  {
    year: '2020年',
    id: 'PHOTO2',
    url: 'https://wmts.nlsc.gov.tw/wmts/PHOTO2/default/GoogleMapsCompatible/{z}/{y}/{x}',
  },
  {
    year: '2021年',
    id: 'landscape2021',
    url: `${LANDSCAPE_TILE_URL}/2021${LANDSCAPE_TILE_SUFFIX}`,
  },
];
