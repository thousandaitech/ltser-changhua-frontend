import React, { useState, useEffect } from 'react';

import ContentTemplate from 'components/ContentTemplate';

import downloadImg from 'img/icon/download.svg';

import { downloadList } from 'data/download';

import usePage from 'hooks/usePage';
import useRender from 'hooks/useRender';

function Download() {
  const pageData = {
    title: '資料下載',
    page: 'download',
    icon: downloadImg,
    link: '/download',
    url: 'download',
  };

  const {
    currentPage,
    setCurrentPage,
    paginationData,
    setPaginationData,
    paginationList,
    isFetchingPages,
  } = usePage();
  const { getListByPage } = useRender();

  const [downloads, setDownloads] = useState(null);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    getListByPage({
      url: pageData.url,
      page: currentPage,
      setList: setDownloads,
      defaultList: downloadList,
      setPaginationData,
    });
  }, [currentPage, refresh]);

  return (
    <>
      <ContentTemplate
        pageData={pageData}
        data={downloads}
        tab={null}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        paginationData={paginationData}
        paginationList={paginationList}
        isFetchingPages={isFetchingPages}
        setRefresh={setRefresh}
      />
    </>
  );
}

export default Download;
