import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import { Form, Spinner, OverlayTrigger, Tooltip } from 'react-bootstrap';

import FormLayouts from 'components/FormLayouts';
import ReCaptchaLayout from 'components/ReCaptchaLayout';

import { useAuthContext } from 'context/AuthContext';
import { useFormContext } from 'context/FormContext';

import { RECAPTCHA_HINT, taiwanCityCountyList, SWAL_TOAST } from 'data/common';

import { AUTH_URL } from 'utils/config';
import Swal from 'sweetalert2';
import SpinnerPlaceholder from 'components/SpinnerPlaceholder';

function Member() {
  const fieldList = [
    {
      id: 'username',
      title: '電子郵件地址(即登入帳號)',
      type: 'email',
      readOnly: true,
      required: false,
    },
    {
      id: 'lastName',
      title: '姓',
      type: 'text',
      readOnly: true,
      required: false,
    },
    {
      id: 'firstName',
      title: '名',
      type: 'text',
      readOnly: true,
      required: false,
    },
    {
      id: 'institution',
      title: '學校/單位',
      type: 'text',
      readOnly: false,
      required: true,
    },
    {
      id: 'institutionLocation',
      title: '單位地點',
      type: 'select',
      options: taiwanCityCountyList,
      required: true,
    },
    {
      id: 'department',
      title: '系所/部門',
      type: 'text',
      readOnly: false,
      required: true,
    },
    {
      id: 'position',
      title: '職稱/身分',
      type: 'text',
      readOnly: false,
      required: true,
    },
    {
      id: 'positionCategory',
      title: '身分類別',
      type: 'select',
      options: ['政府機關', '學術單位', '公司行號', '個人使用'],
      required: true,
    },
    {
      id: 'application',
      title: '資料用途',
      type: 'select',
      options: ['學術研究', '資訊取得', '數據分析', '環境教育'],
      required: false,
    },
    {
      id: 'preferenceCategory',
      title: '關注類別',
      type: 'checkbox',
      options: [
        '生態',
        '水質',
        '氣象',
        '空汙',
        '人口',
        '產業',
        '訪談資料',
        '問卷調查',
        '海空步道人流',
        '其他',
      ],
    },
    {
      id: 'securityQuestion',
      title: '你最喜歡的城市?',
      type: 'text',
      readOnly: false,
      required: true,
      hints: [
        {
          id: 'text',
          content: '此為安全問題，若忘記密碼，將會以此題答案作為驗證。',
        },
      ],
    },
  ];

  const { profile, authTokens } = useAuthContext();
  const { handleInitialState } = useFormContext();

  const [loading, setLoading] = useState(false);
  const [recaptcha, setRecaptcha] = useState(false);
  const [form, setForm] = useState({ ...handleInitialState(fieldList) });

  const isFetchingProfile = profile && profile?.user.id === '';
  const isVerified = profile?.is_verified;
  const isFetchingFormProfile = form && form.username === '';

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    const {
      institution,
      institutionLocation,
      department,
      position,
      positionCategory,
      application,
      preferenceCategory,
      securityQuestion,
    } = form;
    const handleSubmit = async () => {
      try {
        const formData = {
          school: institution,
          location: institutionLocation,
          department: department,
          title: position,
          category: positionCategory,
          application: application,
          attention: preferenceCategory.join('/'),
          securityQuestion,
        };
        const response = await axios({
          method: 'patch',
          url: `${AUTH_URL}/updateUserProfile/`,
          headers: {
            Authorization: `Bearer ${authTokens.access}`,
          },
          data: formData,
        });
        const { status, data } = response;
        if (status === 200) {
          setLoading(false);
          SWAL_TOAST.fire({
            icon: 'success',
            title: '已更新。',
          });
          window.location.reload();
        }
      } catch (err) {
        setLoading(false);
        SWAL_TOAST.fire({
          icon: 'error',
          title: `發生錯誤。(${err})`,
        });
      }
    };
    if (form.securityQuestion) {
      Swal.fire({
        title: '確認更新？',
        text: '請再次確認安全問題是否填寫正確，更新後不得再修改。',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: '確認',
        cancelButtonText: '取消',
        reverseButtons: true,
        confirmButtonColor: '#44a0c9',
      }).then(async (result) => {
        if (result.isConfirmed) {
          handleSubmit();
        }
      });
    } else {
      handleSubmit();
    }
  };

  useEffect(() => {
    if (isVerified && !isFetchingProfile) {
      const {
        user,
        school,
        location,
        department,
        title,
        category,
        application,
        attention,
        securityQuestion,
      } = profile;
      const { username, first_name, last_name } = user;
      setForm({
        username,
        firstName: first_name,
        lastName: last_name,
        institution: school,
        institutionLocation: location,
        department,
        position: title,
        positionCategory: category,
        application,
        preferenceCategory: attention ? attention?.split('/') : [],
        securityQuestion,
      });
    }
  }, [profile]);

  return (
    <>
      <div className="l-auth">
        <h1 className="c-title">會員資料</h1>
        {isVerified ? (
          !isFetchingProfile && !isFetchingProfile ? (
            <Form className="c-form" onSubmit={handleFormSubmit}>
              <span className="c-form__error">*為必填欄位</span>
              {!isFetchingFormProfile && (
                <FormLayouts
                  form={form}
                  setForm={setForm}
                  fieldList={fieldList}
                />
              )}
              {/* <ReCaptchaLayout setValidated={setRecaptcha} /> */}
              <div className="c-form__actions">
                <Link
                  to="/auth/change-password"
                  className={`c-form__action muted e-btn ${
                    loading ? 'e-btn--disabled' : ''
                  }`}
                >
                  修改密碼
                </Link>
                <div>
                  <button
                    type="submit"
                    className="c-form__action action e-btn e-btn--processing"
                    data-count="4"
                    disabled={loading}
                  >
                    {loading ? (
                      <Spinner
                        as="span"
                        animation="border"
                        size="sm"
                        role="status"
                        aria-hidden="true"
                      />
                    ) : (
                      '更新資料'
                    )}
                  </button>
                </div>
                {/* <OverlayTrigger
                placement="top"
                overlay={
                  !recaptcha ? <Tooltip>{RECAPTCHA_HINT}</Tooltip> : <></>
                }
              >
                <div>
                  <button
                    type="submit"
                    className="c-form__action action e-btn e-btn--processing"
                    data-count="4"
                    disabled={loading || !recaptcha}
                  >
                    {loading ? (
                      <Spinner
                        as="span"
                        animation="border"
                        size="sm"
                        role="status"
                        aria-hidden="true"
                      />
                    ) : (
                      '更新資料'
                    )}
                  </button>
                </div>
              </OverlayTrigger> */}
              </div>
            </Form>
          ) : (
            <SpinnerPlaceholder
              layout="graph"
              text="資料讀取中"
              height={'100vh'}
            />
          )
        ) : (
          <>請等候工作人員開通帳號。</>
        )}
      </div>
    </>
  );
}

export default Member;
