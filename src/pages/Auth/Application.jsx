import { useAuthContext } from 'context/AuthContext';
import React from 'react';

const Application = () => {
  const { profile } = useAuthContext();

  const isVerified = profile && profile?.is_verified;

  return (
    <>
      {isVerified ? (
        <article className="l-auth__article c-article">
          <h5 className="c-article__subtitle">
            國立彰化師範大學長期社會生態核心觀測彰化站(LTSER Changhua)
            社會調查資料申請辦法
          </h5>
          <ol className="c-article__ol">
            <li className="c-article__oli">
              長期社會生態核心觀測彰化站(以下稱本站)為國科會計畫「臺灣西部海岸濕地農漁社會生態系統監測」(以下稱本計畫)規劃設立。旨在建立以芳苑地區為核心之資料庫，以為促進資料共享、提升資料收集及科學研究的效率，特訂定本辦法。
            </li>
            <li className="c-article__oli">
              本辦法資料提供之適用對象僅限非營利單位。
            </li>
            <li className="c-article__oli">
              質化訪談資料說明
              <ol className="c-article__ol c-article__ol--sub">
                <li className="c-article__oli">
                  本計畫質化訪談資料為保護受訪者隱私及相關權益，已進行初步資料處理(去識別化、重組及分類)，並取得受訪者資料之授權同意書，提供資料作為研究、教育或其他相關公共(益)性質使用。
                </li>
                <li className="c-article__oli">
                  本辦法提供之資料以地方重要議題進行分類，可依照需求搜尋該類別的訪談內容，供使用者建立對芳苑地區的先備知識。
                </li>
              </ol>
            </li>
            <li className="c-article__oli">
              資料使用規範
              <ol className="c-article__ol c-article__ol--sub">
                <li className="c-article__oli">
                  以本資料作為研究材料時，應嚴守研究倫理，於研究方法內說明分析資料取自本站質化訪談資料，並註明本資料已進行初步資料處理(去識別化、重組及分類)。
                </li>
                <li className="c-article__oli">
                  使用本資料時請註明資料來源（例如：LTSER彰化站社會調查訪談資料）。
                </li>
                <li className="c-article__oli">
                  若自行延伸或過度解讀本資料內容，造成與事實不符之誤解，需由使用者自行負責。
                </li>
                <li className="c-article__oli">
                  使用者請於研究公開發表（如會議論文、期刊論文、博碩士論文、專書、或其他等）三個月內回傳一份成果，並同意本站將研究成果共享至「相關研究」頁面，增加該成果之閱覽量。
                </li>
              </ol>
            </li>
            <li className="c-article__oli">
              申請流程
              <ol className="c-article__ol c-article__ol--sub">
                <li className="c-article__oli">
                  填寫
                  <a
                    href="https://data.depositar.io/dataset/ltser-changhua-s-data-request-guidelines"
                    className="e-link"
                    target="_blank"
                    rel="noreferrer"
                  >
                    社會調查資料申請表
                  </a>
                  ，並詳閱注意事項及簽名。
                </li>
                <li className="c-article__oli">
                  將填寫完成的申請表e-mail至本站：
                  <a
                    href="mailto:ltserchanghuaservice@gmail.com"
                    className="e-link"
                    target="_blank"
                    rel="noreferrer"
                  >
                    ltserchanghuaservice@gmail.com
                  </a>
                </li>
                <li className="c-article__oli">
                  本站將於收到申請文件後盡速審核並回覆結果。
                </li>
                <li className="c-article__oli">
                  使用者請於研究公開發表（如會議論文、期刊論文、博碩士論文、專書、或其他等）三個月內回傳一份成果，並同意本站將研究成果共享至「相關研究」頁面，增加該成果之閱覽量。
                </li>
              </ol>
            </li>
            <li className="c-article__oli">
              申請人如違反本辦法使用規範，本站得終止資料使用權利。
            </li>
            <li className="c-article__oli">
              因政策變更或其他事由，本站得隨時終止提供本服務。
            </li>
            <li className="c-article__oli">
              本服務為無償提供，使用本服務而衍生之法律問題，應由申請人自行責任。
            </li>
          </ol>
        </article>
      ) : (
        <>請等候工作人員開通帳號。</>
      )}
    </>
  );
};

export default Application;
