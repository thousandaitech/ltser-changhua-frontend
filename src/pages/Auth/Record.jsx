import React, { useEffect, useState } from 'react';

import Intro from 'components/Intro';
import PaginationLayout from 'components/PaginationLayout';

import img from 'img/icon/download.svg';

import usePage from 'hooks/usePage';
import useAuthRender from 'hooks/useAuthRender';
import { useAuthContext } from 'context/AuthContext';

const Record = () => {
  const pageData = {
    id: 'record',
    title: '下載紀錄',
    icon: img,
    link: '/record',
    url: 'download-record',
  };
  const {
    currentPage,
    setCurrentPage,
    paginationData,
    paginationList,
    isFetchingPages,
    setPaginationData,
  } = usePage();

  const { getListByPage } = useAuthRender();
  const { profile } = useAuthContext();

  const [records, setRecords] = useState([]);

  const isFetchingList = records.length === 0;
  const isVerified = profile && profile?.is_verified;

  useEffect(() => {
    getListByPage({
      url: pageData.url,
      page: currentPage,
      setList: setRecords,
      setPaginationData,
    });
  }, [currentPage]);

  return (
    <>
      <article className="l-record">
        <section className="l-record__title c-heading u-section">
          <Intro data={pageData} />
        </section>
        <section className="l-record__data">
          {isVerified ? (
            !isFetchingList ? (
              <>
                <div className="c-table">
                  <div className="c-table__row">
                    <div className="c-table__col c-table__col--head">編號</div>
                    <div className="c-table__col c-table__col--head">名稱</div>
                    <div className="c-table__col c-table__col--head">日期</div>
                  </div>
                  {records.map((v, i) => {
                    const { filename, time } = v;
                    return (
                      <div className="c-table__row">
                        <div className="c-table__col">{i + 1}</div>
                        <div className="c-table__col">{filename}</div>
                        <div className="c-table__col">{time}</div>
                      </div>
                    );
                  })}
                </div>
                {!isFetchingPages && (
                  <PaginationLayout
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    paginationData={paginationData}
                    paginationList={paginationList}
                  />
                )}
              </>
            ) : (
              <>目前沒有下載紀錄。</>
            )
          ) : (
            <>請等候工作人員開通帳號。</>
          )}
        </section>
      </article>
    </>
  );
};

export default Record;
