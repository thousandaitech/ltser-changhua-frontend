import React from 'react';

import MainPageTemplate from 'components/MainPageTemplate';

function LocalLiteratureAndNews() {
  const pageData = {
    title: '文史資料',
  };
  return (
    <>
      <div className="l-local-literature-and-news">
        <MainPageTemplate pageData={pageData} />
      </div>
    </>
  );
}

export default LocalLiteratureAndNews;
