import React, { useEffect, useState } from 'react';

import relatedResearchesImg from 'img/icon/related-researches.svg';

import { researchList } from 'data/related-researches';
import useRender from 'hooks/useRender';
import usePage from 'hooks/usePage';
import TagTab from 'components/TagTab';
import ContentTemplate from 'components/ContentTemplate';
import Search from 'components/Search';

const RelatedResearches = () => {
  const pageData = {
    page: 'related-researches',
    title: '相關研究',
    icon: relatedResearchesImg,
    link: '/related-researches',
    url: 'research',
  };

  const { loading, getListByPage } = useRender();
  const {
    currentPage,
    setCurrentPage,
    paginationData,
    setPaginationData,
    paginationList,
    isFetchingPages,
    hasNoRecords,
  } = usePage();

  const [filter, setFilter] = useState({
    tag: 0,
    keyword: '',
  });
  const [researches, setResearches] = useState(null);
  const [tags, setTags] = useState(null);
  const [refresh, setRefresh] = useState(false);
  const [search, setSearch] = useState('');

  const isFetchingTags = tags === null;
  const isAllTag = filter.tag === 0;
  const hasFilterTag = filter.tag !== '';
  const hasFilterKeyword = filter.keyword !== '';
  const hasSearch = search !== '';

  useEffect(() => {
    if (hasFilterKeyword) {
      setFilter({ ...filter, tag: '' });
    }
  }, [filter.keyword]);

  useEffect(() => {
    if (hasFilterTag) {
      setFilter({ ...filter, keyword: '' });
      setSearch('');
    }
  }, [filter.tag]);

  useEffect(() => {
    getListByPage({
      url: pageData.url,
      page: currentPage,
      setList: setResearches,
      defaultList: researchList,
      setPaginationData,
      params: {
        tag: isAllTag ? null : filter.tag,
        keyword: hasFilterKeyword ? filter.keyword : null,
      },
      setTags,
    });
  }, [currentPage, filter.tag, filter.keyword, refresh]);

  return (
    <>
      <ContentTemplate
        pageData={pageData}
        data={researches}
        tab={
          !isFetchingTags && (
            <TagTab
              list={[{ id: 0, title: '全部' }, ...tags]}
              filter={filter}
              setFilter={setFilter}
              setCurrentPage={setCurrentPage}
            />
          )
        }
        search={
          <Search
            loading={loading}
            filter={filter}
            setFilter={setFilter}
            setList={setResearches}
            url={pageData.url}
            currentPage={currentPage}
            setPaginationData={setPaginationData}
            search={search}
            setSearch={setSearch}
            hasSearch={hasSearch}
          />
        }
        tagList={tags}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        paginationData={paginationData}
        paginationList={paginationList}
        isFetchingPages={isFetchingPages}
        hasNoRecords={hasNoRecords}
        setRefresh={setRefresh}
      />
    </>
  );
};

export default RelatedResearches;
