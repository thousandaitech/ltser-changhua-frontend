import React from 'react';

import bgImg from 'img/about/background/project-background.jpg';
import { SCROLL_OFFSET } from 'data/common';

const English = () => {
  return (
    <>
      <article className="l-english__article c-article c-article--english">
        <div style={{ position: 'relative', top: SCROLL_OFFSET }} id="LTSER" />
        <h5 className="c-article__title">LTSER</h5>
        <p className="c-article__paragraph">
          In order to completely understand the characteristics of Taiwan’s
          social-ecological systems, the National Science and Technology Council
          (NSTC) promotes the “Long-Term Social-Ecological Research (LTSER)” as
          core facility to collaborative and sharing investigation data and
          research resources. This platform enables researchers to efficiently
          allocate and adapt limited resources, which can leverage the studies
          on social-ecological systems and steer the society towards sustainable
          development. LTSER platform is not only a means for environmental
          monitoring, but also a place-based social-ecological database,
          enabling local researchers, government agencies, social groups, and
          other stakeholders to fulfill transdisciplinary collaboration,
          information sharing, and generating knowledge. Data and knowledge
          compiled in a LTSER platform can serve as a policy reference to
          achieve sustainability at different temporal-spatial levels.
        </p>
        <div
          style={{ position: 'relative', top: SCROLL_OFFSET }}
          id="LTSERChanghua"
        />
        <h5 className="c-article__title">LTSER Changhua</h5>
        <p className="c-article__paragraph">
          As part of the national synergetic effort, a research team led by the
          National Changhua University of Education has conducted a multi-year
          project (LTSER Changhua) which aims to adopt the context of LTSER at
          Fangyuan, Changhua, to monitor changes in the social-ecosystem of
          coastal wetland and agriculture-fishery. Like a typical costal area of
          southwest Taiwan, Fangyuan benefits greatly from sufficient annual
          sunlight, moderate temperature, high basic coastal productivity, but
          faces challenges of heavy rainfall, strong erosion of organic matter.
          As a result, its complex energy flows and biogeochemical cycles remain
          largely unknown. Moreover, with the rapid population growth and land
          development, the western coast of Taiwan has been drastically
          transformed and densely occupied by manmade infrastructure. This
          causes elevating water problems, including insufficient sewage
          treatment in downstream cities, conflicts of water use between
          large-scale industrial and agricultural sectors, and degraded water
          quality. In addition, there are also impacts associated with excessive
          groundwater withdrawal for supporting coastal aquaculture.
        </p>
        <p className="c-article__paragraph">
          Coastal communities like Fangyuan can be significantly impacted by
          climate change with far-reaching effects. Through changing temporal
          and spatial patterns of currents, ocean warming has a long-term and
          continuous impact on the climate around the world. Excessive water
          vapor in the atmosphere has caused a sudden increase in both rainfall
          and the frequency of extreme weather events. These recent changes
          damage to terrestrial ecology in both direct and indirect ways, which
          can lead to a pernicious socio-economic impact on coastal communities.
        </p>
        <p className="c-article__paragraph">
          Constrained by the natural environment, the agriculture in coastal
          communities is limited to dry-faming with low economic value. This
          implies a inherent barrier in sustaining and advancing the
          socio-economic development in such a community. The traditional
          coastal settlements of Changhua rely on a mix of agriculture and
          fishery. Influenced by both natural setting and the long-standing
          demand of aquaculture, groundwater of this area has been
          over-extracted, causing the threat of land subsidence. Moreover, the
          lack of job opportunities in the secondary and tertiary industries in
          a coastal community often results in the increasing emigration, aging
          population, and partially abandoned farmlands. In addition to the
          ecological and environmental impacts associated with the industrial
          development along the coastal areas, Fangyuan has been undergoing a
          significant transformation caused by the large-scale offshore wind
          turbines and solar projects. To what extent such projects will affect
          the nearshore landscape and coastal ecosystems remains unknown and
          requires long-term monitoring plans.
        </p>
        <p className="c-article__paragraph">
          The landscape and ecosystem transformation of Changhua’s coastal area
          is a microcosm of southwestern Taiwan’s overall coastal development.
          For the past decades, the coastal areas in Changhua have been
          developed by reclaiming numerous stretches of land plots which are
          mainly used for manufacturing industry and aquaculture. In addition,
          solar and onshore wind farms have recently been developed along the
          estuary habitats. Wind and solar energy plays an essential role in
          transforming Taiwan’s energy policies and technology, leading to an
          increasing demand for industrial development. Such transformation
          requires substantial socio-economic development beyond a local scale
          and will eventually change the functions and dynamics of the natural
          and social landscapes at coastal regions.
        </p>
        <p className="c-article__paragraph">
          Although there are various surveys conducted along the coastal area of
          Changhua at different time periods for different purposes, data
          compiled from these surveys fall short to elucidate the interaction
          between social and ecological systems. Therefore, there is an urgent
          need to systematically monitor the coastal social-ecological systems
          in Changhua in order to better understand the evolution of Changhua’s
          coastal social-ecological systems. Data and knowledge gained from such
          a monitoring program can also shed light on revealing the consequence
          and impact of implementing Taiwan’s new energy policies. This
          scientific information can provide the knowledge needed to solve the
          social and ecological problems in the future.
        </p>
        <div className="c-article__img">
          <img
            className="e-img e-img--cover"
            src={bgImg}
            alt="project-background"
          />
          <div className="c-article__caption">
            The coastal social-ecological systems in Fangyuan and Dacheng of
            Changhua
          </div>
        </div>
      </article>
      <article className="c-article__section">
        <div
          style={{ position: 'relative', top: SCROLL_OFFSET }}
          id="missionAndValues"
        />
        <h5 className="c-article__title">Mission & Values</h5>
        <p className="c-article__paragraph">
          Considering Changhua’s coastal features, findings from previous
          studies, local industrial networks, and stakeholder relationships, the
          implementation of the LTSER Changhua project will show three broad
          impacts. First, it can establish a multidisciplinary work force to
          conduct place-based research for monitoring and understanding the
          long-term dynamics of socio-ecological systems. Second, it can
          scientifically and quantitatively illustrate the transformation of
          coastal wetland ecosystems, agricultural and fishery social landscape,
          and local sustainability. Third, it can accumulate observational
          experience, and standardize operating procedures for long-term
          observatory projects. Therefore, the major tasks of this project
          include: cataloging existing research and survey data, organizing and
          analyzing social and ecological issues, conducting core project
          observations, connecting stakeholders, and establishing a place-based
          core project database.
        </p>
        <section className="c-article__paragraph">
          The overall project aims to:
          <ol className="c-article__ol">
            <li className="c-article__oli">
              Balance green energy development and social-ecological
              sustainability The Intergovernmental Panel on Climate Change
              (IPCC) of United Nations released the Sixth Assessment Report on
              Climate Change (IPCC AR6) in 2021 to emphasize the impacts of
              humans on climate change. The report also states that green energy
              development is a necessary means to mitigate climate change by
              achieving the goal of net zero carbon emissions by 2050. However,
              the socio-ecological impacts associated with wind and solar farms
              need to be assessed based on long-term scientific data. This will
              allow us to better understand the interaction between green energy
              development and social ecology, which can justify future energy
              policy amendment for advancing ecological sustainability and
              social prosperity.
            </li>
            <li className="c-article__oli">
              Recognize the dynamics of society-ecology nexus Fish schools and
              paddy fields form a good foundation to support bird habitats. In
              Taiwan, such habitats also provide a critical shelter for winter
              migration. The landscape change caused by the installation of
              solar energy facilities will affect the existing composition of
              the ecosystem. In addition, there are approximately 3% of
              households in Fangyuan earn their living by farming oysters
              (including sea-cattle cart tourism). The oyster fields increase
              the segmentation of coastal habitat by altering the tidal patterns
              and carving series of tidal grooves. This increases the difficulty
              to travel between beaches and oceans for certain species to
              complete their life cycles. However, the tidal grooves can form a
              new refuge for fish and benthos, which can be recognized as an
              added value. Therefore, long-term observational data are critical
              to assist future research. It can also help local residents
              understand the man-land interaction and society-ecology nexus,
              which can enable them to make proper decisions to interact with
              the environment.
            </li>
            <li className="c-article__oli">
              Advance collaborative research on data integration and value-added
              applications for implementing cross-disciplinary adaptive
              management. In addition to self-observation targets, the LTSER
              Changhua project also develop an inventory to categorize relevant
              research and data in the public and private domains through data
              sharing and peer-to-peer data integration. This can establish a
              place-based database providing an accessible platform for
              researchers to obtain needed data. In terms of value-added and
              data sharing, the research team will identify new monitoring needs
              and understand the changes in local issues by analyzing data
              access requests. Based on the principle of reciprocity and
              data-sharing, the database will be continually upgraded and
              expanded through collaborative research and user feedback.
            </li>
          </ol>
        </section>
      </article>
    </>
  );
};

export default English;
