import React, { useEffect } from 'react';
import KeyVisual from 'components/Home/KeyVisual';
import Section from 'components/Home/Section';
import { sectionList } from 'data/common';
import { useLocation } from 'react-router-dom';

function Home() {
  return (
    <>
      <KeyVisual />
      {sectionList.map((item) => {
        const { id } = item;
        return <Section key={id} item={item} page="home" />;
      })}
    </>
  );
}

export default Home;
