import React from 'react';

import MainPageTemplate from 'components/MainPageTemplate';

function Researches() {
  const pageData = {
    title: '主題研究',
  };
  return (
    <>
      <div className="l-researches">
        <MainPageTemplate pageData={pageData} />
      </div>
    </>
  );
}

export default Researches;
