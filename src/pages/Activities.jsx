import React, { useState, useEffect } from 'react';

import TagTab from 'components/TagTab';
import ContentTemplate from 'components/ContentTemplate';

import img from 'img/icon/activities.svg';

import { activityList } from 'data/activities';

import useRender from 'hooks/useRender';
import usePage from 'hooks/usePage';

function Activities() {
  const tagList = [
    { id: 1, title: '地方活動' },
    { id: 2, title: '學術活動' },
  ];

  const pageData = {
    id: 'activies',
    title: '最新消息',
    icon: img,
    link: '/activities',
    url: 'latestEvents',
  };

  const [filter, setFilter] = useState({
    tag: 0,
    sort: 'dateDescending',
  });
  const [activities, setActivities] = useState(null);
  const [refresh, setRefresh] = useState(false);

  const { getListByPage } = useRender();
  const {
    currentPage,
    setCurrentPage,
    paginationData,
    setPaginationData,
    paginationList,
    isFetchingPages,
  } = usePage();

  const isAllTag = filter.tag === 0;

  useEffect(() => {
    getListByPage({
      url: pageData.url,
      page: currentPage,
      setList: setActivities,
      defaultList: activityList,
      setPaginationData,
      params: { sort: filter.sort, tag: isAllTag ? null : filter.tag },
    });
  }, [currentPage, filter.tag, refresh]);

  return (
    <>
      <ContentTemplate
        pageData={pageData}
        data={activities}
        tab={
          <TagTab
            list={[{ id: 0, title: '全部' }, ...tagList]}
            filter={filter}
            setFilter={setFilter}
            setCurrentPage={setCurrentPage}
          />
        }
        tagList={tagList}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        paginationData={paginationData}
        paginationList={paginationList}
        isFetchingPages={isFetchingPages}
        setRefresh={setRefresh}
      />
    </>
  );
}

export default Activities;
