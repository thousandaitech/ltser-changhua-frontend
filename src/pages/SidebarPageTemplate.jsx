import React from 'react';

import Sidebar from 'components/Sidebar';
import Breadcrumb from 'components/Breadcrumb';

function SidebarPageTemplate(props) {
  const { layout, content } = props;
  return (
    <>
      <section className="u-page">
        <div className="row justify-content-center g-0 w-100">
          <div className="col-10">
            <div className="row gy-3 gx-0 gx-lg-5">
              <div className="col-12 col-lg-3">
                <Sidebar layout={layout} />
              </div>
              <div className="col-12 col-lg-9">
                <Breadcrumb />
                {content}
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default SidebarPageTemplate;
