import React from 'react';
import { useLocation } from 'react-router-dom';

import Breadcrumb from 'components/Breadcrumb';

function PageTemplate(props) {
  const { content } = props;
  const { pathname } = useLocation();
  const paths = pathname.split('/');

  const isHiddenX = paths[paths.length - 2] === 'macrobenthos';
  return (
    <>
      <main className={`u-page ${isHiddenX ? 'overflow-hidden' : ''}`}>
        <div className="row justify-content-center g-0 w-100">
          <div className="col-10">
            <Breadcrumb />
            {content}
          </div>
        </div>
      </main>
    </>
  );
}

export default PageTemplate;
