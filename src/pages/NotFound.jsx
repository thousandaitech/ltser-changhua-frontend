import React from 'react';
import { Link } from 'react-router-dom';

function NotFound() {
  return (
    <>
      <div className="l-not-found">
        <div className="row w-100 d-flex justify-content-center">
          <div className="col-10 col-md-8 col-lg-6 d-flex flex-column justify-content-center align-items-center">
            <h1 className="l-not-found__text">404 Not Found</h1>
            <h2 className="l-not-found__text">糟糕！找不到此頁面。</h2>
            <Link to="/" className="e-btn e-btn--primary">
              回首頁
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}

export default NotFound;
