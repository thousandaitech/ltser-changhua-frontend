import React from 'react';

import Form from 'react-bootstrap/Form';

import contactImg from 'img/home/key-visual/1.jpg';

function Contacts() {
  return (
    <>
      <div className="l-contacts l-section">
        <div className="row">
          <div className="col-6">
            <h1 className="c-title">聯絡我們 Contact us</h1>
            <Form className="c-form">
              <Form.Group className="c-form__set" controlId="name">
                <Form.Label className="c-form__label">
                  聯絡人姓名<span className="c-form__required">*</span>
                </Form.Label>
                <Form.Control
                  type="text"
                  placeholder=""
                  className="c-form__input"
                />
              </Form.Group>
              <Form.Group className="c-form__set" controlId="email">
                <Form.Label className="c-form__label">
                  E-mail<span className="c-form__required">*</span>
                </Form.Label>
                <Form.Control
                  type="email"
                  placeholder=""
                  className="c-form__input"
                />
              </Form.Group>
              <Form.Group className="c-form__set" controlId="title">
                <Form.Label className="c-form__label">
                  主旨<span className="c-form__required">*</span>
                </Form.Label>
                <Form.Control
                  type="text"
                  placeholder=""
                  className="c-form__input"
                />
              </Form.Group>
              <Form.Group className="c-form__set" controlId="content">
                <Form.Label className="c-form__label">您的訊息</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  className="c-form__input c-form__input--textarea"
                />
              </Form.Group>
              <div className="c-form__actions">
                <button type="submit" className="c-form__action e-btn action">
                  送出
                </button>
              </div>
            </Form>
          </div>
          <div className="col-6 d-flex align-items-stretch">
            <div className="l-contact__img">
              <img
                src={contactImg}
                className="e-img e-img--cover"
                alt="contact"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Contacts;
