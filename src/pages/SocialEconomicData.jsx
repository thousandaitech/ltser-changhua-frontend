import React, { useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import ReactECharts from 'echarts-for-react';

import Intro from 'components/Intro';

import overviewMapImg from 'img/social-survey/town-overview-map.png';

import {
  GRAPH_COLOR,
  GRAPH_HEIGHT,
  handleOverviewMapSpec,
  handleResponsive,
} from 'data/common';
import town from 'data/changhua_town_geo.json';
import changhuaFY from 'data/changhua_FY_geo.json';

import { useCustomContext } from 'utils/context';
import useWindowDimensions from 'hooks/useWindowDimensions';

function SocialEconomicData() {
  const { currentPageData } = useCustomContext();
  const navigate = useNavigate();
  const { width } = useWindowDimensions();

  const height = handleResponsive(width, 'mobile')
    ? GRAPH_HEIGHT.md
    : GRAPH_HEIGHT.lg;
  let echartRef = useRef(null);

  const handleAreaClick = (params) => {
    if (params.name === '彰化縣') {
      navigate(`/social-survey/social-economic-data/changhua-county`);
    } else {
      navigate(`/social-survey/social-economic-data/fangyuan-township`);
    }
  };

  const onEvents = {
    click: handleAreaClick,
  };

  const option = {
    series: [
      {
        type: 'map',
        map: 'changhuaFY',
        aspectScale: 0.9,
        selectedMode: 'single',
        zIndex: 2,
        z: 2,
        itemStyle: {
          areaColor: 'transparent',
          borderColor: GRAPH_COLOR.legend.border,
          borderWidth: 3,
        },
        label: {
          show: true,
        },
        emphasis: {
          itemStyle: {
            areaColor: GRAPH_COLOR.legend.style.basic.secondary,
          },
          label: {
            color: GRAPH_COLOR.legend.style.basic.light,
          },
        },
        data: [
          {
            name: '芳苑鄉',
            itemStyle: {
              borderColor: GRAPH_COLOR.legend.border,
              borderWidth: 3,
              areaColor: GRAPH_COLOR.legend.style.basic.primary,
            },
            label: {
              show: true,
              color: GRAPH_COLOR.legend.style.basic.light,
            },
            emphasis: {
              itemStyle: {
                areaColor: GRAPH_COLOR.legend.style.basic.secondary,
              },
            },
          },
        ],
      },
      {
        type: 'map',
        map: 'town',
        aspectScale: 0.9,
        selectedMode: 'single',
        zIndex: 1,
        z: 1,
        itemStyle: {
          areaColor: 'transparent',
        },
        emphasis: {
          itemStyle: {
            areaColor: 'transparent',
          },
          label: {
            show: false,
          },
        },
      },
    ],
    graphic: [
      {
        type: 'group',
        left: '0%',
        top: '0%',
        z: 100,
        children: [
          {
            type: 'rect',
            left: 'center',
            top: 'center',
            shape: {
              ...handleOverviewMapSpec(width),
            },
            style: {
              fill: 'transparent',
              stroke: '#999',
              lineWidth: 5,
            },
          },
          {
            type: 'image',
            left: 'center',
            top: 'center',
            style: {
              image: overviewMapImg,
              ...handleOverviewMapSpec(width),
            },
          },
        ],
      },
    ],
  };

  useEffect(() => {
    if (echartRef.current !== null) {
      const echartInstance = echartRef.current.echarts;
      echartInstance.registerMap('town', JSON.stringify(town));
      echartInstance.registerMap('changhuaFY', JSON.stringify(changhuaFY));
    }
  }, [echartRef.current]);

  return (
    <>
      <div className="l-social-economy-data">
        <div className="l-main">
          <div className="row justify-content-center">
            <Intro data={currentPageData} />
            <div className="col-12 col-md-10 col-lg-8">
              <div className="l-social-economy-data__map">
                <ReactECharts
                  option={option}
                  ref={echartRef}
                  notMerge={true}
                  lazyUpdate={true}
                  opts={{
                    renderer: 'canvas',
                    height,
                  }}
                  style={{ height }}
                  onEvents={onEvents}
                />
              </div>
            </div>
            {/* <div className="col-4">
              <div className="row gy-3">
                {!isFetchingPageData &&
                  currentPageData.list.map((item) => {
                    const { id, title, link, icon } = item;
                    return (
                      <div className="col-12">
                        <Link to={`${pathname}/${link}`}>
                          <div
                            className={`l-section__block l-section__block--gray`}
                          >
                            <div className="l-section__img">
                              <img
                                className="e-img e-img--contain"
                                src={icon}
                                alt={title}
                              />
                            </div>
                            <h5 className="l-section__text">{title}</h5>
                          </div>
                        </Link>
                      </div>
                    );
                  })}
              </div>
            </div> */}
          </div>
        </div>
      </div>
    </>
  );
}

export default SocialEconomicData;
