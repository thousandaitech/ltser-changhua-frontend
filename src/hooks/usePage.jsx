import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';

const usePage = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [paginationData, setPaginationData] = useState({
    currentPage: 1,
    recordsPerPage: 1,
    totalPages: 1,
    totalRecords: 1,
  });
  const [paginationList, setPaginationList] = useState([]);

  const isFetchingPages = paginationList.length === 0;
  const hasNoRecords = paginationData.totalRecords === 0;

  useEffect(() => {
    const pageList = [];
    for (let i = 1; i <= paginationData.totalPages; i++) {
      if (i <= currentPage + 2 && i >= currentPage - 2) {
        pageList.push(i);
      }
    }
    setPaginationList([...pageList]);
  }, [paginationData, currentPage]);

  return {
    currentPage,
    setCurrentPage,
    paginationList,
    paginationData,
    setPaginationData,
    isFetchingPages,
    hasNoRecords,
  };
};

export default usePage;
