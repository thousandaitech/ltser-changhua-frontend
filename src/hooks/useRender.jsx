import React, { useEffect, useState } from 'react';
import { useApi } from './useApi';
import { useAuthContext } from 'context/AuthContext';

const useRender = () => {
  const [, loading, handleApi, handleActions] = useApi();
  // const [loading, setLoading] = useState(false);
  const { auth, authTokens } = useAuthContext();

  const headers = {
    Authorization: `Bearer ${authTokens.access}`,
  };

  const getListByPage = async ({
    url,
    page,
    setList,
    defaultList,
    setPaginationData,
    params,
    setTags,
  }) => {
    const result = await handleApi({
      type: 'api',
      method: 'get',
      url: `/${url}/`,
      params: { page, ...params },
    });
    if (result?.status === 'success') {
      setList([...result.response.data.records]);
      if (setPaginationData) {
        setPaginationData({
          ...Object.fromEntries(
            Object.entries(result.response.data).filter(
              ([key]) => key !== 'records' || key !== 'tags'
            )
          ),
        });
      }
      if (setTags) {
        setTags([...result.response.data.tags]);
      }
    } else {
      if (defaultList) {
        setList([...defaultList]);
      }
    }
  };

  const getListWithBearer = async ({
    url,
    page,
    setList,
    setPaginationData,
    params,
  }) => {
    const result = await handleApi({
      type: 'api',
      method: 'get',
      url: `/${url}/`,
      params: { page, ...params },
      headers: auth ? headers : null,
    });
    if (result?.status === 'success') {
      setList([...result.response.data.records]);
      if (setPaginationData) {
        setPaginationData({
          ...Object.fromEntries(
            Object.entries(result.response.data).filter(
              ([key]) => key !== 'records' || key !== 'tags'
            )
          ),
        });
      }
    } else {
      setList([]);
      handleActions({
        result: result,
        error: {
          title: result.response.data.error
            ? result.response.data.error
            : '發生錯誤，讀取失敗。',
        },
      });
    }
  };

  const patchViews = async ({ url, id, setRefresh }) => {
    await handleApi({
      type: 'api',
      method: 'patch',
      url: `/${url}/${id}/`,
    });
    setRefresh(false);
  };

  const getListReturn = async ({ url, page, params }) => {
    const result = await handleApi({
      type: 'api',
      method: 'get',
      url: `/${url}/`,
      params: { ...params },
    });
    if (result?.status === 'success') {
      return result.response.data.records;
    } else {
      return;
    }
  };

  return {
    loading,
    getListByPage,
    patchViews,
    getListReturn,
    getListWithBearer,
  };
};

export default useRender;
