import { useEffect } from 'react';

export const useFormat = (props) => {
  const { setList, data, termList } = props;

  useEffect(() => {
    const newData = data.map((obj) => {
      termList.forEach((key) => {
        const { oldKey, newKey } = key;
        const isExistKey =
          obj[oldKey] || obj[oldKey] === '' || obj[oldKey] === 0;
        if (isExistKey) {
          Object.defineProperty(
            obj,
            newKey,
            Object.getOwnPropertyDescriptor(obj, oldKey)
          );
          delete obj[oldKey];
        }
      });
      return obj;
    });
    setList([...newData]);
  }, []);

  return {};
};
