import { useState } from 'react';
import { saveAs } from 'file-saver';

import { useApi } from 'hooks/useApi';
import { useAuthContext } from 'context/AuthContext';
import { useNavigate } from 'react-router-dom';
import { SWAL_TOAST } from 'data/common';

export const useDownload = () => {
  const [, loading, handleApi, handleActions] = useApi();
  const { authTokens } = useAuthContext();
  const [progress, setProgress] = useState(0);
  const navigate = useNavigate();

  const headers = {
    Authorization: `Bearer ${authTokens.access}`,
  };

  const handleDownloadAction = ({ result, fileName }) => {
    const blob = new Blob([result.response.data], {
      type: 'application/zip',
    });
    saveAs(blob, `${fileName}.zip`);
  };

  const getDownloadFile = async ({ type = 'api', url, fileName, params }) => {
    const result = await handleApi({
      type,
      method: 'get',
      url: `/download/${url}/`,
      params,
      headers,
      responseType: 'arraybuffer',
      onDownloadProgress: (e) => {
        const percentage = Math.round((e.loaded * 100) / e.total);
        setProgress(percentage);
      },
    });
    if (result?.status === 'success') {
      handleDownloadAction({ result, fileName });
    } else {
      if (result?.response.status === 403) {
        SWAL_TOAST.fire({
          icon: 'warning',
          title: '請先填寫社會調查資料申請表，若已填寫未獲回應請與本站聯絡',
        });
        navigate('/auth/application');
      } else {
        handleActions({
          result,
          error: {
            title: '發生錯誤，檔案讀取失敗。',
          },
        });
      }
    }
  };

  const handleDownload = ({ type, url, fileName, params }) => {
    getDownloadFile({ type, url, fileName, params });
  };

  return { handleDownload, progress, loading };
};
