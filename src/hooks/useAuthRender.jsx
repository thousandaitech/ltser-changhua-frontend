import React, { useState } from 'react';

import { useApi } from 'hooks/useApi';
import { useAuthContext } from 'context/AuthContext';

const useAuthRender = () => {
  const [, , handleApi] = useApi();
  const { auth, authTokens } = useAuthContext();

  const headers = {
    Authorization: `Bearer ${authTokens.access}`,
  };

  const getListByPage = async ({
    type = 'auth',
    url,
    page,
    setList,
    defaultList,
    setPaginationData,
    params,
    setTags,
  }) => {
    const result = await handleApi({
      type,
      method: 'get',
      url: `/${url}/`,
      params: { page, ...params },
      headers: auth ? headers : null,
    });
    if (result?.status === 'success') {
      setList([...result.response.data.records]);
      if (setPaginationData) {
        setPaginationData({
          ...Object.fromEntries(
            Object.entries(result.response.data).filter(
              ([key]) => key !== 'records' || key !== 'tags'
            )
          ),
        });
      }
      if (setTags) {
        setTags([...result.response.data.tags]);
      }
    } else {
      if (defaultList) {
        setList([...defaultList]);
      }
    }
  };

  const patchViews = async ({ url, id, setRefresh }) => {
    await handleApi({
      type: 'api',
      method: 'patch',
      url: `/${url}/${id}/`,
    });
    setRefresh(false);
  };

  return { getListByPage, patchViews };
};

export default useAuthRender;
