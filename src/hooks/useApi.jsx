import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

import { API_URL, AUTH_URL } from 'utils/config';
import { SWAL_TOAST } from 'data/common';

export const useApi = () => {
  const [result, setResult] = useState(null);
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const handleSwalRedirect = (title) => {
    let timerInterval;
    SWAL_TOAST.fire({
      icon: 'warning',
      title: `${title.type}，即將於 <b></b> 秒後${title.action}`,
      timerProgressBar: true,
      timer: 10000,
      didOpen: () => {
        const b = Swal.getTitle().querySelector('b');
        timerInterval = setInterval(() => {
          b.textContent = Math.floor(Swal.getTimerLeft() / 1000);
        }, 100);
      },
      willClose: () => {
        clearInterval(timerInterval);
      },
    });
  };

  const handleApi = async ({
    type,
    method,
    params,
    data,
    url,
    headers,
    responseType,
  }) => {
    setLoading(true);
    try {
      let baseUrl;
      let response;
      switch (type) {
        case 'auth':
          baseUrl = AUTH_URL;
          break;
        case 'api':
        default:
          baseUrl = API_URL;
          break;
      }
      response = await axios({
        method,
        baseURL: baseUrl,
        url,
        params,
        data,
        headers,
        responseType,
      });
      setResult(response);
      return { status: 'success', response };
    } catch (err) {
      if (err.response) {
        setResult(err.response);
        return { status: 'fail', response: err.response };
      }
    } finally {
      setLoading(false);
    }
  };

  const handleActions = (actionParams) => {
    const { result, success, error, action } = actionParams;
    const { status, data } = result;
    const handleAction = () => {
      switch (action?.type) {
        case 'redirect':
          navigate(action.path);
          break;
        case 'scrollTop':
          window.scrollTo({
            top: 0,
            behavior: 'auto',
          });
          break;
        default:
          break;
      }
    };
    switch (status) {
      case 200:
      case 201:
      case 204:
        if (success.title) {
          SWAL_TOAST.fire({
            icon: 'success',
            title: success.title,
          });
        }
        if (success.action) {
          success.action();
        }
        handleAction();
        break;
      case 401:
      default:
        switch (error.type) {
          case 'redirect':
            handleSwalRedirect(error.title);
            break;
          default:
            SWAL_TOAST.fire({
              icon: 'error',
              title: error.title,
            });
            break;
        }
        if (error.action) {
          error.action();
        }
        handleAction();
        break;
    }
  };

  return [result, loading, handleApi, handleActions];
};
