import React, { useState } from 'react';
import { useApi } from './useApi';

const useFetch = () => {
  const [, , handleApi] = useApi();

  const getList = async ({ url, method, setList, params }) => {
    const result = await handleApi({
      type: 'api',
      method,
      url,
      params,
    });
    if (result?.status === 'success') {
      setList([...result.response.data.records]);
    }
  };

  return { getList };
};

export default useFetch;
