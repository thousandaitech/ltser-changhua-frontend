import { createContext, useState, useEffect, useContext } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

import { AUTH_URL } from 'utils/config';

import { SWAL_TOAST } from 'data/common';

const AuthContext = createContext();
export const useAuthContext = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
  const INITIAL_PROFILE = {
    id: '',
    user: {
      username: '',
      email: '',
      first_name: '',
      last_name: '',
    },
    school: '',
    location: '',
    department: '',
    title: '',
    category: '',
    application: '',
    attention: '',
    created: '',
    updated: '',
  };
  const INITIAL_AUTH_TOKENS = {
    access: '',
    refresh: '',
  };
  const [profile, setProfile] = useState({ ...INITIAL_PROFILE });
  const [authTokens, setAuthTokens] = useState(() =>
    localStorage.getItem('authTokens')
      ? JSON.parse(localStorage.getItem('authTokens'))
      : { ...INITIAL_AUTH_TOKENS }
  );
  const [auth, setAuth] = useState(false);
  const [loading, setLoading] = useState(false);
  const [init, setInit] = useState(true);
  const [verificationHint, setVerificationHint] = useState(false);

  const navigate = useNavigate();
  const location = useLocation();
  const { pathname } = location;
  const prevPage = location?.state?.previousPage;

  const isExistTokens = authTokens.access !== '';

  const handleLogin = async (e) => {
    e.preventDefault();
    setLoading(true);
    const formData = new FormData(e.target);
    try {
      const response = await axios({
        method: 'post',
        url: `${AUTH_URL}/login/`,
        data: formData,
      });
      const { status, data } = response;
      if (status === 200) {
        setLoading(false);
        setAuthTokens({ ...data });
        localStorage.setItem('authTokens', JSON.stringify(data));
        navigate('/');
        navigate(prevPage ? prevPage : '/');
        SWAL_TOAST.fire({
          icon: 'success',
          title: '已登入。',
        });
      }
    } catch (err) {
      setLoading(false);
      if (err.response) {
        const { data, status } = err.response;
        switch (status) {
          case 401:
            setVerificationHint(true);
            localStorage.setItem('mailVerification', formData.get('username'));
            SWAL_TOAST.fire({
              icon: 'error',
              title: data.detail,
            });
            break;
          default:
            SWAL_TOAST.fire({
              icon: 'error',
              title: `發生錯誤。`,
            });
        }
      }
    }
  };

  const handleLogout = () => {
    setLoading(true);
    setProfile({ ...INITIAL_PROFILE });
    setAuthTokens({ ...INITIAL_AUTH_TOKENS });
    localStorage.removeItem('authTokens');
    SWAL_TOAST.fire({
      icon: 'success',
      title: '已登出。',
    });
    navigate('/auth');
    setLoading(false);
  };

  const handleUpdateTokens = async () => {
    try {
      const response = await axios({
        method: 'post',
        url: `${AUTH_URL}/login/refresh/`,
        data: {
          refresh: authTokens?.refresh,
        },
      });
      const { status, data } = response;
      if (status === 200) {
        setAuthTokens({ ...authTokens, ...data });
        localStorage.setItem('authTokens', JSON.stringify(authTokens));
        console.log('Token updated.');
        if (init) {
          setInit(false);
        }
      }
    } catch (err) {
      let timerInterval;
      SWAL_TOAST.fire({
        icon: 'warning',
        title: `更新權限錯誤，即將於 <b></b> 秒後登出，請重新登入。`,
        timerProgressBar: true,
        timer: 5000,
        didOpen: () => {
          const b = Swal.getTitle().querySelector('b');
          timerInterval = setInterval(() => {
            b.textContent = Math.floor(Swal.getTimerLeft() / 1000);
          }, 100);
        },
        willClose: () => {
          clearInterval(timerInterval);
          handleLogout();
        },
      });
    }
  };

  const handleGetProfile = async () => {
    try {
      const response = await axios({
        method: 'get',
        url: `${AUTH_URL}/getUserProfile`,
        headers: {
          Authorization: `Bearer ${authTokens.access}`,
        },
      });
      const { status, data } = response;
      if (status === 200) {
        setProfile({ ...data });
      }
    } catch (err) {
      let timerInterval;
      SWAL_TOAST.fire({
        icon: 'warning',
        title: `沒有權限或已過期，即將於 <b></b> 秒後登出，請重新登入。`,
        timerProgressBar: true,
        timer: 5000,
        didOpen: () => {
          const b = Swal.getTitle().querySelector('b');
          timerInterval = setInterval(() => {
            b.textContent = Math.floor(Swal.getTimerLeft() / 1000);
          }, 100);
        },
        willClose: () => {
          clearInterval(timerInterval);
          handleLogout();
        },
      });
    }
  };

  useEffect(() => {
    setVerificationHint(false);
  }, [pathname]);

  useEffect(() => {
    if (isExistTokens) {
      setAuth(true);
    } else {
      setAuth(false);
    }
  }, [authTokens.access]);

  useEffect(() => {
    const time = 11.9 * 60 * 60 * 1000;
    const interval = setInterval(() => {
      if (isExistTokens) {
        handleUpdateTokens();
      }
    }, time);
    return () => clearInterval(interval);
  }, [authTokens, init]);

  useEffect(() => {
    if (auth) {
      handleGetProfile();
    }
  }, [auth]);

  useEffect(() => {
    if (isExistTokens) {
      if (init) {
        handleUpdateTokens();
      }
    } else {
      setInit(false);
    }
  }, []);

  const contextData = {
    auth: auth,
    handleLogin: handleLogin,
    handleLogout: handleLogout,
    profile: profile,
    authTokens: authTokens,
    loading: loading,
    verificationHint: verificationHint,
    handleGetProfile,
  };

  return (
    <AuthContext.Provider value={contextData}>
      {init ? null : children}
    </AuthContext.Provider>
  );
};
