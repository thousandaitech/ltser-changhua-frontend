import { createContext, useState, useEffect, useContext } from 'react';
import { WEB_SOCKET_URL } from 'utils/config';
import { useAuthContext } from './AuthContext';
import Swal from 'sweetalert2';
import { SWAL_TOAST } from 'data/common';

const WebSocketContext = createContext();
export const useWebSocketContext = () => useContext(WebSocketContext);

export const WebSocketProvider = ({ children }) => {
  const [message, setMessage] = useState('');
  const [socket, setSocket] = useState(null);

  const { profile, handleGetProfile } = useAuthContext();
  const isFetchingUsername = profile?.user?.username === '';

  const hasMessage = message !== '';

  useEffect(() => {
    if (!isFetchingUsername) {
      const ws = new WebSocket(
        `ws://${WEB_SOCKET_URL}/ws/user/${profile?.user?.username.replace(
          '@',
          '/'
        )}/`
      );

      ws.onopen = () => {
        console.log('WebSocket connected');
      };

      ws.onmessage = (e) => {
        console.log(e);
        const result = JSON.parse(e.data);
        console.log(message);
        setMessage(result.message);
      };

      ws.onclose = (e) => {
        console.log('WebSocket disconnected:', e);
        if (e.code !== 1000) {
          console.error('WebSocket closed unexpectedly:', e);
        }
      };

      ws.onerror = (err) => {
        console.log('WebSocket readyState:', ws.readyState);
        console.error('WebSocket error:', err);
      };

      setSocket(ws);

      return () => {
        if (ws) ws.close();
      };
    }
  }, [profile]);

  useEffect(() => {
    if (hasMessage) {
      handleGetProfile();
    }
    return () => {
      setMessage('');
    };
  }, [message]);

  const contextData = { message, socket, hasMessage, setMessage };

  return (
    <WebSocketContext.Provider value={contextData}>
      {children}
    </WebSocketContext.Provider>
  );
};
