import { createContext, useContext, useState } from 'react';
import axios from 'axios';

import { API_URL } from 'utils/config';

const MacrobenthosContext = createContext();
export const useMacrobenthosContext = () => useContext(MacrobenthosContext);

export const MacrobenthosProvider = ({ children }) => {
  const [sites, setSites] = useState([]);
  const [activeSite, setActiveSite] = useState('');
  const [activeSiteData, setActiveSiteData] = useState({
    id: 0,
    year: '',
    site: '',
    month: '',
    cw: '',
    mm: '',
    sc: '',
    co: '',
    s_temp: '',
    t_sal: '',
    s_ph: '',
  });
  const [siteMacrobenthosSeries, setSiteMacrobenthosSeries] = useState([]);
  const [graphData, setGraphData] = useState({
    year: '',
    Al: 0,
    Charybids_sp: 0,
    Gb: 0,
    Hd: 0,
    Hf: 0,
    Hp: 0,
    If: 0,
    It: 0,
    Ma: 0,
    Mb: 0,
    Mbr: 0,
    Me: 0,
    Mt: 0,
    Mv: 0,
    Oc: 0,
    Pa: 0,
    Pp: 0,
    Ppi: 0,
    Sb: 0,
    Sl: 0,
    Ta: 0,
    Tc: 0,
    Xf: 0,
  });

  const getSiteList = async () => {
    try {
      const response = await axios.get(`${API_URL}/getCrabSites`);
      const result = response.data;
      setSites([...result]);
    } catch (err) {
      console.log(err);
    }
  };

  const getSiteContent = async () => {
    try {
      const response = await axios.get(
        `${API_URL}/getBenthicOrganisms?site=${activeSite}`
      );
      const result = response.data[response.data.length - 1];
      const matchKey = Object.keys(activeSiteData).reduce((target, key) => {
        if (key in result) {
          target[key] = result[key];
        }
        return target;
      }, {});
      setActiveSiteData({ ...matchKey });
    } catch (err) {
      console.log(err);
    }
  };

  const getSiteMacrobenthosSeries = async () => {
    try {
      const response = await axios.get(
        `${API_URL}/getCrabs?site=${activeSite}`
      );
      setSiteMacrobenthosSeries([...response.data]);
    } catch (err) {
      console.log(err);
    }
  };

  const contextData = {
    sites,
    activeSite,
    setActiveSite,
    activeSiteData,
    graphData,
    setGraphData,
    siteMacrobenthosSeries,
    getSiteList,
    getSiteContent,
    getSiteMacrobenthosSeries,
  };

  return (
    <MacrobenthosContext.Provider value={contextData}>
      {children}
    </MacrobenthosContext.Provider>
  );
};
