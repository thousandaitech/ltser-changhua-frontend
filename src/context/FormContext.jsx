import { createContext, useContext } from 'react';

const FormContext = createContext();
export const useFormContext = () => useContext(FormContext);

export const FormProvider = ({ children }) => {
  const handleInitialState = (fieldList) => {
    return fieldList
      .map((v) => {
        const { id, type } = v;
        switch (type) {
          case 'text':
          case 'email':
          case 'password':
          case 'select':
            return { [id]: '' };
          case 'checkbox':
            return { [id]: [] };
          default:
            return;
        }
      })
      .reduce((obj, item) => {
        return { ...obj, ...item };
      }, {});
  };
  const contextData = {
    handleInitialState: handleInitialState,
  };

  return (
    <FormContext.Provider value={contextData}>{children}</FormContext.Provider>
  );
};
