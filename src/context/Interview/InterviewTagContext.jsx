import { createContext, useContext, useState } from 'react';

const InterviewTagContext = createContext();
export const useInterviewTagContext = () => useContext(InterviewTagContext);

export const InterviewTagProvider = ({ children }) => {
  const [tag2List, setTag2List] = useState(null);
  const [tag3List, setTag3List] = useState(null);
  const [stakeholderList, setStakeholderList] = useState(null);
  const [categoryList, setCategoryList] = useState(null);

  const contextData = {
    tag2List,
    setTag2List,
    tag3List,
    setTag3List,
    stakeholderList,
    setStakeholderList,
    categoryList,
    setCategoryList,
  };

  return (
    <InterviewTagContext.Provider value={contextData}>
      {children}
    </InterviewTagContext.Provider>
  );
};
