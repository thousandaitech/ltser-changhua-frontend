import { createContext, useContext, useState } from 'react';

const MultipleContext = createContext();
export const useMultipleContext = () => useContext(MultipleContext);

export const MultipleProvider = ({ children }) => {
  const initialMultipleFilter = {
    tag: [],
  };
  const initialMultipleTag = {
    tag2: [],
    tag3: [],
    stakeholder: [],
  };

  const [multipleFilter, setMultipleFilter] = useState({
    ...initialMultipleFilter,
  });
  const [multipleTag, setMultipleTag] = useState({ ...initialMultipleTag });

  const contextData = {
    initialFilter: initialMultipleFilter,
    initialTag: initialMultipleTag,
    filter: multipleFilter,
    setFilter: setMultipleFilter,
    tag: multipleTag,
    setTag: setMultipleTag,
  };

  return (
    <MultipleContext.Provider value={contextData}>
      {children}
    </MultipleContext.Provider>
  );
};
