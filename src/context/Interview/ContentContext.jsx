import usePage from 'hooks/usePage';
import { createContext, useContext, useState } from 'react';

const ContentContext = createContext();
export const useContentContext = () => useContext(ContentContext);

export const ContentProvider = ({ children }) => {
  const GRID_CLASS = {
    gutter: 'g-2',
    col: 'col-12 col-md-6 col-xl-4',
  };

  const [interviews, setInterviews] = useState(null);
  const [initial, setInitial] = useState(true);
  const [activeTab, setActiveTab] = useState(1);

  const isFetchingList = interviews === null;
  const hasInterviews = !isFetchingList && interviews.length !== 0;

  const {
    currentPage,
    setCurrentPage,
    paginationData,
    paginationList,
    isFetchingPages,
    setPaginationData,
  } = usePage();

  const contextData = {
    GRID_CLASS,
    interviews,
    setInterviews,
    isFetchingList,
    currentPage,
    setCurrentPage,
    paginationData,
    paginationList,
    isFetchingPages,
    setPaginationData,
    initial,
    setInitial,
    activeTab,
    setActiveTab,
    hasInterviews,
  };

  return (
    <ContentContext.Provider value={contextData}>
      {children}
    </ContentContext.Provider>
  );
};
