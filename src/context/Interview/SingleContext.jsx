import { createContext, useContext, useState } from 'react';

const SingleContext = createContext();
export const useSingleContext = () => useContext(SingleContext);

export const SingleProvider = ({ children }) => {
  const initialSingleFilter = {
    tag3: [],
    time: {
      start: {
        year: '',
        month: '',
      },
      end: {
        year: '',
        month: '',
      },
    },
    interviewee: {
      category: '',
      id: '',
    },
  };

  const [singleFilter, setSingleFilter] = useState({
    ...initialSingleFilter,
  });

  const contextData = {
    initialFilter: initialSingleFilter,
    filter: singleFilter,
    setFilter: setSingleFilter,
  };

  return (
    <SingleContext.Provider value={contextData}>
      {children}
    </SingleContext.Provider>
  );
};
