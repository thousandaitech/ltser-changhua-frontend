import { createContext, useContext, useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';

const SignupContext = createContext();
export const useSignupContext = () => useContext(SignupContext);

export const SignupProvider = ({ children }) => {
  const [step, setStep] = useState(1);
  const location = useLocation();
  const token = new URLSearchParams(location.search).get('token');
  const isOnMailVerifyingPage =
    location.pathname === '/mail-verification' ||
    location.pathname === '/mail-verification/';
  const isMailVerifying =
    localStorage.getItem('mailVerification') !== null ||
    (isOnMailVerifyingPage && token);

  useEffect(() => {
    if (!isMailVerifying) {
      setStep(1);
    }
  }, [location.pathname]);

  const contextData = {
    step: step,
    setStep: setStep,
    token: token,
    isMailVerifying: isMailVerifying,
  };

  return (
    <SignupContext.Provider value={contextData}>
      {children}
    </SignupContext.Provider>
  );
};
