import { Outlet, Navigate } from 'react-router-dom';

import { useAuthContext } from 'context/AuthContext';

const ProtectedRoute = ({ redirectPath = '/login', children }) => {
  const { auth } = useAuthContext();

  if (!auth) {
    return <Navigate to={redirectPath} replace />;
  }
  return children ? children : <Outlet />;
};

export default ProtectedRoute;
