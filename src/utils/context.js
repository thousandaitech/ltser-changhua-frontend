import { createContext, useContext } from 'react';

const CustomContext = createContext();

function useCustomContext() {
  return useContext(CustomContext);
}

export { CustomContext, useCustomContext };
