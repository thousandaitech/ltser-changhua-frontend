export const BE_URL = `https://www.ltserchanghua-fy.org`;
export const API_URL = `${BE_URL}/api`;
export const AUTH_URL = `${BE_URL}/auth`;
export const WEB_SOCKET_URL = `www.ltserchanghua-fy.org`;
export const LANDSCAPE_TILE_URL = `${BE_URL}/media/landscape`;
export const LANDSCAPE_TILE_SUFFIX = `/{z}/{x}/{y}.png`;
export const IMAGE_URL = BE_URL;
export const CWA_URL = `https://opendata.cwa.gov.tw/`;
export const CWA_AUTH = `CWB-9AADAE46-0408-4966-8B54-300990E6B8A7`;
export const SERVICE_EMAIL = `ltserchanghua@gmail.com`;

export const RECAPTCHA_V2_SITE_KEY = '6LdJ2u8lAAAAACnIMnnhJ1p_kRvN5szgqxugoM74';
export const RECAPTCHA_V2_SECRET_KEY =
  '6LdJ2u8lAAAAANocan8OL_kOIHHsCQDFjfRvia8-';

export const MOENV_API = `https://data.moenv.gov.tw/api/v2/`;
export const MOENV_API_KEY = '5b537c83-6b6a-4802-bcdf-279368ad172e';
export const WMTS_MAP_URL = `https://wmts.nlsc.gov.tw/wmts/EMAP98/default/GoogleMapsCompatible/{z}/{y}/{x}`;
