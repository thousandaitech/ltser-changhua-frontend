import React, { useState } from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';

// Pages
import Home from 'pages/Home';
import SidebarPageTemplate from 'pages/SidebarPageTemplate';
import PageTemplate from 'pages/PageTemplate';
import Contacts from 'pages/Contacts';
import Activities from 'pages/Activities';
import SocialEconomicData from 'pages/SocialEconomicData';
import LocalLiteratureAndNews from 'pages/LocalLiteratureAndNews';
import RelatedResearches from 'pages/RelatedResearches';
import NotFound from 'pages/NotFound';
import Member from 'pages/Auth/Member';
import Record from 'pages/Auth/Record';

// Common
import Header from 'components/Header/Content';
import Footer from 'components/Footer';
import Section from 'components/Home/Section';
import MainPageTemplate from 'components/MainPageTemplate';

// About
import BackgroundAndPurposes from 'components/About/BackgroundAndPurposes';
import Researcher from 'components/About/Researcher';

// EcoEnvironmentalSurvey
import MacrobenthosContent from 'components/EcoEnvironmentalSurvey/Macrobenthos/Content';
import MacrobenthosDetail from 'components/EcoEnvironmentalSurvey/Macrobenthos/Detail';
import WaterQualityContent from 'components/EcoEnvironmentalSurvey/WaterQuality/Content';
import AirQualityContent from 'components/EcoEnvironmentalSurvey/AirQuality/Content';

// SocialSurvey
import NewsContent from 'components/SocialSurvey/News/Content';
import LocalLiteratureContent from 'components/SocialSurvey/LocalLiterature/Content';
import InterviewsContent from 'components/SocialSurvey/Interviews/Content';
import IssueContent from 'components/SocialSurvey/Issue/Content';
import TownContent from 'components/SocialSurvey/SocialEconomicData/TownContent';
import VillageContent from 'components/SocialSurvey/SocialEconomicData/VillageContent';
import QuestionnaireSurveyContent from 'components/SocialSurvey/QuestionnaireSurvey/Content';

// Landscapes
import Landscapes from 'components/Landscapes/Content';

// Auth
import Login from 'components/Auth/Login';
import SignupContent from 'components/Auth/Signup/Content';
import MailVerification from 'components/Auth/Signup/MailVerification';
import ChangePassword from 'components/Auth/ChangePassword';
import ForgotPasswordContent from 'components/Auth/ForgotPassword/Content';
import ResetPassword from 'components/Auth/ForgotPassword/ResetPassword';

// Data
import { sectionList } from 'data/common';

// Utils
import { CustomContext } from 'utils/context';
import ProtectedRoute from 'utils/ProtectedRoute';

// Context
import { AuthProvider } from 'context/AuthContext';
import { FormProvider } from 'context/FormContext';
import { SignupProvider } from 'context/SignupContext';
import { MacrobenthosProvider } from 'context/MacrobenthosContext';
import { MultipleProvider } from 'context/Interview/MultipleContext';
import { InterviewTagProvider } from 'context/Interview/InterviewTagContext';
import { SingleProvider } from 'context/Interview/SingleContext';
import { ContentProvider } from 'context/Interview/ContentContext';
import Application from 'pages/Auth/Application';
import English from 'pages/English';
import PrivacyPolicy from 'pages/PrivacyPolicy';
import MemberTerms from 'pages/MemberTerms';
import Researches from 'pages/Researches';
import { WebSocketProvider } from 'context/WebSocketContext';
import VerificationHint from 'components/VerificationHint';

function App() {
  const [isLogin, setIsLogin] = useState(false);
  const [breadcrumbItem, setBreadcrumbItem] = useState([]);
  const [breadcrumbLink, setBreadcrumbLink] = useState([]);
  const [breadcrumbParam, setBreadcrumbParam] = useState([]);
  const [filterMap, setFilterMap] = useState([]);
  const [site, setSite] = useState('');
  const [currentPageData, setCurrentPageData] = useState([]);

  const handleMatchedSection = (title, customStyle) => {
    const matchedSection = sectionList.find((item) => item.title === title);
    return (
      <Section
        item={matchedSection}
        page="page"
        cardStyle={`l-section__block--${customStyle}`}
      />
    );
  };

  return (
    <>
      <CustomContext.Provider
        value={{
          breadcrumbItem,
          setBreadcrumbItem,
          breadcrumbLink,
          setBreadcrumbLink,
          breadcrumbParam,
          setBreadcrumbParam,
          filterMap,
          setFilterMap,
          site,
          setSite,
          currentPageData,
          setCurrentPageData,
          isLogin,
          setIsLogin,
        }}
      >
        <BrowserRouter>
          <AuthProvider>
            <WebSocketProvider>
              <SignupProvider>
                <Header />
                <Routes>
                  <Route path="/" element={<Home />} />
                  <Route
                    path="/about"
                    element={<PageTemplate content={<MainPageTemplate />} />}
                  />
                  <Route
                    path="/about/background-and-purposes"
                    element={
                      <SidebarPageTemplate
                        content={<BackgroundAndPurposes />}
                        layout={null}
                      />
                    }
                  />
                  <Route
                    path="/about/researcher"
                    element={
                      <SidebarPageTemplate
                        content={<Researcher />}
                        layout={null}
                      />
                    }
                  />
                  <Route
                    path="/activities/*"
                    element={<PageTemplate content={<Activities />} />}
                  />
                  <Route
                    path="/eco-environmental-survey"
                    element={
                      <PageTemplate
                        content={handleMatchedSection('環境生態調查', 'light')}
                      />
                    }
                  />
                  <Route
                    path="/eco-environmental-survey/macrobenthos"
                    element={
                      <MacrobenthosProvider>
                        <PageTemplate content={<MacrobenthosContent />} />
                      </MacrobenthosProvider>
                    }
                  />
                  <Route
                    path="/eco-environmental-survey/macrobenthos/:macrobenthosId"
                    element={
                      <MacrobenthosProvider>
                        <PageTemplate content={<MacrobenthosDetail />} />
                      </MacrobenthosProvider>
                    }
                  />
                  <Route
                    path="/eco-environmental-survey/water-quality"
                    element={
                      <MacrobenthosProvider>
                        <PageTemplate content={<WaterQualityContent />} />
                      </MacrobenthosProvider>
                    }
                  />
                  <Route
                    path="/eco-environmental-survey/air-quality"
                    element={
                      <MacrobenthosProvider>
                        <PageTemplate content={<AirQualityContent />} />
                      </MacrobenthosProvider>
                    }
                  />
                  <Route
                    path="/social-survey"
                    element={
                      <PageTemplate
                        content={handleMatchedSection('社會調查', 'gray')}
                      />
                    }
                  />
                  <Route
                    path="/social-survey/social-economic-data"
                    element={<PageTemplate content={<SocialEconomicData />} />}
                  />
                  <Route
                    path="/social-survey/social-economic-data/changhua-county"
                    element={
                      <SidebarPageTemplate
                        content={<TownContent />}
                        layout={null}
                      />
                    }
                  />
                  <Route
                    path="/social-survey/social-economic-data/fangyuan-township"
                    element={
                      <SidebarPageTemplate
                        content={<VillageContent />}
                        layout={null}
                      />
                    }
                  />
                  <Route
                    path="/social-survey/interviews"
                    element={
                      <InterviewTagProvider>
                        <ContentProvider>
                          <MultipleProvider>
                            <SingleProvider>
                              <PageTemplate
                                content={<InterviewsContent />}
                                layout={null}
                              />
                            </SingleProvider>
                          </MultipleProvider>
                        </ContentProvider>
                      </InterviewTagProvider>
                    }
                  />
                  <Route
                    path="/social-survey/local-literature-and-news"
                    element={
                      <PageTemplate content={<LocalLiteratureAndNews />} />
                    }
                  />
                  <Route
                    path="/social-survey/local-literature-and-news/news"
                    element={
                      <SidebarPageTemplate
                        content={<NewsContent />}
                        layout={null}
                      />
                    }
                  />
                  <Route
                    path="/social-survey/local-literature-and-news/local-literature"
                    element={
                      <SidebarPageTemplate
                        content={<LocalLiteratureContent />}
                        layout={null}
                      />
                    }
                  />
                  <Route
                    path="/social-survey/questionnaire-survey"
                    element={
                      <PageTemplate
                        content={<QuestionnaireSurveyContent />}
                        layout={null}
                      />
                    }
                  />
                  <Route
                    path="/researches"
                    element={<PageTemplate content={<Researches />} />}
                  />
                  <Route
                    path="/researches/issue"
                    element={
                      <SidebarPageTemplate
                        content={<IssueContent />}
                        layout={null}
                      />
                    }
                  />
                  <Route
                    path="/researches/related-researches"
                    element={
                      <SidebarPageTemplate
                        content={<RelatedResearches />}
                        layout={null}
                      />
                    }
                  />
                  <Route path="/landscapes" element={<Landscapes />} />
                  <Route
                    path="/landscapes/:categoryId"
                    element={<Landscapes />}
                  />
                  <Route
                    path="/contacts"
                    element={<PageTemplate content={<Contacts />} />}
                  />
                  <Route
                    path="/login"
                    element={
                      <FormProvider>
                        <PageTemplate content={<Login />} />
                      </FormProvider>
                    }
                  />
                  <Route
                    path="/signup"
                    element={
                      <FormProvider>
                        <PageTemplate content={<SignupContent />} />
                      </FormProvider>
                    }
                  />
                  <Route
                    path="/mail-verification"
                    element={
                      <FormProvider>
                        <PageTemplate content={<MailVerification />} />
                      </FormProvider>
                    }
                  />
                  <Route
                    path="/login/forgot-password"
                    element={
                      <FormProvider>
                        <PageTemplate content={<ForgotPasswordContent />} />
                      </FormProvider>
                    }
                  />
                  <Route
                    path="/login/reset-password"
                    element={
                      <FormProvider>
                        <PageTemplate content={<ResetPassword />} />
                      </FormProvider>
                    }
                  />
                  <Route element={<ProtectedRoute />}>
                    <Route
                      path="/auth/member"
                      element={
                        <FormProvider>
                          <SidebarPageTemplate content={<Member />} />
                        </FormProvider>
                      }
                    />
                    <Route
                      path="/auth/record"
                      element={<SidebarPageTemplate content={<Record />} />}
                    />
                    <Route
                      path="/auth/application"
                      element={
                        <SidebarPageTemplate content={<Application />} />
                      }
                    />
                    <Route
                      path="/auth/change-password"
                      element={
                        <FormProvider>
                          <SidebarPageTemplate content={<ChangePassword />} />
                        </FormProvider>
                      }
                    />
                    <Route
                      path="/auth/*"
                      element={<Navigate to="/auth/member" replace />}
                    />
                  </Route>
                  <Route
                    path="/english/about-us"
                    element={<SidebarPageTemplate content={<English />} />}
                  />
                  <Route
                    path="/privacy-policy"
                    element={<PageTemplate content={<PrivacyPolicy />} />}
                  />
                  <Route
                    path="/member-terms"
                    element={<PageTemplate content={<MemberTerms />} />}
                  />
                  <Route path="*" element={<NotFound />} />
                </Routes>
                <VerificationHint />
                <Footer />
              </SignupProvider>
            </WebSocketProvider>
          </AuthProvider>
        </BrowserRouter>
      </CustomContext.Provider>
    </>
  );
}

export default App;
