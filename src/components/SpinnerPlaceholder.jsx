import React from 'react';

import Spinner from './Spinner';

const SpinnerPlaceholder = (props) => {
  const { layout, text } = props;
  return (
    <>
      <div className={`c-placeholder c-placeholder--${layout}`}>
        <div className="c-placeholder__icon">
          <Spinner />
          <span className="c-placeholder__text">{text}...</span>
        </div>
      </div>
    </>
  );
};

export default SpinnerPlaceholder;
