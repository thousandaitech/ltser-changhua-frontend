import React, { useRef, useEffect } from 'react';
import { useLocation } from 'react-router-dom';

import axios from 'axios';
import ReCAPTCHA from 'react-google-recaptcha';

import { RECAPTCHA_V2_SITE_KEY, RECAPTCHA_V2_SECRET_KEY } from 'utils/config';
import { SWAL_TOAST } from 'data/common';

const ReCaptchaLayout = (props) => {
  const { setValidated } = props;
  const { recaptchaRef } = useRef;
  const { pathname } = useLocation();

  const handleVerify = async (token) => {
    try {
      setValidated(true);
      // const response = await axios({
      //   method: 'post',
      //   url: `https://www.google.com/recaptcha/api/siteverify`,
      //   params: {
      //     secret: RECAPTCHA_V2_SECRET_KEY,
      //     response: token,
      //   },
      // });
      // const { success } = response.data;
      // if (success) {
      //   setValidated(true);
      // } else {
      //   setValidated(false);
      // }
    } catch (err) {
      SWAL_TOAST.fire({
        icon: 'success',
        title: '發生錯誤，無法完成reCAPTCHA驗證。',
      });
    }
  };

  const handleExpired = () => {
    setValidated(false);
  };

  useEffect(() => {
    if (recaptchaRef && recaptchaRef.current) {
      recaptchaRef.current.reset();
    }
  }, [pathname, recaptchaRef]);

  return (
    <>
      <div className="d-flex justify-content-center">
        <ReCAPTCHA
          ref={recaptchaRef}
          sitekey={RECAPTCHA_V2_SITE_KEY}
          onChange={handleVerify}
          onExpired={handleExpired}
        />
      </div>
    </>
  );
};

export default ReCaptchaLayout;
