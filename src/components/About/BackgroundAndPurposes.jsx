import React, { useState } from 'react';

import bgImg from 'img/about/background/project-background.jpg';

function BackgroundAndPurposes() {
  const [lang, setLang] = useState('cn');
  const languages = [
    {
      id: 1,
      title: '中文',
      value: 'cn',
    },
    {
      id: 2,
      title: 'English',
      value: 'en',
    },
  ];

  const handleLang = (e) => {
    setLang(e.target.attributes['data-value'].value);
  };
  return (
    <>
      {/* <div className="c-tabs">
        {languages.map((item) => {
          const { id, title, value } = item;
          return (
            <button
              key={id}
              type="button"
              data-value={value}
              className={`e-link c-tabs__tab ${
                lang === value ? 'c-tabs__tab--active' : ''
              }`}
              onClick={handleLang}
            >
              {title}
            </button>
          );
        })}
      </div> */}
      <article className="c-article">
        <h5 className="c-article__title">
          {lang === 'cn' ? `《計畫背景》` : `Project Background`}
        </h5>
        <p className="c-article__paragraph">
          {lang === 'cn'
            ? `人類對資源的需求和環境價值觀的改變會導致土地利用和地景的變化，經濟和人口壓力往往會對社會、經濟和環境的永續性產生衝擊。生態系統服務功能長期被低估使人類的生存往不永續的方向傾斜，但人類也透過能不斷修正與環境互動的方式。`
            : `The human need for resources and the transformation of environmental values will cause the change of land use and landscape, while economic and population pressures will often generate impacts on the social, economic, and environmental sustainability. The fact that ecosystem services have been undervalued results in biased human survival towards an unsustainable way, but humans also constantly amend their way to interact with environment.`}
        </p>
        <p className="c-article__paragraph">
          {lang === 'cn'
            ? `長期的社會-生態系統觀測研究不僅能累積有助於了解臺灣的人地互動資訊、更能提供探索環境生態議題與永續治理的知識基礎；為對臺灣社會生態系的特殊性有全面的理解，科技部(因應組織調整改制為國家科學及技術委員會，簡稱國科會)推動「長期社會生態核心觀測站(LTSER)」納入共用核心設施項目，對核心基礎資料項目進行觀監測，讓科學家可共享觀測資源，讓資源運用最大化，以利研究人員將有限資源進行有效率分配及使用，深化社會生態系統研究，引導社會邁向永續發展。`
            : ``}
        </p>
        <p className="c-article__paragraph">
          {lang === 'cn'
            ? `臺灣西南海域終年光照充足，溫度適中，加上降雨量多，有機質沖刷旺盛，沿海基礎生產力高，能量流動和物質循環途徑複雜。唯伴隨人口成長和土地開發，使臺灣西部海岸地高度水泥化、人工化。近年來水域問題日益嚴重，包括下游城市下水道處理不足、大型工業區用水與農業用水的衝突以及環境汙染等問題，加上沿海養殖抽地下水、近海漁獲銳減、全球漁業危機與氣候變遷等議題。其中全球氣候變遷影響深遠。海洋增溫透過洋流，對世界各地的氣候造成長期而持續的衝擊。大氣中的過量水氣，更使得雨量驟增，極端事件頻傳，對陸地生態造成直接或間接危害。`
            : ``}
        </p>
        <p className="c-article__paragraph">
          {lang === 'cn'
            ? `海岸社區受自然環境的限制，農業以經濟價值較低的旱作居多，社會經濟條件不利。彰化沿海的傳統聚落常為半農半漁生產模式，長期在自然因素與養殖漁業的需求影響下，抽取地下水而導致本區面臨地層下陷的威脅；又因缺少二三級產業的就業機會，導致社會淨遷出上升及人口老化，也使得部分農地廢耕。除了鄰近工業區開發對沿海生態的影響，以及地層下陷的問題之外，近期陸續設置的沿海離岸風機和光電板也將進一步對近海與沿岸生態系產生影響，並對近海捕撈、養殖漁業與聚落發展等社會經濟面產生衝擊。`
            : ``}
        </p>
        <p className="c-article__paragraph">
          {lang === 'cn'
            ? `彰化沿海地區的地景與生態系統變遷是臺灣西南沿岸發展的縮影，沿海地區過去數十年已發展多個填海造陸的海埔新生地，其土地使用多為工業生產及養殖漁業為主，近期沿岸陸續開發魚塭光電與陸域風機等設施。風力與太陽能發電為臺灣能源轉型的里程碑，透過再生能源之開發，以滿足與日俱增之工業生產與都市消費生活所需。這些超越在地脈絡的社會發展需求，將改變彰化沿海的自然與社會地景，影響社會生態系統的運作。`
            : ``}
        </p>
        <p className="c-article__paragraph">
          {lang === 'cn'
            ? `此外，氣候變遷、降雨型態改變，加上河川上游土地利用與工業發展帶來的污染，亦衝擊著沿海濕地生態系統。目前彰化沿海雖有各種不同時期與目的之調查報告，唯社會與生態鑲嵌的交互作用需要透過長時間的資料累積。因此，彰化沿海地區社會生態系統的核心基礎觀測甚為重要，這不只是彰化沿海在地所進行社會生態系統演變的監測，也是臺灣能源轉型下對社會生態衝擊的重要基礎觀測，能提供未來思考與解決社會生態問題的重要決策參考資訊。`
            : ``}
        </p>
        <div className="c-article__img">
          <img
            className="e-img e-img--cover"
            src={bgImg}
            alt="project-background"
          />
          <div className="c-article__caption">
            {lang === 'cn'
              ? `彰化縣芳苑、大城沿海社會生態系統及周邊環境衝擊示意圖`
              : ``}
          </div>
        </div>
      </article>
      <article className="c-article">
        <h5 className="c-article__title">
          {lang === 'cn' ? `《成立目的》` : `Purposes of the Project`}
        </h5>
        <p className="c-article__paragraph">
          {lang === 'cn'
            ? `本計畫考量彰化海岸區域的獨特性、過去累積的研究成果、相關在地產業、在地相關權益關係人，選定以芳苑為觀測核心，建立以地方為本之示範性觀測研究，進行長期基礎且關鍵的社會生態系統核心項目之監測，為沿岸濕地生態系統與農漁社會地景及地方永續性提供堅實的科學證據，並從中累積觀測經驗，建立長期觀測項目的標準作業程序。綜上，本站具體工作包含：盤點現有研究和調查資料、整理分析社會生態議題、進行核心項目觀測、串連權益關係人，建立以地方為基礎的核心項目資料庫。`
            : ``}
        </p>
        <section>
          {lang === 'cn' ? `整體計畫目的包含：` : ``}
          <ol className="c-article__ol c-article__ol--title">
            <li className="c-article__oli">
              {lang === 'cn'
                ? `
              促進綠能發展與社會生態永續的平衡`
                : `Promoting the balance between greening energy development and
              social-ecological sustainability.`}
              <p className="c-article__content">
                {lang === 'cn'
                  ? `聯合國政府間氣候變遷專門委員會（Intergovernmental Panel on Climate Change, IPCC）於2021年8月9日公布氣候變遷第六次評估報告（IPCC AR6)更加確認人類對大氣、海洋及陸地暖化的影響；報告中也提到若全球達成2050 年淨零碳排的目標，綠能發展是減緩氣候變遷的必要手段。唯風機與光電板的設置對既有社會生態必然產生影響，需透過長期科學資料進行監測，了解綠能發展與社會生態的互動，以利後續能源政策調整，維護生態永續與社會共榮。`
                  : ``}
              </p>
            </li>
            <li className="c-article__oli">
              {lang === 'cn' ? `洞悉社會與生態的互動關係` : ``}
              <p className="c-article__content">
                {lang === 'cn'
                  ? `魚塭與水田是鳥類良好的棲地，尤其冬季更是鳥類渡冬最佳場所。光電進駐的地景變化，將影響現有生態系的組成。此外，芳苑村大約三、四百戶人家，其中約有一百戶依靠牡蠣生活（包含海牛車觀光）。蚵田對潮水產生阻擋而刻劃出許多的潮溝，使得剛出生的生物不易被潮流帶走，成為魚類及底棲生物躲藏的地方。長期觀測資料可以提供未來研究人員研究的基礎，亦能協助地居民了解人地互動的過程，並選擇與環境互動方式。`
                  : ``}
              </p>
            </li>
            <li className="c-article__oli">
              {lang === 'cn' ? `資料整合與加值應用、滾動跨域之合作研究` : ``}
              <p className="c-article__content">
                {lang === 'cn'
                  ? `本計畫除自行觀測項目之外，會盤點過去公私部門相關研究調查資料，透過資料共享與點對點的資料串連，建立以地方為基礎的資料庫，提供研究者資料獲取的平台。在資料加值與合作方面，本計畫資料在授權取用的過程中，將持續盤點資料需求，以利新增觀測項目並了解在地議題的變化。在資料互惠共享的原則下，透過合作研究與成果回饋機制以充實並擴充資料庫。`
                  : ``}
              </p>
            </li>
          </ol>
        </section>
      </article>
    </>
  );
}

export default BackgroundAndPurposes;
