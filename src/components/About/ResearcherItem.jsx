import React from 'react';

import userIconImg from '../../img/icon/user.svg';
import { IMAGE_URL } from 'utils/config';

function ResearcherItem(props) {
  const { list } = props;
  const { id, name, title, duty, email, image } = list;
  return (
    <>
      <div className="l-about__card c-card">
        <div className="row g-0 gy-3 gy-md-0 gx-md-4 w-100">
          <div className="col-12 col-md-3 d-flex justify-content-center">
            <div className="l-about__img">
              {image !== '' ? (
                <img
                  className="e-img e-img--cover"
                  src={`${IMAGE_URL}${image}`}
                  alt={name}
                />
              ) : (
                <img
                  className="e-img e-img--contain"
                  src={userIconImg}
                  alt={name}
                />
              )}
            </div>
          </div>
          <div className="col-12 col-md-9 d-flex align-items-center">
            <ul className="l-about__list c-block">
              <li className="c-block__item">
                <div className="c-block__title">職稱</div>
                <div className="c-block__content">{title}</div>
              </li>
              <li className="c-block__item">
                <div className="c-block__title">姓名</div>
                <div className="c-block__content">{name}</div>
              </li>
              {duty && (
                <li className="c-block__item">
                  <div className="c-block__title">職責</div>
                  <div className="c-block__content">{duty}</div>
                </li>
              )}
              {email && (
                <li className="c-block__item">
                  <div className="c-block__title">聯絡方式</div>
                  <div className="c-block__content">
                    <a
                      href={`mailto:${email}`}
                      className="c-block__link e-link"
                    >
                      Email
                    </a>
                  </div>
                </li>
              )}
            </ul>
          </div>
        </div>
      </div>
    </>
  );
}

export default ResearcherItem;
