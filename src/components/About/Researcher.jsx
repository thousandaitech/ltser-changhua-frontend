import React, { useEffect, useState } from 'react';

import ResearcherItem from 'components/About/ResearcherItem';
import { researcherList } from 'data/about';
import useRender from 'hooks/useRender';

function Researcher() {
  const [researchers, setResearchers] = useState(null);
  const { getListByPage } = useRender();

  const isFetchingList = researchers === null;

  useEffect(() => {
    getListByPage({
      url: 'staff',
      setList: setResearchers,
      defaultList: researcherList,
    });
  }, []);

  return (
    <>
      <div className="l-about">
        {!isFetchingList &&
          researchers.map((item) => {
            const { id } = item;
            return <ResearcherItem key={id} list={item} />;
          })}
      </div>
    </>
  );
}

export default Researcher;
