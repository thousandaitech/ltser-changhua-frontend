import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

import Dropdown from 'react-bootstrap/Dropdown';
import Accordion from 'react-bootstrap/Accordion';

function AdvancedSearch() {
  const [filter, setFilter] = useState({
    record: ['彰化縣', '芳苑鄉'],
    central: [],
    local: [],
    theme: [],
    service: [],
    file: [],
  });
  const { pathname } = useLocation();
  const isEmptyRecord = filter.record.length === 0;

  useEffect(() => {}, []);
  const categoryList = [
    {
      type: 'central',
      title: '中央機關',
      list: [
        {
          optionId: '1',
          title: '行政院',
          num: 0,
        },
        {
          optionId: '2',
          title: '司法院',
          num: 0,
        },
        {
          optionId: '3',
          title: '監察院',
          num: 0,
        },
      ],
    },
    {
      type: 'local',
      title: '地方機關',
      list: [
        {
          optionId: '1',
          title: '彰化縣',
          num: 0,
        },
      ],
    },
    {
      type: 'theme',
      title: '常用主題',
      list: [
        {
          optionId: '1',
          title: '政府統計',
          num: 0,
        },
        {
          optionId: '2',
          title: '政府支出',
          num: 0,
        },
        {
          optionId: '3',
          title: '政府預算',
          num: 0,
        },
        {
          optionId: '4',
          title: '性別統計',
          num: 0,
        },
      ],
    },
    {
      type: 'service',
      title: '服務分類',
      list: [
        {
          optionId: '1',
          title: '公共資訊',
          num: 0,
        },
        {
          optionId: '2',
          title: '購屋及遷徙',
          num: 0,
        },
        {
          optionId: '3',
          title: '投資理財',
          num: 0,
        },
        {
          optionId: '4',
          title: '生活安全及品質',
          num: 0,
        },
        {
          optionId: '5',
          title: '老年安養',
          num: 0,
        },
        {
          optionId: '6',
          title: '交通及通訊',
          num: 0,
        },
        {
          optionId: '7',
          title: '就醫',
          num: 0,
        },
        {
          optionId: '8',
          title: '求職及就業',
          num: 0,
        },
        {
          optionId: '9',
          title: '生育保健',
          num: 0,
        },
        {
          optionId: '10',
          title: '休閒旅遊',
          num: 0,
        },
        {
          optionId: '11',
          title: '求學及進修',
          num: 0,
        },
        {
          optionId: '12',
          title: '退休',
          num: 0,
        },
        {
          optionId: '13',
          title: '開創事業',
          num: 0,
        },
        {
          optionId: '14',
          title: '選舉及投票',
          num: 0,
        },
        {
          optionId: '15',
          title: '生命禮儀',
          num: 0,
        },
        {
          optionId: '16',
          title: '婚姻',
          num: 0,
        },
      ],
    },
    {
      type: 'file',
      title: '檔案格式',
      list: [
        {
          optionId: '1',
          title: 'CSV',
          num: 0,
        },
        {
          optionId: '2',
          title: 'ZIP',
          num: 0,
        },
        {
          optionId: '3',
          title: 'XML',
          num: 0,
        },
        {
          optionId: '4',
          title: 'JSON',
          num: 0,
        },
        {
          optionId: '5',
          title: 'ODS',
          num: 0,
        },
        {
          optionId: '6',
          title: 'PDF',
          num: 0,
        },
        {
          optionId: '7',
          title: '7Z',
          num: 0,
        },
        {
          optionId: '8',
          title: 'API',
          num: 0,
        },
        {
          optionId: '9',
          title: 'WEBSERVICES',
          num: 0,
        },
      ],
    },
  ];
  const handleCheckboxChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    if (filter[name].includes(value)) {
      const remainList = filter[name].filter((item) => item !== value);
      setFilter({ ...filter, [name]: remainList });
    } else {
      setFilter({
        ...filter,
        [name]: [...filter[name], value],
      });
    }
  };

  return (
    <>
      <div className="l-download__sidebar d-none d-lg-block">
        <div className="c-sidebar__title">
          <FontAwesomeIcon
            icon={solid('bars')}
            className="l-header__icon e-icon e-icon--secondary e-icon--right fa-fw"
          />
          進階搜索
        </div>
        <div className="c-option c-option--sidebar">
          <Dropdown className="c-option__dropdown">
            <Dropdown.Toggle
              bsPrefix="c-option__toggle c-option__toggle--bg"
              variant=""
            >
              搜尋紀錄
            </Dropdown.Toggle>
            {!isEmptyRecord && (
              <Dropdown.Menu className="c-option__menu">
                {filter.record.map((item, i) => {
                  return (
                    <Link
                      key={i}
                      to={`${pathname}?search=${item}`}
                      className="c-option__item"
                    >
                      {item}
                    </Link>
                  );
                })}
              </Dropdown.Menu>
            )}
          </Dropdown>
        </div>
        {categoryList.map((item) => {
          const { type, title, list } = item;
          return (
            <Accordion key={type} className="c-accordion c-accordion--sidebar">
              <Accordion.Item eventKey="0" className="c-accordion__item">
                <Accordion.Button className="c-accordion__toggle">
                  {title}
                  <div className="c-badge">{filter[type].length}</div>
                </Accordion.Button>
                <Accordion.Body className="c-accordion__body">
                  <div className="c-option c-option--sidebar">
                    {list.map((item) => {
                      const { optionId, title, num } = item;
                      return (
                        <div key={optionId} className="c-option__set">
                          <input
                            type="checkbox"
                            id={`${type}${optionId}`}
                            className="btn-check c-option__input"
                            name={type}
                            value={optionId}
                            checked={filter[type].includes(optionId)}
                            onChange={handleCheckboxChange}
                          />
                          <label
                            htmlFor={`${type}${optionId}`}
                            className="c-option__label d-flex align-items-center"
                          >
                            {title}
                            <div className="c-badge c-badge--muted">{num}</div>
                          </label>
                        </div>
                      );
                    })}
                  </div>
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          );
        })}
        <div className="l-download__action">
          <button type="button" className="e-btn e-btn--primary e-btn--w100">
            搜尋
          </button>
          <button type="reset" className="e-btn e-btn--muted e-btn--w100">
            重設
          </button>
        </div>
      </div>
    </>
  );
}

export default AdvancedSearch;
