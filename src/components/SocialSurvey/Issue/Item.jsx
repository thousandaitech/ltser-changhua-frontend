import React, { useState, useEffect } from 'react';

import ItemModal from 'components/SocialSurvey/Issue/ItemModal';
import Header from 'components/SocialSurvey/Issue/Header';
import { Link } from 'react-router-dom';

const Item = ({ data }) => {
  const [active, setActive] = useState({
    modal: '',
  });
  const { href } = data;

  return (
    <>
      <Header layout="col" data={data} setActive={setActive} />
      <ItemModal active={active} setActive={setActive} data={data} />
    </>
  );
};

export default Item;
