import React, { useState, useEffect } from 'react';

import { questionerList } from 'data/social-survey';

const Header = (props) => {
  const { layout, data, setActive } = props;
  const { title, questionerId, href } = data;
  const [questioner, setQuestioner] = useState({ id: 0, title: '' });

  const handleModalShow = () => {
    setActive({ modal: data.id });
  };

  useEffect(() => {
    const matchedQuestioner = questionerList.find(
      (item) => item.id === questionerId
    );
    setQuestioner({ ...matchedQuestioner });
  }, []);

  const ContentLayout = (
    <>
      <div className="l-issue__heading">
        <h3 className="l-issue__title">Q: 問題描述</h3>
        <p className="l-issue__text">{title}</p>
      </div>
      <div className="l-issue__footer">
        <div className="l-issue__subtext">提問人身份</div>
        <h4 className="l-issue__title">{questioner.title}</h4>
      </div>
    </>
  );

  return (
    <>
      <div
        className={`${
          layout === 'col' ? 'col-12 col-md-6 col-xl-4' : 'col-12'
        } d-flex align-items-stretch`}
      >
        {href ? (
          <div
            className={`${
              layout === 'col'
                ? 'l-issue__item l-issue__item--btn'
                : 'l-issue__item'
            }`}
          >
            {ContentLayout}
            <a
              href={href}
              target="_blank"
              rel=" noreferrer"
              className="stretched-link"
            >
              {' '}
            </a>
          </div>
        ) : (
          <button
            className={`${
              layout === 'col'
                ? 'l-issue__item l-issue__item--btn'
                : 'l-issue__item'
            }`}
            onClick={handleModalShow}
          >
            {ContentLayout}
          </button>
        )}
      </div>
    </>
  );
};

export default Header;
