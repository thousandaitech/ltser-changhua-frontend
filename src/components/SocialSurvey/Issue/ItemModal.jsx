import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import Modal from 'react-bootstrap/Modal';

import Header from 'components/SocialSurvey/Issue/Header';

import {
  categoryList,
  issueList,
  tag2List,
  tag3List,
} from 'data/social-survey';
import useRender from 'hooks/useRender';

const ItemModal = (props) => {
  const { data, active, setActive } = props;
  const { type, tags, ref } = data;
  const { title, link } = ref;
  // const [tag2List, setTag2List] = useState(null);
  // const [tag3List, setTag3List] = useState(null);
  // const [tag2s, setTag2s] = useState(null);
  // const [tag3s, setTag3s] = useState(null);
  // const { getListByPage, getListReturn } = useRender();

  // const isFetchingTag2s = tag2s === null;
  // const isFetchingTag3s = tag3s === null;
  // const isFetchingTag2sList = tag2List === null;
  // const isFetchingTag3sList = tag3List === null;
  // const hasNoActiveModal = active.modal === '';

  // const matchIssue = issueList.find((v) => v.id === active.modal);

  // const hasNoTags =
  //   !isFetchingTag2s &&
  //   !isFetchingTag3s &&
  //   tag2s.length === 0 &&
  //   tag3s.length === 0;

  const handleClose = () => {
    setActive({ modal: '' });
  };

  // useEffect(() => {
  //   getListByPage({
  //     url: `interview-multiple/tag2`,
  //     setList: setTag2List,
  //   });
  //   getListByPage({
  //     url: `interview-multiple/tag3`,
  //     setList: setTag3List,
  //   });
  // }, []);

  // useEffect(() => {
  //   if (!hasNoActiveModal && !isFetchingTag2sList && !isFetchingTag3sList) {
  //     const matchTag2 = matchIssue.tag2
  //       .map((item) => tag2List.find((v) => v.tag2 === item))
  //       .map((item) => {
  //         const matchCategory = categoryList.find(
  //           (v) => v.id === item.categoryId
  //         ).title;
  //         return {
  //           category: matchCategory,
  //           tag2: {
  //             id: item.tag2,
  //             title: item.title,
  //           },
  //         };
  //       });
  //     setTag2s([...matchTag2]);
  //     (async () => {
  //       const handleTag3 = async () => {
  //         const promises = matchIssue.tag3
  //           .map((item) => tag3List.find((v) => v.tag3 === item))
  //           .map(async (item) => {
  //             const matchCategory = categoryList.find(
  //               (v) => v.id === item.categoryId
  //             );
  //             const tag2OfTag3List = await getListReturn({
  //               url: `interview-multiple/tag2`,
  //               params: { categoryId: item.categoryId },
  //             });
  //             let key;
  //             if (item.groupId) {
  //               key = 'groupId';
  //             } else {
  //               key = 'optionId';
  //             }
  //             const tag2OfTag3 = tag2OfTag3List.find(
  //               (v) => v[key] === item[key]
  //             );
  //             return {
  //               category: matchCategory.title,
  //               tag2: {
  //                 id: tag2OfTag3.tag2,
  //                 title: tag2OfTag3.title,
  //               },
  //               tag3: {
  //                 id: item.tag3,
  //                 title: item.title,
  //               },
  //             };
  //           });
  //         return await Promise.all(promises);
  //       };
  //       const matchTag3 = await handleTag3();
  //       setTag3s([...matchTag3]);
  //     })();
  //   }
  // }, [active.modal, tag2List, tag3List]);

  return (
    <>
      <Modal
        show={active.modal === data.id}
        onHide={handleClose}
        className="c-modal"
        backdropClassName="c-modal__backdrop"
        dialogClassName="c-modal__dialog"
        contentClassName="c-modal__content"
        scrollable={true}
        centered={true}
      >
        <Modal.Body className="c-modal__body">
          <Header layout="row" data={data} setActive={setActive} />
          <Link to="/social-survey/interviews">
            <h3 className="c-title c-modal__title">訪談資料</h3>
          </Link>
          <div className="c-list__text">
            可於{type === 'single' ? '單一' : '交叉'}查詢搜尋:
          </div>
          <ul className="c-list">
            {tags.map((tag) => {
              return tag.list ? (
                <li key={tag.id} className="c-list__text d-flex">
                  <span className="c-list__heading">{tag.title}</span>
                  <div className="d-flex">
                    {tag.list.map((v, i) => {
                      return (
                        <span key={i}>
                          {v}
                          {i === tag.list.length - 1 ? '' : '、'}
                        </span>
                      );
                    })}
                  </div>
                </li>
              ) : (
                <li key={tag.id} className="c-list__text">
                  {tag.title}
                </li>
              );
            })}
            {/* {!isFetchingTag2s &&
              !isFetchingTag2sList &&
              tag2s.map((v) => {
                return (
                  <li key={v.tag2.id} className="c-list__text">
                    <span className="me-2">{v.category}</span>
                    <Link
                      className="e-link"
                      to={`/social-survey/interviews?tag2=${v.tag2.id}`}
                    >
                      {v.tag2.title}
                    </Link>
                  </li>
                );
              })}
            {!isFetchingTag3s &&
              !isFetchingTag3sList &&
              tag3s.map((v) => {
                return (
                  <li key={v.tag3.id} className="c-list__text">
                    <span className="me-2">{v.category}</span>
                    <Link
                      className="e-link me-2"
                      to={`/social-survey/interviews?tag2=${v.tag2.id}`}
                    >
                      {v.tag2.title}
                    </Link>
                    <Link
                      className="e-link"
                      to={`/social-survey/interviews?tag3=${v.tag3.id}`}
                    >
                      {v.tag3.title}
                    </Link>
                  </li>
                );
              })} */}
          </ul>
          <Link to={link}>
            <div className="c-title c-modal__title">{title}</div>
          </Link>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default ItemModal;
