import React from 'react';

import issueImg from 'img/icon/issue.svg';

import Intro from 'components/Intro';
import Item from 'components/SocialSurvey/Issue/Item';

import { issueList } from 'data/social-survey';

const Content = () => {
  const pageData = {
    title: '聚焦主題',
    page: 'issue',
    icon: issueImg,
  };

  return (
    <>
      <article className="l-issue">
        <section className="l-issue__title u-section">
          <Intro data={pageData} />
        </section>
        <section className="l-issue__data">
          <div className="row g-4">
            {issueList.map((v) => {
              const { id } = v;
              return <Item key={id} data={v} />;
            })}
          </div>
        </section>
      </article>
    </>
  );
};

export default Content;
