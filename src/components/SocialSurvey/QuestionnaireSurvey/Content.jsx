import React from 'react';

import pageImg from 'img/icon/questionnaire-survey.svg';

import Intro from 'components/Intro';
import Placeholder from 'components/Placeholder';
import { Link } from 'react-router-dom';

const Content = () => {
  const pageData = {
    title: '問卷調查',
    icon: pageImg,
  };
  return (
    <>
      <article className="l-questionnaire-survey u-section">
        <section className="l-questionnaire-survey__title u-section">
          <Intro data={pageData} />
          <div className="row g-0 justify-content-center">
            <div className="col-12">
              <p className="l-questionnaire-survey__text">
                本站進行「LTSER長期社會生態核心觀測：2025芳苑鄉調查」之「第一波：濱海三區調查」問卷調查，總計抽取芳苑鄉沿海1,173
                戶作為受訪家戶。
              </p>
              <p className="l-questionnaire-survey__text">
                調查期間將派面訪員前往拜訪施測，訪問期間為2025年2月22日至5月31日。
              </p>
              <p className="l-questionnaire-survey__text">
                您的寶貴意見對我們的研究非常重要，詳請可參考說明：
                <br />
                <Link href="https://data.depositar.io/dataset/ltser-2025fyquestionnaire">
                  https://data.depositar.io/dataset/ltser-2025fyquestionnaire
                </Link>
              </p>
            </div>
          </div>
        </section>
      </article>
    </>
  );
};

export default Content;
