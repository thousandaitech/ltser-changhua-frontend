import React, { useEffect, useState } from 'react';

import Form from 'react-bootstrap/Form';

import { useContentContext } from 'context/Interview/ContentContext';
import { useSingleContext } from 'context/Interview/SingleContext';

import useRender from 'hooks/useRender';
import { useLocation, useNavigate } from 'react-router-dom';
import { Dropdown } from 'react-bootstrap';

const KeywordSelect = () => {
  const { filter, setFilter } = useSingleContext();
  const {
    GRID_CLASS,
    setInterviews,
    currentPage,
    setCurrentPage,
    setPaginationData,
    setInitial,
  } = useContentContext();
  // 要帶 Bearer
  const { loading, getListByPage, getListWithBearer } = useRender();
  const [tags, setTags] = useState(null);
  const navigate = useNavigate();

  const isFetchingTags = tags === null;
  const isDisabled = loading || filter.tag3.length === 0;
  const hasTag3 = filter.tag3.length !== 0;

  const handleCheckboxChange = (e) => {
    const { value, checked } = e.target;
    if (checked) {
      setFilter((prevFilter) => ({
        ...prevFilter,
        tag3: [...prevFilter.tag3, value],
      }));
    } else {
      setFilter((prevFilter) => ({
        ...prevFilter,
        tag3: prevFilter.tag3.filter((tag) => tag !== value),
      }));
    }
  };

  const handleQuerySubmit = (e) => {
    e.preventDefault();
    setInitial(false);
    setCurrentPage(1);
    getListWithBearer({
      type: 'api',
      url: 'interview-single',
      page: 1,
      setList: setInterviews,
      params: {
        tag3: filter.tag3.join(','),
      },
      setPaginationData,
    });
    navigate('/social-survey/interviews#result');
  };

  useEffect(() => {
    getListWithBearer({
      type: 'api',
      url: 'interview-single',
      page: currentPage,
      setList: setInterviews,
      params: {
        tag3: filter.tag3.join(','),
      },
      setPaginationData,
    });
  }, [currentPage]);

  useEffect(() => {
    getListByPage({
      type: 'api',
      url: 'interview-multiple/tag3',
      params: {
        categoryId: '0',
        groupId: '1',
      },
      setList: setTags,
    });
  }, []);

  return (
    <>
      <form className={`row ${GRID_CLASS.gutter}`} onSubmit={handleQuerySubmit}>
        <div className="col-12 col-lg-10">
          <div className={`row ${GRID_CLASS.gutter}`}>
            <div className="col-12">
              <Dropdown className="c-single-dropdown">
                <Dropdown.Toggle bsPrefix="c-single-dropdown__toggle">
                  請選擇關鍵字
                  {hasTag3 && (
                    <div className="c-option__badge">{filter.tag3.length}</div>
                  )}
                </Dropdown.Toggle>
                <Dropdown.Menu className="c-single-dropdown__menu">
                  {!isFetchingTags &&
                    tags.map((v) => {
                      const { tag3, title } = v;
                      return (
                        <div className="c-single-dropdown__wrapper" key={tag3}>
                          <input
                            id={tag3}
                            type="checkbox"
                            value={tag3}
                            className="btn-check"
                            onChange={handleCheckboxChange}
                          />
                          <label
                            htmlFor={tag3}
                            className={`c-single-dropdown__tag  ${
                              filter.tag3.includes(String(tag3))
                                ? 'checked'
                                : ''
                            }`}
                          >
                            {title}
                          </label>
                        </div>
                      );
                    })}
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
        </div>
        <div className="col-md-12 col-lg-2">
          <button
            type="submit"
            className="e-btn e-btn--primary c-option__btn e-btn--w100"
            disabled={isDisabled}
          >
            送出
          </button>
        </div>
        <div className="col-12">
          <div className="c-option__hint">
            {hasTag3 && <span className="c-option__seg">已選擇:</span>}
            {!isFetchingTags &&
              filter.tag3.map((tag) => {
                const matchTitleById = tags.find(
                  (v) => String(v.tag3) === String(tag)
                );
                return (
                  <span key={matchTitleById.tag3} className="me-2">
                    {matchTitleById.title}
                  </span>
                );
              })}
          </div>
        </div>
      </form>
    </>
  );
};

export default KeywordSelect;
