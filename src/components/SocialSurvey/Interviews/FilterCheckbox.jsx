import React from 'react';

import { useMultipleContext } from 'context/Interview/MultipleContext';
import { useInterviewTagContext } from 'context/Interview/InterviewTagContext';

function FilterCheckbox({ data, dropdown = false }) {
  const { categoryId, groupId, optionId, title, tag2, tag3, stakeholder } =
    data;

  const { filter, setFilter, tag, setTag } = useMultipleContext();
  const { tag3List } = useInterviewTagContext();

  const optionValue =
    categoryId === '0'
      ? optionId
      : `${categoryId}${groupId ? `.${groupId}` : ''}${
          optionId ? `.${optionId}` : ''
        }`;
  const shownOptionValue =
    categoryId !== '0' &&
    `${categoryId}${groupId ? `.${groupId}` : ''}${
      optionId ? `.${optionId}` : ''
    }`;

  const handleCheckboxChecked = () => {
    return (
      tag.tag2.includes(String(tag2)) ||
      tag.tag3.includes(String(tag3)) ||
      tag.stakeholder.includes(String(stakeholder))
    );
  };

  const handleCheckboxChange = (e) => {
    let subFilter = [];
    if (filter.tag.includes(e.target.value)) {
      const remainList = filter.tag.filter((v) => v !== e.target.value);
      if (dropdown) {
        subFilter = remainList.filter(
          (v) => v.slice(0, 4) !== `${categoryId}.${groupId}.`
        );
        setFilter({ ...filter, tag: [...subFilter] });
      } else {
        setFilter({ ...filter, tag: [...remainList] });
      }
    } else {
      if (dropdown) {
        subFilter = tag3List
          .filter((v) => `${v.categoryId}.${v.groupId}` === e.target.value)
          .map((v) => `${v.categoryId}.${v.groupId}.${v.optionId}`)
          .filter((v) => !filter.tag.includes(v));
      }
      setFilter({
        ...filter,
        tag: [...filter.tag, e.target.value, ...subFilter],
      });
    }
    const newTag = { ...tag };
    ['tag2', 'tag3', 'stakeholder'].forEach((v) => {
      const value = e.target.dataset[v];
      if (value) {
        if (newTag[v].includes(value)) {
          newTag[v] = newTag[v].filter((v) => v !== value);
          if (v === 'tag2' && dropdown) {
            const tag3Values = tag3List
              .filter((v) => `${v.categoryId}.${v.groupId}` === e.target.value)
              .map((v) => String(v.tag3));
            newTag.tag3 = newTag.tag3.filter((v) => !tag3Values.includes(v));
          }
        } else {
          newTag[v].push(value);
          if (v === 'tag2' && dropdown) {
            const tag3Values = tag3List
              .filter((v) => `${v.categoryId}.${v.groupId}` === e.target.value)
              .map((v) => String(v.tag3));
            newTag.tag3 = [...newTag.tag3, ...tag3Values];
          }
        }
      }
    });
    setTag(newTag);
  };

  return (
    <>
      <div className="c-checkbox c-checkbox--btn">
        <div className="c-checkbox__set">
          <input
            type="checkbox"
            id={optionValue}
            className="c-checkbox__input form-check-input"
            name={categoryId}
            value={optionValue}
            data-tag2={tag2}
            data-tag3={tag3}
            data-stakeholder={stakeholder}
            onChange={handleCheckboxChange}
            checked={handleCheckboxChecked()}
          />
          <label htmlFor={optionValue} className="c-checkbox__label">
            {shownOptionValue} {title}
          </label>
        </div>
      </div>
    </>
  );
}

export default FilterCheckbox;
