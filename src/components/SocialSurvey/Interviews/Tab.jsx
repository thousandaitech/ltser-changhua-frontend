import { useContentContext } from 'context/Interview/ContentContext';
import React, { useEffect, useState } from 'react';

function Tab(props) {
  const { data, setList } = props;
  const { tabs } = data;
  const { setInitial, activeTab, setActiveTab } = useContentContext();

  useEffect(() => {
    if (setList) {
      setInitial(true);
      setList(null);
    }
  }, [activeTab]);

  return (
    <>
      <div className="c-tab c-tab--heading">
        <div className="c-tab__tabs">
          {tabs.map((item) => {
            const { id, title } = item;
            return (
              <button
                key={id}
                type="button"
                className={`c-tab__tab ${activeTab === id ? 'active' : ''}`}
                onClick={() => {
                  setActiveTab(id);
                }}
              >
                {title}
              </button>
            );
          })}
        </div>
        {tabs.map((item) => {
          const { id, content } = item;
          return (
            activeTab === id && (
              <React.Fragment key={id}>{content}</React.Fragment>
            )
          );
        })}
      </div>
    </>
  );
}

export default Tab;
