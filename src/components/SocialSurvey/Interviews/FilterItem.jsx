import React, { useEffect, useState } from 'react';

import FilterCheckbox from 'components/SocialSurvey/Interviews/FilterCheckbox';
import FilterDropdown from 'components/SocialSurvey/Interviews/FilterDropdown';

import { useInterviewTagContext } from 'context/Interview/InterviewTagContext';
import { useContentContext } from 'context/Interview/ContentContext';

function FilterItem({ data }) {
  const { id, title } = data;

  const { GRID_CLASS } = useContentContext();
  const { tag2List } = useInterviewTagContext();

  const [list, setList] = useState([]);

  const isCategoryKeyword = title === '0 重要關鍵詞';

  useEffect(() => {
    const matchTag2ByCategoryId = tag2List.filter(
      (v) => v.categoryId === title[0]
    );
    setList([...matchTag2ByCategoryId]);
  }, []);

  return (
    <>
      {!isCategoryKeyword && (
        <div className={`${GRID_CLASS.col} d-flex align-items-stretch`}>
          <div className="c-option__col">
            <h3 className="c-option__title">{title}</h3>
            {list.map((tag2Item) => {
              return tag2Item.groupId ? (
                <FilterDropdown
                  key={`${tag2Item.categoryId}.${tag2Item.groupId}`}
                  data={tag2Item}
                  id={id}
                  title={title}
                />
              ) : (
                <FilterCheckbox
                  key={`${tag2Item.categoryId}.${tag2Item.optionId}`}
                  data={tag2Item}
                />
              );
            })}
          </div>
        </div>
      )}
    </>
  );
}

export default FilterItem;
