import React, { useEffect, useState } from 'react';

import { useMultipleContext } from 'context/Interview/MultipleContext';

import Dropdown from 'react-bootstrap/Dropdown';
import FilterCheckbox from './FilterCheckbox';
import { useInterviewTagContext } from 'context/Interview/InterviewTagContext';

const FilterDropdown = ({ id, title, data }) => {
  const { categoryId, groupId } = data;

  const { filter } = useMultipleContext();
  const { tag3List } = useInterviewTagContext();

  const [checkedLength, setCheckedLength] = useState(0);
  const [list, setList] = useState([]);

  const isFetchingList = list === undefined || list.length === 0;

  useEffect(() => {
    const checked = filter.tag.filter(
      (v) => v.slice(0, 4) === `${categoryId}.${groupId}.`
    ).length;
    setCheckedLength(checked);
  }, [filter.tag]);

  useEffect(() => {
    const matchTag3ByGroupId = tag3List.filter(
      (v) => v.categoryId === title[0] && v.groupId === groupId
    );
    setList([...matchTag3ByGroupId]);
  }, []);

  const isNoChecked = checkedLength === 0;

  return (
    <>
      <Dropdown className="c-option__dropdown">
        <Dropdown.Toggle bsPrefix="c-option__toggle" variant="">
          <FilterCheckbox
            key={`${categoryId}.${groupId}`}
            data={data}
            dropdown={true}
          />
          {!isNoChecked && (
            <div className="c-option__badge">
              {isNoChecked ? '' : checkedLength}
            </div>
          )}
        </Dropdown.Toggle>
        <Dropdown.Menu className="c-option__menu">
          {!isFetchingList &&
            list.map((tag3Item) => {
              return (
                <FilterCheckbox
                  key={`${tag3Item.categoryId}.${tag3Item.groupId}.${tag3Item.optionId}`}
                  data={tag3Item}
                />
              );
            })}
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
};

export default FilterDropdown;
