import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

import FilterItem from 'components/SocialSurvey/Interviews/FilterItem';
import FilterCheckbox from 'components/SocialSurvey/Interviews/FilterCheckbox';

import { categoryList, stakeholderList } from 'data/social-survey';

import { useMultipleContext } from 'context/Interview/MultipleContext';
import { useInterviewTagContext } from 'context/Interview/InterviewTagContext';
import { useContentContext } from 'context/Interview/ContentContext';

import useRender from 'hooks/useRender';
import { useDownload } from 'hooks/useDownload';
import { Spinner } from 'react-bootstrap';
import { SWAL_TOAST } from 'data/common';
import { useAuthContext } from 'context/AuthContext';

function MultipleQuery() {
  // 要帶 Bearer
  const { getListByPage, getListWithBearer } = useRender();
  const { initialFilter, initialTag, filter, setFilter, tag, setTag } =
    useMultipleContext();
  const {
    tag2List,
    setTag2List,
    tag3List,
    setTag3List,
    stakeholderList,
    setStakeholderList,
    categoryList,
    setCategoryList,
  } = useInterviewTagContext();
  const {
    GRID_CLASS,
    setInterviews,
    currentPage,
    setCurrentPage,
    setPaginationData,
    setInitial,
    activeTab,
    hasInterviews,
  } = useContentContext();
  const { handleDownload, loading } = useDownload();
  const { pathname, hash, search } = useLocation();
  const { auth } = useAuthContext();

  const navigate = useNavigate();
  const [tags, setTags] = useState(null);
  const [hint, setHint] = useState({
    stakeholder: false,
    tags: false,
  });

  const isFetchingTag2List = tag2List === null;
  const isFetchingTag3List = tag3List === null;
  const isFetchingStakeholder = stakeholderList === null;
  const isFetchingCategory = categoryList === null;
  const isFetchingTags = tags === null;

  const hasNoChecked =
    tag.tag2.length === 0 &&
    tag.tag3.length === 0 &&
    tag.stakeholder.length === 0;
  const hasTag2 = tag.tag2.length !== 0;
  const hasTag3 = tag.tag3.length !== 0;
  const hasStakeholder = tag.stakeholder.length !== 0;
  const hasTags = hasTag2 || hasTag3;

  const tag2Value = hasTag2 ? tag.tag2.join(',') : null;
  const tag3Value = hasTag3 ? tag.tag3.join(',') : null;

  const handleResetFilter = () => {
    setFilter({ ...initialFilter });
    setTag({ ...initialTag });
    setInterviews([]);
    setCurrentPage(1);
  };

  const handleQuerySubmit = (e) => {
    e.preventDefault();
    if (hasStakeholder) {
      if (hasTags) {
        setInitial(false);
        setCurrentPage(1);
        getListWithBearer({
          type: 'api',
          url: 'interview-multiple',
          page: 1,
          setList: setInterviews,
          params: {
            tag2: tag2Value,
            tag3: tag3Value,
            stakeholder: tag.stakeholder.join(','),
          },
          setPaginationData,
        });
        navigate('/social-survey/interviews#result');
      } else {
        setHint({
          ...hint,
          tags: true,
        });
      }
    } else {
      setHint({
        ...hint,
        stakeholder: true,
      });
    }
  };

  const handleDownloadClick = () => {
    if (auth) {
      handleDownload({
        type: 'api',
        url: 'interview-multiple',
        fileName: 'interview-multiple-query',
        params: {
          tag2: tag2Value,
          tag3: tag3Value,
          stakeholder: tag.stakeholder.join(','),
        },
      });
    } else {
      SWAL_TOAST.fire({
        icon: 'error',
        title: `該功能需登入後方可使用。請先登入。`,
      });
      navigate('/login', { state: { previousPage: `${pathname}${hash}` } });
    }
  };

  useEffect(() => {
    getListByPage({
      url: `interview-multiple/stakeholder`,
      setList: setStakeholderList,
    });
    getListByPage({
      url: `interview-multiple/tag1`,
      setList: setCategoryList,
    });
    getListByPage({
      url: `interview-multiple/tag2`,
      setList: setTag2List,
    });
    getListByPage({
      url: `interview-multiple/tag3`,
      setList: setTag3List,
    });
  }, []);

  useEffect(() => {
    handleResetFilter();
  }, [activeTab]);

  useEffect(() => {
    if (!isFetchingTag2List && !isFetchingTag3List) {
      let tagList = [];
      Object.entries(tag).forEach(([key, value]) => {
        if (value.length !== 0) {
          let tagListByKey;
          switch (key) {
            case 'tag2':
              tagListByKey = tag2List;
              break;
            case 'tag3':
              tagListByKey = tag3List;
              break;
            case 'stakeholder':
              tagListByKey = stakeholderList;
              break;
            default:
              break;
          }
          value.forEach((item) => {
            const matchTag = tagListByKey.find((v) => v[key] === Number(item));
            if (matchTag) {
              tagList.push(matchTag);
            }
          });
        }
      });
      setTags([...tagList]);
    }
  }, [tag2List, tag3List, tag]);

  useEffect(() => {
    getListWithBearer({
      type: 'api',
      url: 'interview-multiple',
      page: currentPage,
      setList: setInterviews,
      params: {
        tag2: tag2Value,
        tag3: tag3Value,
        stakeholder: hasStakeholder ? tag.stakeholder.join(',') : null,
      },
      setPaginationData,
    });
  }, [currentPage]);

  useEffect(() => {
    if (hint.stakeholder) {
      navigate(`${pathname}#stakeholder`);
    } else if (hint.tags) {
      navigate(`${pathname}#tags`);
    }
  }, [hint]);

  useEffect(() => {
    if (hasStakeholder) {
      setHint({
        ...hint,
        stakeholder: false,
      });
    }
  }, [hasStakeholder]);

  useEffect(() => {
    if (hasTags) {
      setHint({
        ...hint,
        tags: false,
      });
    }
  }, [hasTags]);

  return (
    <>
      <div className="c-tab__content">
        <form className="c-option" onSubmit={handleQuerySubmit}>
          <div className="c-option__content" id="stakeholder">
            <h4 className="c-option__title">受訪對象</h4>
            <div className={`row ${GRID_CLASS.gutter}`}>
              {!isFetchingStakeholder &&
                stakeholderList.map((v) => {
                  const { categoryId, optionId } = v;
                  return (
                    <div
                      key={`${categoryId}.${optionId}`}
                      className={GRID_CLASS.col}
                    >
                      <FilterCheckbox data={v} />
                    </div>
                  );
                })}
            </div>
            {hint.stakeholder && (
              <div className="c-option__hint">
                受訪對象為必填，請至少選擇一項。
              </div>
            )}
            <hr className="e-hr" />
          </div>
          <div className="c-option__content">
            <div className={`row ${GRID_CLASS.gutter}`}>
              {!isFetchingCategory &&
                !isFetchingTag2List &&
                !isFetchingTag3List &&
                categoryList.map((v) => {
                  const { id } = v;
                  return (
                    <FilterItem key={id} data={v} GRID_CLASS={GRID_CLASS} />
                  );
                })}
              {hint.tags && (
                <div className="c-option__hint" id="tags">
                  標籤為必填，請至少選擇一項。
                </div>
              )}
              {!hasNoChecked && (
                <div className="col-12">
                  <hr className="e-hr" />
                  <div className="c-option__hint">
                    <span className="c-option__seg">已選擇:</span>
                    {!isFetchingTags &&
                      tags.map((v, i) => {
                        return (
                          <span key={i} className="me-2">
                            {v.categoryId ? `${v.categoryId}.` : ''}
                            {v.groupId ? `${v.groupId}.` : ''}
                            {v.optionId ? `${v.optionId}` : ''}
                            {v.title}
                          </span>
                        );
                      })}
                  </div>
                </div>
              )}
            </div>
          </div>
          <div className="c-option__action">
            <button
              type="button"
              className="e-btn e-btn--muted c-option__btn"
              onClick={handleResetFilter}
            >
              清除所有選項
            </button>
            <button
              type="submit"
              className="e-btn e-btn--primary c-option__btn"
            >
              搜尋
            </button>
          </div>
        </form>
      </div>
      {hasInterviews && (
        <button
          type="button"
          className="e-btn e-btn--primary ms-auto mt-3 e-btn--processing"
          data-count="icon-4"
          onClick={handleDownloadClick}
          disabled={loading}
        >
          {loading ? (
            <Spinner
              as="span"
              animation="border"
              size="sm"
              role="status"
              aria-hidden="true"
            />
          ) : (
            <>
              <FontAwesomeIcon
                icon={solid('download')}
                className="e-icon e-icon--primary e-icon--light e-icon--right"
              />
              申請資料
            </>
          )}
        </button>
      )}
    </>
  );
}

export default MultipleQuery;
