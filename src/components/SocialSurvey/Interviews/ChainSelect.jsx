import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Form from 'react-bootstrap/Form';

import { useContentContext } from 'context/Interview/ContentContext';
import { useSingleContext } from 'context/Interview/SingleContext';
import { useInterviewTagContext } from 'context/Interview/InterviewTagContext';

import useRender from 'hooks/useRender';

function ChainSelect() {
  const { filter, setFilter } = useSingleContext();
  const { stakeholderList } = useInterviewTagContext();
  const {
    GRID_CLASS,
    setInterviews,
    currentPage,
    setCurrentPage,
    setPaginationData,
    setInitial,
  } = useContentContext();
  // 要帶 Bearer
  const { loading, getListWithBearer } = useRender();
  const [optionList, setOptionList] = useState(null);
  const navigate = useNavigate();

  const isNoCategory = filter.interviewee.category === '';
  const isDisabled =
    loading ||
    filter.interviewee.category === '' ||
    filter.interviewee.id === '';
  const isFetchingStakeholder = stakeholderList === null;
  const isFetchingOption = optionList === null;

  const handleSelectChange = (e) => {
    setFilter({
      ...filter,
      interviewee: {
        ...filter.interviewee,
        [e.target.name]: e.target.value,
      },
    });
  };

  const handleQuerySubmit = (e) => {
    e.preventDefault();
    setInitial(false);
    setCurrentPage(1);
    getListWithBearer({
      type: 'api',
      url: 'interview-single',
      page: 1,
      setList: setInterviews,
      params: {
        people: filter.interviewee.id,
      },
      setPaginationData,
    });
    navigate('/social-survey/interviews#result');
  };

  useEffect(() => {
    if (!isFetchingStakeholder && !isNoCategory) {
      const matchInterviewee = stakeholderList.find(
        (item) => String(item.stakeholder) === filter.interviewee.category
      );
      if (matchInterviewee) {
        setOptionList([...matchInterviewee.people]);
        setFilter({
          ...filter,
          interviewee: {
            ...filter.interviewee,
            id: matchInterviewee.people[0],
          },
        });
      }
    }
  }, [stakeholderList, filter.interviewee.category]);

  useEffect(() => {
    getListWithBearer({
      type: 'api',
      url: 'interview-single',
      page: currentPage,
      setList: setInterviews,
      params: {
        people: filter.interviewee.id,
      },
      setPaginationData,
    });
  }, [currentPage]);

  return (
    <>
      <form className={`row ${GRID_CLASS.gutter}`} onSubmit={handleQuerySubmit}>
        <div className="col-12 col-lg-10">
          <div className={`row ${GRID_CLASS.gutter}`}>
            <div className="col-12 col-md-6">
              <Form.Select
                className="c-select__input c-option__input"
                name="category"
                onChange={(e) => handleSelectChange(e)}
                value={filter.interviewee.category}
              >
                <option value="" disabled>
                  請選擇類別
                </option>
                {!isFetchingStakeholder &&
                  stakeholderList.map((item) => {
                    const { stakeholder, title } = item;
                    return (
                      <option key={stakeholder} value={stakeholder}>
                        {title}
                      </option>
                    );
                  })}
              </Form.Select>
            </div>
            <div className="col-12 col-md-6">
              <Form.Select
                className="c-select__input c-option__input"
                name="id"
                onChange={(e) => handleSelectChange(e)}
                value={filter.interviewee.id}
              >
                <option value="" disabled>
                  請選擇子類別
                </option>
                {!isFetchingOption &&
                  optionList.map((item, i) => {
                    return (
                      <option key={i} value={item}>
                        {item}
                      </option>
                    );
                  })}
              </Form.Select>
            </div>
          </div>
        </div>
        <div className="col-12 col-lg-2">
          <button
            type="submit"
            className="e-btn e-btn--primary c-option__btn e-btn--w100"
            disabled={isDisabled}
          >
            送出
          </button>
        </div>
      </form>
    </>
  );
}

export default ChainSelect;
