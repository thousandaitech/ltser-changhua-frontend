import React, { useEffect, useState } from 'react';

import { useLocation } from 'react-router-dom';

import interviewsImg from 'img/icon/interviews.svg';

import Intro from 'components/Intro';
import MultipleQuery from 'components/SocialSurvey/Interviews/MultipleQuery';
import SingleQuery from 'components/SocialSurvey/Interviews/SingleQuery';
import Item from 'components/SocialSurvey/Item';
import Tab from 'components/SocialSurvey/Interviews/Tab';
import PaginationLayout from 'components/PaginationLayout';

import { useContentContext } from 'context/Interview/ContentContext';
import SpinnerPlaceholder from 'components/SpinnerPlaceholder';

const Content = () => {
  const { pathname } = useLocation();
  const {
    interviews,
    setInterviews,
    isFetchingList,
    currentPage,
    setCurrentPage,
    paginationData,
    paginationList,
    isFetchingPages,
    initial,
    setInitial,
    hasInterviews,
  } = useContentContext();

  const pageData = {
    title: '訪談資料',
    page: 'local-literature',
    icon: interviewsImg,
    tabs: [
      {
        id: 1,
        title: '交叉查詢',
        content: <MultipleQuery />,
      },
      {
        id: 2,
        title: '單一查詢',
        content: <SingleQuery />,
      },
    ],
  };

  useEffect(() => {
    setInitial(true);
    setInterviews(null);
  }, [pathname]);

  return (
    <>
      <article className="l-interviews">
        <section className="l-interviews__title c-heading u-section">
          <Intro data={pageData} />
        </section>
        <section className="l-interviews__filter">
          <Tab data={pageData} setList={setInterviews} />
        </section>
        <section className="l-interviews__data u-section" id="result">
          {!initial ? (
            !isFetchingList ? (
              hasInterviews ? (
                <>
                  <div className="l-interviews__subtext my-2">
                    搜尋結果共計{paginationData.totalRecords}筆
                  </div>
                  {interviews.map((v) => {
                    const { id } = v;
                    return <Item key={id} data={v} />;
                  })}
                  {!isFetchingPages && (
                    <PaginationLayout
                      currentPage={currentPage}
                      setCurrentPage={setCurrentPage}
                      paginationData={paginationData}
                      paginationList={paginationList}
                    />
                  )}
                </>
              ) : (
                <>目前沒有資料。</>
              )
            ) : (
              <>
                <div className="row gy-2">
                  <div className="col-2 ms-auto">
                    <SpinnerPlaceholder layout="list" />
                  </div>
                  <div className="col-12">
                    <SpinnerPlaceholder layout="list" />
                  </div>
                  <div className="col-12">
                    <SpinnerPlaceholder layout="list" />
                  </div>
                  <div className="col-12">
                    <SpinnerPlaceholder layout="list" />
                  </div>
                </div>
              </>
            )
          ) : (
            <>請先選擇搜尋條件。</>
          )}
        </section>
      </article>
    </>
  );
};

export default Content;
