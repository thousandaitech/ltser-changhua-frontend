import React, { useEffect, useState } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

import TimeSelect from 'components/SocialSurvey/Interviews/TimeSelect';
import ChainSelect from 'components/SocialSurvey/Interviews/ChainSelect';
import KeywordSelect from 'components/SocialSurvey/Interviews/KeywordSelect';

import { useSingleContext } from 'context/Interview/SingleContext';
import { useContentContext } from 'context/Interview/ContentContext';

import { useDownload } from 'hooks/useDownload';
import { Spinner } from 'react-bootstrap';
import { SWAL_TOAST } from 'data/common';
import { useAuthContext } from 'context/AuthContext';
import { useLocation, useNavigate } from 'react-router-dom';

function SingleQuery() {
  const { initialFilter, filter, setFilter } = useSingleContext();
  const { setCurrentPage, activeTab, setInterviews, hasInterviews } =
    useContentContext();
  const { handleDownload, loading } = useDownload();
  const { auth } = useAuthContext();
  const navigate = useNavigate();
  const { pathname, hash } = useLocation();

  const hasStartDate =
    filter.time.start.year !== '' || filter.time.start.month !== '';
  const hasEndDate =
    filter.time.end.year !== '' || filter.time.end.month !== '';
  const hasInterviewee =
    filter.interviewee.category !== '' || filter.interviewee.id !== '';
  const hasTag3 = filter.tag3.length !== 0;

  const handleResetFilter = () => {
    setFilter({ ...initialFilter });
    setInterviews([]);
    setCurrentPage(1);
  };

  const handleDownloadClick = () => {
    if (auth) {
      handleDownload({
        type: 'api',
        url: 'interview-single',
        fileName: 'interview-single-query',
        params: {
          d1: hasStartDate
            ? `${filter.time.start.year}-${filter.time.start.month}`
            : null,
          d2: hasEndDate
            ? `${filter.time.end.year}-${filter.time.end.month}`
            : null,
          people: hasInterviewee ? filter.interviewee.id : null,
          tag3: hasTag3 ? filter.tag3.join(',') : null,
        },
      });
    } else {
      SWAL_TOAST.fire({
        icon: 'error',
        title: `該功能需登入後方可使用。請先登入。`,
      });
      navigate('/login', { state: { previousPage: `${pathname}${hash}` } });
    }
  };

  useEffect(() => {
    if (hasStartDate || hasEndDate) {
      setFilter((prev) => ({
        ...prev,
        tag3: [],
        interviewee: {
          category: '',
          id: '',
        },
      }));
    }
  }, [hasStartDate, hasEndDate]);

  useEffect(() => {
    if (hasInterviewee) {
      setFilter((prev) => ({
        ...prev,
        tag3: [],
        time: {
          start: {
            year: '',
            month: '',
          },
          end: {
            year: '',
            month: '',
          },
        },
      }));
    }
  }, [hasInterviewee]);

  useEffect(() => {
    if (hasTag3) {
      setFilter((prev) => ({
        ...prev,
        interviewee: {
          category: '',
          id: '',
        },
        time: {
          start: {
            year: '',
            month: '',
          },
          end: {
            year: '',
            month: '',
          },
        },
      }));
    }
  }, [hasTag3]);

  useEffect(() => {
    handleResetFilter();
  }, [activeTab]);

  return (
    <>
      <div className="c-tab__content">
        <div className="c-option">
          <div className="c-option__content">
            <h4 className="c-option__title">依時間區間</h4>
            <TimeSelect hasStartDate={hasStartDate} hasEndDate={hasEndDate} />
            <hr className="e-hr" />
          </div>
          <div className="c-option__content">
            <h4 className="c-option__title">依受訪者</h4>
            <ChainSelect />
            <hr className="e-hr" />
          </div>
          <div className="c-option__content">
            <h4 className="c-option__title">其他關鍵字</h4>
            <KeywordSelect />
            {/* <hr className="e-hr" /> */}
          </div>
        </div>
      </div>
      {hasInterviews && (
        <button
          type="button"
          className="e-btn e-btn--primary ms-auto mt-3 e-btn--processing"
          data-count="icon-4"
          onClick={handleDownloadClick}
          disabled={loading}
        >
          {loading ? (
            <Spinner
              as="span"
              animation="border"
              size="sm"
              role="status"
              aria-hidden="true"
            />
          ) : (
            <>
              <FontAwesomeIcon
                icon={solid('download')}
                className="e-icon e-icon--primary e-icon--light e-icon--right"
              />
              申請資料
            </>
          )}
        </button>
      )}
    </>
  );
}

export default SingleQuery;
