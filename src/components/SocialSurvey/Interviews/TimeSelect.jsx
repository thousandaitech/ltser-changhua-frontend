import React, { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';

import { useContentContext } from 'context/Interview/ContentContext';
import { useSingleContext } from 'context/Interview/SingleContext';

import useRender from 'hooks/useRender';
import { useNavigate } from 'react-router-dom';

function TimeSelect(props) {
  const { hasStartDate, hasEndDate } = props;
  const { filter, setFilter } = useSingleContext();
  const {
    GRID_CLASS,
    setInterviews,
    currentPage,
    setCurrentPage,
    setPaginationData,
    setInitial,
  } = useContentContext();
  // 要帶 Bearer
  const { loading, getListWithBearer } = useRender();
  const navigate = useNavigate();
  const [timeRange, setTimeRange] = useState({
    year: [],
    month: [],
  });

  const isDisabled =
    loading ||
    filter.time.start.year === '' ||
    filter.time.start.month === '' ||
    filter.time.end.year === '' ||
    filter.time.end.month === '';

  const handleSelectChange = (e, type) => {
    setFilter({
      ...filter,
      time: {
        ...filter.time,
        [type]: {
          ...filter.time[type],
          [e.target.name]: e.target.value,
        },
      },
    });
  };

  const handleTimeRange = () => {
    const START_YEAR = 2022;
    const CURRENT_YEAR = 2024;
    const years = [];
    const months = [];
    for (let i = START_YEAR; i <= CURRENT_YEAR; i++) {
      years.push(i);
    }
    for (let i = 1; i <= 12; i++) {
      months.push(i);
    }
    setTimeRange({ ...timeRange, year: [...years], month: [...months] });
  };

  const timeRangeList = [
    {
      type: 'start',
      scales: ['year', 'month'],
    },
    {
      type: 'end',
      scales: ['year', 'month'],
    },
  ];

  const handleQuerySubmit = (e) => {
    e.preventDefault();
    setInitial(false);
    setCurrentPage(1);
    getListWithBearer({
      type: 'api',
      url: 'interview-single',
      page: 1,
      setList: setInterviews,
      params: {
        d1: hasStartDate
          ? `${filter.time.start.year}-${filter.time.start.month}`
          : null,
        d2: hasEndDate
          ? `${filter.time.end.year}-${filter.time.end.month}`
          : null,
      },
      setPaginationData,
    });
    navigate('/social-survey/interviews#result');
  };

  useEffect(() => {
    getListWithBearer({
      type: 'api',
      url: 'interview-single',
      page: currentPage,
      setList: setInterviews,
      params: {
        d1: hasStartDate
          ? `${filter.time.start.year}-${filter.time.start.month}`
          : null,
        d2: hasEndDate
          ? `${filter.time.end.year}-${filter.time.end.month}`
          : null,
      },
      setPaginationData,
    });
  }, [currentPage]);

  useEffect(() => {
    handleTimeRange();
  }, []);

  return (
    <>
      <form className={`row ${GRID_CLASS.gutter}`} onSubmit={handleQuerySubmit}>
        <div className="col-12 col-md- col-lg-10">
          <div className={`row ${GRID_CLASS.gutter}`}>
            {timeRangeList.map((types) => {
              const { type, scales } = types;
              return scales.map((scale) => {
                return (
                  <div
                    key={`${type}${scale}`}
                    className="col-12 col-md-6 col-lg-3"
                  >
                    <Form.Select
                      className="c-select__input c-option__input"
                      onChange={(e) => handleSelectChange(e, type)}
                      name={scale}
                      value={filter.time[type][scale]}
                    >
                      <option value="" disabled>
                        請選擇{type === 'start' ? '起始' : '結束'}
                        {scale === 'year' ? '年' : '月'}份
                      </option>
                      {timeRange[scale].map((item, i) => {
                        return (
                          <option key={i} value={item}>
                            {item}
                            {scale === 'year' ? '年' : '月'}
                          </option>
                        );
                      })}
                    </Form.Select>
                  </div>
                );
              });
            })}
          </div>
        </div>
        <div className="col-12 col-md-12 col-lg-2">
          <button
            type="submit"
            className="e-btn e-btn--primary c-option__btn e-btn--w100"
            disabled={isDisabled}
          >
            送出
          </button>
        </div>
      </form>
    </>
  );
}

export default TimeSelect;
