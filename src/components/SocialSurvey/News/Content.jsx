import React, { useEffect, useState } from 'react';

import newsImg from 'img/icon/news.svg';

import ContentTemplate from 'components/ContentTemplate';
import TagTab from 'components/TagTab';

import { newsList } from 'data/social-survey';

import usePage from 'hooks/usePage';
import useRender from 'hooks/useRender';
import Search from 'components/Search';

const Content = () => {
  const tagList = [
    { id: 1, title: '即時' },
    { id: 2, title: '社會' },
    { id: 3, title: '產經' },
    { id: 4, title: '政治' },
    { id: 5, title: '旅遊' },
    { id: 6, title: '治理' },
    { id: 7, title: '文教' },
    { id: 8, title: '環境' },
  ];

  const pageData = {
    title: '新聞報導整理',
    page: 'news',
    icon: newsImg,
    link: '/social-survey/local-literature-and-news/news',
    url: 'news',
  };

  const { loading, getListByPage } = useRender();
  const {
    currentPage,
    setCurrentPage,
    paginationData,
    setPaginationData,
    paginationList,
    isFetchingPages,
    hasNoRecords,
  } = usePage();

  const [filter, setFilter] = useState({
    tag: 0,
    keyword: '',
  });
  const [news, setNews] = useState(null);
  const [tags, setTags] = useState(null);
  const [refresh, setRefresh] = useState(false);
  const [search, setSearch] = useState('');

  const isFetchingTags = tags === null;
  const isAllTag = filter.tag === 0;
  const hasFilterTag = filter.tag !== '';
  const hasFilterKeyword = filter.keyword !== '';
  const hasSearch = search !== '';

  useEffect(() => {
    if (hasFilterKeyword) {
      setFilter({ ...filter, tag: '' });
    }
  }, [filter.keyword]);

  useEffect(() => {
    if (hasFilterTag) {
      setFilter({ ...filter, keyword: '' });
      setSearch('');
    }
  }, [filter.tag]);

  useEffect(() => {
    getListByPage({
      url: pageData.url,
      page: currentPage,
      setList: setNews,
      defaultList: newsList,
      setPaginationData,
      params: {
        tag: isAllTag
          ? null
          : hasFilterTag && !hasFilterKeyword
          ? filter.tag
          : null,
        keyword: hasFilterKeyword && !hasFilterTag ? filter.keyword : null,
      },
      setTags,
    });
  }, [currentPage, filter.tag, filter.keyword, refresh]);

  return (
    <>
      <ContentTemplate
        pageData={pageData}
        data={news}
        tab={
          !isFetchingTags && (
            <TagTab
              list={[{ id: 0, title: '最新' }, ...tags]}
              filter={filter}
              setFilter={setFilter}
              setCurrentPage={setCurrentPage}
            />
          )
        }
        search={
          <Search
            loading={loading}
            filter={filter}
            setFilter={setFilter}
            setList={setNews}
            url={pageData.url}
            currentPage={currentPage}
            setPaginationData={setPaginationData}
            search={search}
            setSearch={setSearch}
            hasSearch={hasSearch}
          />
        }
        tagList={tagList}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        paginationData={paginationData}
        paginationList={paginationList}
        isFetchingPages={isFetchingPages}
        hasNoRecords={hasNoRecords}
        setRefresh={setRefresh}
      />
    </>
  );
};

export default Content;
