import React from 'react';

import { Form } from 'react-bootstrap';

function ComparasionSelect(props) {
  const { scale, tab, target, setTarget, targetList } = props;
  const handleSelect = (e) => {
    setTarget({
      ...target,
      [scale]: { ...target[scale], [e.target.name]: e.target.value },
    });
  };
  const handlePlaceTitle = () => {
    switch (scale) {
      case 'town':
        return '彰化縣各';
      case 'village':
        return '芳苑鄉各';
      default:
        return;
    }
  };
  const handleScaleTitle = () => {
    switch (scale) {
      case 'town':
        return '鄉鎮';
      case 'village':
        return '村里';
      default:
        return;
    }
  };
  const handleTabTitle = () => {
    switch (tab) {
      case 'population':
        return '人口';
      case 'economy-and-industry':
        return '產業';
      default:
        return;
    }
  };
  return (
    <>
      <section className="u-section">
        <section className="l-social-economy-data__heading">
          <h5 className="l-social-economy-data__title">
            {handleTabTitle()}概況比較
          </h5>
        </section>
        <Form className="c-select d-flex flex-wrap">
          {Object.entries(target[scale]).map(([key, value]) => {
            return (
              <Form.Group key={key} className="c-select__set mb-3 mb-lg-0">
                <div className="c-select__deco" data-color={key}></div>
                <Form.Select
                  name={key}
                  className="c-select__input"
                  value={target[scale][key]}
                  onChange={(e) => handleSelect(e)}
                >
                  <option value="" disabled>
                    請選擇要比較的{handleScaleTitle()}
                  </option>
                  {targetList[scale].map((item) => {
                    const { id, title } = item;
                    return (
                      <option
                        key={id}
                        value={title}
                        disabled={title === target[scale][key]}
                      >
                        {title}
                      </option>
                    );
                  })}
                </Form.Select>
              </Form.Group>
            );
          })}
        </Form>
      </section>
    </>
  );
}

export default ComparasionSelect;
