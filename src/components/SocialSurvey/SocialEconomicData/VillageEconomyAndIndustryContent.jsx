import React, { useState } from 'react';

import BarLineGraph from 'components/SocialSurvey/SocialEconomicData/BarLineGraph';
import FishesSelect from 'components/SocialSurvey/SocialEconomicData/FishesSelect';
import FishesLineGraph from 'components/SocialSurvey/SocialEconomicData/FishesLineGraph';

import {
  villageClamsData,
  villageMulletData,
  villageMilkfishData,
  villageClamData,
  villageWhiteShrimpData,
  villageTilapiaData,
  villageEelData,
  villageYamatoClamData,
  villagePurpleClamData,
} from 'data/village';
import ScaleComparasionContent from './ScaleComparasionContent';

function VillageEconomyAndIndustryContent() {
  const [active, setActive] = useState({
    fish: '文蛤',
    year: '2022',
  });

  const isNoActiveFish = active.fish === '';

  const fishesData = {
    文蛤: villageClamsData,
    烏魚: villageMulletData,
    虱目魚: villageMilkfishData,
    蜆: villageClamData,
    白蝦: villageWhiteShrimpData,
    吳郭魚: villageTilapiaData,
    鰻魚: villageEelData,
    日本黑蜆: villageYamatoClamData,
    西施貝: villagePurpleClamData,
  };

  const fishesGraphList = [
    { id: '1', title: '養殖戶數', cols: ['魚苗戶', '養殖戶'], unit: '' },
    { id: '2', title: '養殖面積', cols: ['養殖面積'], unit: '公頃' },
    {
      id: '3',
      title: '放養量',
      cols: ['在池', '新放養'],
      unit: '尾、粒、隻',
    },
  ];

  return (
    <>
      <div className="mb-3 text-end text-muted">
        資料來源： 農業部漁業署養殖漁業放養查詢平臺
      </div>
      <section className="u-section">
        <div className="l-social-economy-data__section">
          <h4 className="l-social-economy-data__subtitle">牡蠣放養統計</h4>
          <BarLineGraph />
          <ScaleComparasionContent active={active} setActive={setActive} />
        </div>
      </section>
      <section className="u-section">
        <div className="l-social-economy-data__section">
          <h4 className="l-social-economy-data__subtitle">
            芳苑鄉養殖漁業放養量
          </h4>
          <FishesSelect
            active={active}
            setActive={setActive}
            rawData={fishesData}
          />
          {!isNoActiveFish &&
            fishesGraphList.map((item) => {
              const { id } = item;
              return (
                <FishesLineGraph
                  key={id}
                  active={active}
                  allData={fishesData}
                  item={item}
                />
              );
            })}
        </div>
      </section>
    </>
  );
}

export default VillageEconomyAndIndustryContent;
