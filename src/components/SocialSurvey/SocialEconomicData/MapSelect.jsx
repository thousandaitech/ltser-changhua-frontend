import React, { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';

import {
  populationThemeList,
  populationThemeTermList,
} from 'data/social-economic-data';

import { useFormat } from 'hooks/useFormat';
import { townPopulationThemeData } from 'data/town';
import { villagePopulationThemeData } from 'data/village';

function MapSelect(props) {
  const { scale, type, target, setTarget, data, themeList } = props;
  const [townThemeData, setTownThemeData] = useState([]);
  const [villageThemeData, setVillageThemeData] = useState([]);

  const formatTownPopulation = useFormat({
    setList: setTownThemeData,
    data: townPopulationThemeData,
    termList: populationThemeTermList,
  });

  const formatVillagePopulation = useFormat({
    setList: setVillageThemeData,
    data: villagePopulationThemeData,
    termList: populationThemeTermList,
  });

  const [targetList, setTargetList] = useState({
    year: [],
    theme: [],
  });

  const [showData, setShowData] = useState({
    id: '',
    unit: '',
    value: 0,
  });

  const isFetchingYear = targetList.year.length === 0;
  const isFetchingTarget =
    target[scale].year === '' || target[scale].theme === '';
  const isFetchingThemeData =
    townThemeData.length === 0 || villageThemeData.length === 0;
  const isFetchingShowData = showData.id === '';

  const handleSelect = (e) => {
    setTarget({
      ...target,
      [scale]: { ...target[scale], [e.target.name]: e.target.value },
    });
  };

  useEffect(() => {
    const years = data.map((item) => {
      return item.year;
    });
    const themes = themeList.map((item) => {
      const { id, unit } = item;
      return id;
    });
    setTargetList({
      ...targetList,
      year: [...years],
      theme: [...themes],
    });
  }, []);

  useEffect(() => {
    if (!isFetchingTarget && !isFetchingThemeData) {
      let themeData = [];
      switch (scale) {
        case 'town':
          themeData = townThemeData;
          break;
        case 'village':
          themeData = villageThemeData;
          break;
        default:
          themeData = [];
          break;
      }
      if (type === 'population') {
        const matchThemeDataByYear = themeData.find(
          (v) =>
            Number(v['資料時間'].split('Y')[0]) + 1911 ===
            Number(target[scale].year)
        );
        const matchThemeDataByTheme = Object.entries(matchThemeDataByYear).find(
          ([key]) => key === target[scale].theme
        );
        const matchThemeListByTheme = populationThemeList.find(
          (v) => v.id === matchThemeDataByTheme[0]
        );
        setShowData({
          ...matchThemeListByTheme,
          value: matchThemeDataByTheme[1],
        });
      }
    }
  }, [target, isFetchingThemeData]);

  return (
    <>
      <section className="u-section">
        <Form className="c-select d-flex flex-column align-items-start">
          <div className="w-100 d-flex flex-column flex-md-row">
            {Object.entries(target[scale]).map(([key]) => {
              const title = key === 'year' ? '年份' : '主題';
              const unit = key === 'year' ? '年' : '';
              return (
                <Form.Group
                  key={key}
                  className="c-select__set w-100 mb-2 mb-md-0"
                >
                  <Form.Select
                    name={key}
                    className="c-select__input"
                    value={target[scale][key]}
                    onChange={(e) => handleSelect(e)}
                  >
                    <option value="" disabled>
                      請選擇{title}
                    </option>
                    {!isFetchingYear &&
                      targetList[key].map((item) => {
                        return (
                          <option
                            key={item}
                            value={item}
                            disabled={item === target[scale][key]}
                          >
                            {item}
                            {unit}
                          </option>
                        );
                      })}
                  </Form.Select>
                </Form.Group>
              );
            })}
          </div>
          {!isFetchingShowData && (
            <div className="c-select__text my-2">
              <span className="c-select__seg">
                {scale === 'village' ? '芳苑鄉' : '彰化縣'}總{showData.id}:
              </span>
              <span>
                {showData.value}
                {showData.unit}
              </span>
            </div>
          )}
        </Form>
      </section>
    </>
  );
}

export default MapSelect;
