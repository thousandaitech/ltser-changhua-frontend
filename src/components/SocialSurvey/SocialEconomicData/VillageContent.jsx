import React, { useEffect, useState } from 'react';

import PopulationContent from 'components/SocialSurvey/SocialEconomicData/PopulationContent';
import VillageEconomyAndIndustryContent from 'components/SocialSurvey/SocialEconomicData/VillageEconomyAndIndustryContent';

import TagTab from 'components/TagTab';

import village from 'data/changhua_village_FY_geo.json';

import { villagePopulationData } from 'data/village';
import { useLocation } from 'react-router-dom';

function VillageContent() {
  const searchParams = new URLSearchParams(useLocation().search).get('tag');
  const [filter, setFilter] = useState({
    tag: 'population',
  });

  const tabList = [
    {
      id: 'population',
      title: '人口概況',
      content: (
        <PopulationContent
          scale="village"
          rawData={villagePopulationData}
          map={village}
        />
      ),
    },
    {
      id: 'economy-and-industry',
      title: '經濟與產業',
      content: <VillageEconomyAndIndustryContent />,
    },
  ];

  useEffect(() => {
    if (searchParams === 'economy-and-industry') {
      setFilter({ ...filter, tag: 'economy-and-industry' });
    }
  }, []);

  return (
    <>
      <article className="l-social-economy-data">
        <TagTab list={tabList} filter={filter} setFilter={setFilter} />
        {tabList.find((item) => item.id === filter.tag).content}
      </article>
    </>
  );
}

export default VillageContent;
