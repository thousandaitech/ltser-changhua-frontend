import React, { useState, useRef } from 'react';

import HandleAllData from 'components/SocialSurvey/SocialEconomicData/HandleAllData';
import MapSelect from 'components/SocialSurvey/SocialEconomicData/MapSelect';
import AreaMap from 'components/SocialSurvey/SocialEconomicData/AreaMap';
import LineGraph from 'components/SocialSurvey/SocialEconomicData/LineGraph';
import HandleGraphData from 'components/SocialSurvey/SocialEconomicData/HandleGraphData';
import BarGraph from 'components/SocialSurvey/SocialEconomicData/BarGraph';
import ComparasionSelect from 'components/SocialSurvey/SocialEconomicData/ComparasionSelect';
import GraphRow from 'components/SocialSurvey/SocialEconomicData/GraphRow';

import {
  targetList,
  economyAndIndustryThemeList,
  economyAndIndustryThemeTermList,
  agriculturePlantedAreaTermList,
  agricultureYieldAreaTermList,
  agricultureYieldAmountTermList,
  fisheryTermList,
  animalHusbandaryPoultryTermList,
  animalHusbandaryLivestockTermList,
} from 'data/social-economic-data';
import {
  townEconomyAndIndustryMapData,
  townIndustryAndCommerceData,
  townAgricultureData,
  townFisheryData,
  townAnimalHusbandaryPoultryData,
  townAnimalHusbandaryLivestockData,
} from 'data/town';

import town from 'data/changhua_town_geo.json';

function EconomyAndIndustryContent({ filter }) {
  const [allData, setAllData] = useState([]);
  const comparasionRef = useRef();
  const [select, setSelect] = useState({
    town: {
      year: '2021',
      theme: '耕作地面積',
    },
    village: {
      year: '2021',
      theme: '耕作地面積',
    },
  });
  const [comparasion, setComparasion] = useState({
    town: {
      main: '芳苑鄉',
      sub: '',
    },
    village: {
      main: '芳苑村',
      sub: '',
    },
  });

  const [plantAreaData, setPlantAreaData] = useState([]);
  const [yieldAreaData, setYieldAreaData] = useState([]);
  const [yieldAmountData, setYieldAmountData] = useState([]);
  const [fisheryData, setFisheryData] = useState([]);
  const [poultryData, setPoultryData] = useState([]);
  const [livestockData, setLivestockData] = useState([]);

  const graphType = {
    overall: {
      industryAndCommerce: {
        id: 'industryAndCommerce',
        col: '行政區工商家數',
        title: {
          line: '歷年行政區工商總家數',
          bar: '工商業總家數',
        },
        unit: '家',
        data: townIndustryAndCommerceData,
      },
    },
    agriculture: {
      plantArea: {
        id: 'plantArea',
        col: '耕作地面積',
        title: {
          line: '歷年耕作地面積變化',
          bar: '農耕土地面積',
        },
        unit: '公頃',
        data: townAgricultureData,
        filterList: ['鄉鎮市區名稱', '耕作地/合計', '長期休閒地'],
        termList: agriculturePlantedAreaTermList,
        graphData: plantAreaData,
        setGraphData: setPlantAreaData,
      },
      yieldArea: {
        id: 'yieldArea',
        col: '作物面積總計',
        title: {
          line: '歷年農產品收穫面積變化',
          bar: '各類別農產品收穫面積',
        },
        unit: '公頃',
        data: townAgricultureData,
        filterList: [
          '鄉鎮市區名稱',
          '稻米合計_收穫面積',
          '果品總計_收穫面積',
          '雜糧總計_收穫面積',
          '特用作物總計_收穫面積',
        ],
        termList: agricultureYieldAreaTermList,
        graphData: yieldAreaData,
        setGraphData: setYieldAreaData,
      },
      yieldAmount: {
        id: 'yieldAmount',
        col: '作物產量總計(稻米、蔬菜、果品、特用、雜糧)',
        title: {
          line: '歷年農產品產量變化',
          bar: '各類別農產品產量',
        },
        unit: '公噸',
        data: townAgricultureData,
        filterList: [
          '鄉鎮市區名稱',
          '稻米合計_產量',
          '果品總計_產量',
          '雜糧總計_產量',
          '特用作物總計_產量',
        ],
        termList: agricultureYieldAmountTermList,
        graphData: yieldAmountData,
        setGraphData: setYieldAmountData,
      },
    },
    fishery: {
      numberOfPeople: {
        id: 'numberOfPeople',
        col: '漁戶人口數(遠洋、近海、沿海、海面養殖、內陸漁撈、內陸養殖)',
        title: {
          line: '歷年漁戶人口數變化',
          bar: '各類別漁業人口數',
        },
        unit: '人',
        data: townFisheryData,
        filterList: [
          '鄉鎮市區名稱',
          '人口數_遠洋漁業',
          '人口數_近海漁業',
          '人口數_沿海漁業',
          '人口數_海面養殖業',
          '人口數_內陸魚撈業',
          '人口數_內陸養殖業',
        ],
        termList: fisheryTermList,
        graphData: fisheryData,
        setGraphData: setFisheryData,
      },
    },
    animalHusbandry: {
      poultry: {
        id: 'poultry',
        col: '家禽數(蛋雞、肉雞、蛋鴨、肉鴨、鵝、火雞)',
        title: {
          line: '歷年家禽總數變化',
          bar: '家禽類別數量',
        },
        unit: '千隻',
        data: townAnimalHusbandaryPoultryData,
        filterList: [
          '鄉鎮市區名稱',
          '蛋雞',
          '肉雞',
          '蛋鴨',
          '肉鴨',
          '鵝',
          '火雞',
        ],
        termList: animalHusbandaryPoultryTermList,
        graphData: poultryData,
        setGraphData: setPoultryData,
      },
      livestock: {
        id: 'livestock',
        col: '家畜數(牛、馬、豬、鹿、兔、羊)',
        title: {
          line: '歷年家畜總數變化',
          bar: '家畜類別數量',
        },
        unit: '頭',
        data: townAnimalHusbandaryLivestockData,
        filterList: ['鄉鎮市區名稱', '乳牛', '馬', '豬', '鹿', '兔', '羊'],
        termList: animalHusbandaryLivestockTermList,
        graphData: livestockData,
        setGraphData: setLivestockData,
      },
    },
  };

  return (
    <>
      <section className="l-social-economy-data__economy-and-industry">
        <HandleAllData
          setAllData={setAllData}
          rawData={townEconomyAndIndustryMapData}
          termList={economyAndIndustryThemeTermList}
        />
        <div className="mb-3 text-end text-muted">
          資料來源： 內政部社會經濟資料服務平台
        </div>
        <section className="l-social-economy-data__map u-section">
          <MapSelect
            scale="town"
            type="economyAndIndustry"
            target={select}
            setTarget={setSelect}
            data={townEconomyAndIndustryMapData}
            themeList={economyAndIndustryThemeList}
          />
          <AreaMap
            scale="town"
            map={town}
            target={select}
            allData={allData}
            themeList={economyAndIndustryThemeList}
            comparasion={comparasion}
            setComparasion={setComparasion}
            comparasionRef={comparasionRef}
            filter={filter}
          />
        </section>
        <section
          className="l-social-economy-data__graph u-section"
          ref={comparasionRef}
        >
          <ComparasionSelect
            scale="town"
            tab="economy-and-industry"
            target={comparasion}
            setTarget={setComparasion}
            targetList={targetList}
          />
          {Object.entries(graphType).map(([key, value]) => {
            switch (key) {
              case 'agriculture':
                return (
                  <React.Fragment key={key}>
                    <h4 className="l-social-economy-data__subtitle">農業</h4>
                    {Object.keys(value).map((subKey, i) => {
                      return (
                        <GraphRow
                          key={subKey}
                          graphType={graphType}
                          mainKey={key}
                          subKey={subKey}
                          comparasion={comparasion}
                          allData={allData}
                          select={select}
                        />
                      );
                    })}
                  </React.Fragment>
                );
              case 'fishery':
                return (
                  <React.Fragment key={key}>
                    <h4 className="l-social-economy-data__subtitle">漁業</h4>
                    {Object.keys(value).map((subKey, i) => {
                      return (
                        <GraphRow
                          key={subKey}
                          graphType={graphType}
                          mainKey={key}
                          subKey={subKey}
                          comparasion={comparasion}
                          allData={allData}
                          select={select}
                        />
                      );
                    })}
                  </React.Fragment>
                );
              case 'animalHusbandry':
                return (
                  <React.Fragment key={key}>
                    <h4 className="l-social-economy-data__subtitle">畜牧業</h4>
                    {Object.keys(value).map((subKey, i) => {
                      return (
                        <GraphRow
                          key={subKey}
                          graphType={graphType}
                          mainKey={key}
                          subKey={subKey}
                          comparasion={comparasion}
                          allData={allData}
                          select={select}
                        />
                      );
                    })}
                  </React.Fragment>
                );
              default:
                return;
            }
          })}
          <div className="l-social-economy-data__section">
            <h4 className="l-social-economy-data__subtitle">工商總家數</h4>
            <div className="row g-5">
              <LineGraph
                scale="town"
                col={graphType.overall.industryAndCommerce.col}
                title={graphType.overall.industryAndCommerce.title.line}
                unit={graphType.overall.industryAndCommerce.unit}
                target={comparasion}
                allData={allData}
              />
              <BarGraph
                scale="town"
                title={graphType.overall.industryAndCommerce.title.bar}
                unit={graphType.overall.industryAndCommerce.unit}
                target={comparasion}
                allData={graphType.overall.industryAndCommerce.data}
                select={select}
              />
            </div>
          </div>
        </section>
      </section>
    </>
  );
}

export default EconomyAndIndustryContent;
