import React, { useState, useEffect } from 'react';
import ReactECharts from 'echarts-for-react';

import { commonOptions } from 'data/eco-environmental-survey';
import { GRAPH_HEIGHT, GRAPH_COLOR } from 'data/common';

function BarGraph(props) {
  const { scale, title, unit, target, allData, select } = props;
  const { main, sub } = target[scale];
  const graphYear = select[scale].year;
  const [xAxis, setXAxis] = useState([]);
  const [series, setSeries] = useState({
    main: [],
    sub: [],
  });
  const [mainData, setMainData] = useState([]);
  const [subData, setSubData] = useState([]);

  const height = GRAPH_HEIGHT.default;
  const isNoSub = !sub || sub === '';
  const isFetchingData = allData.length === 0;
  const isFetchingMain = mainData.length === 0;
  const isFetchingSub = subData.length === 0;

  const option = {
    ...commonOptions,
    color: GRAPH_COLOR.legend.double,
    title: {
      text: `${graphYear}年${title}`,
    },
    dataZoom: {
      show: true,
    },
    tooltip: {
      trigger: 'axis',
      backgroundColor: GRAPH_COLOR.tooltip.background.dark,
      axisPointer: {
        type: 'shadow',
        label: {
          show: true,
        },
      },
    },
    legend: {
      top: 'auto',
      left: 'center',
      bottom: '12.5%',
      itemGap: 5,
    },
    grid: {
      width: '90%',
      left: '15%',
      height: '60%',
    },
    xAxis: {
      type: 'category',
      boundaryGap: true,
      data: xAxis,
    },
    yAxis: {
      name: `(${unit})`,
      type: 'value',
    },
    series: [
      {
        data: [...series.main],
        type: 'bar',
        name: target[scale].main,
      },
      {
        data: [...series.sub],
        type: 'bar',
        name: target[scale].sub,
      },
    ],
  };

  const handleKey = (key, result) => {
    switch (key) {
      case '縣市代碼':
      case '縣市名稱':
      case '鄉鎮市區代碼':
      case '鄉鎮市區名稱':
      case '資料時間':
        return '-';
      default:
        return result;
    }
  };

  useEffect(() => {
    if (!isFetchingData) {
      const main = allData
        .find((item) => item.year === graphYear)
        .data.find((v) => v['鄉鎮市區名稱'] === target[scale].main);
      setMainData({ ...main });
    }
  }, [allData, target[scale].main, graphYear]);

  useEffect(() => {
    if (!isFetchingData && !isNoSub) {
      const sub = allData.find((item) => {
        const { year, data } = item;
        if (year === graphYear) {
          return data.find((v) => v['鄉鎮市區名稱'] === target[scale].sub);
        }
      });
      setSubData({ ...sub });
    }
  }, [allData, target[scale].sub, graphYear]);

  useEffect(() => {
    if (!isFetchingMain) {
      const x = Object.entries(mainData)
        .map(([key, value]) => handleKey(key, key))
        .filter((item) => item !== '-');
      setXAxis([...x]);
      const seriesData = Object.entries(mainData)
        .map(([key, value]) => handleKey(key, value))
        .filter((item) => item !== '-');
      setSeries({
        ...series,
        main: [...seriesData],
      });
    }
  }, [mainData]);

  useEffect(() => {
    if (!isFetchingSub) {
      const seriesData = Object.entries(subData)
        .map(([key, value]) => handleKey(key, value))
        .filter((item) => item !== '-');
      setSeries({
        ...series,
        sub: [...seriesData],
      });
    }
  }, [subData]);

  return (
    <>
      <div className="col-12 col-md-6">
        <ReactECharts
          option={option}
          notMerge={true}
          lazyUpdate={true}
          opts={{ renderer: 'canvas', height: `${height}` }}
          style={{ height }}
        />
      </div>
    </>
  );
}

export default BarGraph;
