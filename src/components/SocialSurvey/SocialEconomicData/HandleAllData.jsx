import React, { useEffect } from 'react';

function HandleAllData(props) {
  const { setAllData, rawData, termList } = props;

  useEffect(() => {
    const formatData = rawData.map((item) => {
      const { year, data } = item;
      const newData = data.map((obj) => {
        termList.forEach((key) => {
          const { oldKey, newKey } = key;
          const isExistKey =
            obj[oldKey] || obj[oldKey] === '' || obj[oldKey] === 0;
          if (isExistKey) {
            Object.defineProperty(
              obj,
              newKey,
              Object.getOwnPropertyDescriptor(obj, oldKey)
            );
            delete obj[oldKey];
          }
        });
        return obj;
      });
      return {
        year: year,
        data: [...newData],
      };
    });
    setAllData([...formatData]);
  }, []);

  return null;
}

export default HandleAllData;
