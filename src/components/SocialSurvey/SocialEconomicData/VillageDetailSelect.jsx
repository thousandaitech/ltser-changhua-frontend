import React from 'react';

import Form from 'react-bootstrap/Form';

import { targetList } from 'data/social-economic-data';

function VillageDetailSelect(props) {
  const { detail, setDetail, comparasion, setComparasion } = props;
  const selectList = [
    {
      id: 'county',
      title: '縣市',
      list: [
        {
          id: 1,
          title: '彰化縣',
        },
      ],
    },
    {
      id: 'town',
      title: '鄉鎮',
      list: [
        {
          id: 1,
          title: '芳苑鄉',
        },
      ],
    },
    {
      id: 'village',
      title: '村里',
      list: targetList.village,
    },
  ];
  const handleSelect = (e) => {
    setDetail({
      ...detail,
      [e.target.name]: e.target.value,
    });
    if (e.target.name === 'village') {
      setComparasion({
        ...comparasion,
        village: {
          ...comparasion.village,
          main: e.target.value,
        },
      });
    }
  };
  return (
    <>
      <Form className="c-select d-flex flex-column align-items-start flex-lg-row align-items-lg-center mb-4">
        <h4 className="c-select__title mb-2 mb-lg-0 text-nowrap">進階搜尋:</h4>
        <div className="w-100 d-flex flex-column flex-md-row">
          {selectList.map((item) => {
            const { id, title, list } = item;
            return (
              <Form.Group key={id} className="c-select__set mb-2 mb-lg-0">
                <Form.Label className="c-select__label">{title}</Form.Label>
                <Form.Select
                  className="c-select__input"
                  name={id}
                  value={detail[id]}
                  onChange={handleSelect}
                >
                  <option value="" disabled>
                    請選擇{title}
                  </option>
                  {list.map((item) => {
                    const { id, title } = item;
                    const isAll = id === 0;
                    return (
                      !isAll && (
                        <option key={id} value={title}>
                          {title}
                        </option>
                      )
                    );
                  })}
                </Form.Select>
              </Form.Group>
            );
          })}
        </div>
      </Form>
    </>
  );
}

export default VillageDetailSelect;
