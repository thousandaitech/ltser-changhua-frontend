import React, { useState, useEffect } from 'react';
import ReactECharts from 'echarts-for-react';

import { commonOptions } from 'data/eco-environmental-survey';
import { GRAPH_HEIGHT, GRAPH_COLOR } from 'data/common';
import { villagePopulationThemeData } from 'data/village';

function LineGraph(props) {
  const { scale, col, title, unit, target, allData } = props;
  const { main, sub } = target[scale];
  const [xAxis, setXAxis] = useState([]);
  const [mainYAxis, setMainYAxis] = useState([]);
  const [subYAxis, setSubYAxis] = useState([]);

  const height = GRAPH_HEIGHT.default;
  const isNoSub = !sub || sub === '';
  const isFetchingData = allData.length === 0;

  const handleScaleKey = () => {
    switch (scale) {
      case 'town':
        return '鄉鎮市區名稱';
      case 'village':
        return '村里名稱';
      default:
        return;
    }
  };

  const option = {
    ...commonOptions,
    color: GRAPH_COLOR.legend.double,
    title: {
      text: title,
    },
    dataZoom: {
      show: true,
      start: 0,
      end: 100,
      top: '85%',
    },
    tooltip: {
      trigger: 'axis',
      backgroundColor: GRAPH_COLOR.tooltip.background.dark,
    },
    legend: {
      top: 'auto',
      left: 'center',
      bottom: '0',
    },
    grid: {
      left: '15%',
      right: '10%',
      height: '60%',
    },
    xAxis: {
      type: 'category',
      name: '(年)',
      boundaryGap: true,
      data: xAxis,
      axisLabel: {
        interval: 'auto',
        rotate: 45,
      },
    },
    yAxis: {
      name: `(${unit})`,
      type: 'value',
    },
    series: [
      {
        type: 'line',
        data: mainYAxis,
        name: main,
      },
      {
        type: 'line',
        data: subYAxis,
        name: sub,
      },
    ],
  };

  useEffect(() => {
    if (!isFetchingData) {
      const x = allData.map((item) => {
        return `${item.year}`;
      });
      setXAxis([...x]);
      const main = allData.map((item) => {
        const { year, data } = item;
        let finalData = data;
        if (scale === 'village') {
          const townData = villagePopulationThemeData.find(
            (o) => o['資料時間'] === `${Number(year - 1911)}Y`
          );
          finalData = [...data, { ...townData, 村里名稱: '全芳苑鄉' }];
        }
        const matchMain = finalData.find(
          (v) => v[handleScaleKey()] === target[scale].main
        );
        return matchMain[col];
      });
      setMainYAxis([...main]);
    }
  }, [allData, target[scale]]);

  useEffect(() => {
    if (!isFetchingData && !isNoSub) {
      const sub = allData.map((item) => {
        const { year, data } = item;
        let finalData = data;
        if (scale === 'village') {
          const townData = villagePopulationThemeData.find(
            (o) => o['資料時間'] === `${Number(year - 1911)}Y`
          );
          finalData = [...data, { ...townData, 村里名稱: '全芳苑鄉' }];
        }
        const matchSub = finalData.find(
          (v) => v[handleScaleKey()] === target[scale].sub
        );
        return matchSub[col];
      });
      setSubYAxis([...sub]);
    }
  }, [allData, target[scale]]);

  return (
    <>
      <div className="col-12 col-md-6">
        <ReactECharts
          option={option}
          notMerge={true}
          lazyUpdate={true}
          opts={{ renderer: 'canvas', height: `${height}` }}
          style={{ height }}
        />
      </div>
    </>
  );
}

export default LineGraph;
