import React, { useState, useEffect } from 'react';

import Form from 'react-bootstrap/Form';

import { villageAgeGenderData } from 'data/village';

function VillageDetailYearSelect(props) {
  const { year, setYear } = props;
  const [yearList, setYearList] = useState([]);
  const handleSelect = (e) => {
    setYear({ ...year, [e.target.name]: e.target.value });
  };
  const selectList = [
    {
      id: 'main',
      type: 'sub',
      style: 'solid',
      list: yearList,
    },
    {
      id: 'sub',
      type: 'main',
      style: 'line',
      list: yearList,
    },
  ];
  useEffect(() => {
    const years = villageAgeGenderData.map((item) => {
      return item.year;
    });
    setYearList([...years]);
  }, []);

  return (
    <>
      <Form className="c-select">
        {selectList.map((item) => {
          const { id, type, style, list } = item;
          return (
            <Form.Group key={id} className="c-select__set">
              <div className="c-select__deco" data-color={style}></div>
              <Form.Select
                className="c-select__input"
                name={id}
                value={year[id]}
                onChange={handleSelect}
              >
                <option value="" disabled>
                  請選擇年份
                </option>
                {list.map((item) => {
                  return (
                    <option
                      key={item}
                      value={item}
                      disabled={item === year[type]}
                    >
                      {item}年
                    </option>
                  );
                })}
              </Form.Select>
            </Form.Group>
          );
        })}
      </Form>
    </>
  );
}

export default VillageDetailYearSelect;
