import React, { useState, useEffect } from 'react';
import ReactECharts from 'echarts-for-react';

import { commonOptions } from 'data/eco-environmental-survey';
import { GRAPH_HEIGHT, GRAPH_COLOR } from 'data/common';

import HandleVillageGraphData from './HandleVillageGraphData';

import { villageOysterHorizontalHangingData } from 'data/village';
import { villageOysterHorizontalHangingTermList } from 'data/social-economic-data';

function BarLineGraph() {
  const [xAxis, setXAxis] = useState([]);
  const [data, setData] = useState({
    bar: [],
    line: [],
  });
  const [series, setSeries] = useState({
    bar: [],
    line: [],
  });
  const height = GRAPH_HEIGHT.default;
  const isFetchingBar = data.bar.length === 0;
  const isFetchingSeries = series.bar.length === 0 || series.line.length === 0;

  const handleSeries = (data, col) => {
    let target = [];
    data.forEach((item) => {
      Object.entries(item).forEach(
        ([key, value]) => key === col && target.push(value)
      );
    });
    return target;
  };

  useEffect(() => {
    if (!isFetchingBar) {
      const yearList = data.bar.map((item) => item['年份']);
      setXAxis([...yearList]);
      const bar = handleSeries(data.bar, '設施(組)');
      const line = handleSeries(data.line, '養殖（戶）');
      setSeries({ ...series, bar: [...bar], line: [...line] });
    }
  }, [data.bar, data.line]);

  const option = {
    ...commonOptions,
    color: GRAPH_COLOR.legend.double,
    title: {
      text: `芳苑鄉歷年牡蠣養殖(平掛式)`,
    },
    dataZoom: {
      show: true,
    },
    tooltip: {
      trigger: 'axis',
      backgroundColor: GRAPH_COLOR.tooltip.background.dark,
      axisPointer: {
        type: 'shadow',
        label: {
          show: true,
        },
      },
    },
    legend: {
      top: 'auto',
      left: 'center',
      bottom: '12.5%',
      itemGap: 5,
    },
    grid: {
      width: '80%',
      left: '12.5%',
      height: '60%',
    },
    xAxis: {
      type: 'category',
      boundaryGap: true,
      data: xAxis,
    },
    yAxis: {
      type: 'value',
    },
    series: [
      {
        data: [...series.bar],
        type: 'bar',
        name: '設施(組)',
      },
      {
        data: [...series.line],
        type: 'line',
        name: '養殖（戶）',
      },
    ],
  };

  return (
    <>
      <HandleVillageGraphData
        setData={setData}
        type="bar"
        rawData={villageOysterHorizontalHangingData.slice(2)}
        termList={villageOysterHorizontalHangingTermList}
        result="obj"
      />
      <HandleVillageGraphData
        setData={setData}
        type="line"
        rawData={villageOysterHorizontalHangingData.slice(2)}
        termList={villageOysterHorizontalHangingTermList}
        result="obj"
      />
      <div className="col-12">
        {!isFetchingSeries && (
          <ReactECharts
            option={option}
            notMerge={true}
            lazyUpdate={true}
            opts={{ renderer: 'canvas', height: `${height}` }}
            style={{ height }}
          />
        )}
      </div>
    </>
  );
}

export default BarLineGraph;
