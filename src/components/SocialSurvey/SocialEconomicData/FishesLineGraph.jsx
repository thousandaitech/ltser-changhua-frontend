import React, { useState, useEffect } from 'react';
import ReactECharts from 'echarts-for-react';

import { commonOptions } from 'data/eco-environmental-survey';
import { GRAPH_HEIGHT, GRAPH_COLOR } from 'data/common';

import HandleVillageGraphData from 'components/SocialSurvey/SocialEconomicData/HandleVillageGraphData';
import { villageFishesTermList } from 'data/social-economic-data';

function FishesLineGraph(props) {
  const { active, allData, item } = props;
  const { cols, title, unit } = item;
  const [rawData, setRawData] = useState([]);
  const [xAxis, setXAxis] = useState([]);
  const [data, setData] = useState([]);
  const [series, setSeries] = useState([]);
  const height = GRAPH_HEIGHT.default;

  const isFetchingData = data.length === 0;
  const isFetchingRawData = rawData.length === 0;
  const isFetchingSeries = series.length === 0;

  useEffect(() => {
    const matchRawData = allData[active.fish];
    setRawData([...matchRawData]);
  }, [active.fish]);

  useEffect(() => {
    if (!isFetchingData) {
      const yearList = data.map((item) => item['年份']);
      setXAxis([...yearList]);
      let seriesObj = {};
      cols.forEach((col) => {
        const filterCols = data.map((item) => item[col]);
        seriesObj = {
          ...seriesObj,
          [col]: filterCols,
        };
      });
      const seriesArr = Object.entries(seriesObj).map(([key, value]) => {
        return {
          data: [...value],
          type: 'line',
          name: key,
        };
      });
      setSeries([...seriesArr]);
    }
  }, [data, active.fish]);

  const option = {
    ...commonOptions,
    color: GRAPH_COLOR.legend.double,
    title: {
      text: title,
    },
    dataZoom: {
      show: true,
    },
    tooltip: {
      trigger: 'axis',
      backgroundColor: GRAPH_COLOR.tooltip.background.dark,
      axisPointer: {
        type: 'shadow',
        label: {
          show: true,
        },
      },
    },
    legend: {
      top: 'auto',
      left: 'center',
      bottom: '12.5%',
      itemGap: 5,
    },
    grid: {
      width: '80%',
      left: '12.5%',
      height: '60%',
    },
    xAxis: {
      type: 'category',
      boundaryGap: true,
      data: xAxis,
    },
    yAxis: {
      type: 'value',
      name: unit,
    },
    series: series,
  };

  return (
    <>
      {!isFetchingRawData && (
        <HandleVillageGraphData
          setData={setData}
          type={active.fish}
          rawData={rawData}
          termList={villageFishesTermList}
          result="arr"
        />
      )}
      {!isFetchingSeries && (
        <div className="col-12">
          <ReactECharts
            option={option}
            notMerge={true}
            lazyUpdate={true}
            opts={{ renderer: 'canvas', height: `${height}` }}
            style={{ height }}
          />
        </div>
      )}
    </>
  );
}

export default FishesLineGraph;
