import React, { useState, useRef } from 'react';

import HandleAllData from 'components/SocialSurvey/SocialEconomicData/HandleAllData';
import MapSelect from 'components/SocialSurvey/SocialEconomicData/MapSelect';
import AreaMap from 'components/SocialSurvey/SocialEconomicData/AreaMap';
import ComparasionSelect from 'components/SocialSurvey/SocialEconomicData/ComparasionSelect';
import LineGraph from 'components/SocialSurvey/SocialEconomicData/LineGraph';
import VillageDetailSelect from 'components/SocialSurvey/SocialEconomicData/VillageDetailSelect';
import VillageDetail from 'components/SocialSurvey/SocialEconomicData/VillageDetail';
import AgeGenderGraph from 'components/SocialSurvey/SocialEconomicData/AgeGenderGraph';
import AgeGroupBarGraph from 'components/SocialSurvey/SocialEconomicData/AgeGroupBarGraph';

import {
  targetList,
  populationThemeList,
  populationThemeTermList,
} from 'data/social-economic-data';

function PopulationContent(props) {
  const { scale, rawData, map, filter } = props;
  const comparasionRef = useRef();
  const [allData, setAllData] = useState([]);

  const [select, setSelect] = useState({
    town: {
      year: '2022',
      theme: '人口數',
    },
    village: {
      year: '2022',
      theme: '人口數',
    },
  });
  const [comparasion, setComparasion] = useState({
    town: {
      main: '芳苑鄉',
      sub: '',
    },
    village: {
      main: '芳苑村',
      sub: '',
    },
  });
  const [detail, setDetail] = useState({
    county: '彰化縣',
    town: '芳苑鄉',
    village: '芳苑村',
  });
  const [year, setYear] = useState({
    main: '2022',
    sub: '2021',
  });

  return (
    <>
      <section className="l-social-economy-data__population">
        <HandleAllData
          setAllData={setAllData}
          rawData={rawData}
          termList={populationThemeTermList}
        />
        <div className="mb-3 text-end text-muted">
          資料來源：內政部社會經濟資料服務平台
        </div>
        <section className="l-social-economy-data__map u-section">
          <MapSelect
            scale={scale}
            type="population"
            target={select}
            setTarget={setSelect}
            data={rawData}
            themeList={populationThemeList}
          />
          <AreaMap
            scale={scale}
            map={map}
            target={select}
            allData={allData}
            themeList={populationThemeList}
            comparasion={comparasion}
            setComparasion={setComparasion}
            comparasionRef={comparasionRef}
            detail={detail}
            setDetail={setDetail}
            filter={filter}
          />
        </section>
        {scale === 'village' && (
          <section className="l-social-economy-data__detail u-section">
            <VillageDetailSelect
              detail={detail}
              setDetail={setDetail}
              comparasion={comparasion}
              setComparasion={setComparasion}
            />
            <VillageDetail detail={detail} />
            <AgeGenderGraph detail={detail} year={year} setYear={setYear} />
          </section>
        )}
        <section
          className="l-social-economy-data__graph u-section"
          ref={comparasionRef}
        >
          <ComparasionSelect
            scale={scale}
            tab="population"
            target={comparasion}
            setTarget={setComparasion}
            targetList={targetList}
          />
          <div className="row g-5">
            {populationThemeList.map((item) => {
              const { id, unit } = item;
              const isNotShownGraph =
                id === '幼年人口數' ||
                id === '青壯年人口數' ||
                id === '老年人口數';
              return (
                !isNotShownGraph && (
                  <LineGraph
                    key={id}
                    scale={scale}
                    col={id}
                    title={id}
                    unit={unit}
                    target={comparasion}
                    allData={allData}
                  />
                )
              );
            })}
            <AgeGroupBarGraph
              scale={scale}
              target={comparasion}
              allData={allData}
            />
          </div>
        </section>
      </section>
    </>
  );
}

export default PopulationContent;
