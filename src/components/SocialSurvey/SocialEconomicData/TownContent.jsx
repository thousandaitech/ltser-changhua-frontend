import React, { useState } from 'react';

import PopulationContent from 'components/SocialSurvey/SocialEconomicData/PopulationContent';
import EconomyAndIndustryContent from 'components/SocialSurvey/SocialEconomicData/EconomyAndIndustryContent';

import TagTab from 'components/TagTab';

import town from 'data/changhua_town_geo.json';

import { townPopulationData } from 'data/town';

function TownContent() {
  const [filter, setFilter] = useState({
    tag: 'population',
  });

  const tabList = [
    {
      id: 'population',
      title: '人口概況',
      content: (
        <PopulationContent
          scale="town"
          rawData={townPopulationData}
          map={town}
          filter={filter}
        />
      ),
    },
    {
      id: 'economy-and-industry',
      title: '經濟與產業',
      content: <EconomyAndIndustryContent filter={filter} />,
    },
  ];
  return (
    <>
      <article className="l-social-economy-data">
        <TagTab list={tabList} filter={filter} setFilter={setFilter} />
        {tabList.find((item) => item.id === filter.tag).content}
      </article>
    </>
  );
}

export default TownContent;
