import React from 'react';

import LineGraph from 'components/SocialSurvey/SocialEconomicData/LineGraph';
import HandleGraphData from 'components/SocialSurvey/SocialEconomicData/HandleGraphData';
import BarGraph from 'components/SocialSurvey/SocialEconomicData/BarGraph';

function GraphRow(props) {
  const { graphType, mainKey, subKey, comparasion, allData, select } = props;
  return (
    <>
      <div className={`l-social-economy-data__section`}>
        <div className="row g-5">
          <LineGraph
            scale="town"
            col={graphType[mainKey][subKey].col}
            title={graphType[mainKey][subKey].title.line}
            unit={graphType[mainKey][subKey].unit}
            target={comparasion}
            allData={allData}
          />
          <HandleGraphData
            type={graphType[mainKey][subKey].id}
            graphData={graphType[mainKey][subKey].graphData}
            setGraphData={graphType[mainKey][subKey].setGraphData}
            rawData={graphType[mainKey][subKey].data}
            filterList={graphType[mainKey][subKey].filterList}
            termList={graphType[mainKey][subKey].termList}
          />
          <BarGraph
            scale="town"
            title={graphType[mainKey][subKey].title.bar}
            unit={graphType[mainKey][subKey].unit}
            target={comparasion}
            allData={graphType[mainKey][subKey].graphData}
            select={select}
          />
        </div>
      </div>
    </>
  );
}

export default GraphRow;
