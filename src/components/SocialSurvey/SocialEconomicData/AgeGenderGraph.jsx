import React, { useState, useEffect } from 'react';

import ReactECharts from 'echarts-for-react';

import VillageDetailYearSelect from 'components/SocialSurvey/SocialEconomicData/VillageDetailYearSelect';

import { commonOptions } from 'data/eco-environmental-survey';
import { villageAgeGenderData } from 'data/village';
import { GRAPH_HEIGHT, GRAPH_COLOR } from 'data/common';

function AgeGenderGraph(props) {
  const { detail, year, setYear } = props;
  const { village } = detail;
  const [main, setMain] = useState({
    male: [],
    female: [],
  });
  const [sub, setSub] = useState({
    male: [],
    female: [],
  });

  const ageList = [
    '0-4歲',
    '5-9歲',
    '10-14歲',
    '15-19歲',
    '20-24歲',
    '25-29歲',
    '30-34歲',
    '35-39歲',
    '40-44歲',
    '45-49歲',
    '50-54歲',
    '55-59歲',
    '60-64歲',
    '65-69歲',
    '70-74歲',
    '75-79歲',
    '80-84歲',
    '85-89歲',
    '90-94歲',
    '95-99歲',
    '100歲以上',
  ];

  const height = GRAPH_HEIGHT.lg;

  const option = {
    ...commonOptions,
    color: GRAPH_COLOR.legend.double,
    dataZoom: [
      {
        show: false,
      },
    ],
    tooltip: {
      trigger: 'axis',
      backgroundColor: GRAPH_COLOR.tooltip.background.light,
      textStyle: {
        color: GRAPH_COLOR.tooltip.text.light,
      },
      axisPointer: {
        type: 'shadow',
        label: {
          show: true,
        },
      },
    },
    grid: [
      {
        left: 'auto',
        right: '7%',
        top: '10%',
        height: '80%',
        width: '39%',
      },
      {
        right: 'auto',
        left: '7%',
        top: '10%',
        height: '80%',
        width: '39%',
      },
    ],
    legend: {
      top: 'auto',
      left: 'center',
      bottom: '0',
      itemStyle: {
        borderColor: GRAPH_COLOR.legend.border,
        borderWidth: 1,
      },
    },
    yAxis: [
      {
        type: 'category',
        boundaryGap: true,
        axisLine: { onZero: true },
        data: ageList,
        show: false,
      },
      {
        gridIndex: 1,
        type: 'category',
        boundaryGap: true,
        axisLine: { onZero: true },
        axisTick: 'inside',
        data: ageList,
        position: 'right',
      },
    ],
    xAxis: [
      {
        name: '女性(人)',
        type: 'value',
      },
      {
        gridIndex: 1,
        name: '男性(人)',
        type: 'value',
        inverse: true,
      },
    ],
    series: [
      {
        name: `${year.main}年男性`,
        type: 'bar',
        barGap: 0,
        xAxisIndex: 1,
        yAxisIndex: 1,
        data: main.male,
        itemStyle: {
          borderColor: GRAPH_COLOR.legend.double,
          borderWidth: 1,
        },
      },
      {
        name: `${year.main}年女性`,
        type: 'bar',
        barGap: 0,
        data: main.female,
        itemStyle: {
          borderColor: GRAPH_COLOR.legend.double,
          borderWidth: 1,
        },
      },
      {
        name: `${year.sub}年男性`,
        type: 'bar',
        barGap: 0,
        xAxisIndex: 1,
        yAxisIndex: 1,
        data: sub.male,
        itemStyle: {
          color: GRAPH_COLOR.legend.plain[0],
          borderColor: GRAPH_COLOR.legend.double,
          borderWidth: 1,
        },
      },
      {
        name: `${year.sub}年女性`,
        type: 'bar',
        barGap: 0,
        data: sub.female,
        itemStyle: {
          color: GRAPH_COLOR.legend.plain[1],
          borderColor: GRAPH_COLOR.legend.double,
          borderWidth: 1,
        },
      },
    ],
  };

  const matchedVillage = (year) => {
    return villageAgeGenderData
      .find((item) => item.year === year)
      .data.find((item) => item['村里名稱'] === village);
  };

  const matchedPopulation = (village, keyword) => {
    return Object.entries(village)
      .filter(([key]) => key.includes(keyword))
      .map((item) => Number(item[1]));
  };

  // const handleXAxisMax = (genderData) => {
  //   const data = [...genderData];
  //   return data.sort((a, b) => b - a)[0];
  // };

  useEffect(() => {
    setMain({
      male: [...matchedPopulation(matchedVillage(year.main), '男性')],
      female: [...matchedPopulation(matchedVillage(year.main), '女性')],
    });
    setSub({
      male: [...matchedPopulation(matchedVillage(year.sub), '男性')],
      female: [...matchedPopulation(matchedVillage(year.sub), '女性')],
    });
  }, [village, year]);

  // useEffect(() => {
  //   if (male && female) {
  //     const maleMax = handleXAxisMax(male);
  //     const femaleMax = handleXAxisMax(female);
  //     if (maleMax >= femaleMax) {
  //       setMax(maleMax + 20);
  //     } else {
  //       setMax(femaleMax + 20);
  //     }
  //   }
  // }, [male, female]);

  return (
    <>
      <div className="c-article">
        <h4 className="c-article__subtitle">人口金字塔</h4>
        <VillageDetailYearSelect
          detail={detail}
          year={year}
          setYear={setYear}
        />
        <ReactECharts
          option={option}
          notMerge={true}
          lazyUpdate={true}
          opts={{ renderer: 'canvas', height: `${height}` }}
          style={{ height }}
        />
      </div>
    </>
  );
}

export default AgeGenderGraph;
