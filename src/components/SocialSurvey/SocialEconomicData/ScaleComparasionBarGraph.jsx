import React, { useState, useEffect } from 'react';
import ReactECharts from 'echarts-for-react';

import { commonOptions } from 'data/eco-environmental-survey';
import { GRAPH_HEIGHT, GRAPH_COLOR } from 'data/common';

function ScaleComparasionBarGraph(props) {
  const { active, data, item } = props;
  const { title, col, unit, termList } = item;
  const [xAxis, setXAxis] = useState([]);
  const [series, setSeries] = useState([]);
  const height = GRAPH_HEIGHT.default;

  const isFetchingSeries = series.length === 0;

  const handleFilter = (col) => {
    const filterResult = Object.fromEntries(
      Object.entries(data).map(([key, value]) => {
        const matchYear = value.find((item) => item['年份'] === active.year);
        return [
          [key],
          Object.fromEntries(
            Object.entries(matchYear).filter(([key, value]) =>
              key.includes(col)
            )
          ),
        ];
      })
    );
    let termResult;
    termList.forEach((key) => {
      const { oldKey, newKey } = key;
      termResult = Object.fromEntries(
        Object.entries(filterResult).map(([k, v]) => {
          const isExistKey = v[oldKey] || v[oldKey] === '' || v[oldKey] === 0;
          if (isExistKey) {
            Object.defineProperty(
              v,
              newKey,
              Object.getOwnPropertyDescriptor(v, oldKey)
            );
            delete v[oldKey];
          }
          return [k, v];
        })
      );
    });
    return termResult;
  };

  useEffect(() => {
    const filterData = handleFilter(col);
    const series = Object.entries(filterData).map(([key, value]) => {
      const xAxis = Object.keys(value).map((k) => k);
      const yAxis = Object.values(value).map((v) => v);
      setXAxis([...xAxis]);
      return {
        data: [...yAxis],
        type: 'bar',
        name: key,
      };
    });
    setSeries([...series]);
  }, [data, active.year]);

  const option = {
    ...commonOptions,
    color: GRAPH_COLOR.legend.multiple,
    title: {
      text: `${title}`,
    },
    dataZoom: {
      show: true,
    },
    tooltip: {
      trigger: 'axis',
      backgroundColor: GRAPH_COLOR.tooltip.background.dark,
      axisPointer: {
        type: 'shadow',
        label: {
          show: true,
        },
      },
    },
    legend: {
      top: 'auto',
      left: 'center',
      bottom: '12.5%',
      itemGap: 5,
    },
    grid: {
      width: '80%',
      left: '12.5%',
      height: '60%',
    },
    xAxis: {
      type: 'category',
      boundaryGap: true,
      data: xAxis,
    },
    yAxis: {
      type: 'value',
      name: `(${unit})`,
    },
    series: series,
  };

  return (
    <>
      {!isFetchingSeries && (
        <div className="col-6">
          <ReactECharts
            option={option}
            notMerge={true}
            lazyUpdate={true}
            opts={{ renderer: 'canvas', height: `${height}` }}
            style={{ height }}
          />
        </div>
      )}
    </>
  );
}

export default ScaleComparasionBarGraph;
