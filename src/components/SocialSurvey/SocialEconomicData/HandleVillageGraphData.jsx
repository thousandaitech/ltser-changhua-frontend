import { useEffect } from 'react';

function HandleVillageGraphData(props) {
  const { setData, type, rawData, termList, result } = props;

  useEffect(() => {
    const formatData = rawData.map((obj) => {
      termList.forEach((key) => {
        const { oldKey, newKey } = key;
        const isExistKey =
          obj[oldKey] || obj[oldKey] === '' || obj[oldKey] === 0;
        if (isExistKey) {
          Object.defineProperty(
            obj,
            newKey,
            Object.getOwnPropertyDescriptor(obj, oldKey)
          );
          delete obj[oldKey];
        }
      });
      return Object.fromEntries(
        Object.entries(obj).map(([key, value]) => {
          const formatValue = String(value).includes(',')
            ? Number(value.replace(',', ''))
            : Number(value);
          return [[key], key === '年份' ? value : formatValue];
        })
      );
    });
    result === 'obj'
      ? setData((prev) => ({ ...prev, [type]: [...formatData] }))
      : setData((prev) => [...formatData]);
  }, [type]);

  return null;
}

export default HandleVillageGraphData;
