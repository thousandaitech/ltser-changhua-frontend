import React, { useState, useEffect, useRef } from 'react';
import ReactECharts from 'echarts-for-react';
import { useNavigate } from 'react-router-dom';

import townOverviewMapImg from 'img/social-survey/town-overview-map.png';
import villageOverviewMapImg from 'img/social-survey/village-overview-map.png';

import {
  GRAPH_HEIGHT,
  GRAPH_COLOR,
  handleOverviewMapSpec,
  handleResponsive,
} from 'data/common';
import useWindowDimensions from 'hooks/useWindowDimensions';

function AreaMap(props) {
  const {
    scale,
    map,
    target,
    allData,
    themeList,
    comparasion,
    setComparasion,
    comparasionRef,
    detail,
    setDetail,
    filter,
  } = props;
  const { year, theme } = target[scale];
  let echartRef = useRef(null);
  const navigate = useNavigate();
  const { width } = useWindowDimensions();

  const [mapData, setMapData] = useState([]);
  const [range, setRange] = useState({ min: 0, max: 0 });
  const [themeData, setThemeData] = useState({
    id: '',
    unit: '',
  });
  const [value, setValue] = useState(0);

  const height = GRAPH_HEIGHT.lg;
  const isTown = scale === 'town';
  const isFetchingData = allData.length === 0;
  const isFetchingMapData = mapData.length === 0;

  const handlePlaceTitle = () => {
    switch (scale) {
      case 'town':
        return '彰化縣各';
      case 'village':
        return '芳苑鄉各';
      default:
        return;
    }
  };
  const handleScaleTitle = () => {
    switch (scale) {
      case 'town':
        return '鄉鎮';
      case 'village':
        return '村里';
      default:
        return;
    }
  };
  const handleScaleCol = () => {
    switch (scale) {
      case 'town':
        return '鄉鎮市區名稱';
      case 'village':
        return '村里名稱';
      default:
        return;
    }
  };

  const handleScaleSeries = () => {
    if (isTown) {
      return [
        ...mapData.filter((item) => item.name !== '芳苑鄉'),
        {
          name: '芳苑鄉',
          value: Number(value),
          itemStyle: {
            borderColor: GRAPH_COLOR.legend.border,
            borderWidth: 3,
          },
        },
      ];
    } else {
      return [...mapData];
    }
  };

  const handleAreaClick = (params) => {
    if (params.name === '芳苑鄉') {
      navigate(
        `/social-survey/social-economic-data/fangyuan-township${
          filter.tag === 'economy-and-industry'
            ? '?tag=economy-and-industry'
            : ''
        }`
      );
    } else if (params.name) {
      setComparasion({
        ...comparasion,
        [scale]: {
          ...comparasion[scale],
          sub: params.name,
        },
      });
      if (detail) {
        setDetail({
          ...detail,
          [scale]: params.name,
        });
      }
      setTimeout(() => {
        comparasionRef.current.scrollIntoView({
          block: 'start',
          behavior: 'instant',
        });
      }, 0);
    } else {
      return;
    }
  };

  const onEvents = {
    click: handleAreaClick,
  };

  const option = {
    tooltip: {
      trigger: 'item',
      formatter: `{b} ${target[scale].year}年<br/>${theme} {c}${themeData.unit}`,
      backgroundColor: GRAPH_COLOR.tooltip.background.dark,
    },
    visualMap: {
      type: 'continuous',
      min: range.min,
      max: range.max,
      text: ['高', '低'],
      realtime: false,
      calculable: true,
      inRange: {
        color: ['lightskyblue', 'yellow', 'orangered'],
      },
      top: 0,
      right: 0,
      formatter: function (value) {
        return `${value}${themeData.unit}`;
      },
    },
    series: [
      {
        type: 'map',
        name: `${handlePlaceTitle()}${handleScaleTitle()}主題面量圖`,
        map: 'town',
        aspectScale: 0.9,
        label: {
          show: true,
        },
        selectedMode: 'single',
        data: handleScaleSeries(),
        itemStyle: {
          borderColor: GRAPH_COLOR.legend.border,
          borderWidth: 0.25,
        },
      },
    ],
    graphic: [
      {
        type: 'group',
        left: '0%',
        top: '0%',
        z: 100,
        children: [
          {
            type: 'rect',
            left: 'center',
            top: 'center',
            shape: {
              ...handleOverviewMapSpec(width, scale),
            },
            style: {
              fill: 'transparent',
              stroke: '#999',
              lineWidth: 5,
            },
          },
          {
            type: 'image',
            left: 'center',
            top: 'center',
            style: {
              image:
                scale === 'town' ? townOverviewMapImg : villageOverviewMapImg,
              ...handleOverviewMapSpec(width, scale),
            },
          },
        ],
      },
    ],
  };

  useEffect(() => {
    if (echartRef.current !== null) {
      const echartInstance = echartRef.current.echarts;
      echartInstance.registerMap('town', JSON.stringify(map));
    }
  }, [echartRef.current]);

  useEffect(() => {
    const matchTheme = themeList.find((item) => item.id === theme);
    setThemeData({ ...matchTheme });
  }, [target[scale]]);

  useEffect(() => {
    if (!isFetchingData) {
      const matchYear = allData.find((item) => item.year === year);
      const filterData = matchYear.data.filter(
        (item) => item['村里名稱'] !== '全芳苑鄉'
      );
      const matchData = filterData.map((item) => {
        return (
          item['村里名稱'] !== '全芳苑鄉' && {
            name: item[handleScaleCol()],
            value: item[theme] === '' ? 0 : Number(item[theme]),
          }
        );
      });
      setMapData([...matchData]);
      const min = filterData.sort((a, b) => a[theme] - b[theme])[0][theme];
      const max = filterData.sort((a, b) => b[theme] - a[theme])[0][theme];
      setRange({ min: Number(min), max: Number(max) });
    }
  }, [allData, target[scale]]);

  useEffect(() => {
    if (isTown) {
      if (!isFetchingMapData) {
        const matchHighlight = mapData.find(
          (item) => item.name === '芳苑鄉'
        ).value;
        setValue(matchHighlight);
      }
    }
  }, [mapData]);

  return (
    <>
      <ReactECharts
        option={option}
        ref={echartRef}
        notMerge={true}
        lazyUpdate={true}
        opts={{ renderer: 'canvas', height: `${height}` }}
        style={{ height }}
        onEvents={onEvents}
      />
    </>
  );
}

export default AreaMap;
