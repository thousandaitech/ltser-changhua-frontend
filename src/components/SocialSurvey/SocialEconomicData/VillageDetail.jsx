import React, { useState, useEffect } from 'react';
import { targetList, populationThemeList } from 'data/social-economic-data';
import { villagePopulationData } from 'data/village';

function VillageDetail(props) {
  const { detail } = props;
  const { county, town, village } = detail;
  const [info, setInfo] = useState({
    id: '',
    title: '',
    content: '',
  });
  const [data, setData] = useState({
    year: '',
    data: [],
  });

  const isFetchingData = data.year === '';

  useEffect(() => {
    const matchedInfo = targetList.village.find(
      (item) => item.title === village
    );
    setInfo({ ...matchedInfo });
    const matchedData = villagePopulationData[villagePopulationData.length - 1];
    setData({ ...matchedData });
  }, [detail]);

  return (
    <>
      <div className="c-article">
        <h3 className="c-article__title">
          {county}
          {town}
          {village}
        </h3>
        <p className="c-article__paragraph">{info.content}</p>
        <div className="c-article__heading">
          <h4 className="c-article__subtitle">基礎資料</h4>
          <div className="d-flex flex-column aligh-items-end">
            <span className="c-article__subtext">更新時間: 2022/08</span>
            <span className="c-article__subtext">數據年份: {data.year}年</span>
          </div>
        </div>
        <div className="row g-3">
          {!isFetchingData &&
            populationThemeList.map((item) => {
              const { id, unit } = item;
              const result = data.data.find(
                (item) => item['村里名稱'] === village
              )[id];
              return (
                <div
                  key={id}
                  className="col-6 col-md-4 col-xl-3 col-xxl-2 d-flex align-items-stretch"
                >
                  <div className="c-card">
                    <div className="c-card__title">{id}</div>
                    <div className="d-flex align-items-center flex-wrap justify-content-center">
                      <span className="c-card__highlight">{result}</span>
                      <span className="c-card__subtext">{unit}</span>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </>
  );
}

export default VillageDetail;
