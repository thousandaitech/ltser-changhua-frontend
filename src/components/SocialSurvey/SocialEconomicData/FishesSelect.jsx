import React from 'react';

import { Form } from 'react-bootstrap';

function FishesSelect(props) {
  const { active, setActive, rawData } = props;
  const handleSelect = (e) => {
    setActive({
      ...active,
      [e.target.name]: e.target.value,
    });
  };
  return (
    <>
      <Form className="c-select">
        <Form.Group className="c-select__set">
          <Form.Select
            name="fish"
            className="c-select__input"
            value={active.fish}
            onChange={(e) => handleSelect(e)}
          >
            <option value="" disabled>
              請選擇魚種別
            </option>
            {Object.keys(rawData).map((key) => {
              return (
                <option key={key} value={key} disabled={key === active.rawData}>
                  {key}
                </option>
              );
            })}
          </Form.Select>
        </Form.Group>
      </Form>
    </>
  );
}

export default FishesSelect;
