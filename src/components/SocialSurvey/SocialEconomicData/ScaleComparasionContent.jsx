import React, { useState, useEffect } from 'react';

import HandleVillageGraphData from './HandleVillageGraphData';
import ScaleComparasionLineGraph from './ScaleComparasionLineGraph';
import ScaleComparasionBarGraph from './ScaleComparasionBarGraph';

import {
  villageOysterTermList,
  villageOysterFacilityTermList,
  villageOysterFarmingTermList,
} from 'data/social-economic-data';
import {
  countryOysterData,
  townOysterData,
  villageOysterData,
} from 'data/village';
import YearSelect from './YearSelect';

function ScaleComparasionContent(props) {
  const { active, setActive } = props;
  const [data, setData] = useState({});
  const [seriesName, setSeriesName] = useState([]);

  const isFetchingName = seriesName.length === 0;
  const isFetchingData = !isFetchingName && data[seriesName[0]].length === 0;

  const oysterData = {
    全國: countryOysterData,
    彰化縣: townOysterData,
    芳苑鄉: villageOysterData,
  };

  const oysterFarmingList = [
    {
      id: 'facility',
      title: '設施數',
      col: '設施',
      unit: '組、棚',
      termList: villageOysterFacilityTermList,
    },
    {
      id: 'farming',
      title: '養殖戶數',
      col: '養殖',
      unit: '戶',
      termList: villageOysterFarmingTermList,
    },
  ];

  useEffect(() => {
    let obj = {};
    let name = [];
    Object.entries(oysterData).forEach(([key, value], i) => {
      obj = { ...obj, [key]: [...value] };
      name.push(key);
    });
    setData({ ...obj });
    setSeriesName([...name]);
  }, []);

  return (
    <>
      {!isFetchingData &&
        Object.entries(data).map(([key, value]) => {
          return (
            <HandleVillageGraphData
              key={key}
              setData={setData}
              type={key}
              rawData={value}
              termList={villageOysterTermList}
              result="obj"
            />
          );
        })}
      {!isFetchingName && !isFetchingData && (
        <>
          <div className="l-social-economy-data__section">
            <ScaleComparasionLineGraph data={data} seriesName={seriesName} />
          </div>
          <div className="l-social-economy-data__section">
            <YearSelect
              active={active}
              setActive={setActive}
              data={data}
              seriesName={seriesName}
            />
            <div className="row">
              {oysterFarmingList.map((item) => {
                const { id } = item;
                return (
                  <ScaleComparasionBarGraph
                    key={id}
                    active={active}
                    data={data}
                    item={item}
                  />
                );
              })}
            </div>
          </div>
        </>
      )}
    </>
  );
}

export default ScaleComparasionContent;
