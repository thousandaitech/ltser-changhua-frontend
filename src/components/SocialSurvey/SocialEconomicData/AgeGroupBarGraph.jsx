import React, { useState, useEffect } from 'react';
import ReactECharts from 'echarts-for-react';

import { commonOptions } from 'data/eco-environmental-survey';
import { GRAPH_HEIGHT, GRAPH_COLOR } from 'data/common';
import { villagePopulationThemeData } from 'data/village';

function AgeGroupBarGraph(props) {
  const { scale, target, allData } = props;
  const { main, sub } = target[scale];
  const [xAxis, setXAxis] = useState();
  const [series, setSeries] = useState({
    main: [],
    sub: [],
  });
  const [mainData, setMainData] = useState([]);
  const [subData, setSubData] = useState([]);

  const seriesList = ['幼年人口數', '青壯年人口數', '老年人口數'];

  const height = GRAPH_HEIGHT.default;
  const isNoSub = !sub || sub === '';
  const isFetchingData = allData.length === 0;
  const isFetchingMain = mainData.length === 0;
  const isFetchingSub = subData.length === 0;

  const option = {
    ...commonOptions,
    color: GRAPH_COLOR.legend.multiple,
    title: {
      text: '年齡三段組',
    },
    dataZoom: {
      show: false,
    },
    tooltip: {
      backgroundColor: GRAPH_COLOR.tooltip.background.dark,
    },
    legend: {
      top: 'auto',
      left: 'center',
      bottom: '0',
    },
    grid: {
      width: '90%',
      left: '10%',
      height: '60%',
    },
    xAxis: {
      type: 'category',
      boundaryGap: true,
      data: xAxis,
    },
    yAxis: {
      name: `(人)`,
      type: 'value',
    },
    series: [...series.main, ...series.sub],
  };

  const handleScaleKey = () => {
    switch (scale) {
      case 'town':
        return '鄉鎮市區名稱';
      case 'village':
        return '村里名稱';
      default:
        return;
    }
  };

  const handleSeriesData = (data, item) => {
    return data.map((obj) => {
      return obj[item];
    });
  };

  useEffect(() => {
    if (!isFetchingData) {
      const x = allData.map((item) => {
        return `${item.year}年`;
      });
      setXAxis([...x]);
      const main = allData.map((item) => {
        const { year, data } = item;
        let finalData = data;
        if (scale === 'village') {
          const townData = villagePopulationThemeData.find(
            (o) => o['資料時間'] === `${Number(year - 1911)}Y`
          );
          finalData = [...data, { ...townData, 村里名稱: '全芳苑鄉' }];
        }
        const matchMain = finalData.find(
          (v) => v[handleScaleKey()] === target[scale].main
        );
        return matchMain;
      });
      setMainData([...main]);
    }
  }, [allData, target[scale].main]);

  useEffect(() => {
    if (!isFetchingData && !isNoSub) {
      const sub = allData.map((item) => {
        const { year, data } = item;
        let finalData = data;
        if (scale === 'village') {
          const townData = villagePopulationThemeData.find(
            (o) => o['資料時間'] === `${Number(year - 1911)}Y`
          );
          finalData = [...data, { ...townData, 村里名稱: '全芳苑鄉' }];
        }
        const matchSub = finalData.find(
          (v) => v[handleScaleKey()] === target[scale].sub
        );
        return matchSub;
      });
      setSubData([...sub]);
    }
  }, [allData, target[scale].sub]);

  useEffect(() => {
    if (!isFetchingMain) {
      const seriesData = seriesList.map((item) => {
        return {
          name: `${mainData[0][handleScaleKey()]}${item}`,
          type: 'bar',
          stack: 'main',
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowColor: 'rgba(0,0,0,0.3)',
            },
          },
          data: handleSeriesData(mainData, item),
        };
      });
      setSeries({
        ...series,
        main: [...seriesData],
      });
    }
  }, [mainData]);

  useEffect(() => {
    if (!isFetchingSub) {
      const seriesData = seriesList.map((item) => {
        return {
          name: `${subData[0][handleScaleKey()]}${item}`,
          type: 'bar',
          stack: 'sub',
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowColor: 'rgba(0,0,0,0.3)',
            },
          },
          data: handleSeriesData(subData, item),
        };
      });
      setSeries({
        ...series,
        sub: [...seriesData],
      });
    }
  }, [subData]);

  return (
    <>
      <div className="col-12 col-md-6">
        <ReactECharts
          option={option}
          notMerge={true}
          lazyUpdate={true}
          opts={{ renderer: 'canvas', height: `${height}` }}
          style={{ height }}
        />
      </div>
    </>
  );
}

export default AgeGroupBarGraph;
