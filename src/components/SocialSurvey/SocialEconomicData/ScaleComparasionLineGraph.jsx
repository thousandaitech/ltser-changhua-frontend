import React, { useState, useEffect } from 'react';
import ReactECharts from 'echarts-for-react';

import { commonOptions } from 'data/eco-environmental-survey';
import { GRAPH_HEIGHT, GRAPH_COLOR } from 'data/common';

function ScaleComparasionLineGraph(props) {
  const { data, seriesName } = props;
  const [xAxis, setXAxis] = useState([]);

  const [series, setSeries] = useState([]);
  const height = GRAPH_HEIGHT.default;

  const isFetchingSeries = series.length === 0;

  const handleSeries = (data, col) => {
    let target = [];
    data.forEach((item) => {
      Object.entries(item).forEach(
        ([key, value]) => key === col && target.push(value)
      );
    });
    return target;
  };

  useEffect(() => {
    const yearList = data[seriesName[0]].map((item) => item['年份']);
    setXAxis([...yearList]);
    let seriesObj = {};
    Object.entries(data).forEach(([key, value]) => {
      seriesObj = {
        ...seriesObj,
        [key]: handleSeries(value, '申報（調查）總戶數'),
      };
    });
    const seriesArr = Object.entries(seriesObj).map(([key, value]) => {
      return {
        data: [...value],
        type: 'line',
        name: key,
      };
    });
    setSeries([...seriesArr]);
  }, [data]);

  const option = {
    ...commonOptions,
    color: GRAPH_COLOR.legend.multiple,
    title: {
      text: `牡蠣養殖申報(調查)總戶數`,
    },
    dataZoom: {
      show: true,
    },
    tooltip: {
      trigger: 'axis',
      backgroundColor: GRAPH_COLOR.tooltip.background.dark,
      axisPointer: {
        type: 'shadow',
        label: {
          show: true,
        },
      },
    },
    legend: {
      top: 'auto',
      left: 'center',
      bottom: '12.5%',
      itemGap: 5,
    },
    grid: {
      width: '80%',
      left: '12.5%',
      height: '60%',
    },
    xAxis: {
      type: 'category',
      boundaryGap: true,
      data: xAxis,
    },
    yAxis: {
      type: 'value',
    },
    series: series,
  };

  return (
    <>
      <div className="col-12">
        {!isFetchingSeries && (
          <ReactECharts
            option={option}
            notMerge={true}
            lazyUpdate={true}
            opts={{ renderer: 'canvas', height: `${height}` }}
            style={{ height }}
          />
        )}
      </div>
    </>
  );
}

export default ScaleComparasionLineGraph;
