import React from 'react';

import { Form } from 'react-bootstrap';

function YearSelect(props) {
  const { active, setActive, data, seriesName } = props;
  const handleSelect = (e) => {
    setActive({
      ...active,
      [e.target.name]: e.target.value,
    });
  };
  return (
    <>
      <Form className="c-select">
        <h4 className="c-select__title">養殖方式:</h4>
        <Form.Group className="c-select__set">
          <Form.Select
            name="year"
            className="c-select__input"
            value={active.year}
            onChange={(e) => handleSelect(e)}
          >
            <option value="" disabled>
              請選擇年份
            </option>
            {data[seriesName[0]].map((item, i) => {
              const year = item['年份'];
              return (
                <option key={i} value={year} disabled={year === active.year}>
                  {year}年
                </option>
              );
            })}
          </Form.Select>
        </Form.Group>
      </Form>
    </>
  );
}

export default YearSelect;
