import React, { useEffect } from 'react';

function HandleGraphData(props) {
  const { setGraphData, rawData, filterList, termList } = props;

  useEffect(() => {
    const filterData = rawData.map((item) => {
      const { year, data } = item;
      const filter = data.map((item) => {
        return {
          ...Object.fromEntries(
            Object.entries(item).filter(([key, value]) =>
              filterList.includes(key)
            )
          ),
        };
      });
      return {
        year: year,
        data: [...filter],
      };
    });
    const formatData = filterData.map((item) => {
      const { year, data } = item;
      const newData = data.map((obj) => {
        termList.forEach((key) => {
          const { oldKey, newKey } = key;
          const isExistKey =
            obj[oldKey] || obj[oldKey] === '' || obj[oldKey] === 0;
          if (isExistKey) {
            Object.defineProperty(
              obj,
              newKey,
              Object.getOwnPropertyDescriptor(obj, oldKey)
            );
            delete obj[oldKey];
          }
        });
        return obj;
      });
      return {
        year: year,
        data: [...newData],
      };
    });
    setGraphData([...formatData]);
  }, []);

  return null;
}

export default HandleGraphData;
