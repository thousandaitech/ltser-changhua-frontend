import React, { useState, useEffect } from 'react';

import localLiteratureImg from 'img/icon/local-literature.svg';

import ContentTemplate from 'components/ContentTemplate';

import { literatureList } from 'data/social-survey';
import useRender from 'hooks/useRender';
import usePage from 'hooks/usePage';
import Search from 'components/Search';

const Content = () => {
  const pageData = {
    title: '地方文獻',
    page: 'local-literature',
    icon: localLiteratureImg,
    link: '/social-survey/local-literature-and-news/local-literature',
    url: 'literatures',
  };

  const { loading, getListByPage } = useRender();
  const {
    currentPage,
    setCurrentPage,
    paginationData,
    setPaginationData,
    paginationList,
    isFetchingPages,
  } = usePage();

  const [filter, setFilter] = useState({
    keyword: '',
  });
  const [literatures, setLiteratures] = useState(null);
  const [refresh, setRefresh] = useState(false);
  const [search, setSearch] = useState('');

  const hasFilterKeyword = filter.keyword !== '';
  const hasSearch = search !== '';

  useEffect(() => {
    getListByPage({
      url: pageData.url,
      page: currentPage,
      setList: setLiteratures,
      defaultList: literatureList,
      setPaginationData,
      params: {
        keyword: hasFilterKeyword ? filter.keyword : null,
      },
    });
  }, [currentPage, filter.keyword, refresh]);

  return (
    <>
      <ContentTemplate
        pageData={pageData}
        data={literatures}
        tab={null}
        search={
          <Search
            loading={loading}
            filter={filter}
            setFilter={setFilter}
            setList={setLiteratures}
            url={pageData.url}
            currentPage={currentPage}
            setPaginationData={setPaginationData}
            search={search}
            setSearch={setSearch}
            hasSearch={hasSearch}
          />
        }
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        paginationData={paginationData}
        paginationList={paginationList}
        isFetchingPages={isFetchingPages}
        setRefresh={setRefresh}
      />
    </>
  );
};

export default Content;
