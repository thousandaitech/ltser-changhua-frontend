import { useContentContext } from 'context/Interview/ContentContext';
import { useMultipleContext } from 'context/Interview/MultipleContext';
import { useSingleContext } from 'context/Interview/SingleContext';
import React from 'react';

const Item = ({ data }) => {
  const { date, people, stakeholder, combined_tags, content } = data;
  const { activeTab } = useContentContext();
  const { tag } = useMultipleContext();
  const { filter } = useSingleContext();

  const target = activeTab === 1 ? tag : filter;

  return (
    <>
      <div className="l-interviews__item">
        <div className="d-flex flex-column align-item-center flex-md-row">
          <div className="d-flex mb-2 mb-md-0">
            {stakeholder.map((v, i) => {
              return (
                <h4 key={i} className="l-interviews__subtitle">
                  {v}
                </h4>
              );
            })}
          </div>
          <div className="d-flex flex-wrap">
            {people.map((v, i) => {
              return (
                <div
                  key={i}
                  className="l-interviews__tag l-interviews__tag--category c-tag"
                >
                  {v}
                </div>
              );
            })}
            {combined_tags.map((v, i) => {
              const isCategoryKeyword = v.title[0] === '0';
              const isChecked =
                activeTab === 1
                  ? target.tag2.includes(String(v.tag2)) ||
                    target.tag3.includes(String(v.tag3))
                  : target.tag3.includes(String(v.tag3));
              return isCategoryKeyword ? (
                <div
                  key={i}
                  className={`l-interviews__tag c-tag ${
                    isChecked ? 'active' : ''
                  }`}
                >
                  {v.title.split(' ')[1]}
                </div>
              ) : (
                <div
                  key={i}
                  className={`l-interviews__tag c-tag ${
                    isChecked ? 'active' : ''
                  }`}
                >
                  {v.title}
                </div>
              );
            })}
          </div>
        </div>
        <hr className="e-hr my-2" />
        <p className="l-interviews__text">{content}</p>
        <div className="l-interviews__subtext">訪談日期: {date}</div>
      </div>
    </>
  );
};

export default Item;
