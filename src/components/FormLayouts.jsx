import React, { useState, useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';

import { Form } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

import { useAuthContext } from 'context/AuthContext';

const FormLayouts = (props) => {
  const { form, setForm, fieldList } = props;
  const [pswVisible, setPswVisible] = useState(false);
  const { verificationHint } = useAuthContext();
  const verificationInputRef = useRef(null);
  const verificationSetRef = useRef(null);

  const handleTogglePsw = () => setPswVisible(!pswVisible);
  const handleInputChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const handleCheckboxChange = (e, id) => {
    const name = e.target.name;
    const value = e.target.value;
    if (form[name].includes(value)) {
      const remainList = form[name].filter((v) => v !== value);
      setForm({ ...form, [name]: remainList });
    } else {
      setForm({
        ...form,
        [name]: [...form[name], value],
      });
    }
  };

  const handleFocus = () => {
    if (verificationHint) {
      setTimeout(() => {
        verificationSetRef.current.scrollIntoView({
          behavior: 'smooth',
          block: 'center',
        });
      }, 0);
    }
  };

  useEffect(() => {
    if (verificationInputRef.current) {
      verificationInputRef.current.focus();
    }
  }, [verificationHint, verificationInputRef]);

  return fieldList.map((item) => {
    const { id, type, title, subtitle, options, readOnly, required, hints } =
      item;
    if (id === 'securityQuestion' && form.securityQuestion === undefined)
      return;
    switch (type) {
      case 'text':
      case 'email':
        const isEmail = type === 'email';
        return (
          <Form.Group
            key={id}
            className="c-form__set"
            controlId={id}
            ref={isEmail ? verificationSetRef : null}
          >
            <Form.Label className="c-form__label">
              {title}
              {required && <span className="c-form__error">*</span>}
            </Form.Label>
            <Form.Control
              type={type}
              placeholder={`請輸入${title}`}
              className="c-form__input"
              readOnly={readOnly}
              required={required}
              name={id}
              value={form[id]}
              ref={isEmail ? verificationInputRef : null}
              onChange={handleInputChange}
              onFocus={handleFocus}
            />
            {hints && (
              <div className="c-form__hints">
                {hints.map((v) => {
                  const { id, link, content } = v;
                  switch (id) {
                    case 'text':
                      return (
                        <div key={id} className="c-form__hint">
                          {content}
                        </div>
                      );
                    case 'forgot':
                      return (
                        <Link
                          key={id}
                          to={link}
                          className="c-form__link e-link"
                        >
                          忘記{title}
                        </Link>
                      );
                    case 'mail-verification':
                      return (
                        <Link
                          key={id}
                          to={link}
                          className="c-form__link e-link"
                        >
                          重發驗證信
                        </Link>
                      );
                    default:
                      return;
                  }
                })}
              </div>
            )}
            {/* {isEmail && verificationHint && (
              <div className="c-form__hint c-form__hint--error">
                帳號尚未驗證，請先至信箱收信，或
                <Link
                  to="/mail-verification"
                  className="c-form__link e-link mx-1"
                >
                  點此重發驗證信
                </Link>
                。
              </div>
            )} */}
          </Form.Group>
        );
      case 'select':
        return (
          <Form.Group key={id} className="c-form__set" controlId={id}>
            <Form.Label className="c-form__label">
              {title}
              {required && <span className="c-form__error">*</span>}
            </Form.Label>
            <Form.Select
              className="c-form__input"
              name={id}
              value={form[id]}
              onChange={handleInputChange}
              required={required}
            >
              <option value="" disabled>
                請選擇{title}
              </option>
              {options.map((v) => {
                return (
                  <option key={v} value={v} disabled={item === form[id]}>
                    {v}
                  </option>
                );
              })}
            </Form.Select>
          </Form.Group>
        );
      case 'checkbox':
        return (
          <div
            key={id}
            className="c-form__set c-checkbox c-checkbox--col c-checkbox--check"
          >
            <div className="c-checkbox__title">
              {title}
              {required && <span className="c-form__error">*</span>}
            </div>
            <div className="c-checkbox__row">
              {options.map((v, i) => {
                return (
                  <Form.Group
                    key={i}
                    className="c-checkbox__set"
                    controlId={`${id}${i}`}
                  >
                    <Form.Control
                      type="checkbox"
                      className="c-checkbox__input"
                      name={id}
                      value={v}
                      checked={form[id].includes(v)}
                      onChange={handleCheckboxChange}
                    />
                    <Form.Label className="c-checkbox__label">{v}</Form.Label>
                  </Form.Group>
                );
              })}
            </div>
          </div>
        );
      case 'password':
        return (
          <Form.Group key={id} className="c-form__set" controlId={id}>
            <Form.Label className="c-form__label">
              {title}
              {required && <span className="c-form__error">*</span>}
            </Form.Label>
            <div className="c-form__wrapper">
              <Form.Control
                type={pswVisible ? 'text' : 'password'}
                placeholder={`請輸入${title}`}
                className="c-form__input"
                readOnly={readOnly}
                required={required}
                name={id}
                value={form[id]}
                onChange={handleInputChange}
              />
              <button
                type="button"
                className="c-form__icon-btn e-btn"
                onClick={handleTogglePsw}
              >
                {pswVisible ? (
                  <FontAwesomeIcon
                    icon={solid('eye-slash')}
                    className="c-form__icon"
                  />
                ) : (
                  <FontAwesomeIcon
                    icon={solid('eye')}
                    className="c-form__icon"
                  />
                )}
              </button>
            </div>
            {hints && (
              <div className="c-form__hints">
                {hints.map((v) => {
                  const { id, link, content } = v;
                  switch (id) {
                    case 'text':
                      return (
                        <div key={id} className="c-form__hint">
                          {content}
                        </div>
                      );
                    case 'forgot':
                      return (
                        <Link
                          key={id}
                          to={link}
                          className="c-form__link e-link"
                        >
                          忘記{title}
                        </Link>
                      );
                    default:
                      return;
                  }
                })}
              </div>
            )}
          </Form.Group>
        );
      default:
        return;
    }
  });
};

export default FormLayouts;
