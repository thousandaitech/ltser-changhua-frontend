import React, { useEffect } from 'react';

import { Pagination } from 'react-bootstrap';

const PaginationLayout = (props) => {
  const { currentPage, setCurrentPage, paginationData, paginationList } = props;

  const hasPrevPage = currentPage - 1 > 0;
  const hasNextPage = currentPage + 1 <= paginationData.totalPages;

  const isFirstPage = currentPage === 1;
  const isLastPage = currentPage === paginationData.totalPages;

  const handlePage = (page) => {
    setCurrentPage(page);
    window.scrollTo({
      top: 0,
      behavior: 'instant',
    });
  };

  return (
    <>
      <div className="d-flex justify-content-center mt-4">
        <Pagination className="c-pagination">
          {!isFirstPage && (
            <Pagination.First
              className="c-pagination__btn"
              onClick={() => {
                handlePage(1);
              }}
            />
          )}
          {hasPrevPage && (
            <Pagination.Prev
              className="c-pagination__btn"
              onClick={() => {
                handlePage(currentPage - 1);
              }}
            />
          )}
          {paginationList.map((v) => {
            return (
              <Pagination.Item
                key={v}
                className="c-pagination__btn"
                active={currentPage === v}
                onClick={() => {
                  handlePage(v);
                }}
              >
                {v}
              </Pagination.Item>
            );
          })}
          {hasNextPage && (
            <Pagination.Next
              className="c-pagination__btn"
              onClick={() => {
                handlePage(currentPage + 1);
              }}
            />
          )}
          {!isLastPage && (
            <Pagination.Last
              className="c-pagination__btn"
              onClick={() => {
                handlePage(paginationData.totalPages);
              }}
            />
          )}
        </Pagination>
      </div>
    </>
  );
};

export default PaginationLayout;
