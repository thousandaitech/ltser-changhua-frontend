import React from 'react';

const Item = (props) => {
  const { item } = props;
  const { title, value, unit, temperature, tide, sun } = item;
  return (
    <>
      <div className="col-12 col-md-6 col-lg-3 d-flex align-items-stretch">
        <div className="c-card">
          <div className="c-card__title">{title}</div>
          <hr className="e-hr my-2" />
          <div className="c-card__content">
            <div className="d-flex align-items-center flex-wrap">
              <span className="c-card__highlight">{value}</span>
              <span className="c-card__subtext">{unit}</span>
            </div>
            {temperature &&
              Object.entries(temperature).map(([key, value]) => {
                return (
                  <div key={key} className="c-card__text">
                    {value}
                  </div>
                );
              })}
            {tide && (
              <div className="row g-2 align-items-center">
                {Object.entries(tide).map(([key, value]) => {
                  return (
                    <React.Fragment key={key}>
                      <div className="col-6 d-flex justify-content-center align-item-center">
                        <span className="c-card__text">
                          {key === 'high' ? '漲' : '退'}潮
                        </span>
                      </div>
                      <div className="col-6 d-flex flex-column">
                        {value.map((item, i) => {
                          return (
                            <span
                              key={i}
                              className="c-card__highlight c-card__highlight--normal"
                            >
                              {item}
                            </span>
                          );
                        })}
                      </div>
                    </React.Fragment>
                  );
                })}
              </div>
            )}
            {sun && (
              <>
                <div className="row g-2 align-items-center">
                  {Object.entries(sun).map(([key, value]) => {
                    return (
                      <React.Fragment key={key}>
                        <div className="col-6 d-flex justify-content-center align-item-center">
                          <span className="c-card__text">
                            日{key === 'rise' ? '出' : '落'}
                          </span>
                        </div>
                        <div className="col-6 d-flex justify-content-center align-item-center">
                          <span className="c-card__highlight c-card__highlight--normal">
                            {value}
                          </span>
                        </div>
                      </React.Fragment>
                    );
                  })}
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Item;
