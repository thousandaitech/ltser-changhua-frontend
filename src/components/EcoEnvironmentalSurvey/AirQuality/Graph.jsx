import React, { useState, useEffect } from 'react';

import axios from 'axios';
import ReactECharts from 'echarts-for-react';

import {
  airQualitySiteItemList,
  commonOptions,
} from 'data/eco-environmental-survey';
import {
  GRAPH_HEIGHT,
  GRAPH_COLOR,
  GRAPH_SPEC,
  handleResponsive,
} from 'data/common';
import { MOENV_API, MOENV_API_KEY } from 'utils/config';
import useFetch from 'hooks/useFetch';
import SpinnerPlaceholder from 'components/SpinnerPlaceholder';
import useWindowDimensions from 'hooks/useWindowDimensions';

const Graph = ({ siteItemList, active }) => {
  const [activeSiteData, setActiveSiteData] = useState(null);
  const [records, setRecords] = useState(null);
  const [series, setSeries] = useState(null);
  const [time, setTime] = useState(null);
  const [items, setItems] = useState(null);
  const [graphData, setGraphData] = useState(null);
  const [option, setOption] = useState({});
  const [height, setHeight] = useState(0);

  const { getList } = useFetch();
  const { width } = useWindowDimensions();

  const hasActiveSite = active.site !== '';

  const isFetchingList = records === null;
  const isFetchingSeries = series === null;
  const isFetchingData = activeSiteData === null;
  const isFetchingItems = items === null;
  const isFetchingTime = time === null;
  const isFetchingGraphData = graphData === null;

  const staticOption = {
    ...commonOptions,
    title: {
      text: `${active.site}站小時值監測變化`,
    },
    dataZoom: {
      start: 0,
      end: 100,
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
      },
      backgroundColor: GRAPH_COLOR.tooltip.background.dark,
    },
    xAxis: {
      type: 'category',
      boundaryGap: true,
      data: time,
    },
    yAxis: {
      type: 'value',
    },
    series: series,
    dataset: {
      source: graphData,
    },
    toolbox: {
      feature: {
        ...commonOptions.toolbox.feature,
        saveAsImage: {
          show: false,
        },
      },
    },
  };

  const AIR_QUALITY_API_URL = `${MOENV_API}aqx_p_143?api_key=${MOENV_API_KEY}`;

  useEffect(() => {
    if (!isFetchingGraphData && width) {
      let responsiveOption = {};
      let responsiveHeight = 0;
      if (handleResponsive(width, 'xsm')) {
        responsiveOption = {
          ...staticOption,
          grid: {
            width: 'auto',
            height: '70%',
            right: '10%',
            left: '10%',
          },
          legend: {
            top: '85%',
            left: 'center',
            type: 'scroll',
          },
        };
        responsiveHeight = GRAPH_HEIGHT.md;
      } else {
        responsiveOption = {
          ...staticOption,
          grid: {
            width: 'auto',
            height: '60%',
            right: '5%',
            left: '5%',
          },
          legend: {
            top: '75%',
            left: 'center',
          },
        };
        responsiveHeight = GRAPH_HEIGHT.md;
      }
      setOption(responsiveOption);
      setHeight(responsiveHeight);
    }
  }, [graphData, width]);

  useEffect(() => {
    setRecords(null);
    setSeries(null);
    setActiveSiteData(null);
    setItems(null);
    setGraphData(null);
    if (hasActiveSite) {
      const matchItemBySite = siteItemList.find((v) => v.title === active.site);
      getList({
        method: 'get',
        url: `${AIR_QUALITY_API_URL}${matchItemBySite.url}`,
        setList: setRecords,
      });
      setActiveSiteData({ ...matchItemBySite });
    }
  }, [active.site]);

  useEffect(() => {
    if (!isFetchingList && !isFetchingData) {
      const itemList = activeSiteData.list.map((item) => {
        const matchDataByName = records.find((v) => v.itemengname === item);
        return {
          id: item,
          title: matchDataByName?.itemname,
          unit: matchDataByName?.itemunit,
        };
      });
      setItems([...itemList]);
      const series = itemList.map((item) => {
        const { title, unit } = item;
        return {
          type: 'line',
          name: `${title}(${unit})`,
          showSymbol: false,
        };
      });
      setSeries([...series]);
      const date = records.map((item) => item.monitordate).sort();
      const time = new Set(date);
      setTime([...time]);
    }
  }, [records, activeSiteData]);

  useEffect(() => {
    if (!isFetchingTime && !isFetchingItems && !isFetchingSeries) {
      const dataByTime = time.map((v) => {
        const matchListByTime = records.filter((item) => {
          const { monitordate } = item;
          return monitordate === v;
        });
        const defaultData = items.reduce((obj, item) => {
          const { title, unit } = item;
          return { ...obj, [`${title}(${unit})`]: '-' };
        }, {});
        const data = matchListByTime.map((item) => {
          const { itemname, itemunit, concentration } = item;
          return [`${itemname}(${itemunit})`, Number(concentration)];
        });
        return {
          time: v,
          ...defaultData,
          ...Object.fromEntries(data),
        };
      });
      setGraphData([...dataByTime]);
    }
  }, [time, items, series]);

  return (
    <>
      {!isFetchingGraphData ? (
        <>
          <div className="text-end text-muted my-3">
            資料來源: 環境部環境資料開放平臺
          </div>
          <ReactECharts
            option={option}
            notMerge={true}
            lazyUpdate={true}
            opts={{ renderer: 'canvas', height }}
            style={{ height }}
          />
        </>
      ) : (
        <SpinnerPlaceholder layout="graph" text="資料讀取中" height={height} />
      )}
    </>
  );
};

export default Graph;
