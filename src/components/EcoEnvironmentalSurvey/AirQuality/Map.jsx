import React, { useEffect } from 'react';

import {
  MapContainer,
  LayersControl,
  ScaleControl,
  Marker,
  LayerGroup,
  TileLayer,
  Tooltip,
} from 'react-leaflet';

import { divIcon } from 'leaflet';
import { renderToStaticMarkup } from 'react-dom/server';
import { WMTS_MAP_URL } from 'utils/config';

import { useLocation, useNavigate } from 'react-router-dom';

import airQualityIconImg from 'img/icon/air-quality.svg';

const Map = (props) => {
  const { active, setActive, markerList } = props;
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const iconMarkup = (img, title) =>
    renderToStaticMarkup(
      <div className="c-map__marker" data-type={title}>
        <img
          src={img}
          alt={title}
          className="e-img e-img--contain c-map__icon"
        />
      </div>
    );
  const mapIcon = (img, title) =>
    divIcon({
      html: iconMarkup(img, title),
    });
  return (
    <>
      <MapContainer
        id="leafletmap"
        className="c-map"
        center={[23.8900528, 120.339647]}
        zoom={11}
        scrollWheelZoom={true}
        doubleClickZoom={false}
      >
        <TileLayer
          attribution='&copy; <a href="https://wmts.nlsc.gov.tw/wmts/" target="blank">國土測繪圖資服務雲</a>'
          url={WMTS_MAP_URL}
        />
        <LayersControl>
          <LayerGroup>
            {markerList.map((v) => {
              const { id, title, position } = v;
              return (
                <Marker
                  key={id}
                  position={position}
                  icon={mapIcon(airQualityIconImg, 'air-quality')}
                  eventHandlers={{
                    click: () => {
                      setActive({ ...active, site: title });
                      navigate(`${pathname}#graph`);
                    },
                  }}
                >
                  <Tooltip offset={[20, 22.5]}>{`${title}站`}</Tooltip>
                </Marker>
              );
            })}
          </LayerGroup>
          <ScaleControl position="bottomleft" imperial={false} />
        </LayersControl>
      </MapContainer>
    </>
  );
};

export default Map;
