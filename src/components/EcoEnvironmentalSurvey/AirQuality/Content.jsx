import React, { useState, useEffect } from 'react';

import airQualityImg from 'img/icon/air-quality.svg';
import Intro from 'components/Intro';
import Graph from 'components/EcoEnvironmentalSurvey/AirQuality/Graph';
import Map from 'components/EcoEnvironmentalSurvey/AirQuality/Map';
import Weather from './Weather';
import { airQualitySiteItemList } from 'data/eco-environmental-survey';

const Content = () => {
  const [active, setActive] = useState({
    site: '',
  });

  const pageData = {
    title: '空氣品質與氣象',
    icon: airQualityImg,
  };

  const hasNoActiveSite = active.site === '';

  return (
    <>
      <article className="l-eco-environmental-survey l-air-quality">
        <section className="l-eco-environmental-survey__title u-section">
          <Intro data={pageData} />
        </section>
        <section className="l-eco-environmental-survey__card u-section">
          <Weather />
        </section>
        <section className="l-eco-environmental-survey__map l-air-quality__map u-section">
          <h2 className="c-title">空氣品質</h2>
          <Map
            active={active}
            setActive={setActive}
            markerList={airQualitySiteItemList}
          />
        </section>
        {!hasNoActiveSite && (
          <section
            id="graph"
            className="l-eco-environmental-survey__graph l-air-quality__graph u-section"
          >
            <Graph active={active} siteItemList={airQualitySiteItemList} />
          </section>
        )}
      </article>
    </>
  );
};

export default Content;
