import React, { useEffect, useState } from 'react';
import Item from './Item';
import axios from 'axios';
import { CWA_AUTH, CWA_URL } from 'utils/config';
import dayjs from 'dayjs';

const Weather = () => {
  const [weather, setWeather] = useState([]);
  const STATION_ID = 'C0G870'; // 芳苑測站
  const handleWindForce = (s) => {
    if (s <= 0.2) {
      return '0';
    } else if (s > 0.2 && s <= 1.5) {
      return '1';
    } else if (s > 1.5 && s <= 3.3) {
      return '2';
    } else if (s > 3.3 && s <= 5.4) {
      return '3';
    } else if (s > 5.4 && s <= 7.9) {
      return '4';
    } else if (s > 7.9 && s <= 10.7) {
      return '5';
    } else if (s > 10.7 && s <= 13.8) {
      return '6';
    } else if (s > 13.8 && s <= 17.1) {
      return '7';
    } else if (s > 17.1 && s <= 20.7) {
      return '8';
    } else if (s > 20.7 && s <= 24.4) {
      return '9';
    } else if (s > 24.4 && s <= 28.4) {
      return '10';
    } else if (s > 28.4 && s <= 32.6) {
      return '11';
    } else if (s >= 32.7) {
      return '12';
    } else {
      return '-';
    }
  };

  const getWeatherApiData = async () => {
    const WEATHER_API_URL = `${CWA_URL}api/v1/rest/datastore/O-A0001-001`;
    const weatherElement = [
      'WindDirection',
      'WindSpeed',
      'AirTemperature',
      'RelativeHumidity',
    ]; // 風向、風速、溫度、相對濕度
    const response = await axios({
      method: 'get',
      url: WEATHER_API_URL,
      params: {
        Authorization: CWA_AUTH,
        StationId: STATION_ID,
        WeatherElement: weatherElement.join(','),
      },
    });
    return { ...response.data.records.Station[0] };
  };

  const getTideApiData = async () => {
    const TIDE_API_URL = `${CWA_URL}api/v1/rest/datastore/F-A0021-001`;
    const LOCATION_NAME = '彰化縣芳苑鄉';
    const startTime = dayjs().format('YYYY-MM-DD');
    const response = await axios({
      method: 'get',
      url: TIDE_API_URL,
      params: {
        Authorization: CWA_AUTH,
        LocationName: LOCATION_NAME,
        Date: startTime,
        sort: 'DateTime',
      },
    });
    return [
      ...response.data.records.TideForecasts[0].Location.TimePeriods.Daily[0]
        .Time,
    ];
  };

  const getSunApiData = async () => {
    const SUN_API_URL = `${CWA_URL}api/v1/rest/datastore/A-B0062-001`;
    const LOCATION_NAME = '彰化縣';
    const dataTime = dayjs().format('YYYY-MM-DD');
    const response = await axios({
      method: 'get',
      url: SUN_API_URL,
      params: {
        Authorization: CWA_AUTH,
        CountyName: LOCATION_NAME,
        Date: dataTime,
        sort: 'dataTime',
      },
    });
    return { ...response.data.records.locations.location[0].time[0] };
  };

  const getRainApiData = async () => {
    const RAIN_API_URL = `${CWA_URL}api/v1/rest/datastore/O-A0002-001`;
    const response = await axios({
      method: 'get',
      url: RAIN_API_URL,
      params: {
        Authorization: CWA_AUTH,
        stationId: STATION_ID,
        RainfallElement: 'Now',
      },
    });
    return response.data.records.Station[0].RainfallElement.Now.Precipitation;
  };

  const handleApi = async () => {
    const weatherResult = await getWeatherApiData();
    const weatherDate = weatherResult.ObsTime.DateTime.split('T')[0];
    const weatherTime =
      weatherResult.ObsTime.DateTime.split('T')[1].split('+')[0];
    const tideResult = await getTideApiData();
    const sunResult = await getSunApiData();
    const rainResult = await getRainApiData();
    const handleTideValue = (keyword) => {
      return tideResult
        .filter((item) => item.Tide === keyword)
        .map((item) => dayjs(item.DateTime).format('HH:mm'));
    };
    const handleSunValue = (keyword) => {
      return Object.entries(sunResult).find(
        ([key, value]) => key === keyword
      )[1];
    };
    setWeather([
      {
        id: 'temperature',
        title: '溫度',
        value: Number(weatherResult.WeatherElement.AirTemperature),
        unit: '°C',
        temperature: {
          date: weatherDate,
          time: weatherTime,
        },
      },
      {
        id: 'humidity',
        title: '相對濕度',
        value: Number(weatherResult.WeatherElement.RelativeHumidity),
        unit: '',
      },
      {
        id: 'rain',
        title: '時雨量',
        value: rainResult === '-998.00' ? '0' : rainResult,
        unit: 'mm',
      },
      {
        id: 'wind-direction',
        title: '風向',
        value: Number(weatherResult.WeatherElement.WindDirection),
        unit: '度',
      },
      {
        id: 'wind-speed',
        title: '風速',
        value: Number(weatherResult.WeatherElement.WindSpeed),
        unit: 'm/s',
      },
      {
        id: 'wind-force',
        title: '風力',
        value: handleWindForce(Number(weatherResult.WeatherElement.WindSpeed)),
        unit: '級',
      },
      {
        id: 'tide',
        title: '潮汐',
        value: '',
        unit: '',
        tide: {
          high: handleTideValue('滿潮'),
          low: handleTideValue('乾潮'),
        },
      },
      {
        id: 'sun',
        title: '日出日落',
        value: '',
        unit: '',
        sun: {
          rise: handleSunValue('SunRiseTime'),
          set: handleSunValue('SunSetTime'),
        },
      },
    ]);
  };

  useEffect(() => {
    handleApi();
    getRainApiData();
  }, []);
  return (
    <>
      <h2 className="c-title">芳苑測站氣象觀測資料</h2>
      <div className="row g-4">
        {weather.map((item) => {
          const { id } = item;
          return <Item key={id} item={item} />;
        })}
      </div>
      <div className="text-end text-muted mt-3">資料來源: 交通部中央氣象署</div>
    </>
  );
};

export default Weather;
