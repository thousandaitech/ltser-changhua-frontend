import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Intro from 'components/Intro';
import Tab from 'components/Tab';
import LeafletMap from 'components/EcoEnvironmentalSurvey/LeafletMap/Content';
import Graph from 'components/EcoEnvironmentalSurvey/Graph';

import waterQualityImg from 'img/icon/water-quality.svg';

import { useCustomContext } from 'utils/context';

import {
  commonOptions,
  contentList,
  waterQualitySiteData,
} from 'data/eco-environmental-survey';

import { API_URL } from 'utils/config';

const Content = () => {
  const { site } = useCustomContext();
  const [graphData, setGraphData] = useState([]);
  const [series, setSeries] = useState([]);

  const isNoSite = site === '';
  const isFetchingData = site !== '' && graphData.length === 0;

  const pageData = {
    title: '水質觀測',
    icon: waterQualityImg,
    page: 'water-quality',
    tabs: [
      {
        id: 1,
        title: '人工觀測',
        content:
          '芳苑鄉的漢寶、王功與永興養殖區分別被劃分為太陽能光電設置的關注減緩區與待分區，是未來有可能架設光電設施的區域。且彰化海岸潮間帶為候鳥之重要度冬點，為探討光電板之設置可能對環境與候鳥產生影響。每月進行一次水質調查，以期能在光電設施設置前收集當地的水質資料，作為施工前的水質資料參考，並進行長期監測以探討施工前後之水質變化。',
      },
      {
        id: 2,
        title: '自動監測',
        content: `使用自動監測之儀器連續監測水質，以獲得長時間且連續之水質資料。[目前故障維護中]
      `,
      },
    ],
    url: 'waterQualityManual',
  };

  const option = {
    ...commonOptions,
    dataZoom: {
      start: 0,
      end: 100,
    },
    // legend: {
    //   top: '10%',
    //   right: 0,
    //   orient: 'vertical',
    // },
    // grid: {
    //   width: '78%',
    //   left: '5%',
    // },
    xAxis: {
      type: 'category',
      boundaryGap: true,
      axisLabel: {
        rotate: -45,
      },
    },
    yAxis: {
      name: '',
      type: 'value',
    },
    series: series,
    dataset: {
      source: graphData,
    },
  };

  const getGraphData = async () => {
    try {
      const response = await axios.get(
        `${API_URL}/getWaterQualityManuals?site=${site}`
      );
      return response.data;
    } catch (err) {
      console.log(err);
    }
  };

  const handleRawData = async () => {
    const data = await getGraphData();
    const processedData = data.map((item) => {
      const { year, month, data } = item;
      const formatMonth = (month) => {
        if (month < 10) {
          return `0${month}`;
        } else {
          return month;
        }
      };
      const formatData = () =>
        Object.fromEntries(
          Object.entries(data)
            .filter(([key, value]) => key !== 'site')
            .map(([key, value]) => {
              const titleById = contentList.waterQuality.find(
                (v) => v.id === key
              )?.title;
              return [titleById, value];
            })
        );
      return {
        year: `${year}-${formatMonth(month)}`,
        ...formatData(),
      };
    });
    setGraphData([...processedData]);
    data.forEach((item) => {
      const { data } = item;
      const result = Object.keys(data)
        .map((item) => {
          const itemById = contentList.waterQuality.find((v) => v.id === item);
          return itemById
            ? {
                type: 'line',
                name: `${itemById.title}${itemById.unit}`,
              }
            : null;
        })
        .filter((v) => v !== null);
      setSeries([...result]);
    });
  };

  useEffect(() => {
    if (!isNoSite) {
      handleRawData();
    }
  }, [site]);

  return (
    <>
      <article className="l-eco-environmental-survey">
        <section className="l-eco-environmental-survey__title u-section">
          <Intro data={pageData} />
          <Tab data={pageData} />
        </section>
        <section className="l-eco-environmental-survey__site u-section">
          <LeafletMap />
          {!isNoSite && (
            <>
              {!isFetchingData ? (
                <Graph
                  pageData={pageData}
                  graphData={graphData}
                  staticOption={option}
                  isFetchingData={isFetchingData}
                />
              ) : (
                <div className="c-placeholder l-eco-environmental-survey__placeholder">
                  圖表加載中...
                </div>
              )}
            </>
          )}
        </section>
      </article>
    </>
  );
};

export default Content;
