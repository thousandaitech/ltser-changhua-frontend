import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const Item = ({ data }) => {
  const { trivialName, scientificName, img } = data;

  const paramName = scientificName.toLowerCase().replace(' ', '-');

  return (
    <>
      <div className="col-12 col-md-4 col-lg-3">
        <Link to={`/eco-environmental-survey/macrobenthos/${paramName}`}>
          <div className="l-macrobenthos__card c-card">
            <div className="l-macrobenthos__img">
              <picture>
                <source
                  className="e-img e-img--cover"
                  srcSet={img}
                  type="image/webp"
                />
                <img
                  className="e-img e-img--cover"
                  src={img}
                  alt={trivialName}
                />
              </picture>
              <h3 className="l-macrobenthos__name">{trivialName}</h3>
            </div>
          </div>
        </Link>
      </div>
    </>
  );
};

export default Item;
