import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

import { useCustomContext } from 'utils/context';

import { macrobenthosList } from 'data/eco-environmental-survey';

const Detail = () => {
  const { macrobenthosId } = useParams();
  const navigate = useNavigate();
  const { breadcrumbLink, setBreadcrumbParam } = useCustomContext();
  const [macrobenthosData, setMacrobenthosData] = useState({
    id: '',
    trivialName: '',
    scientificName: '',
    taxonomy: '',
    characteristic: '',
    distributionArea: '',
    img: '',
    photographer: '',
    reference: [],
  });
  const {
    id,
    trivialName,
    scientificName,
    taxonomy,
    characteristic,
    distributionArea,
    img,
    photographer,
    reference,
  } = macrobenthosData;

  const isFetchingParams = macrobenthosId === undefined;
  const isFetchingBreadcrumbLink = breadcrumbLink.length === 0;
  const isFetchingMacrobenthosData = macrobenthosData.id === '';

  useEffect(() => {
    if (!isFetchingParams) {
      const restoredName =
        macrobenthosId.charAt(0).toUpperCase() +
        macrobenthosId.slice(1).replace('-', ' ');
      const matchedResult = macrobenthosList.find(
        (item) => item.scientificName === restoredName
      );
      setMacrobenthosData({ ...matchedResult });
    }
  }, [macrobenthosId]);

  useEffect(() => {
    if (
      !isFetchingParams &&
      !isFetchingBreadcrumbLink &&
      !isFetchingMacrobenthosData
    ) {
      setBreadcrumbParam([
        ...breadcrumbLink,
        {
          id: `${breadcrumbLink[breadcrumbLink.length - 1].id}-${id}`,
          link: `${
            breadcrumbLink[breadcrumbLink.length - 1].link
          }/${macrobenthosId}`,
          title: trivialName,
        },
      ]);
    }
  }, [macrobenthosId, breadcrumbLink, macrobenthosData]);
  return (
    <>
      <main className="l-macrobenthos__detail">
        <div className="l-macrobenthos__cover">
          <img className="e-img e-img--cover" src={img} alt={trivialName} />
          <div className="l-macrobenthos__info">
            {photographer && (
              <h3 className="l-macrobenthos__tag">攝影者: {photographer}</h3>
            )}
            <h1 className="l-macrobenthos__title">{trivialName}</h1>
            <h3 className="l-macrobenthos__subtitle">{taxonomy}</h3>
            <h3 className="l-macrobenthos__subtitle l-macrobenthos__subtitle--i">
              {scientificName}
            </h3>
          </div>
        </div>
        <article className="l-macrobenthos__article">
          <section className="l-macrobenthos__section">
            <h3 className="l-macrobenthos__heading">特徵習性</h3>
            <p className="l-macrobenthos__text">{characteristic}</p>
          </section>
          <section className="l-macrobenthos__section">
            <h3 className="l-macrobenthos__heading">分布範圍</h3>
            <p className="l-macrobenthos__text">{distributionArea}</p>
          </section>
          <section className="l-macrobenthos__section">
            <h3 className="l-macrobenthos__heading">資料來源</h3>
            <ul className="l-macrobenthos__list c-block">
              {reference.map((item) => {
                const {
                  id,
                  category,
                  author,
                  title,
                  publicationPlace,
                  publisher,
                  year,
                } = item;
                return (
                  <li key={id} className="l-macrobenthos__item c-item">
                    <span className="l-macrobenthos__seg">({category})</span>
                    <span className="l-macrobenthos__seg">{author}</span>
                    <span className="l-macrobenthos__seg">{title}</span>
                    {publicationPlace && (
                      <span className="l-macrobenthos__seg">
                        {publicationPlace}
                      </span>
                    )}
                    {publisher && (
                      <span className="l-macrobenthos__seg">{publisher}</span>
                    )}
                    {year && (
                      <span className="l-macrobenthos__seg">{year}</span>
                    )}
                  </li>
                );
              })}
            </ul>
          </section>
          <div className="d-flex justify-content-center mt-4">
            <button
              className="e-btn e-btn--primary e-btn--wmax"
              onClick={() => {
                navigate(-1);
              }}
            >
              回上一頁
            </button>
          </div>
        </article>
      </main>
    </>
  );
};

export default Detail;
