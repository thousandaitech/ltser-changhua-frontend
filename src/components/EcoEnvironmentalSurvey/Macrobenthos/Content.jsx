import React, { useState, useEffect } from 'react';

import macrobenthosImg from 'img/icon/macrobenthos.svg';

import {
  commonOptions,
  macrobenthosList,
  macrobenthosNameList,
} from 'data/eco-environmental-survey';

import LeafletMap from 'components/EcoEnvironmentalSurvey/LeafletMap/Content';
import Graph from 'components/EcoEnvironmentalSurvey/Graph';
import Intro from 'components/Intro';
import Tab from 'components/Tab';

import { useCustomContext } from 'utils/context';
import { useMacrobenthosContext } from 'context/MacrobenthosContext';
import Item from './Item';

const Content = () => {
  const { site } = useCustomContext();
  const [series, setSeries] = useState([]);
  const { graphData, setGraphData, siteMacrobenthosSeries } =
    useMacrobenthosContext();

  const isNoSite = site === '';
  const isFetchingData = site !== '' && graphData.length === 0;

  const pageData = {
    title: '底棲生物',
    icon: macrobenthosImg,
    page: 'macrobenthos',
    tabs: [
      {
        id: 1,
        title: '人工觀測',
        content: `為探討海空步道建置後對濕地的環境與生態可能產生的影響，在芳苑海空步道周邊與疏伐區域每季進行蟹類與底質調查。`,
      },
      {
        id: 2,
        title: '自動監測',
        content:
          '於海空步道底棲觀測平台架設自動照相機拍攝灘地之蟹類，收集灘地影像，以期作為未來訓練AI自動辨識蟹類使用。',
      },
    ],
    url: 'crab',
  };

  const option = {
    ...commonOptions,
    dataZoom: {
      start: 0,
      end: 100,
    },
    tooltip: {
      trigger: 'axis',
      backgroundColor: 'rgba(1, 116, 187, 0.8)',
      formatter: (params) => {
        let title = `${params[0].name}<br/>`;
        params.forEach((v) => {
          const matchId = macrobenthosNameList.find(
            (macrobentho) => macrobentho.title === v.seriesName
          ).id;
          if (v.value[matchId] > 0) {
            title += `
              <div class="d-flex align-items-center">
                <div class="c-tooltip__legend" style="background-color: ${v.color}"></div>
                <div class="c-tooltip__row">
                  <div class="c-tooltip__column">
                  ${v.seriesName}
                  </div>
                  <div class="c-tooltip__column">
                  ${v.value[matchId]}
                  </div>
                </div>
              </div>
              `;
          }
        });
        return title;
      },
    },
    xAxis: {
      type: 'category',
      boundaryGap: true,
    },
    yAxis: {
      name: '隻次/3平方公尺',
      type: 'value',
    },
    series: series,
    dataset: {
      source: graphData,
    },
    toolbox: {
      feature: {
        ...commonOptions.toolbox.feature,
        saveAsImage: {
          show: false,
        },
      },
    },
  };

  const getSeries = () => {
    const result = macrobenthosNameList.map((item) => {
      const { id, title } = item;
      return {
        type: 'bar',
        stack: 'Total',
        name: title,
      };
    });
    setSeries([...result]);
  };

  const handleRawData = async () => {
    const processedData = siteMacrobenthosSeries.map((item) => {
      const { year, month, species } = item;
      const handleMonth = (month) => {
        if (month < 10) {
          return `0${month}`;
        } else {
          return month;
        }
      };
      return {
        year: `${year}-${handleMonth(month)}`,
        ...species,
      };
    });
    setGraphData([...processedData]);
    getSeries();
  };

  useEffect(() => {
    if (!isNoSite) {
      handleRawData();
    }
  }, [site]);

  return (
    <>
      <article className="l-eco-environmental-survey">
        <section className="l-eco-environmental-survey__title u-section">
          <Intro data={pageData} />
          <Tab data={pageData} />
        </section>
        <section className="l-eco-environmental-survey__site u-section">
          <LeafletMap />
          {!isNoSite && (
            <>
              {!isFetchingData ? (
                <Graph
                  pageData={pageData}
                  graphData={graphData}
                  staticOption={option}
                  isFetchingData={isFetchingData}
                />
              ) : (
                <div className="c-placeholder l-eco-environmental-survey__placeholder">
                  圖表加載中...
                </div>
              )}
            </>
          )}
        </section>
        <section className="l-macrobenthos u-section" id="data">
          <div className="row justify-content-center">
            <div className="col-12">
              <div className="c-heading">
                <h2 className="c-title">底棲生物</h2>
              </div>
              <div className="l-macrobenthos__content u-section">
                <div className="row gy-4">
                  {macrobenthosList.map((v) => {
                    return <Item key={v.id} data={v} />;
                  })}
                </div>
              </div>
            </div>
          </div>
        </section>
      </article>
    </>
  );
};

export default Content;
