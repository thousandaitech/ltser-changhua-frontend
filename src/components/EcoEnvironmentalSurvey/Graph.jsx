import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import ReactECharts from 'echarts-for-react';
import { DateTime } from 'luxon';

import { GRAPH_HEIGHT, SWAL_TOAST, handleResponsive } from 'data/common';

import { useDownload } from 'hooks/useDownload';
import useWindowDimensions from 'hooks/useWindowDimensions';
import { useAuthContext } from 'context/AuthContext';

function Graph(props) {
  const { pageData, graphData, staticOption, isFetchingData } = props;
  const { title, page, url } = pageData;

  const [option, setOption] = useState({});
  const [height, setHeight] = useState(0);

  const { handleDownload } = useDownload();
  const { auth } = useAuthContext();
  const navigate = useNavigate();
  const { pathname, hash } = useLocation();
  const { width } = useWindowDimensions();

  const handleDownloadClick = (e) => {
    e.preventDefault();
    if (auth) {
      const today = DateTime.local().toFormat('yyyyMMdd');
      handleDownload({
        url,
        fileName: `LTSER Changhua_${title}資料_${today}`,
      });
    } else {
      SWAL_TOAST.fire({
        icon: 'error',
        title: `該功能需登入後方可使用。請先登入。`,
      });
      navigate('/login', { state: { previousPage: `${pathname}${hash}` } });
    }
  };

  useEffect(() => {
    if (!isFetchingData && width) {
      let responsiveOption = {};
      let responsiveHeight = 0;
      if (handleResponsive(width, 'xsm')) {
        responsiveOption = {
          ...staticOption,
          grid: {
            width: 'auto',
            height: '70%',
            right: '10%',
            left: '15%',
          },
          legend: {
            top: '85%',
            left: 'center',
            type: 'scroll',
          },
        };
        responsiveHeight = GRAPH_HEIGHT.md;
      } else if (handleResponsive(width, 'md')) {
        responsiveOption = {
          ...staticOption,
          grid: {
            width: '70%',
            height: '65%',
            left: '5%',
          },
          legend: {
            top: '15%',
            right: 0,
            type: 'scroll',
            orient: 'vertical',
          },
        };
        responsiveHeight = GRAPH_HEIGHT.default;
      } else {
        responsiveOption = {
          ...staticOption,
          grid: {
            width: '80%',
            height: '65%',
            left: '5%',
          },
          legend: {
            top: '15%',
            right: 0,
            type: 'scroll',
            orient: 'vertical',
          },
        };
        responsiveHeight = GRAPH_HEIGHT.default;
      }
      setOption(responsiveOption);
      setHeight(responsiveHeight);
    }
  }, [graphData, width]);

  return (
    <>
      <div className={`l-${page}__graph`} id="graph">
        <ReactECharts
          option={option}
          notMerge={true}
          lazyUpdate={true}
          opts={{ renderer: 'canvas', height: `${height}` }}
          style={{ height }}
        />
        <button
          type="button"
          className={`l-${page}__download e-btn e-btn--primary e-btn--wmax mt-4 mx-auto`}
          onClick={handleDownloadClick}
        >
          資料下載
        </button>
      </div>
    </>
  );
}

export default Graph;
