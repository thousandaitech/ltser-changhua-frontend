import React, { useState } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

import { filterMapList } from 'data/eco-environmental-survey';
import { useCustomContext } from 'utils/context';

function Menu(props) {
  const { baseMapList, baseMap, setBaseMap } = props;
  const [menuShow, setMenuShow] = useState('');
  const { filterMap, setFilterMap } = useCustomContext();
  const handleBaseMapChange = (e) => {
    setBaseMap(e.target.value);
  };
  const isActiveMenu = menuShow !== '';

  const handleSwitchChange = (e) => {
    if (e.target.checked) {
      if (filterMap.includes(e.target.value)) {
        return;
      } else {
        setFilterMap([...filterMap, e.target.value]);
      }
    } else {
      const remainResult = filterMap.filter((item) => item !== e.target.value);
      setFilterMap(remainResult);
    }
  };
  return (
    <>
      <div className={`c-map__wrapper ${isActiveMenu ? 'active' : ''}`}>
        <div className={`c-map__menu`}>
          {menuShow === 'base' && (
            <section className="c-map__base">
              <div className="c-map__title">底圖類型</div>
              {baseMapList.map((item) => {
                const { id, title } = item;
                return (
                  <React.Fragment key={id}>
                    <input
                      type="radio"
                      className="btn-check"
                      name="basemap"
                      value={id}
                      id={id}
                      onChange={handleBaseMapChange}
                      checked={baseMap === id}
                      autoComplete="off"
                    />
                    <label
                      className={`c-form__label e-btn c-map__btn ${
                        baseMap === id ? 'active' : ''
                      }`}
                      htmlFor={id}
                    >
                      {title}
                    </label>
                  </React.Fragment>
                );
              })}
            </section>
          )}
          {menuShow === 'filter' && (
            <section className="c-map__filter">
              <div className="c-map__title">篩選點位</div>
              {filterMapList.map((item) => {
                const { id, title } = item;
                return (
                  <div key={id} className="form-check form-switch c-switch">
                    <input
                      className="form-check-input c-switch__input"
                      type="checkbox"
                      role="switch"
                      id={id}
                      value={id}
                      onChange={handleSwitchChange}
                      checked={filterMap.includes(id)}
                    />
                    <label
                      className="form-check-label c-switch__label"
                      htmlFor={id}
                    >
                      {title}
                    </label>
                  </div>
                );
              })}
            </section>
          )}
        </div>
        <button
          type="button"
          className={`c-map__tag ${menuShow === 'base' ? 'active' : ''}`}
          onClick={() => setMenuShow('base')}
        >
          <FontAwesomeIcon
            icon={solid('map')}
            className={`e-icon e-icon--muted ${
              menuShow === 'base' ? 'active' : ''
            }`}
          />
        </button>
        <button
          type="button"
          className={`c-map__tag ${menuShow === 'filter' ? 'active' : ''}`}
          onClick={() => setMenuShow('filter')}
        >
          <FontAwesomeIcon
            icon={solid('filter')}
            className={`e-icon e-icon--muted ${
              menuShow === 'filter' ? 'active' : ''
            }`}
          />
        </button>
      </div>
      <div className="c-map__backdrop" onClick={() => setMenuShow('')} />
    </>
  );
}

export default Menu;
