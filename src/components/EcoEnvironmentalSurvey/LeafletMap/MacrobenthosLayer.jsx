import React, { useState, useEffect } from 'react';
import axios from 'axios';

import LayerTemplate from 'components/EcoEnvironmentalSurvey/LeafletMap/LayerTemplate';

import {
  contentList,
  macrobenthosNameList,
} from 'data/eco-environmental-survey';

import { useMacrobenthosContext } from 'context/MacrobenthosContext';

function MacrobenthosLayer(props) {
  const { currentPath, mapIcon } = props;
  const {
    sites,
    activeSite,
    setActiveSite,
    activeSiteData,
    getSiteMacrobenthosSeries,
    getSiteList,
    getSiteContent,
    siteMacrobenthosSeries,
  } = useMacrobenthosContext();

  const isNoActiveSite = activeSite === '';

  const handleContent = (data, id) => {
    if (data[id] !== null) {
      switch (id) {
        case 'macrobenthos':
          const matchSeries = siteMacrobenthosSeries.find(
            (v) =>
              v.year === activeSiteData.year && v.month === activeSiteData.month
          )?.species;
          const matchTitle =
            matchSeries &&
            Object.entries(matchSeries)
              .map(([key, value]) =>
                value !== 0
                  ? macrobenthosNameList.find((v) => v.id === key)
                  : null
              )
              .filter((v) => v !== null)
              .map((v) => v.title);
          return matchTitle && matchTitle?.length !== 0 ? (
            matchTitle.map((v, i) => {
              const isLastItem = i === matchTitle.length - 1;
              return (
                <span key={i}>
                  {v}
                  {!isLastItem && '、'}
                </span>
              );
            })
          ) : (
            <span>-</span>
          );
        default:
          const matchUnit = contentList.macrobenthos.find(
            (v) => v.id === id
          ).unit;
          return `${data[id]} ${matchUnit ? matchUnit : ''}`;
      }
    } else {
      return '-';
    }
  };

  useEffect(() => {
    getSiteList();
  }, []);

  useEffect(() => {
    if (!isNoActiveSite) {
      getSiteContent();
      getSiteMacrobenthosSeries();
    }
  }, [activeSite]);

  return (
    <>
      <LayerTemplate
        baseLayer=""
        layerName={{ title: '底棲生物觀測點位', en: 'macrobenthos' }}
        currentPath={currentPath}
        siteList={sites}
        contentList={contentList.macrobenthos}
        handleContent={handleContent}
        mapIcon={mapIcon}
        activeSite={activeSite}
        setActiveSite={setActiveSite}
        activeSiteData={activeSiteData}
      />
    </>
  );
}

export default MacrobenthosLayer;
