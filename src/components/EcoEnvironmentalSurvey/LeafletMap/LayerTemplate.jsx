import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { HashLink } from 'react-router-hash-link';
import axios from 'axios';

import { API_URL } from 'utils/config';

import {
  Marker,
  Popup,
  LayersControl,
  LayerGroup,
  Tooltip,
} from 'react-leaflet';

import { useCustomContext } from 'utils/context';

function LayerTemplate(props) {
  const {
    baseLayer,
    layerName,
    currentPath,
    siteList,
    contentList,
    handleContent,
    mapIcon,
    activeSite,
    setActiveSite,
    activeSiteData,
  } = props;
  const { filterMap, setFilterMap, setSite } = useCustomContext();
  const { pathname } = useLocation();

  const isFetchingData = activeSiteData.id === 0;

  useEffect(() => {
    if (currentPath === layerName.en) {
      setFilterMap([layerName.en]);
    }
  }, []);

  useEffect(() => {
    setSite('');
  }, [pathname]);

  return (
    <>
      <LayersControl.Overlay
        name={layerName.title}
        checked={filterMap.includes(layerName.en)}
      >
        <LayerGroup>
          {baseLayer}
          {siteList.map((site) => {
            const { id, title, longitude, latitude } = site;
            return (
              <Marker
                key={id}
                position={[latitude, longitude]}
                icon={mapIcon}
                eventHandlers={{
                  click: () => {
                    const isActiveSite = activeSite === title;
                    if (!isActiveSite) {
                      setActiveSite(title);
                    } else {
                      return;
                    }
                  },
                }}
              >
                <Popup className="c-map__popup">
                  <div className="c-map__heading">{title}</div>
                  <div className="c-map__table">
                    <div className="c-map__row">
                      <div className="c-map__col c-map__col--title">
                        調查月份
                      </div>
                      <div className="c-map__col">
                        {!isFetchingData && (
                          <>
                            {activeSiteData.year}年{activeSiteData.month}月
                          </>
                        )}
                      </div>
                    </div>
                    {contentList.map((item) => {
                      const { id, title } = item;
                      return (
                        <div key={id} className="c-map__row">
                          <div className="c-map__col c-map__col--title">
                            {title}
                          </div>
                          <div className="c-map__col">
                            {!isFetchingData &&
                              handleContent(activeSiteData, id)}
                          </div>
                        </div>
                      );
                    })}
                  </div>
                  <HashLink
                    to={`/eco-environmental-survey/${layerName.en}#graph`}
                    className="e-btn e-btn--primary"
                    onClick={() => {
                      setSite(title);
                    }}
                  >
                    查看圖表
                  </HashLink>
                </Popup>
                <Tooltip offset={[20, 12.5]}>{title}</Tooltip>
              </Marker>
            );
          })}
        </LayerGroup>
      </LayersControl.Overlay>
    </>
  );
}

export default LayerTemplate;
