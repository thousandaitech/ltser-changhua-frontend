import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';

import {
  MapContainer,
  TileLayer,
  LayersControl,
  ScaleControl,
} from 'react-leaflet';

import { divIcon, Layer } from 'leaflet';
import { renderToStaticMarkup } from 'react-dom/server';

import MacrobenthosLayer from 'components/EcoEnvironmentalSurvey/LeafletMap/MacrobenthosLayer';
import WaterQualityLayer from 'components/EcoEnvironmentalSurvey/LeafletMap/WaterQualityLayer';
import Menu from 'components/EcoEnvironmentalSurvey/LeafletMap/Menu';

import { baseMapList } from 'data/common';

import macrobenthosIconImg from 'img/icon/macrobenthos.svg';
import waterQualityIconImg from 'img/icon/water-quality.svg';

function Content() {
  const { pathname } = useLocation();
  const paths = pathname.split('/');
  const currentPath =
    paths[paths.length - 1] === ''
      ? paths[paths.length - 2]
      : paths[paths.length - 1];
  const [baseMap, setBaseMap] = useState('emap98');

  const iconMarkup = (img, title) =>
    renderToStaticMarkup(
      <div className="c-map__marker" data-type={title}>
        <img
          src={img}
          alt={title}
          className="e-img e-img--contain c-map__icon"
        />
      </div>
    );
  const mapIcon = (img, title) =>
    divIcon({
      html: iconMarkup(img, title),
    });

  const handleCenter = () => {
    switch (currentPath) {
      case 'macrobenthos':
        return { center: [23.927627352463173, 120.3126043504604], zoom: 15 };
      case 'water-quality':
        return { center: [23.95420348627543, 120.32102450000002], zoom: 13 };
      default:
        return { center: [23.95420348627543, 120.32102450000002], zoom: 13 };
    }
  };

  return (
    <>
      <div className="l-macrobenthos__map u-section">
        <h2 className="c-title">觀測點位</h2>
        <MapContainer
          id="leafletmap"
          className="c-map"
          center={handleCenter().center}
          zoom={handleCenter().zoom}
          scrollWheelZoom={true}
          doubleClickZoom={false}
        >
          <Menu
            baseMapList={baseMapList}
            baseMap={baseMap}
            setBaseMap={setBaseMap}
          />
          {/* <TileLayer
            attribution='&copy; <a href="https://wmts.nlsc.gov.tw/wmts/" target="blank">國土測繪圖資服務雲</a>'
            url="https://wmts.nlsc.gov.tw/wmts/EMAP98/default/GoogleMapsCompatible/{z}/{y}/{x}"
          /> */}
          <LayersControl>
            <MacrobenthosLayer
              currentPath={currentPath}
              mapIcon={mapIcon(macrobenthosIconImg, 'macrobenthos')}
            />
            <WaterQualityLayer
              currentPath={currentPath}
              mapIcon={mapIcon(waterQualityIconImg, 'water-quality')}
            />
          </LayersControl>
          {baseMapList.map((item) => {
            const { id, url, attribution } = item;
            return (
              baseMap === id && (
                <TileLayer key={id} url={url} attribution={attribution} />
              )
            );
          })}
          <ScaleControl position="bottomleft" imperial={false} />
        </MapContainer>
      </div>
    </>
  );
}

export default Content;
