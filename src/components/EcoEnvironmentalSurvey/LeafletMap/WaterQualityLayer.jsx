import React, { useState, useEffect } from 'react';
import axios from 'axios';

import LayerTemplate from 'components/EcoEnvironmentalSurvey/LeafletMap/LayerTemplate';

import { contentList } from 'data/eco-environmental-survey';

import { API_URL } from 'utils/config';

function WaterQualityLayer(props) {
  const { currentPath, mapIcon } = props;
  const [sites, setSites] = useState([]);
  const [activeSite, setActiveSite] = useState('');
  const [activeSiteData, setActiveSiteData] = useState({
    id: 0,
    year: '',
    site: '',
    cond: '',
    do: '',
    orp: '',
    phmv: '',
    sg: '',
    tds: '',
    turb: '',
    w_ph: '',
    w_sal: '',
    w_temp: '',
  });

  const isNoActiveSite = activeSite === '';

  const handleUnit = (data, id) => {
    if (data[id] !== null) {
      const unitById = contentList.waterQuality.find((v) => v.id === id).unit;
      return `${data[id]} ${unitById}`;
    } else {
      return '-';
    }
  };

  const getSiteList = async () => {
    try {
      const response = await axios.get(`${API_URL}/getWaterQualityManualSites`);
      const result = response.data;
      setSites([...result]);
    } catch (err) {
      console.log(err);
    }
  };

  const getSiteContent = async () => {
    try {
      const response = await axios.get(
        `${API_URL}/getWaterQualityManuals?site=${activeSite}`
      );
      const result = response.data[response.data.length - 1];
      setActiveSiteData({
        id: result.id,
        year: result.year,
        month: result.month,
        site: result.site,
        ...result.data,
      });
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getSiteList();
  }, []);

  useEffect(() => {
    if (!isNoActiveSite) {
      getSiteContent();
    }
  }, [activeSite]);

  useEffect(() => {
    getSiteList();
  }, []);
  return (
    <>
      <LayerTemplate
        baseLayer=""
        layerName={{ title: '水質監測觀測點位', en: 'water-quality' }}
        currentPath={currentPath}
        siteList={sites}
        contentList={contentList.waterQuality}
        handleContent={handleUnit}
        mapIcon={mapIcon}
        activeSite={activeSite}
        setActiveSite={setActiveSite}
        activeSiteData={activeSiteData}
      />
    </>
  );
}

export default WaterQualityLayer;
