import React from 'react';

function Intro(props) {
  const { data } = props;
  const { icon, title } = data;
  return (
    <>
      <div className="c-intro">
        <div className="row justify-content-center w-100 gx-0 gy-3">
          <div className="col-10 col-md-6 col-lg-5 col-xl-4 d-flex flex-column align-items-center">
            <div className="c-intro__img">
              <img className="e-img e-img--contain" src={icon} alt={title} />
            </div>
            <h1 className="c-intro__title">{title}</h1>
          </div>
        </div>
      </div>
    </>
  );
}

export default Intro;
