import React, { useEffect, useState } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import { OverlayTrigger, Tooltip as BSTooltip } from 'react-bootstrap';
import { useWebSocketContext } from 'context/WebSocketContext';
import { SWAL_TOAST } from 'data/common';
import { useAuthContext } from 'context/AuthContext';
import axios from 'axios';
import { AUTH_URL } from 'utils/config';

const VerificationHint = () => {
  const Tooltip = <BSTooltip>帳號開通提醒</BSTooltip>;
  const [showHint, setShowHint] = useState(false);

  const { message, setMessage, hasMessage } = useWebSocketContext();
  const { profile, handleGetProfile, authTokens } = useAuthContext();

  const isVerified = profile.is_verified === true;
  const isViewed = profile.has_viewed_verification === true;

  const updateProfile = async () => {
    await axios({
      method: 'patch',
      url: `${AUTH_URL}/updateUserProfile/`,
      headers: {
        Authorization: `Bearer ${authTokens.access}`,
      },
      data: { has_viewed_verification: true },
    });
  };

  const handleClick = () => {
    updateProfile();
    SWAL_TOAST.fire({
      icon: 'info',
      title: hasMessage ? message : `${profile.user.username}驗證已通過。`,
    });
    handleGetProfile();
    setMessage('');
  };

  useEffect(() => {
    if (hasMessage || (isVerified && !isViewed)) {
      setShowHint(true);
    } else {
      setShowHint(false);
    }
  }, [message, profile]);

  return (
    <>
      {showHint && (
        <div className="c-verification-hint">
          <OverlayTrigger placement="top" overlay={Tooltip}>
            <button
              className="c-verification-hint__btn e-btn e-btn--icon"
              onClick={handleClick}
            >
              <FontAwesomeIcon
                icon={solid('exclamation')}
                className="c-verification-hint__icon e-icon e-icon--light"
              />
            </button>
          </OverlayTrigger>
        </div>
      )}
    </>
  );
};

export default VerificationHint;
