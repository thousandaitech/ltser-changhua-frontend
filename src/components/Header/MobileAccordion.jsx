import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

import Accordion from 'react-bootstrap/Accordion';
import HeaderItem from 'components/Header/HeaderItem';

import { useAuthContext } from 'context/AuthContext';

function MobileAccordion(props) {
  const { item } = props;
  const { title, link, list, isAuthOnly } = item;
  const { auth } = useAuthContext();
  return (
    <>
      <Accordion className="c-accordion">
        <Accordion.Item eventKey="0" className="c-accordion__item">
          <Accordion.Button className="c-accordion__toggle">
            {auth && isAuthOnly ? (
              <FontAwesomeIcon
                icon={solid('user')}
                className="e-icon e-icon--secondary"
              />
            ) : (
              title
            )}
          </Accordion.Button>
          <Accordion.Body className="c-accordion__body">
            <ul className="c-block l-header__list">
              {list.map((item) => {
                const { id } = item;
                return (
                  <HeaderItem
                    key={id}
                    item={item}
                    lastPath={link}
                    layout="c-accordion__link"
                  />
                );
              })}
            </ul>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </>
  );
}

export default MobileAccordion;
