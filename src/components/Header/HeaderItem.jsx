import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { HashLink } from 'react-router-hash-link';

import { useAuthContext } from 'context/AuthContext';

function HeaderItem(props) {
  const { item, lastPath, layout } = props;
  const { id, title, link, hash, website, paragraph, btn } = item;
  const { handleLogout } = useAuthContext();

  return (
    <>
      <li className="l-header__item" key={id}>
        {hash && (
          <HashLink to={`/#${hash}`} className={`l-header__link ${layout}`}>
            {title}
          </HashLink>
        )}
        {link &&
          (paragraph ? (
            <Link
              to={`${
                lastPath ? `/${lastPath}/${link}` : `/${link}`
              }#${paragraph}`}
              className={`l-header__link l-header__link--sub ${layout}`}
            >
              {title}
            </Link>
          ) : (
            <Link
              to={lastPath ? `/${lastPath}/${link}` : `/${link}`}
              className={`l-header__link ${layout}`}
            >
              {title}
            </Link>
          ))}
        {website && (
          <a
            href={website}
            className={`l-header__link ${layout}`}
            target="_blank"
            rel="noreferrer"
          >
            {title}
          </a>
        )}
        {btn && (
          <button
            type="button"
            className={`l-header__link ${layout}`}
            onClick={handleLogout}
          >
            登出
          </button>
        )}
      </li>
    </>
  );
}

export default HeaderItem;
