import React from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

import Dropdown from 'react-bootstrap/Dropdown';

import HeaderItem from 'components/Header/HeaderItem';

import { useAuthContext } from 'context/AuthContext';

function HeaderDropdown(props) {
  const { item, currentDropdown, setCurrentDropdown } = props;
  const { id, title, list, link, hash, isAuthOnly } = item;
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const { auth, profile } = useAuthContext();

  const handleItemClick = () => {
    setCurrentDropdown(id);
    if (hash && pathname === '/') {
      navigate(`#${hash}`);
    }
  };

  const customToggle = React.forwardRef(({ children, onClick }, ref) => (
    <button
      className={`dropdown-toggle c-dropdown__toggle l-header__item`}
      ref={ref}
      onClick={handleItemClick}
      onMouseEnter={() => setCurrentDropdown(id)}
    >
      {auth && isAuthOnly ? (
        <span className="c-dropdown__toggle--highlight">
          <FontAwesomeIcon
            icon={solid('user')}
            className="e-icon e-icon--right"
          />
          {profile.user?.first_name}
        </span>
      ) : (
        children
      )}
    </button>
  ));

  return (
    <>
      <Dropdown
        className="c-dropdown c-dropdown--header"
        show={currentDropdown === id}
        onMouseLeave={() => setCurrentDropdown('')}
        autoClose="inside"
      >
        <Dropdown.Toggle as={customToggle}>{title}</Dropdown.Toggle>
        <Dropdown.Menu as="ul" className="dropdown-menu c-dropdown__menu">
          {list.map((item) => {
            const { id } = item;
            return (
              <HeaderItem key={id} item={item} lastPath={link} layout="" />
            );
          })}
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
}

export default HeaderDropdown;
