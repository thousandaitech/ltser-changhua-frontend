import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';

import Offcanvas from 'react-bootstrap/Offcanvas';

import MobileAccordion from 'components/Header/MobileAccordion';
import HeaderDropdown from 'components/Header/HeaderDropdown';
import HeaderItem from 'components/Header/HeaderItem';

import { routeList } from 'data/common';

import { useAuthContext } from 'context/AuthContext';

function MobileOffcanvas() {
  const [currentDropdown, setCurrentDropdown] = useState('');
  const { auth } = useAuthContext();
  const { pathname } = useLocation();

  useEffect(() => {
    setCurrentDropdown('');
  }, [pathname]);

  return (
    <>
      <Offcanvas.Header className="l-header__top" />
      <Offcanvas.Body bsPrefix="l-header__body">
        <ul className="l-header__list d-flex d-xxl-none">
          {routeList.map((item) => {
            const { id, isHeaderShow, isAuthOnly, list } = item;
            return (
              isHeaderShow &&
              (auth ? (
                isAuthOnly ? (
                  list ? (
                    <MobileAccordion key={id} item={item} />
                  ) : null
                ) : list ? (
                  <MobileAccordion key={id} item={item} />
                ) : (
                  <HeaderItem key={id} item={item} lastPath="" layout="" />
                )
              ) : isAuthOnly ? (
                list ? null : (
                  <HeaderItem key={id} item={item} lastPath="" layout="" />
                )
              ) : list ? (
                <MobileAccordion key={id} item={item} />
              ) : (
                <HeaderItem key={id} item={item} lastPath="" layout="" />
              ))
            );
          })}
        </ul>
        <ul className="l-header__list d-none d-xxl-flex">
          {routeList.map((item) => {
            const { id, isHeaderShow, isAuthOnly, list } = item;
            return (
              isHeaderShow &&
              (auth ? (
                isAuthOnly ? (
                  list ? (
                    <HeaderDropdown
                      key={id}
                      item={item}
                      currentDropdown={currentDropdown}
                      setCurrentDropdown={setCurrentDropdown}
                    />
                  ) : null
                ) : list ? (
                  <HeaderDropdown
                    key={id}
                    item={item}
                    currentDropdown={currentDropdown}
                    setCurrentDropdown={setCurrentDropdown}
                  />
                ) : (
                  <HeaderItem key={id} item={item} lastPath="" layout="" />
                )
              ) : isAuthOnly ? (
                list ? null : (
                  <HeaderItem key={id} item={item} lastPath="" layout="" />
                )
              ) : list ? (
                <HeaderDropdown
                  key={id}
                  item={item}
                  currentDropdown={currentDropdown}
                  setCurrentDropdown={setCurrentDropdown}
                />
              ) : (
                <HeaderItem key={id} item={item} lastPath="" layout="" />
              ))
            );
          })}
        </ul>
      </Offcanvas.Body>
    </>
  );
}

export default MobileOffcanvas;
