import React, { useState, useEffect, useContext } from 'react';
import { Link, useLocation } from 'react-router-dom';

import Navbar from 'react-bootstrap/Navbar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

import HeaderOffcanvas from 'components/Header/HeaderOffcanvas';

import { useCustomContext } from 'utils/context';
import { useAuthContext } from 'context/AuthContext';

import { routeList } from 'data/common';

import logo from 'img/icon/logo.png';

const Content = () => {
  const { pathname, hash, key } = useLocation();
  const paths = pathname.split('/');
  const listPaths = pathname.split('/').slice(1);
  const [showCanvas, setShowCanvas] = useState(false);
  const {
    breadcrumbItem,
    setBreadcrumbItem,
    currentPageData,
    setCurrentPageData,
  } = useCustomContext();
  const { auth } = useAuthContext();
  const [currentPath, setCurrentPath] = useState({
    id: '',
    title: '',
    link: '',
    list: [],
  });
  const isFetchingCurrentPageData = currentPageData.length === 0;

  const handleMatchedRoute = (path, list) => {
    const matchedResult = list.find((item) => item.link === path);
    if (pathname !== '/' && matchedResult !== undefined) {
      setBreadcrumbItem((prev) => [...prev, matchedResult]);
      setCurrentPath(matchedResult);
    }
  };

  const handleMatchingRoute = (routes, path) => {
    return routes.find((item) => item.link === path);
  };

  const handleIteration = (routes, paths) => {
    for (let i = 0; i <= paths.length; i++) {
      if (
        handleMatchingRoute(routes, paths[i]) &&
        handleMatchingRoute(routes, paths[i]).list
      ) {
        setCurrentPageData(handleMatchingRoute(routes, paths[i]));
        if (
          handleMatchingRoute(
            handleMatchingRoute(routes, paths[i]).list,
            paths[i + 1]
          )
        ) {
          return handleIteration(
            handleMatchingRoute(routes, paths[i]).list,
            paths.slice(i + 1)
          );
        } else {
          return;
        }
      } else {
        return;
      }
    }
  };

  useEffect(() => {
    setShowCanvas(false);
    setBreadcrumbItem([]);
    paths.forEach((path) => {
      handleMatchedRoute(path, routeList);
    });
  }, [pathname, auth]);

  useEffect(() => {
    if (hash === '') {
      window.scrollTo({
        top: 0,
        behavior: 'instant',
      });
    } else {
      setTimeout(() => {
        const id = hash.split('?')[0].replace('#', '');
        const element = document.getElementById(id);
        if (element) {
          element.scrollIntoView({ block: 'start', behavior: 'smooth' });
        }
      }, 0);
    }
  }, [pathname, hash, key]);

  useEffect(() => {
    if (currentPath !== undefined && currentPath.list) {
      paths.forEach((path) => {
        handleMatchedRoute(path, currentPath.list);
      });
    }
  }, [breadcrumbItem]);

  useEffect(() => {
    handleIteration(routeList, listPaths);
  });

  const toggleLayout = () => {
    if (!showCanvas) {
      return (
        <FontAwesomeIcon
          icon={solid('bars')}
          className="l-header__icon e-icon e-icon--secondary fa-fw"
        />
      );
    } else {
      return (
        <FontAwesomeIcon
          icon={solid('xmark')}
          className="l-header__icon e-icon e-icon--secondary fa-fw"
        />
      );
    }
  };

  return (
    <>
      <Navbar expand="xxl" className="l-header">
        <div className="row gx-0 justify-content-center w-100 h-100">
          <div className="col-11 d-flex justify-content-between align-items-center">
            <Link to="/">
              <div className="l-header__logo d-flex align-items-center">
                <div className="l-header__img">
                  <img className="e-img e-img--contain" src={logo} alt="logo" />
                </div>
                <div className="l-header__logo-text">
                  <h6 className="l-header__text">LTSER Changhua</h6>
                  <h6 className="l-header__text">長期社會生態核心觀測彰化站</h6>
                </div>
              </div>
            </Link>
            <Navbar.Toggle
              aria-controls="navOffcanvas"
              className="l-header__toggle"
              onClick={() => setShowCanvas(!showCanvas)}
              children={toggleLayout()}
            />
            <Navbar.Offcanvas
              id="navOffcanvas"
              aria-labelledby="navOffcanvas"
              placement="end"
              backdropClassName="l-header__backdrop"
              className="l-header__offcanvas"
              show={showCanvas ? 1 : 0}
              onHide={() => setShowCanvas(!showCanvas)}
              scroll="true"
            >
              <HeaderOffcanvas />
            </Navbar.Offcanvas>
          </div>
        </div>
      </Navbar>
    </>
  );
};

export default Content;
