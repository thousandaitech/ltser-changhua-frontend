import React from 'react';

import Form from 'react-bootstrap/Form';

function Sort(props) {
  const { filter, setFilter } = props;
  const sortByList = [
    {
      id: 'dateDescending',
      title: '上架日期新至舊',
    },
    {
      id: 'dateAscending',
      title: '上架日期舊至新',
    },
    {
      id: 'views',
      title: '點閱次數',
    },
  ];
  const pageList = [
    {
      id: '1',
      num: 10,
    },
    {
      id: '2',
      num: 20,
    },
  ];

  const handleSortChange = (e) => {
    setFilter({ ...filter, sort: e.target.value });
  };
  return (
    <>
      <Form className="c-select">
        <Form.Group className="c-select__set">
          <Form.Label className="c-select__label">排序:</Form.Label>
          <Form.Select
            className="c-select__input"
            onChange={(e) => {
              handleSortChange(e);
            }}
          >
            {sortByList.map((item) => {
              const { id, title } = item;
              return (
                <option key={id} value={id}>
                  {title}
                </option>
              );
            })}
          </Form.Select>
        </Form.Group>
        <Form.Select className="c-select__input">
          {pageList.map((item) => {
            const { id, num } = item;
            return (
              <option key={id} value={id}>
                {num}筆/頁
              </option>
            );
          })}
        </Form.Select>
      </Form>
    </>
  );
}

export default Sort;
