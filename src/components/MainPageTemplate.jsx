import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';

import Intro from 'components/Intro';

import { useCustomContext } from 'utils/context';

function MainPageTemplate() {
  const { pathname } = useLocation();
  const { currentPageData } = useCustomContext();

  const isFetchingData = currentPageData.length === 0;

  return (
    <>
      <div className="l-main">
        <div className="row justify-content-center">
          {!isFetchingData && (
            <>
              <Intro data={currentPageData} />
              <div className="row gx-4 gy-3 justify-content-center">
                {currentPageData.list.map((item) => {
                  const { id, title, link, icon } = item;
                  return (
                    <div key={id} className="col-12 col-sm-6 col-md-4 col-xl-3">
                      <Link to={`${pathname}/${link}`}>
                        <div
                          className={`l-section__block l-section__block--gray`}
                        >
                          <div className="l-section__img">
                            <img
                              className="e-img e-img--contain"
                              src={icon}
                              alt={title}
                            />
                          </div>
                          <h5 className="l-section__text">{title}</h5>
                        </div>
                      </Link>
                    </div>
                  );
                })}
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
}

export default MainPageTemplate;
