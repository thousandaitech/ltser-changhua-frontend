import React, { useState, useEffect } from 'react';
import axios from 'axios';

import { Swiper, SwiperSlide } from 'swiper/react';
import { EffectFade, Autoplay } from 'swiper';

import 'swiper/css/effect-fade';
import 'swiper/css';

import { API_URL, IMAGE_URL } from 'utils/config';

function KeyVisual() {
  const [images, setImages] = useState([]);

  const isFetchingImages = images.length === 0;
  const handleImageIndex = (index) => {
    if (index >= images.length - 1) {
      return images.length - 1;
    } else {
      return index;
    }
  };
  const getImages = async () => {
    try {
      const response = await axios({
        method: 'get',
        url: `${API_URL}/getHomepagePhotos`,
      });
      setImages([...response.data]);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getImages();
  }, []);
  return (
    <>
      <div className="l-key-visual">
        {!isFetchingImages && (
          <Swiper
            speed={3500}
            spaceBetween={0}
            slidesPerView={1}
            loop={true}
            effect={'fade'}
            fadeEffect={{
              crossFade: false,
            }}
            autoplay={{
              delay: 3500,
            }}
            modules={[EffectFade, Autoplay]}
            className="h-100"
          >
            <SwiperSlide>
              <img
                className="e-img e-img--cover"
                src={`${IMAGE_URL}${images[handleImageIndex(0)].image}`}
                alt="slide"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                className="e-img e-img--cover"
                src={`${IMAGE_URL}${images[handleImageIndex(1)].image}`}
                alt="slide"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                className="e-img e-img--cover"
                src={`${IMAGE_URL}${images[handleImageIndex(2)].image}`}
                alt="slide"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                className="e-img e-img--cover"
                src={`${IMAGE_URL}${images[handleImageIndex(3)].image}`}
                alt="slide"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                className="e-img e-img--cover"
                src={`${IMAGE_URL}${images[handleImageIndex(4)].image}`}
                alt="slide"
              />
            </SwiperSlide>
            <SwiperSlide>
              <img
                className="e-img e-img--cover"
                src={`${IMAGE_URL}${images[handleImageIndex(5)].image}`}
                alt="slide"
              />
            </SwiperSlide>
          </Swiper>
        )}
      </div>
    </>
  );
}

export default KeyVisual;
