import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { routeList } from 'data/common';

function Section(props) {
  const { item, page } = props;
  const { id, title, subtitle, hash, customStyle } = item;
  const [data, setData] = useState({
    id: '',
    title: '',
    link: '',
    list: [],
  });

  const oppositeStyle = customStyle === 'gray' ? 'light' : 'gray';

  const handleMatchedData = (title) => {
    const matchedData = routeList.find((item) => item.title === title);
    setData(matchedData);
  };

  useEffect(() => {
    handleMatchedData(title);
  }, []);

  const isFetchingData = data.id === '';

  return (
    <>
      {!isFetchingData && (
        <section
          className={`l-home__${id} l-section l-section--${customStyle}`}
          id={hash}
        >
          <div className="row justify-content-center g-0 w-100">
            <div className="col-10 col-sm-6">
              <h2 className="l-section__title ">{title}</h2>
              <p className="l-section__paragraph">{subtitle}</p>
            </div>
            <div className={`${page === 'home' ? 'col-10' : 'col-11'}`}>
              <div className="row gy-5 gx-4">
                {data.list.map((item) => {
                  const { id, title, icon, link, website } = item;
                  return (
                    <div
                      key={id}
                      className={`col-12 col-sm-6 col-lg-${Math.ceil(
                        12 / data.list.length
                      )}`}
                    >
                      {link && (
                        <Link
                          to={`/${data.link}/${link}`}
                          className="l-section__wrapper"
                        >
                          <div
                            className={`l-section__block l-section__block--${oppositeStyle}`}
                          >
                            <div className="l-section__img">
                              <img
                                className="e-img e-img--contain"
                                src={icon}
                                alt={title}
                              />
                            </div>
                            <h5 className="l-section__text">{title}</h5>
                          </div>
                        </Link>
                      )}
                      {website && (
                        <a href={website} className="l-section__wrapper">
                          <div
                            className={`l-section__block l-section__block--${oppositeStyle}`}
                          >
                            <div className="l-section__img">
                              <img
                                className="e-img e-img--contain"
                                src={icon}
                                alt={title}
                              />
                            </div>
                            <h5 className="l-section__text">{title}</h5>
                          </div>
                        </a>
                      )}
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </section>
      )}
    </>
  );
}

export default Section;
