import React, { useState, useEffect } from 'react';

import { useMap, GeoJSON } from 'react-leaflet';
import L from 'leaflet';

import townGraph from 'data/changhua_town_FY_geo.json';
import { villageGraph } from 'data/changhua_village_FY_geo.js';
import MapChangeView from 'components/Landscapes/MapChangeView';

function Polygon(props) {
  const { active, setActive, mapParams, setMapParams, DEFAULT_MAP_PARAMS } =
    props;
  const map = useMap();
  const [polygon, setPolygon] = useState();
  const [villageBounds, setVillageBounds] = useState(false);

  const isAllVillage = active.village === '';
  const isNoSite = active.site === '';

  const ACTIVE_STYLE = {
    weight: 5,
    opacity: 1,
    color: 'rgb(68, 160, 201)',
    fillOpacity: 0.1,
    fillColor: '#fff',
  };

  const filter = (feature) => {
    return feature.properties.VILLAGE === active.village;
  };

  useEffect(() => {
    setPolygon({ ...townGraph });
  }, []);

  useEffect(() => {
    setMapParams({ ...DEFAULT_MAP_PARAMS });
    setActive({ ...active, image: '' });
    // if (!isAllVillage) {
    //   setVillageBounds(true);
    // }
  }, [active.village]);

  // useEffect(() => {
  //   if (villageBounds) {
  //     const villageObj = L.geoJSON(
  //       villageGraph.find(
  //         (feature) => feature.properties.VILLAGE === active.village
  //       )
  //     );
  //     map.fitBounds(villageObj.getBounds());
  //     setVillageBounds(false);
  //   }
  // }, [villageBounds]);

  if (polygon) {
    if (isAllVillage) {
      return (
        <>
          <GeoJSON
            key={active.village}
            data={{ ...townGraph }}
            style={ACTIVE_STYLE}
          />
          <MapChangeView mapParams={mapParams} />
        </>
      );
    } else {
      return (
        <>
          <GeoJSON
            key={active.village}
            data={[...villageGraph]}
            filter={filter}
            style={ACTIVE_STYLE}
          />
          <MapChangeView mapParams={mapParams} />
        </>
      );
    }
  }
}

export default Polygon;
