import React, { useState } from 'react';

const YearImageRadio = (props) => {
  const { data, active, setActive, setImage, DEFAULT_IMAGE, radio, setRadio } =
    props;

  const handleShowLayer = (time, image) => {
    const isActiveImage = active.image === image;
    if (!isActiveImage) {
      setImage({ ...DEFAULT_IMAGE });
    }
    setActive({ ...active, image: image, year: time });
  };
  const handleRadioChange = (e, time, image) => {
    const value = Number(e.target.value);
    const isActiveRadio = radio === value;
    if (!isActiveRadio) {
      setRadio(value);
      handleShowLayer(time, image);
    } else {
      setRadio(0);
      setImage({ ...DEFAULT_IMAGE });
      setActive({ ...active, image: '', year: '' });
    }
  };
  return (
    <>
      <h5 className="c-menu__subtitle">影像變遷說明</h5>
      <ul>
        {data &&
          data.map((item) => {
            const { id, time, title, image } = item;
            return time !== '' ? (
              <li key={id} className="c-menu__item">
                <div className="c-checkbox">
                  <div className="c-checkbox__set">
                    <input
                      type="radio"
                      id={id}
                      name="image"
                      className="form-check-input c-checkbox__input"
                      value={id}
                      onClick={(e) => handleRadioChange(e, time, image)}
                      checked={id === radio}
                    />
                    <label
                      htmlFor={id}
                      className="c-checkbox__label c-menu__action"
                    >
                      {time}
                    </label>
                  </div>
                  <span className="c-checkbox__text">{title}</span>
                </div>
              </li>
            ) : (
              <li key={id} className="c-menu__item">
                <span className="c-checkbox__text">{title}</span>
              </li>
            );
          })}
      </ul>
    </>
  );
};

export default YearImageRadio;
