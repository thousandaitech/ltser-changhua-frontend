import React from 'react';
import { Link } from 'react-router-dom';

import { navList } from 'data/landscapes';

function Nav(props) {
  const { active, setActive, setMapParams, DEFAULT_MAP_PARAMS } = props;

  const handleCategoryChange = (id) => {
    setActive({
      ...active,
      site: '',
      year: '',
      image: '',
      category: id,
      keyword: '',
      village: '',
    });
    setMapParams({ ...DEFAULT_MAP_PARAMS });
  };
  return (
    <>
      <div className="l-landscapes__nav c-nav">
        <div className="c-nav__wrapper">
          <div className="c-nav__divider c-nav__divider--left">
            <h2 className="c-nav__title">地景資料</h2>
          </div>
          <div className="c-nav__divider c-nav__divider--right">
            <h2 className="c-nav__title">主題導覽</h2>
            {navList.map((item) => {
              const { id, title } = item;
              return (
                <Link
                  role="button"
                  key={id}
                  className={`c-nav__tab ${
                    active.category === id ? 'active' : ''
                  }`}
                  data-category={id}
                  onClick={() => {
                    handleCategoryChange(id);
                  }}
                >
                  {title}
                </Link>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
}

export default Nav;
