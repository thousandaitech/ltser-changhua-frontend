import React from 'react';

import Modal from 'react-bootstrap/Modal';

function ImageModal(props) {
  const { show, setShow, item, title } = props;

  const handleClose = () => setShow({ ...show, modal: false });

  return (
    <>
      <Modal
        show={show.modal}
        onHide={handleClose}
        className="c-modal"
        backdropClassName="c-modal__backdrop"
        dialogClassName="c-modal__dialog"
        contentClassName="c-modal__content"
        scrollable={true}
        centered={true}
      >
        <Modal.Body className="c-modal__body ">
          <img
            className="e-img e-img--cover"
            src={require(`../../img/landscapes/pic/${item}.jpg`)}
            alt={title}
          />
        </Modal.Body>
      </Modal>
    </>
  );
}

export default ImageModal;
