import React, { useState, useEffect, useRef } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

import Filter from 'components/Landscapes/Filter';
import ImageModal from 'components/Landscapes/ImageModal';
import MapChangeView from 'components/Landscapes/MapChangeView';
import YearImageRadio from 'components/Landscapes/YearImageRadio';

import { navList } from 'data/landscapes';
import { siteList } from 'data/sites';

function Menu(props) {
  const {
    show,
    setShow,
    active,
    setActive,
    setImage,
    DEFAULT_IMAGE,
    sites,
    setSites,
    mapParams,
    setMapParams,
    zoomMapParams,
    DEFAULT_MAP_PARAMS,
  } = props;
  const topEl = useRef();

  const [siteData, setSiteData] = useState({
    id: '',
    date: '',
    title: '',
    category: [],
    categories: [],
    address: '',
    location: [],
    intro: [],
    history: {},
    stakeholder: '',
    reference: {},
    pics: [],
  });
  const [radio, setRadio] = useState(0);

  const isNoSite = active.site === '';
  const isNoVillage = sites.length === 0;
  const isFetchingSiteData = siteData.id === '';
  const hasHistoryStory =
    !isFetchingSiteData && siteData.history.story.length !== 0;

  const handleToggleSite = (id) => {
    const isActiveSite = active.site === id;
    if (!isActiveSite) {
      setActive({ ...active, site: id, image: '', year: '' });
      if (topEl.current) {
        topEl.current.scrollIntoView({ block: 'center', behavior: 'instant' });
      }
    } else {
      return;
    }
  };

  const handleDisableScrollZoom = (e) => {
    e.stopPropagation();
  };

  const handleShowModal = () => {
    setShow({ ...show, modal: true });
  };

  const handleCloseSubMenu = () => {
    setRadio(0);
    setActive({ ...active, site: '', image: '', year: '' });
    setMapParams({ ...DEFAULT_MAP_PARAMS });
    return <MapChangeView mapParams={mapParams} />;
  };

  useEffect(() => {
    if (!isNoSite) {
      setRadio(0);
      const matchedSite = siteList.find((item) => item.id === active.site);
      const matchedCategories = matchedSite.category.map((category) => {
        return navList.find((item) => item.id === category).title;
      });
      setSiteData({ ...matchedSite, categories: [...matchedCategories] });
      setShow({ ...show, main: true, sub: true });
    }
  }, [active.site]);

  useEffect(() => {
    if (!isFetchingSiteData) {
      setMapParams({ ...zoomMapParams(siteData.location) });
    }
  }, [siteData]);

  return (
    <>
      <div
        className="c-map__wrapper"
        onScrollCapture={handleDisableScrollZoom}
        onWheelCapture={handleDisableScrollZoom}
        onTouchMoveCapture={handleDisableScrollZoom}
      >
        <div className="c-map__menu">
          <div className="c-map__content">
            <Filter
              active={active}
              setActive={setActive}
              sites={sites}
              setSites={setSites}
            />
            <ul className="c-list">
              {!isNoVillage ? (
                sites.map((item) => {
                  const { id, title } = item;
                  return (
                    <li key={id} className="c-list__item">
                      <button
                        type="button"
                        className={`c-list__btn ${
                          active.site === id ? 'active' : ''
                        }`}
                        onClick={() => {
                          handleToggleSite(id);
                        }}
                      >
                        {title}
                      </button>
                    </li>
                  );
                })
              ) : (
                <h4 className="c-list__text">沒有相符的結果。</h4>
              )}
            </ul>
          </div>
        </div>
        {!isNoSite && (
          <div className={`c-menu ${show.sub && !isNoSite ? 'active' : ''}`}>
            <button
              type="button"
              className="c-menu__btn e-btn e-btn--icon"
              onClick={handleCloseSubMenu}
            >
              <FontAwesomeIcon
                icon={solid('xmark')}
                className="e-icon e-icon--muted"
              />
            </button>
            <ul>
              <li className="c-menu__item c-menu__item--seg" ref={topEl}>
                <h4 className="c-menu__title">{siteData.title}</h4>
              </li>
              <li className="c-menu__item c-menu__item--seg">
                {siteData.pics &&
                  siteData.pics.map((item, i) => {
                    return (
                      <React.Fragment key={i}>
                        <button
                          type="button"
                          className="c-menu__img"
                          onClick={handleShowModal}
                        >
                          <img
                            className="e-img e-img--cover"
                            src={require(`../../img/landscapes/pic/${item}.jpg`)}
                            alt={siteData.title}
                          />
                        </button>
                        <ImageModal
                          show={show}
                          setShow={setShow}
                          item={item}
                          title={siteData.title}
                        />
                      </React.Fragment>
                    );
                  })}
              </li>
              <li className="c-menu__item c-menu__item--seg">
                <h4 className="c-menu__title">主題類別</h4>
                {siteData.categories &&
                  siteData.categories.map((item, i) => {
                    return (
                      <span key={i} className="c-menu__text">
                        {item}
                      </span>
                    );
                  })}
              </li>
              <li className="c-menu__item c-menu__item--seg">
                <h4 className="c-menu__title">地點</h4>
                {siteData.address}
              </li>
              <li className="c-menu__item c-menu__item--seg">
                <h4 className="c-menu__title">簡介</h4>
                {siteData.intro}
              </li>
              <li className="c-menu__item c-menu__item--seg">
                <h4 className="c-menu__title">變遷故事</h4>
                {hasHistoryStory && (
                  <>
                    <h5 className="c-menu__subtitle">歷史資料</h5>
                    <ul>
                      {siteData.history.story &&
                        siteData.history.story.map((item) => {
                          const { id, time, title } = item;
                          return time !== '' ? (
                            <li
                              key={id}
                              className="c-menu__item c-menu__item--dot"
                            >
                              <span className="c-menu__text c-menu__text--muted">
                                {time}
                              </span>
                              {title}
                            </li>
                          ) : (
                            <li
                              key={id}
                              className="c-menu__item c-menu__item--dot"
                            >
                              {title}
                            </li>
                          );
                        })}
                    </ul>
                  </>
                )}
                <YearImageRadio
                  data={siteData.history.data}
                  active={active}
                  setActive={setActive}
                  setImage={setImage}
                  DEFAULT_IMAGE={DEFAULT_IMAGE}
                  radio={radio}
                  setRadio={setRadio}
                />
              </li>
              <li className="c-menu__item c-menu__item--seg">
                <h4 className="c-menu__title">權益關係人</h4>
                {siteData.stakeholder}
              </li>
              <li className="c-menu__item c-menu__item--seg">
                <h4 className="c-menu__title">參考資料</h4>
                {Object.entries(siteData.reference).map(([key, value]) => {
                  return (
                    <ul>
                      <h5 className="c-menu__subtitle">{key}</h5>
                      {value.map((item) => {
                        const { id, time, title, link } = item;
                        return link ? (
                          <li
                            key={id}
                            className="c-menu__item c-menu__item--dot"
                          >
                            <a
                              href={link}
                              className="e-link"
                              target="_blank"
                              rel="noreferrer"
                            >
                              {time} {title}
                            </a>
                          </li>
                        ) : (
                          <li
                            key={id}
                            className="c-menu__item c-menu__item--dot"
                          >
                            <span>
                              {time} {title}
                            </span>
                          </li>
                        );
                      })}
                    </ul>
                  );
                })}
              </li>
              <li className="c-menu__item c-menu__item--seg">
                <h4 className="c-menu__title">調查/最後更新日期</h4>
                {siteData.date}
              </li>
            </ul>
          </div>
        )}
      </div>
    </>
  );
}

export default Menu;
