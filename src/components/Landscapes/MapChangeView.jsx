import { useMap } from 'react-leaflet';

function MapChangeView(props) {
  const { mapParams } = props;
  const { center, zoom } = mapParams;
  const map = useMap();
  map.setView(center, zoom);
  return null;
}

export default MapChangeView;
