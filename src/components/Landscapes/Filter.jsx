import React, { useState, useEffect } from 'react';

import Form from 'react-bootstrap/Form';

import { villageList } from 'data/landscapes';
import { siteList } from 'data/sites';

function Filter(props) {
  const { active, setActive } = props;

  const handleFieldChange = (e) => {
    setActive((prev) => ({
      ...prev,
      category: '',
      village: '',
      keyword: e.target.value,
    }));
  };

  const handleSelectChange = (e) => {
    setActive((prev) => ({
      ...prev,
      site: '',
      year: '',
      image: '',
      category: '',
      keyword: '',
      village: e.target.value,
    }));
  };
  return (
    <>
      <Form className="c-map__form">
        <Form.Control
          type="text"
          placeholder="請輸入點位名稱"
          className="c-map__input c-form__input mb-2"
          value={active.keyword}
          onChange={handleFieldChange}
          name="keyword"
        />
        <div className="c-select mb-0">
          <Form.Group className="c-select__set">
            <Form.Select
              className="c-select__input"
              value={active.village}
              onChange={handleSelectChange}
            >
              <option value="">全芳苑鄉</option>
              {villageList.map((item, i) => {
                return (
                  <option key={i} value={item}>
                    {item}
                  </option>
                );
              })}
            </Form.Select>
          </Form.Group>
        </div>
        <hr className="e-hr my-3" />
      </Form>
    </>
  );
}

export default Filter;
