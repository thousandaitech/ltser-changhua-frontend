import React from 'react';
import Dropdown from 'react-bootstrap/Dropdown';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import { baseMapList } from 'data/common';

function BaseLayerDropdown(props) {
  const { active, setActive } = props;

  const handleCheckChange = (e) => {
    setActive({ ...active, base: e.target.value });
  };
  return (
    <>
      <Dropdown bsPrefix="c-dropdown">
        <Dropdown.Toggle bsPrefix="c-dropdown__toggle">
          <FontAwesomeIcon
            icon={solid('layer-group')}
            className="e-icon e-icon--muted"
          />
        </Dropdown.Toggle>
        <Dropdown.Menu className="c-dropdown__menu">
          <section className="c-map__base">
            {baseMapList.map((item) => {
              const { id, title } = item;
              return (
                <React.Fragment key={id}>
                  <input
                    type="radio"
                    className="btn-check"
                    name="basemap"
                    value={id}
                    id={id}
                    onChange={handleCheckChange}
                    checked={active.base === id}
                    autoComplete="off"
                  />
                  <label
                    className={`c-form__label e-btn c-map__btn ${
                      active.base === id ? 'active' : ''
                    }`}
                    htmlFor={id}
                  >
                    {title}
                  </label>
                </React.Fragment>
              );
            })}
          </section>
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
}

export default BaseLayerDropdown;
