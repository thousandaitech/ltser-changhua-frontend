import { useMapEvents } from 'react-leaflet';

function MapZoomLevel(props) {
  const { mapParams, setMapParams } = props;
  const mapEvents = useMapEvents({
    zoomend: () => {
      setMapParams({
        center: [mapEvents.getCenter().lat, mapEvents.getCenter().lng],
        zoom: mapEvents.getZoom(),
      });
    },
  });
  return null;
}

export default MapZoomLevel;
