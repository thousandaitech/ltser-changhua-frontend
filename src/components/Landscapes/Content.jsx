import React, { useState, useEffect } from 'react';

import {
  MapContainer,
  TileLayer,
  LayersControl,
  ZoomControl,
  ScaleControl,
  LayerGroup,
  Marker,
  Tooltip,
} from 'react-leaflet';

import { divIcon } from 'leaflet';
import { renderToStaticMarkup } from 'react-dom/server';

import Nav from 'components/Landscapes/Nav';
import Menu from 'components/Landscapes/Menu';
import BaseLayerDropdown from 'components/Landscapes/BaseLayerDropdown';
import Polygon from 'components/Landscapes/Polygon';
import MapZoomLevel from 'components/Landscapes/MapZoomLevel';

import markerImg from 'img/icon/location.svg';
import markerImgActive from 'img/icon/location--active.svg';

import { overlayList, yearMapList } from 'data/landscapes';
import { siteList } from 'data/sites';
import { baseMapList } from 'data/common';

function Content() {
  const DEFAULT_IMAGE = {
    id: '',
    image: '',
    title: '',
    coordinate: [],
  };
  const DEFAULT_MAP_PARAMS = {
    center: [23.95920348627543, 120.32102450000002],
    zoom: 12,
  };
  const [show, setShow] = useState({
    main: true,
    sub: false,
    modal: false,
  });
  const [active, setActive] = useState({
    keyword: '',
    category: '',
    site: '',
    year: '',
    base: 'photo2020',
    village: '',
    image: '',
  });
  const [sites, setSites] = useState([]);
  const [image, setImage] = useState({ ...DEFAULT_IMAGE });
  const [mapParams, setMapParams] = useState({ ...DEFAULT_MAP_PARAMS });
  const [yearMap, setYearMap] = useState({
    year: '',
    id: '',
    url: '',
  });

  const iconMarkup = (id, img, title, firstCategory) =>
    renderToStaticMarkup(
      <div
        className={`c-map__marker c-map__marker--sm ${
          active.site === id ? 'active' : ''
        }`}
        data-category={`${active.category ? active.category : firstCategory}`}
      >
        {/* <img
          src={img}
          alt={title}
          className="e-img e-img--contain c-map__icon"
        /> */}
      </div>
    );
  const mapIcon = (id, img, title, firstCategory) =>
    divIcon({
      html: iconMarkup(id, img, title, firstCategory),
    });

  const isNoYear = active.year === '';
  const isAllVillage = active.village === '';
  const isNoImage = active.image === '';
  const isFetchingYearMap = yearMap.id === '';

  const zoomMapParams = (location) => {
    return {
      center: [location[0], location[1] + 0.001],
      zoom: 20,
    };
  };

  const handleResetMenu = () => {
    setShow({ main: true, sub: false, modal: false });
  };

  useEffect(() => {
    let filteredSites = [...siteList];
    if (active.village) {
      filteredSites = filteredSites.filter((item) =>
        item.address.includes(active.village)
      );
    }
    if (active.category) {
      filteredSites = filteredSites.filter((item) =>
        item.category.includes(active.category)
      );
    }
    if (active.keyword) {
      filteredSites = filteredSites.filter((v) =>
        v.title.includes(active.keyword)
      );
    }
    setSites(filteredSites);
  }, [active, siteList]);

  useEffect(() => {
    if (!isNoImage) {
      const matchedImage = overlayList.find(
        (item) => item.image === active.image
      );
      if (matchedImage) {
        setImage({ ...matchedImage });
      } else {
        setImage({ ...DEFAULT_IMAGE });
      }
    }
  }, [active.image]);

  useEffect(() => {
    if (!isNoYear) {
      const matchedYearMap = yearMapList.find(
        (item) => item.year === active.year
      );
      if (matchedYearMap) {
        setYearMap({ ...matchedYearMap });
      } else {
        setYearMap({ id: '', year: '', url: '' });
      }
    } else {
      setYearMap({ id: '', year: '', url: '' });
    }
  }, [active.year]);

  return (
    <>
      <article className="l-landscapes">
        <div className="l-landscapes__main">
          <Nav
            active={active}
            setActive={setActive}
            isAllVillage={isAllVillage}
            setMapParams={setMapParams}
            DEFAULT_MAP_PARAMS={DEFAULT_MAP_PARAMS}
          />
          <div className="d-flex">
            <Menu
              show={show}
              setShow={setShow}
              active={active}
              setActive={setActive}
              setImage={setImage}
              DEFAULT_IMAGE={DEFAULT_IMAGE}
              sites={sites}
              setSites={setSites}
              mapParams={mapParams}
              setMapParams={setMapParams}
              zoomMapParams={zoomMapParams}
              DEFAULT_MAP_PARAMS={DEFAULT_MAP_PARAMS}
              isAllVillage={isAllVillage}
            />
            <MapContainer
              className="c-map l-landscapes__map"
              scrollWheelZoom={true}
              doubleClickZoom={false}
              zoomControl={false}
              center={mapParams.center}
              zoom={mapParams.zoom}
            >
              <BaseLayerDropdown active={active} setActive={setActive} />
              {/* {!isNoImage && !isFetchingImage && (
              <ImageOverlay
                url={`https://django-ltser-ncue.s3.ap-northeast-1.amazonaws.com/${image.image}.jpg`}
                bounds={image.coordinate}
              ></ImageOverlay>
            )} */}
              <Polygon
                active={active}
                setActive={setActive}
                mapParams={mapParams}
                setMapParams={setMapParams}
                DEFAULT_MAP_PARAMS={DEFAULT_MAP_PARAMS}
              />
              <MapZoomLevel mapParams={mapParams} setMapParams={setMapParams} />
              <LayersControl>
                <LayersControl.Overlay name="map" checked={true}>
                  <LayerGroup>
                    {sites.map((item) => {
                      const { id, title, category, location } = item;
                      const isActiveSite = active.site === id;
                      const img = isActiveSite ? markerImgActive : markerImg;
                      const firstCategory = category[0];
                      return (
                        <Marker
                          key={id}
                          position={location}
                          icon={mapIcon(id, img, title, firstCategory)}
                          eventHandlers={{
                            click: () => {
                              if (!isActiveSite) {
                                setActive({ ...active, site: id });
                                setMapParams({ ...zoomMapParams(location) });
                              } else {
                                return;
                              }
                            },
                          }}
                        >
                          <Tooltip offset={[20, 12.5]}>{title}</Tooltip>
                        </Marker>
                      );
                    })}
                  </LayerGroup>
                </LayersControl.Overlay>
              </LayersControl>
              {baseMapList.map((item) => {
                const { id, url, attribution } = item;
                return (
                  active.base === id && (
                    <TileLayer key={id} url={url} attribution={attribution} />
                  )
                );
              })}
              {!isFetchingYearMap && (
                <TileLayer key={yearMap.id} url={yearMap.url} attribution="" />
              )}
              <ZoomControl position="topright" />
              <ScaleControl position="bottomright" imperial={false} />
            </MapContainer>
          </div>
        </div>
        <div className="l-landscapes__container">
          <div className="row w-100 justify-content-center">
            <div className="col-10 col-md-12">
              <h1 className="l-landscapes__text">
                請使用螢幕寬度768px以上的裝置查看地圖。
              </h1>
            </div>
          </div>
        </div>
      </article>
    </>
  );
}

export default Content;
