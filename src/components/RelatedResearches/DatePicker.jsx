import React from 'react';

import Form from 'react-bootstrap/Form';

function DatePicker(props) {
  const { filter, setFilter } = props;
  const handleFieldChange = (e) => {
    setFilter({
      ...filter,
      date: { ...filter.date, [e.target.name]: e.target.value },
    });
  };
  return (
    <>
      <Form className="c-form w-100">
        <Form.Group className="c-form__actions">
          <label className="c-form__text" htmlFor="start">
            日期:
          </label>
          <Form.Control
            type="date"
            placeholder="請輸入關鍵字"
            className="c-form__input me-3"
            value={filter.date.start}
            name="start"
            id="start"
            onChange={handleFieldChange}
          />
          <label className="c-form__text" htmlFor="end">
            至
          </label>
          <Form.Control
            type="date"
            placeholder="請輸入關鍵字"
            className="c-form__input"
            value={filter.date.start}
            name="end"
            id="end"
            onChange={handleFieldChange}
          />
        </Form.Group>
      </Form>
    </>
  );
}

export default DatePicker;
