import React from 'react';
import { Link } from 'react-router-dom';

import { footerList, sponsorList } from 'data/common';

function Footer() {
  return (
    <>
      <div className="l-footer">
        <div className="row justify-content-center g-0 w-100">
          <div className="col-10">
            <ul className="l-footer__list">
              {sponsorList.map((item) => {
                const { id, title, subtitle, img, link } = item;
                return (
                  <li key={id}>
                    <a
                      href={link}
                      target="_blank"
                      rel="noreferrer"
                      className="l-footer__wrapper"
                      title={title}
                    >
                      <div className="l-footer__subtitle">{subtitle}</div>
                      <div className="l-footer__img-wrapper">
                        <img
                          className="e-img e-img--contain l-footer__img"
                          src={img}
                          alt={title}
                        />
                      </div>
                    </a>
                  </li>
                );
              })}
            </ul>
            <ul className="l-footer__list">
              {footerList.map((item) => {
                const { id, title, link } = item;
                return (
                  <li key={id} className="l-footer__item">
                    <Link to={link} className="l-footer__link e-link">
                      {title}
                    </Link>
                  </li>
                );
              })}
            </ul>
            <div className="l-footer__text">
              ©2022 LTSER 長期社會生態核心觀測彰化站
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Footer;
