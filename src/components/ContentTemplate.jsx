import React, { useState, useEffect } from 'react';

import Intro from 'components/Intro';
import Item from 'components/Item';
import PaginationLayout from 'components/PaginationLayout';
import SpinnerPlaceholder from './SpinnerPlaceholder';

const ContentTemplate = (props) => {
  const {
    pageData,
    data,
    tab,
    search,
    tagList,
    currentPage,
    setCurrentPage,
    paginationData,
    paginationList,
    isFetchingPages,
    hasNoRecords,
    setRefresh,
  } = props;
  const { page } = pageData;

  const isFetchingList = data === null;
  const hasNoList = !isFetchingList && data.length === 0;

  return (
    <>
      <article className={`l-${page}`}>
        <section className={`l-${page}__title c-heading u-section`}>
          <Intro data={pageData} />
        </section>
        <section className={`l-${page}__data`}>
          {search && search}
          {tab}
          {!isFetchingList ? (
            !hasNoList ? (
              data.map((item) => {
                const { id } = item;
                return (
                  <div key={id} className={`l-${page}__item`}>
                    <Item
                      url={pageData.url}
                      data={item}
                      tagList={tagList}
                      setRefresh={setRefresh}
                    />
                  </div>
                );
              })
            ) : (
              <>目前沒有資料。</>
            )
          ) : (
            <>
              <SpinnerPlaceholder layout="list" />
              <SpinnerPlaceholder layout="list" />
              <SpinnerPlaceholder layout="list" />
              <SpinnerPlaceholder layout="list" />
              <SpinnerPlaceholder layout="list" />
            </>
          )}
        </section>
        {!isFetchingPages && !hasNoRecords && (
          <PaginationLayout
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            paginationData={paginationData}
            paginationList={paginationList}
          />
        )}
      </article>
    </>
  );
};

export default ContentTemplate;
