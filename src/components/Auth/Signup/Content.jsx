import React from 'react';
import { Navigate } from 'react-router-dom';

import Step1 from 'components/Auth/Signup/Step1';
import Step2 from 'components/Auth/Signup/Step2';

import { useAuthContext } from 'context/AuthContext';
import { useSignupContext } from 'context/SignupContext';

const Signup = () => {
  const { auth } = useAuthContext();
  const { step } = useSignupContext();

  const stepList = [
    {
      id: 1,
      title: '一',
      subtitle: '閱讀條款',
      content: <Step1 />,
    },
    {
      id: 2,
      title: '二',
      subtitle: '填寫資料',
      content: <Step2 />,
    },
  ];

  return auth ? (
    <Navigate to="/auth/member" replace />
  ) : (
    <>
      <div className="l-auth">
        <h1 className="c-title">註冊</h1>
        {stepList.map((v) => {
          const { id, title, subtitle, content } = v;
          return (
            id === step && (
              <div key={id} className="l-auth__section">
                <h2 className="c-subtitle">
                  步驟{title}｜{subtitle}
                </h2>
                {content}
              </div>
            )
          );
        })}
      </div>
    </>
  );
};

export default Signup;
