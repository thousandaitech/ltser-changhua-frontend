import React, { useState, useEffect, useRef } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';

import axios from 'axios';
import { Form, Spinner, OverlayTrigger, Tooltip } from 'react-bootstrap';

import FormLayouts from 'components/FormLayouts';
import ReCaptchaLayout from 'components/ReCaptchaLayout';

import { useFormContext } from 'context/FormContext';
import { useSignupContext } from 'context/SignupContext';

import { RECAPTCHA_HINT, SWAL_TOAST, taiwanCityCountyList } from 'data/common';
import { AUTH_URL } from 'utils/config';
import Swal from 'sweetalert2';

const Step2 = () => {
  const fieldList = [
    {
      id: 'username',
      title: '電子郵件地址(即登入帳號)',
      type: 'email',
      readOnly: false,
      required: true,
      hints: [
        {
          id: 'text',
          content: '信箱務必填寫正確，以便後續會員通知。',
        },
      ],
    },
    {
      id: 'password',
      title: '密碼',
      type: 'password',
      readOnly: false,
      required: true,
      hints: [
        {
          id: 'text',
          content: '密碼長度至少8位，且需包含英文及數字。',
        },
      ],
    },
    {
      id: 'confirmPassword',
      title: '確認密碼',
      type: 'password',
      readOnly: false,
      required: true,
    },
    {
      id: 'lastName',
      title: '姓',
      type: 'text',
      readOnly: false,
      required: true,
    },
    {
      id: 'firstName',
      title: '名',
      type: 'text',
      readOnly: false,
      required: true,
    },
    {
      id: 'institution',
      title: '學校/單位',
      type: 'text',
      readOnly: false,
      required: true,
    },
    {
      id: 'institutionLocation',
      title: '單位地點',
      type: 'select',
      options: taiwanCityCountyList,
      required: true,
    },
    {
      id: 'department',
      title: '系所/部門',
      type: 'text',
      readOnly: false,
      required: true,
    },
    {
      id: 'position',
      title: '職稱/身分',
      type: 'text',
      readOnly: false,
      required: true,
    },
    {
      id: 'positionCategory',
      title: '身分類別',
      type: 'select',
      options: ['政府機關', '學術單位', '公司行號', '個人使用'],
      required: true,
    },
    {
      id: 'application',
      title: '資料用途',
      type: 'select',
      options: ['學術研究', '資訊取得', '數據分析', '環境教育'],
      required: false,
    },
    {
      id: 'preferenceCategory',
      title: '關注類別',
      type: 'checkbox',
      options: [
        '生態',
        '水質',
        '氣象',
        '空汙',
        '人口',
        '產業',
        '訪談資料',
        '問卷調查',
        '海空步道人流',
        '其他',
      ],
    },
    {
      id: 'securityQuestion',
      title: '你最喜歡的城市?',
      type: 'text',
      readOnly: false,
      required: true,
      hints: [
        {
          id: 'text',
          content: '此為安全問題，若忘記密碼，將會以此題答案作為驗證。',
        },
      ],
    },
  ];
  const [form, setForm] = useState(null);
  const [loading, setLoading] = useState(false);
  const [recaptcha, setRecaptcha] = useState(false);

  const { handleInitialState } = useFormContext();
  const { setStep } = useSignupContext();

  const navigate = useNavigate();

  const handlePrevClick = () => setStep(1);

  const handleSignup = async (e) => {
    e.preventDefault();
    Swal.fire({
      title: '確認註冊？',
      text: '請再次確認信箱以及安全問題是否填寫正確，註冊後不得再修改。',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: '確認',
      cancelButtonText: '取消',
      reverseButtons: true,
      confirmButtonColor: '#44a0c9',
    }).then(async (result) => {
      if (result.isConfirmed) {
        const {
          username,
          password,
          confirmPassword,
          lastName,
          firstName,
          institution,
          institutionLocation,
          department,
          position,
          positionCategory,
          application,
          preferenceCategory,
          securityQuestion,
        } = form;
        if (password === confirmPassword) {
          setLoading(true);
          try {
            const formData = {
              user: {
                username: username,
                password: password,
                password2: confirmPassword,
                email: username,
                first_name: firstName,
                last_name: lastName,
              },
              school: institution,
              location: institutionLocation,
              department: department,
              title: position,
              category: positionCategory,
              application: application,
              attention: preferenceCategory.join('/'),
              securityQuestion,
            };
            const response = await axios({
              method: 'post',
              url: `${AUTH_URL}/signUp/`,
              data: formData,
            });
            const { status, data } = response;
            if (status === 201) {
              setLoading(false);
              // setStep(3);
              // localStorage.setItem('mailVerification', username);
              // setTimeout(() => {
              //   navigate('/mail-verification');
              // }, 0);
              let timerInterval;
              SWAL_TOAST.fire({
                icon: 'warning',
                title: `註冊成功，即將於 <b></b> 秒後重新導向登入頁。`,
                timerProgressBar: true,
                timer: 5000,
                didOpen: () => {
                  // localStorage.removeItem('mailVerification');
                  const b = Swal.getTitle().querySelector('b');
                  timerInterval = setInterval(() => {
                    b.textContent = Math.floor(Swal.getTimerLeft() / 1000);
                  }, 100);
                },
                willClose: () => {
                  clearInterval(timerInterval);
                  navigate('/login');
                },
              });
            }
          } catch (err) {
            setLoading(false);
            if (err.response) {
              const { data, status } = err.response;
              switch (status) {
                case 400:
                  if (data.user.password) {
                    SWAL_TOAST.fire({
                      icon: 'error',
                      title: `密碼格式不正確或確認密碼不一致。`,
                    });
                  } else if (data.user.username) {
                    SWAL_TOAST.fire({
                      icon: 'error',
                      title: `帳號已存在或尚未驗證。`,
                    });
                  }
                  break;
                default:
                  SWAL_TOAST.fire({
                    icon: 'error',
                    title: `發生錯誤。`,
                  });
              }
            }
          }
        } else {
          SWAL_TOAST.fire({
            icon: 'error',
            title: `確認密碼不一致。`,
          });
        }
      }
    });
  };

  useEffect(() => {
    setForm({ ...handleInitialState(fieldList) });
  }, []);

  return (
    <>
      <Form className="c-form" onSubmit={handleSignup}>
        <span className="c-form__error">*為必填欄位</span>
        {form && (
          <FormLayouts form={form} setForm={setForm} fieldList={fieldList} />
        )}
        {/* <ReCaptchaLayout setValidated={setRecaptcha} /> */}
        <div className="c-form__actions">
          <button
            type="button"
            className="c-form__action muted e-btn"
            onClick={handlePrevClick}
            disabled={loading}
          >
            上一步
          </button>
          <div>
            <button
              type="submit"
              className="c-form__action action e-btn e-btn--processing"
              data-count="2"
              disabled={loading}
            >
              {loading ? (
                <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
              ) : (
                '註冊'
              )}
            </button>
          </div>
          {/* <OverlayTrigger
            placement="top"
            overlay={!recaptcha ? <Tooltip>{RECAPTCHA_HINT}</Tooltip> : <></>}
          >
            <div>
              <button
                type="submit"
                className="c-form__action action e-btn e-btn--processing"
                data-count="2"
                disabled={loading || !recaptcha}
              >
                {loading ? (
                  <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />
                ) : (
                  '註冊'
                )}
              </button>
            </div>
          </OverlayTrigger> */}
        </div>
      </Form>
    </>
  );
};

export default Step2;
