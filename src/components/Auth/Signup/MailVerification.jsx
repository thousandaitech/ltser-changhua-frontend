import React, { useState, useEffect } from 'react';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

import { Form, Spinner } from 'react-bootstrap';

import FormLayouts from 'components/FormLayouts';

import { useFormContext } from 'context/FormContext';
import { useSignupContext } from 'context/SignupContext';

import { AUTH_URL } from 'utils/config';
import { SWAL_TOAST } from 'data/common';

const MailVerificaiton = () => {
  const { token } = useSignupContext();
  const fieldList = [
    {
      id: 'username',
      title: '電子郵件地址(即登入帳號)',
      type: 'email',
      readOnly: false,
      required: true,
    },
  ];
  const [form, setForm] = useState(null);
  const { handleInitialState } = useFormContext();
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const handleFormSubmit = async (e) => {
    setLoading(true);
    e.preventDefault();
    try {
      const response = await axios({
        method: 'post',
        url: `${AUTH_URL}/resend-email-verify/`,
        data: {
          email: form.username,
        },
      });
      const { status, data } = response;
      setLoading(false);
      if (data.message === '使用者已被激活') {
        let timerInterval;
        SWAL_TOAST.fire({
          icon: 'warning',
          title: `${data.message}，即將於 <b></b> 秒後重新導向登入頁。`,
          timerProgressBar: true,
          timer: 5000,
          didOpen: () => {
            localStorage.removeItem('mailVerification');
            const b = Swal.getTitle().querySelector('b');
            timerInterval = setInterval(() => {
              b.textContent = Math.floor(Swal.getTimerLeft() / 1000);
            }, 100);
          },
          willClose: () => {
            clearInterval(timerInterval);
            navigate('/login');
          },
        });
      } else {
        SWAL_TOAST.fire({
          icon: 'warning',
          title: data.message,
        });
      }
    } catch (err) {
      setLoading(false);
      if (err.response) {
        console.log(err.response);
        if (err.response.data.non_field_errors) {
          SWAL_TOAST.fire({
            icon: 'error',
            title: '提供的電子郵件尚未被註冊過，請前往註冊。',
          });
        } else {
          SWAL_TOAST.fire({
            icon: 'error',
            title: `發生錯誤。`,
          });
        }
      }
    }
  };

  const handleMailVerify = async () => {
    try {
      const response = await axios.get(
        `${AUTH_URL}/email-verify/?token=${token}`
      );
      const { status, data } = response;
      if (status === 200) {
        let timerInterval;
        SWAL_TOAST.fire({
          icon: 'success',
          title: `驗證成功，即將於 <b></b> 秒後重新導向登入頁。`,
          timerProgressBar: true,
          timer: 5000,
          didOpen: () => {
            localStorage.removeItem('mailVerification');
            const b = Swal.getTitle().querySelector('b');
            timerInterval = setInterval(() => {
              b.textContent = Math.floor(Swal.getTimerLeft() / 1000);
            }, 100);
          },
          willClose: () => {
            clearInterval(timerInterval);
            navigate('/login');
          },
        });
      } else {
        SWAL_TOAST.fire({
          icon: 'warning',
          title: '驗證失敗，請點擊發送驗證信，重新驗證。',
        });
      }
    } catch (err) {
      SWAL_TOAST.fire({
        icon: 'error',
        title: '驗證失敗，請點擊發送驗證信，重新驗證。',
      });
    }
  };

  useEffect(() => {
    setForm({
      ...handleInitialState(fieldList),
      username: localStorage.getItem('mailVerification'),
    });
  }, []);

  useEffect(() => {
    if (token) {
      handleMailVerify();
    }
  }, [token]);

  return (
    <>
      <article className="l-auth">
        <section className="l-auth__section">
          <h1 className="c-title">註冊</h1>
          <h2 className="c-subtitle">步驟三｜收驗證信</h2>
          <p className="l-auth__text">
            驗證信已發送至註冊時使用的電子信箱，請至信箱點擊信件內的連結以啟用帳號。
          </p>
          <div className="l-auth__actions">
            <Link
              to="/login"
              className={`e-btn e-btn--primary e-btn--wmax mx-auto ${
                loading ? 'e-btn--disabled' : ''
              }`}
            >
              已驗證，前往登入
            </Link>
          </div>
        </section>
        <section className="l-auth__section">
          <hr className="e-hr" />
          <h2 className="c-subtitle">沒有收到驗證信?</h2>
          <p className="l-auth__text">填寫下方欄位以重新發送。</p>
          <Form className="c-form" onSubmit={handleFormSubmit}>
            <span className="c-form__error">*為必填欄位</span>
            {form && (
              <FormLayouts
                form={form}
                setForm={setForm}
                fieldList={fieldList}
              />
            )}
            <div className="c-form__actions">
              <button
                type="submit"
                className="c-form__action action e-btn e-btn--processing"
                data-count="4"
                disabled={loading}
              >
                {loading ? (
                  <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />
                ) : (
                  '重新發送'
                )}
              </button>
            </div>
          </Form>
        </section>
      </article>
    </>
  );
};

export default MailVerificaiton;
