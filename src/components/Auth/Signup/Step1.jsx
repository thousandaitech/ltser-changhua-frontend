import React, { useState, useEffect } from 'react';
import { Navigate } from 'react-router-dom';

import { Form } from 'react-bootstrap';

import { useSignupContext } from 'context/SignupContext';

import { SWAL_TOAST } from 'data/common';

const Step1 = () => {
  const [term, setTerm] = useState(false);
  const { setStep } = useSignupContext();

  const handleCheckboxChange = () => setTerm(!term);

  const handleNextClick = () => {
    if (term) {
      setStep(2);
    } else {
      SWAL_TOAST.fire({
        icon: 'error',
        title: `請先同意條款。`,
      });
    }
  };

  return (
    <>
      <div className="d-flex flex-column align-items-center">
        <article className="l-auth__article c-article">
          <h5 className="c-article__subtitle">
            國立彰化師範大學長期社會生態核心觀測彰化站(LTSER Changhua)
            會員約定條款
          </h5>
          <p className="c-article__paragraph">
            長期社會生態核心觀測彰化站為科技部計畫「臺灣西部海岸濕地農漁社會生態系統監測」規劃設立，其目的為建立一個以芳苑地區為基礎之資料庫，以資料共享之方式提升資料收集及科學研究的效率與便利性，供一般大眾與學界人士參考、使用。為便於資料庫管理，本資料庫採會員制，凡加入本資料庫會員者，方可使用本資料庫提供之各項會員服務。
          </p>
          <ol className="c-article__ol">
            <li className="c-article__oli">
              為管理資料庫與部份需考量研究倫理之資料，本資料庫之資料瀏覽與下載需於資料存放平台申請會員帳號，申請時需註明姓名、所屬單位、身分類別、資料用途(如學術研究)等項目，並於審核後開放資料之瀏覽與使用，且部分資料需額外提交申請並於審核後方可使用。
              <ol className="c-article__ol c-article__ol--sub">
                <li className="c-article__oli">
                  一般資料：生物、水質、土壤等生態調查資料屬一般性公開資料，申請帳號即可下載。
                </li>
                <li className="c-article__oli">
                  申請資料：部分人文社會之資料因涉及研究倫理，需申請並審核通過後方可使用。
                </li>
              </ol>
            </li>
            <li className="c-article__oli">
              會員同意提供完整且真實之個人資料，如有服務單位或職稱異動等情形時，應即時更新或告知本資料庫更新之。
            </li>
            <li className="c-article__oli">
              會員有義務妥善保管個人之帳號與密碼，並為此組帳號與密碼登入系統後所進行之一切活動負責。為維護自身權益，請勿將帳號與密碼洩露或提供予第三人知悉，或出借或轉讓他人使用。若同一時間內有第二人以同一帳號及密碼登入使用，將拒絕登入。如發現帳號或密碼遭人非法使用或有任何異常破壞使用安全之情形時，應立即通知本資料庫。
            </li>
            <li className="c-article__oli">
              會員不得擅自提供、拷貝、影印從本資料庫取得之資料給其他個人或機構。
            </li>
            <li className="c-article__oli">
              會員同意不使用本服務之任何功能或下載資料從事下列行為：
              <ol className="c-article__ol c-article__ol--sub">
                <li className="c-article__oli">
                  張貼任何內容具詐欺、毀謗、侮辱他人或違反法律之文字、圖片或任何形式之檔案。
                </li>
                <li className="c-article__oli">
                  散布任何具有侵害、破壞、中止、干擾或毀損電腦、系統、程式、軟體或類似物件之病毒軟體、程式、檔案等類似載體。
                </li>
                <li className="c-article__oli">
                  侵犯他人著作權、商標權、專利權、營業祕密或其他具智慧財產權之圖片、文字、圖形、言論、軟體、檔案或程式。
                </li>
                <li className="c-article__oli">從事任何廣告或商業交易。</li>
                <li className="c-article__oli">
                  違法或未經授權擅自進入其他會員之帳號或本資料庫未開放使用之空間。
                </li>
              </ol>
            </li>
            <li className="c-article__oli">
              會員風險資訊
              <ol className="c-article__ol c-article__ol--sub">
                <li className="c-article__oli">
                  會員同意使用本資料庫各項服務，係基於個人之意願及決定，並同意自負任何使用風險，包括因為自本資料庫下載資料導致會員的電腦系統損壞，或是發生資料流失、錯誤等問題。
                </li>
                <li className="c-article__oli">
                  會員應瞭解並同意，因軟硬體設備、相關電信業者網路系統之故障，或遭第三人侵入系統篡改或偽造變造資料等，可能造成本資料庫網站系統全部或部份中斷、暫時無法使用、遲延，或造成資料傳輸或儲存上之錯誤，會員不得因此而要求任何補償或賠償。
                </li>
                <li className="c-article__oli">
                  本資料庫在網站上提供之網路連結，可能連結到其他個人、公司或組織之網站。提供該等連結之目的，僅係為便利會員自行搜集或取得資訊。對於所連結之該等個人、公司或組織之網站上所提供之產品、服務或資訊，本資料庫無法擔保其真實性、完整性、即時性或可信度；該等個人、公司或組織，亦不因此而與本資料庫有任何僱佣、委任、代理、合夥或其他類似之關係。
                </li>
                <li className="c-article__oli">
                  本資料庫得隨時停止或變更各項服務內容，且無需事先知會個別會員。
                </li>
              </ol>
            </li>
            <li className="c-article__oli">
              基於互惠原則與擴大資料庫資料之基礎，資料使用者需在研究結束後繳交衍生成果，並授權本站依以一般資料或申請資料之方式共享。
            </li>
            <li className="c-article__oli">
              資料使用者因運用該調查資料撰成之一切論著（如會議論文、期刊論文、博碩士論文、專書或其他等），在出版或發表後，須告知本資料庫該論著之書目，以利其它資料使用者參考。
            </li>
            <li className="c-article__oli">
              如違反本約定條款之規範者，本資料庫得視情節停止或終止帳號使用權益，若侵害第三人之權利或財產，或因而致第三人受有損害者，應自負損害賠償與法律責任。若該第三人對本資料庫提出告訴，會員同意負責應訟，以及單獨負擔一切律師及行政費用，包括本資料庫之費用，以確保本資料庫不致因而受到任何損害；若本資料庫因而被判賠償者，會員亦全額負責。
            </li>
            <li className="c-article__oli">
              會員因使用本資料庫服務而與本資料庫間所產生之權利義務關係，應以中華民國法律為準據法，所涉及之一切訴訟，均以臺灣彰化地方法院為第一審管轄法院。
            </li>
            <li className="c-article__oli">
              本資料庫保留隨時修改本條款之權利。本資料庫於條款修改後，於本資料庫網頁公告修改內容，不另作個別通知。如不同意修改之內容，可要求本資料庫中止帳號。如會員未主動告知中止會員資格，則視為會員已同意並接受本資料庫修訂條款內容。
            </li>
          </ol>
        </article>
        <div className="c-form__set c-checkbox c-checkbox--check mt-4">
          <Form.Group className="c-checkbox__set mb-3" controlId="term">
            <Form.Control
              type="checkbox"
              className="c-checkbox__input"
              name="term"
              value={term}
              checked={term}
              onChange={handleCheckboxChange}
            />
            <Form.Label className="c-checkbox__label text-wrap">
              我已閱讀上述說明，並將遵守上述規則使用資料庫平臺資料
            </Form.Label>
          </Form.Group>
        </div>
        <button
          type="button"
          className="c-form__action e-btn action"
          onClick={handleNextClick}
        >
          下一步
        </button>
      </div>
    </>
  );
};

export default Step1;
