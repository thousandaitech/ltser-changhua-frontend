import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';

import { Form, Spinner, OverlayTrigger, Tooltip } from 'react-bootstrap';

import FormLayouts from 'components/FormLayouts';
import ReCaptchaLayout from 'components/ReCaptchaLayout';

import { useAuthContext } from 'context/AuthContext';
import { useFormContext } from 'context/FormContext';
import { AUTH_URL } from 'utils/config';
import { SWAL_TOAST, RECAPTCHA_HINT } from 'data/common';

const ChangePassword = () => {
  const fieldList = [
    {
      id: 'oldPassword',
      title: '舊密碼',
      type: 'password',
      readOnly: false,
      required: true,
    },
    {
      id: 'newPassword',
      title: '新密碼',
      type: 'password',
      readOnly: false,
      required: true,
    },
    {
      id: 'confirmNewPassword',
      title: '確認新密碼',
      type: 'password',
      readOnly: false,
      required: true,
    },
  ];
  const [form, setForm] = useState(null);

  const [loading, setLoading] = useState(false);
  const [recaptcha, setRecaptcha] = useState(false);

  const { handleInitialState } = useFormContext();
  const { authTokens } = useAuthContext();

  const navigate = useNavigate();

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    const { oldPassword, newPassword, confirmNewPassword } = form;
    if (newPassword === confirmNewPassword) {
      setLoading(true);
      try {
        const formData = {
          oldPassword: oldPassword,
          newPassword: newPassword,
          newPassword2: confirmNewPassword,
        };
        const response = await axios({
          method: 'patch',
          url: `${AUTH_URL}/updateUserPassword/`,
          headers: {
            Authorization: `Bearer ${authTokens.access}`,
          },
          data: formData,
        });
        const { status, data } = response;
        if (status === 200) {
          setLoading(false);
          SWAL_TOAST.fire({
            icon: 'success',
            title: '已重設密碼。',
          });
          navigate('/auth/member');
        }
      } catch (err) {
        setLoading(false);
        if (err.response) {
          const { data, status } = err.response;
          SWAL_TOAST.fire({
            icon: 'error',
            title: data.message,
          });
        }
      }
    } else {
      SWAL_TOAST.fire({
        icon: 'error',
        title: `確認密碼不一致。`,
      });
    }
  };

  useEffect(() => {
    setForm({ ...handleInitialState(fieldList) });
  }, []);
  return (
    <>
      <div className="l-auth">
        <h1 className="c-title">修改密碼</h1>
        <Form className="c-form" onSubmit={handleFormSubmit}>
          <span className="c-form__error">*為必填欄位</span>
          {form && (
            <FormLayouts form={form} setForm={setForm} fieldList={fieldList} />
          )}
          {/* <ReCaptchaLayout setValidated={setRecaptcha} /> */}
          <div className="c-form__actions">
            <Link
              to="/auth/member"
              className={`c-form__action muted e-btn ${
                loading ? 'e-btn--disabled' : ''
              }`}
            >
              返回
            </Link>
            <div>
              <button
                type="submit"
                className="c-form__action action e-btn e-btn--processing"
                data-count="2"
                disabled={loading}
              >
                {loading ? (
                  <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />
                ) : (
                  '修改'
                )}
              </button>
            </div>
            {/* <OverlayTrigger
              placement="top"
              overlay={!recaptcha ? <Tooltip>{RECAPTCHA_HINT}</Tooltip> : <></>}
            >
              <div>
                <button
                  type="submit"
                  className="c-form__action action e-btn e-btn--processing"
                  data-count="2"
                  disabled={loading || !recaptcha}
                >
                  {loading ? (
                    <Spinner
                      as="span"
                      animation="border"
                      size="sm"
                      role="status"
                      aria-hidden="true"
                    />
                  ) : (
                    '修改'
                  )}
                </button>
              </div>
            </OverlayTrigger> */}
          </div>
        </Form>
      </div>
    </>
  );
};

export default ChangePassword;
