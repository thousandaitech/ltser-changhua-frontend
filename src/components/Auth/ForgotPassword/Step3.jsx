import React, { useEffect, useState } from 'react';

const Step3 = (props) => {
  const { resetUrl } = props;
  const newUrl = resetUrl.replace('forgot-password', 'reset-password');
  return (
    <>
      <section className="l-auth__section">
        <h2 className="c-subtitle">步驟三｜點擊重設密碼按鈕</h2>
        <p className="l-auth__text">點擊下面按鈕以開始重設密碼。</p>
        <div className="d-flex justify-content-center">
          <a className="e-btn e-btn--wmax e-btn--primary" href={newUrl}>
            重設密碼
          </a>
        </div>
      </section>
    </>
  );
};

export default Step3;
