import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';

import Step1 from 'components/Auth/ForgotPassword/Step1';
import Step2 from 'components/Auth/ForgotPassword/Step2';

import Step3 from './Step3';

const Content = () => {
  const [step, setStep] = useState(1);
  const [resetUrl, setResetUrl] = useState('');

  const [form, setForm] = useState(null);

  const stepContentData = {
    1: <Step1 step={step} setStep={setStep} form={form} setForm={setForm} />,
    2: (
      <Step2
        step={step}
        setStep={setStep}
        form={form}
        setForm={setForm}
        setResetUrl={setResetUrl}
      />
    ),
    3: <Step3 resetUrl={resetUrl} />,
  };

  return (
    <>
      <article className="l-auth">
        <h1 className="c-title">忘記密碼</h1>
        {stepContentData[step]}
      </article>
    </>
  );
};

export default Content;
