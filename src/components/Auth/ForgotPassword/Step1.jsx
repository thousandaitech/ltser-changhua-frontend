import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Form, Spinner } from 'react-bootstrap';

import FormLayouts from 'components/FormLayouts';

import { useFormContext } from 'context/FormContext';

import { useApi } from 'hooks/useApi';

const Step1 = (props) => {
  const { setStep, form, setForm } = props;
  const fieldList = [
    {
      id: 'username',
      title: '電子郵件地址(即登入帳號)',
      type: 'email',
      readOnly: false,
      required: true,
    },
  ];
  const { handleInitialState } = useFormContext();
  const [result, loading, handleApi, handleActions] = useApi();

  const handleFormSubmit = (e) => {
    e.preventDefault();
    handleApi({
      type: 'auth',
      method: 'post',
      data: {
        email: form.username,
      },
      url: 'password-reset-validate-email/',
    });
  };

  useEffect(() => {
    if (result) {
      handleActions({
        result: result,
        success: {},
        error: {
          title: result.data.message,
        },
      });
      if (result.status === 200 && result.data.status === 'success') {
        setStep(2);
      }
    }
  }, [result]);

  useEffect(() => {
    setForm({ ...handleInitialState(fieldList) });
  }, []);

  return (
    <>
      <section className="l-auth__section">
        <h2 className="c-subtitle">步驟一｜發送密碼重設信</h2>
        <p className="l-auth__text">
          填寫電子郵件地址(即登入帳號)，以發送密碼重設信。
        </p>
        <Form className="c-form" onSubmit={handleFormSubmit}>
          <span className="c-form__error">*為必填欄位</span>
          {form && (
            <FormLayouts form={form} setForm={setForm} fieldList={fieldList} />
          )}
          <div className="c-form__actions">
            <button
              type="submit"
              className="c-form__action action e-btn e-btn--processing"
              data-count="2"
              disabled={loading}
            >
              {loading ? (
                <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
              ) : (
                '送出'
              )}
            </button>
          </div>
        </Form>
      </section>
    </>
  );
};

export default Step1;
