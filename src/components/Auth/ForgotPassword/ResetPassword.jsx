import React, { useState, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import { Form, Spinner } from 'react-bootstrap';

import FormLayouts from 'components/FormLayouts';

import { useFormContext } from 'context/FormContext';

import { useApi } from 'hooks/useApi';

const ResetPassword = () => {
  const fieldList = [
    {
      id: 'newPassword',
      title: '新密碼',
      type: 'password',
      readOnly: false,
      required: true,
    },
    {
      id: 'confirmNewPassword',
      title: '確認新密碼',
      type: 'password',
      readOnly: false,
      required: true,
    },
  ];
  const [form, setForm] = useState(null);
  const { handleInitialState } = useFormContext();
  const [result, loading, handleApi, handleActions] = useApi();
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const token = searchParams.get('token');
  const uidb64 = searchParams.get('uidb64');
  const navigate = useNavigate();

  const handleFormSubmit = (e) => {
    e.preventDefault();
    handleApi({
      type: 'auth',
      method: 'patch',
      data: {
        password: form.newPassword,
        uidb64: uidb64,
        token: token,
      },
      url: 'password-reset-complete/',
    });
  };

  useEffect(() => {
    if (result) {
      handleActions({
        result: result,
        success: {
          title: result.data.message,
          action: () => {
            navigate('/login');
          },
        },
        error: {
          title: result.data.detail,
        },
      });
    }
  }, [result]);

  useEffect(() => {
    setForm({ ...handleInitialState(fieldList) });
  }, []);

  return (
    <>
      <article className="l-auth">
        <h1 className="c-title">忘記密碼</h1>
        <section className="l-auth__section">
          <h2 className="c-subtitle">步驟四｜重設密碼</h2>
          <p className="l-auth__text">填寫下方欄位以重新設定您的密碼。</p>
          <Form className="c-form" onSubmit={handleFormSubmit}>
            <span className="c-form__error">*為必填欄位</span>
            {form && (
              <FormLayouts
                form={form}
                setForm={setForm}
                fieldList={fieldList}
              />
            )}
            <div className="c-form__actions">
              <button
                type="submit"
                className="c-form__action action e-btn e-btn--processing"
                data-count="2"
                disabled={loading}
              >
                {loading ? (
                  <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />
                ) : (
                  '送出'
                )}
              </button>
            </div>
          </Form>
        </section>
      </article>
    </>
  );
};

export default ResetPassword;
