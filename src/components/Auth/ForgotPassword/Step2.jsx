import FormLayouts from 'components/FormLayouts';
import { useFormContext } from 'context/FormContext';
import { useApi } from 'hooks/useApi';
import React, { useEffect, useState } from 'react';
import { Form, Spinner } from 'react-bootstrap';

const Step2 = (props) => {
  const { setStep, form, setForm, setResetUrl } = props;
  const fieldList = [
    {
      id: 'securityQuestion',
      title: '你最喜歡的城市?',
      type: 'text',
      readOnly: false,
      required: true,
      hints: [
        {
          id: 'text',
          content: '此為安全問題，若忘記密碼，將會以此題答案作為驗證。',
        },
      ],
    },
  ];

  const { handleInitialState } = useFormContext();
  const [result, loading, handleApi, handleActions] = useApi();

  const handleFormSubmit = (e) => {
    e.preventDefault();
    handleApi({
      type: 'auth',
      method: 'post',
      data: {
        email: form.username,
        securityQuestion: form.securityQuestion,
      },
      url: 'password-reset-verify-security-question/',
    });
  };

  useEffect(() => {
    if (result) {
      handleActions({
        result: result,
        success: { title: result.data.message },
        error: {
          title: result.data.message,
        },
      });
      if (result.status === 200 && result.data.status === 'success') {
        setStep(3);
        setResetUrl(result.data.url);
      }
    }
  }, [result]);

  useEffect(() => {
    setForm({ ...form, ...handleInitialState(fieldList) });
  }, []);

  return (
    <>
      <section className="l-auth__section">
        <h2 className="c-subtitle">步驟二｜輸入安全問題的答案</h2>
        <p className="l-auth__text">
          註冊時曾回答過安全問題，若忘記密碼，將會以此題答案作為驗證。
        </p>
        <span className="c-form__error">*為必填欄位</span>
        <Form className="c-form" onSubmit={handleFormSubmit}>
          {form && (
            <FormLayouts form={form} setForm={setForm} fieldList={fieldList} />
          )}
          <div className="c-form__actions">
            <button
              type="submit"
              className="c-form__action action e-btn e-btn--processing"
              data-count="2"
              disabled={loading}
            >
              {loading ? (
                <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
              ) : (
                '送出'
              )}
            </button>
          </div>
        </Form>
      </section>
    </>
  );
};

export default Step2;
