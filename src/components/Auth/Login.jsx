import React, { useState, useEffect, useRef } from 'react';
import { Link, Navigate } from 'react-router-dom';

import { Form, Spinner, OverlayTrigger, Tooltip } from 'react-bootstrap';

import FormLayouts from 'components/FormLayouts';
import ReCaptchaLayout from 'components/ReCaptchaLayout';

import contactImg from 'img/home/key-visual/1.jpg';

import { useAuthContext } from 'context/AuthContext';
import { useFormContext } from 'context/FormContext';

import { RECAPTCHA_HINT } from 'data/common';

const Login = () => {
  const fieldList = [
    {
      id: 'username',
      title: '帳號',
      type: 'email',
      readOnly: false,
      required: true,
      hints: [
        {
          id: 'text',
          content: '電子郵件地址即登入帳號。',
        },
        // {
        //   id: 'mail-verification',
        //   link: '/mail-verification',
        // },
      ],
    },
    {
      id: 'password',
      title: '密碼',
      type: 'password',
      readOnly: false,
      required: true,
      hints: [
        {
          id: 'forgot',
          link: '/login/forgot-password',
        },
      ],
    },
  ];
  const [form, setForm] = useState(null);
  const [recaptcha, setRecaptcha] = useState(false);

  const { handleInitialState } = useFormContext();
  const { auth, handleLogin, loading, verificationHint } = useAuthContext();

  useEffect(() => {
    setForm({ ...handleInitialState(fieldList) });
  }, []);

  return auth ? (
    <Navigate to="/auth/member" replace />
  ) : (
    <>
      <div className="l-auth">
        <div className="row">
          <div className="col-12 col-lg-6">
            <h1 className="c-title">登入</h1>
            <p className="l-auth__text">
              長期社會生態核心觀測彰化站為國科會計畫「臺灣西部海岸濕地農漁社會生態系統監測」規劃設立。其目的為建立一個以彰化芳苑地區為基礎之資料庫，以資料共享之方式提升資料收集及科學研究的效率與便利性，供一般大眾與學界人士參考、使用。為便於資料庫管理，本資料庫採會員制，本資料庫之資料瀏覽與下載需於網站平台登入會員後使用，如未有會員帳號，請先註冊會員並於審核後開放資料之瀏覽與使用。
            </p>
            <Form className="c-form" onSubmit={handleLogin}>
              {form && (
                <FormLayouts
                  form={form}
                  setForm={setForm}
                  fieldList={fieldList}
                />
              )}
              {/* <ReCaptchaLayout setValidated={setRecaptcha} /> */}
              <div className="c-form__actions">
                <Link
                  to="/signup"
                  className={`c-form__action muted e-btn ${
                    loading || verificationHint ? 'e-btn--disabled' : ''
                  }`}
                >
                  註冊
                </Link>
                <div>
                  <button
                    type="submit"
                    className="c-form__action action e-btn e-btn--processing"
                    data-count="2"
                    disabled={loading}
                  >
                    {loading ? (
                      <Spinner
                        as="span"
                        animation="border"
                        size="sm"
                        role="status"
                        aria-hidden="true"
                      />
                    ) : (
                      '登入'
                    )}
                  </button>
                </div>
                {/* <OverlayTrigger
                  placement="top"
                  overlay={
                    !recaptcha ? <Tooltip>{RECAPTCHA_HINT}</Tooltip> : <></>
                  }
                >
                  <div>
                    <button
                      type="submit"
                      className="c-form__action action e-btn e-btn--processing"
                      data-count="2"
                      disabled={loading || !recaptcha}
                    >
                      {loading ? (
                        <Spinner
                          as="span"
                          animation="border"
                          size="sm"
                          role="status"
                          aria-hidden="true"
                        />
                      ) : (
                        '登入'
                      )}
                    </button>
                  </div>
                </OverlayTrigger> */}
              </div>
            </Form>
          </div>
          <div className="col-6 d-none d-lg-flex align-items-stretch">
            <div className="l-auth__img">
              <img
                src={contactImg}
                className="e-img e-img--cover"
                alt="contact"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
