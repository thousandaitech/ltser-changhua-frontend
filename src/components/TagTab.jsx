import React from 'react';

function TagTab(props) {
  const { list, filter, setFilter, setCurrentPage } = props;
  const handleTagClick = (id) => {
    setFilter({ ...filter, tag: id });
    if (setCurrentPage) {
      setCurrentPage(1);
    }
  };
  return (
    <>
      <div className="c-tab c-tab--tag">
        <div className="c-tab__tabs">
          {list.map((item) => {
            const { id, title } = item;
            return (
              <button
                key={id}
                type="button"
                className={`c-tab__tab c-tab__tab--pill ${
                  filter.tag === id ? 'active' : ''
                }`}
                onClick={() => handleTagClick(id)}
              >
                {title}
              </button>
            );
          })}
        </div>
      </div>
    </>
  );
}

export default TagTab;
