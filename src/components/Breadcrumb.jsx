import React, { useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { Link, useLocation, useParams } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

import { useCustomContext } from 'utils/context';

function Breadcrumb() {
  const [fullLinks, setFullLinks] = useState(['/']);
  const [breadcrumb, setBreadcrumb] = useState([]);

  const { pathname, search } = useLocation();
  const {
    breadcrumbItem,
    breadcrumbLink,
    setBreadcrumbLink,
    breadcrumbParam,
    setBreadcrumbParam,
  } = useCustomContext();
  const isFetchingBreadcrumb = breadcrumbItem.length === 0;
  const isFetchingLinks = breadcrumbItem.length !== fullLinks.length;
  const isFetchingBreadcrumbLink = breadcrumbLink.length === 0;
  const isEmptyBreadcrumbParam = breadcrumbParam.length === 0;

  const homeLayout = () => {
    return (
      <FontAwesomeIcon
        icon={solid('house')}
        className="e-icon e-icon--primary c-breadcrumb__link"
      />
    );
  };

  useEffect(() => {
    if (!isFetchingBreadcrumb) {
      const fullLink = breadcrumbItem
        .map((item) => (item.link === '' ? '' : `/${item.link}`))
        .reduce((prev, curr) => `${prev}${curr}`, '');
      setFullLinks((prev) => [...prev, fullLink]);
    }
  }, [breadcrumbItem]);

  useEffect(() => {
    if (!isFetchingLinks) {
      breadcrumbItem.forEach((item, i) => {
        setBreadcrumbLink((prev) => [...prev, { ...item, link: fullLinks[i] }]);
      });
    }
  }, [fullLinks]);

  useEffect(() => {
    if (!isFetchingBreadcrumbLink) {
      if (isEmptyBreadcrumbParam) {
        setBreadcrumb([...breadcrumbLink]);
      } else {
        setBreadcrumb([...breadcrumbParam]);
      }
    }
  }, [breadcrumbLink, breadcrumbParam]);

  useEffect(() => {
    setFullLinks(['/']);
    setBreadcrumbLink([]);
    setBreadcrumbParam([]);
  }, [pathname]);

  return (
    <>
      <div className="c-breadcrumb">
        {breadcrumb.map((item, i) => {
          const { id, title, link } = item;
          return link === pathname && search === '' ? (
            <span
              key={id}
              className="c-breadcrumb__seg c-breadcrumb__link c-breadcrumb__link--disabled"
            >
              {title}
            </span>
          ) : (
            <Link
              key={id}
              to={link}
              className="c-breadcrumb__seg c-breadcrumb__link"
            >
              {link === '/' ? homeLayout() : title}
            </Link>
          );
        })}
      </div>
    </>
  );
}

export default Breadcrumb;
