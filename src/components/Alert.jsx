import React from 'react';

function Alert() {
  return (
    <>
      <div className="c-alert c-alert--hint">
        網站維護中，內容資料尚在建置中， 請勿作正式用途使用。
      </div>
    </>
  );
}

export default Alert;
