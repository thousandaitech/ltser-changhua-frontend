import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';

import { routeList } from 'data/common';
import { useCustomContext } from 'utils/context';

function Sidebar(props) {
  const { layout } = props;
  const { pathname, hash } = useLocation();
  const paths = pathname.split('/');
  const prevPaths = paths.slice(0, paths.length - 1).join('/');
  const { currentPageData } = useCustomContext();

  const isUndefinedPage = currentPageData.length === 0;

  return (
    <>
      <aside className="c-sidebar d-flex flex-lg-column">
        {!isUndefinedPage &&
          currentPageData.list.map((item) => {
            const { id, title, link, website, paragraph } = item;
            const isCurrentPage = () => {
              if (paragraph) {
                if (paths[paths.length - 1] === '') {
                  return (
                    paths[paths.length - 2] === link &&
                    hash.replace('#', '') === paragraph
                  );
                } else {
                  return (
                    paths[paths.length - 1] === link &&
                    hash.replace('#', '') === paragraph
                  );
                }
              } else {
                if (paths[paths.length - 1] === '') {
                  return paths[paths.length - 2] === link;
                } else {
                  return paths[paths.length - 1] === link;
                }
              }
            };
            if (isCurrentPage()) {
              if (paragraph) {
                return (
                  <div
                    key={id}
                    className="c-sidebar__tab c-sidebar__tab--sub active"
                  >
                    {title}
                  </div>
                );
              } else {
                return (
                  <div key={id} className="c-sidebar__tab active">
                    {title}
                  </div>
                );
              }
            } else {
              if (link) {
                if (paragraph) {
                  return (
                    <Link
                      key={id}
                      to={`#${paragraph}`}
                      className="c-sidebar__tab c-sidebar__tab--sub"
                    >
                      {title}
                    </Link>
                  );
                } else {
                  return (
                    <Link
                      key={id}
                      to={`${prevPaths}/${link}`}
                      className="c-sidebar__tab"
                    >
                      {title}
                    </Link>
                  );
                }
              } else if (website) {
                return (
                  <a key={id} href={website} className="c-sidebar__tab">
                    {title}
                  </a>
                );
              }
            }
          })}
        {layout}
      </aside>
    </>
  );
}

export default Sidebar;
