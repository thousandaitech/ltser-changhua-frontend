import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import Form from 'react-bootstrap/Form';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

const Search = (props) => {
  const { loading, filter, setFilter, search, setSearch, hasSearch } = props;

  const handleFieldChange = (e) => {
    setSearch(e.target.value);
  };

  const handleFieldClear = () => {
    setSearch('');
  };

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    setFilter({ ...filter, keyword: search });
  };

  return (
    <>
      <div className="c-search">
        <Form className="c-form w-100" onSubmit={handleSearchSubmit}>
          <Form.Group className="d-flex align-items-center" controlId="search">
            <div className="c-form__wrapper c-search__wrapper">
              <Form.Control
                type="text"
                placeholder="請輸入關鍵字"
                className="c-form__input c-search__input"
                value={search}
                onChange={handleFieldChange}
              />
              {hasSearch && (
                <button
                  type="button"
                  className="c-search__clear"
                  onClick={handleFieldClear}
                >
                  <FontAwesomeIcon
                    icon={solid('xmark')}
                    className="l-header__icon e-icon e-icon--secondary fa-fw"
                  />
                </button>
              )}
            </div>
            <button
              type="submit"
              className="e-btn e-btn--primary e-btn--wmax text-nowrap ms-3"
              disabled={loading || !hasSearch}
            >
              搜尋
            </button>
          </Form.Group>
        </Form>
      </div>
    </>
  );
};

export default Search;
