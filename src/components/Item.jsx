import React from 'react';
import axios from 'axios';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';

import useRender from 'hooks/useRender';

function Item(props) {
  const { url, data, tagList, setRefresh } = props;
  const {
    id,
    title,
    activities,
    news,
    literature,
    download,
    research,
    link,
    tags,
    views,
  } = data;
  const { patchViews } = useRender();

  const hasLink = link !== '無' || link !== '';

  const handleViews = () => {
    setRefresh(true);
    patchViews({ url, id, setRefresh });
  };

  const renderItem = () => {
    if (activities) {
      const { reference, time } = activities;
      return (
        <>
          <div className="d-flex align-items-center">
            <h4 className="c-list__text">
              <FontAwesomeIcon
                icon={solid('building')}
                className="e-icon e-icon--right"
              />
              {reference}
            </h4>
            <h4 className="c-list__text">
              <FontAwesomeIcon
                icon={solid('clock')}
                className="e-icon e-icon--right"
              />
              {time}
            </h4>
          </div>
        </>
      );
    } else if (news) {
      const { reference, date, reporter, photographer } = news;
      return (
        <>
          <div className="d-flex flex-column">
            <div className="d-flex align-items-center">
              <h4 className="c-list__text">
                <FontAwesomeIcon
                  icon={solid('book-open')}
                  className="e-icon e-icon--right"
                />
                {reference}
              </h4>
              <h4 className="c-list__text">
                <FontAwesomeIcon
                  icon={solid('calendar')}
                  className="e-icon e-icon--right"
                />
                {date}
              </h4>
            </div>
            <h4 className="c-list__text c-list__text--muted">
              記者 (採訪) {reporter}｜ 攝影 (剪輯) {photographer}
            </h4>
          </div>
        </>
      );
    } else if (literature) {
      const { subtitle, author, publisher, place, date, refId } = literature;
      const handleDateText = () => {
        if (String(date).includes('-')) {
          if (String(date).split('-').length === 1) {
            return '年';
          } else {
            return '日期';
          }
        } else {
          return '年';
        }
      };
      return (
        <>
          <div className="d-flex flex-column">
            <h4 className="c-list__text">{subtitle}</h4>
            <div className="d-flex align-items-center">
              {author && (
                <h4 className="c-list__text c-list__text--muted">
                  著｜{author}
                </h4>
              )}
              {publisher && (
                <h4 className="c-list__text c-list__text--muted">
                  出版者｜{publisher}
                </h4>
              )}
              {place && <h4 className="c-list__text">出版地｜{place}</h4>}
              <h4 className="c-list__text c-list__text--muted">
                出版{handleDateText()}｜{date}
              </h4>
            </div>
            {refId && (
              <h4 className="c-list__text c-list__text--muted">
                文獻編號｜{refId}
              </h4>
            )}
          </div>
        </>
      );
    } else if (download) {
      const { content, col, reference, time, counts, file } = download;
      return (
        <>
          <div className="d-flex justify-content-between">
            <div className="d-flex flex-column">
              <h4 className="c-list__text">{content}</h4>
              {col && (
                <h4 className="c-list__text">
                  <strong className="c-list__text--strong">
                    主要欄位說明:{' '}
                  </strong>
                  {col}
                </h4>
              )}
              <div className="d-flex align-items-center">
                <h4 className="c-list__text c-list__text--muted">
                  <FontAwesomeIcon
                    icon={solid('building')}
                    className="e-icon e-icon--muted e-icon--right"
                  />
                  {reference}
                </h4>
                <h4 className="c-list__text c-list__text--muted">
                  <FontAwesomeIcon
                    icon={solid('clock')}
                    className="e-icon e-icon--muted e-icon--right"
                  />
                  {time}
                </h4>
                <h4 className="c-list__text c-list__text--muted">
                  <FontAwesomeIcon
                    icon={solid('file-arrow-down')}
                    className="e-icon e-icon--muted e-icon--right"
                  />
                  {counts}
                </h4>
              </div>
            </div>
            <div className="d-flex align-items-start">
              {file.map((item, i) => {
                return (
                  <span key={i} className="c-list__tag c-tag">
                    {item}
                  </span>
                );
              })}
            </div>
          </div>
        </>
      );
    } else if (research) {
      const { author, year, reference } = research;
      return (
        <>
          <div className="d-flex flex-column">
            <div className="d-flex align-items-center">
              {author && (
                <h4 className="c-list__text c-list__text--muted">
                  作者｜{author}
                </h4>
              )}
              {year && (
                <h4 className="c-list__text c-list__text--muted">
                  出版年｜{year}
                </h4>
              )}
              {reference && (
                <h4 className="c-list__text c-list__text--muted">
                  {reference}
                </h4>
              )}
            </div>
          </div>
        </>
      );
    }
  };

  const renderContent = () => {
    return (
      <>
        <h3 className="c-list__title">
          {title}
          {hasLink && (
            <div className="c-list__subtext">
              <FontAwesomeIcon
                icon={solid('eye')}
                className="e-icon e-icon--muted"
              />
              <span className="ms-2">{views}</span>
            </div>
          )}
        </h3>
        <div className="c-list__content">
          {renderItem()}
          <div className="d-flex">
            {tags &&
              tags.map((v) => {
                const matchTitle = tagList.find((tag) => tag.id === v);
                return (
                  matchTitle && (
                    <span key={matchTitle.id} className="c-list__tag c-tag">
                      {matchTitle.title}
                    </span>
                  )
                );
              })}
          </div>
        </div>
        <hr className="e-hr" />
      </>
    );
  };

  return (
    <>
      {hasLink ? (
        <a
          href={link}
          className="e-link e-link--block"
          target="_blank"
          rel="noreferrer"
          onClick={handleViews}
        >
          {renderContent()}
        </a>
      ) : (
        renderContent()
      )}
    </>
  );
}

export default Item;
